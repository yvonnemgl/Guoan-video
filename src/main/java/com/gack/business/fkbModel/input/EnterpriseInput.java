package com.gack.business.fkbModel.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 组织架构查询接口的输入参数对应类型
 * @author ws
 * 2018-7-18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EnterpriseInput extends BasicInput{

	private EnterpriseInputData data;//业务数据
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public static class EnterpriseInputData{
		private String orgcode;//公司id
	}
	
}

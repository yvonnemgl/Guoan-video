package com.gack.business.fkbModel.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 部门查询接口的输入参数对应类型
 * @author ws
 * 2018-7-18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentInput extends BasicInput{

	private DepartmentInputData data;//业务数据
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public static class DepartmentInputData{
		private String orgcode;//公司id
		private String deptcode;//部门id
	}
	
}

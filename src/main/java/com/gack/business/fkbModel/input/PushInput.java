package com.gack.business.fkbModel.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 移动消息推送接口的输入参数对应类型
 * @author ws
 * 2018-7-24
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PushInput extends BasicInput{

	private PushInputData data;//业务数据

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public static class PushInputData{
		private String operatorNo;//通知对象(人员登录名，多个用逗号隔开)
		private String remarks;//通知内容
		private String H5url;//手机端入口链接
		private String PCurl;//PC端入口链接
		private String type;//1.审批提醒 2.消息提醒 3.其他
	}
}

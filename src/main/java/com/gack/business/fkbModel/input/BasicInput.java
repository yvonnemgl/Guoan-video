package com.gack.business.fkbModel.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 所有查询接口的输入参数所对应类型的公共属性
 * @author ws
 * 2018-7-18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class BasicInput {

	private String appid;
	private String time;
	private String token;//appid+appkey+time 组成字符串，在用MD5加密出32位长度的字符串，字符串为小写
	
}

package com.gack.business.fkbModel.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 人员信息查询接口的输入参数对应类型
 * @author ws
 * 2018-7-18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PeopleInput extends BasicInput{

	private PeopleInputData data;//业务数据
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public static class PeopleInputData{
		private String orgcode;//公司id
		private String deptcode;//部门id
		private String peoplecode;//人员id
	}
	
}

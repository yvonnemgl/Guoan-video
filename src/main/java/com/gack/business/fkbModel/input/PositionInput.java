package com.gack.business.fkbModel.input;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 职位职级查询接口的输入参数对应类型
 * @author ws
 * 2018-9-10
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PositionInput extends BasicInput{

	private PositionInputData data;//业务数据
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public static class PositionInputData{
		private String orgcode;//公司id
	}
	
}

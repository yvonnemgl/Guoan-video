package com.gack.business.fkbModel.output;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 部门查询接口的返回值中data对应的类型
 * @author ws
 * 2018-7-18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class DepartmentOutputData{

	private String orgcode;//公司id
	@Id
	private String deptcode;//部门id
	private String deptname;//部门名称
	private String fatherdept;//上级部门id(若无上级部门,则为0)
	private Date ts;//时间标志(若部门未删除,则表示修改名称时间;若部门已删除,则表示删除部门时间)
	private int dr;//删除标志(0为未删除,1为删除)
	
}

package com.gack.business.fkbModel.output;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 职位职级查询接口的返回值中data对应的类型
 * @author ws
 * 2018-7-18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class PositionOutputData {
	
	private String gradeid;//职级id
	private String gradename;//职级名称
	private Integer gradelevel;//职级等级(1-99)
	@Id
	private String postid;//职位id
	private String postname;//职位名称
	private String orgcode;//公司id
	private String sort;//职级大小规则(DESC---等级越小，权限越高、ASC---等级越大,权限越高) 
	private Date ts;//时间标志(若职位未删除,则表示修改时间;若职位已删除,则表示删除职位时间)
	private int dr;//删除标志(0为未删除,1为删除)
	
}

package com.gack.business.fkbModel.output;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 组织架构查询接口的返回值中data对应的类型
 * @author ws
 * 2018-7-18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class EnterpriseOutputData{

	@Id
	private String orgcode;//公司id
	private String codename;//公司名称
	private Date ts;//时间标志(若公司未删除,则表示修改名称时间;若公司已删除,则表示删除公司时间)
	private int dr;//删除标志(0为未删除,1为删除)
	
}

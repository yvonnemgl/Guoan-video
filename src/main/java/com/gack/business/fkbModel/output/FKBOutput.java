package com.gack.business.fkbModel.output;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 所有查询接口的返回值所对应类型的公共属性
 * @author ws
 * 2018-7-18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class FKBOutput {
	
	private int status;//查询结果(0-查询失败,1-查询成功)
	private String message;//消息提示
	private Object data;//业务数据
	
}

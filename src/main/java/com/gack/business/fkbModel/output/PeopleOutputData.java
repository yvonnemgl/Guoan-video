package com.gack.business.fkbModel.output;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 人员信息查询接口的返回值中data对应的类型
 * @author ws
 * 2018-7-18
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PeopleOutputData {

	private String peoplecode;//用户id
	private String name;//昵称
	private String mobile;//手机号
	private Date ts;//时间标志(表示用户修改昵称的时间)
	private JobData jobdata;//任职情况
	
	public PeopleOutputData(String peoplecode, String name, String mobile, Date ts1, String orgcode, String positioncode, String deptcode, String remarks, Date ts2){
		this.peoplecode = peoplecode;
		this.name = name;
		this.mobile = mobile;
		this.ts = ts1;
		JobData temp = new JobData();
		temp.setOrgcode(orgcode);
		temp.setPositioncode(positioncode);
		temp.setDeptcode(deptcode);
		temp.setRemarks(remarks);
		temp.setTs(ts2);
		this.jobdata = temp;
	}
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public static class JobData{
		private String orgcode;//公司id
		private String positioncode;//职位id
		private String deptcode;//部门id
		private String remarks;//企业备注
		private Date ts;//时间标志(表示职位、部门、企业备注的最近变更时间)
	}
	
}

package com.gack.business.vo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class ContactsVo {
	
	private String  nickname;//昵称
	
	private String  userUsername;//手机号
	@Id
	private String id;
	private String username;
	private String friendRemarks;
	private String useridFriendId;
	private String portrait;//头像
	private Date friendAddtime;//添加时间
	private Date friendPasstime;//通过时间
	/*private String  eventPicUrl;//图片链接地址
	private String  eventInfor;//活动详情 
	
*/
	
	public ContactsVo(String nickname, String userUsername) {
		super();
		this.nickname = nickname;
		this.userUsername = userUsername;
	}

	public ContactsVo() {
		super();
	}
	}

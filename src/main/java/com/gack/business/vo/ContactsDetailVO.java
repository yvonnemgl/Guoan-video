package com.gack.business.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * 联系人详情展示
 * @author ws
 * 2018-6-22
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ContactsDetailVO {

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public static class CompanyCard{
		private String id; //公司id
		private String name; //公司名称
		private String enterpriseRemarks;//企业备注
		private String departmentName = ""; //部门(若非一级部门,则需写上一级部门直到当前部门的所有名称)
		private String positionName; //职位
	}
	
	private String id; //用户id
	private String portrait; //头像地址
	private String nameForShow; //与头像并排放置的名称(好友备注>昵称)
	private String nickname; //昵称
	private String friendRemarks; //好友备注
	private String username; //手机号
	private boolean couldVideo; //是否可以发起视频会议
	private List<CompanyCard> companyCardList = new ArrayList<>(); //公司卡片
	
}

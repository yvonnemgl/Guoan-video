package com.gack.business.vo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.gack.business.model.Permission;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-5-31
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserDetailVO {

	@Id
	private String id; //用户id
	private String portrait; //头像地址
	private String nameForShow; //用于展示的名称     规则:好用备注>企业备注
	private String nickname; //昵称
	@Transient
	private String friendRemarks; //好友备注
	private String username; //手机号
	@Transient
	private boolean couldVideo;
	
	private String enterpriseName; //公司名称
	private String enterpriseRemarks; //企业备注
	private String roleId; //角色id  '1'为公司主管理员,'2'为管理员,'3'为公司员工
	private String departmentName = ""; //部门名称(一级部门_二级部门_三级部门)
	private String positionName; //职位名称
	@Transient
	private List<Permission> manageRange = new ArrayList<>(); //拥有的基本权限
	
	public UserDetailVO(String id, String portrait, String nameForShow, String nickname, String username, String enterpriseName, String enterpriseRemarks, String departmentName, String positionName){
		this.id = id;
		this.portrait = portrait;
		this.nameForShow = nameForShow;
		this.nickname = nickname;
		this.username = username;
		this.enterpriseName = enterpriseName;
		this.enterpriseRemarks = enterpriseRemarks;
		this.departmentName = departmentName;
		this.positionName = positionName;
	}
}

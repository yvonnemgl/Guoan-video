package com.gack.business.vo;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-5-30
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class UserItemVO{

	@Id
	private String id; //用户id
	private String username; //用户名,即手机号
	private String portrait; //头像地址
	private String nameForShow; //用于展示的名称     规则:好用备注>企业备注
	private String roleId; //角色id  '1'为公司主管理员,'2'为管理员,'3'为普通员工
	
	public UserItemVO(String id, String username, String portrait, String nameForShow){
		this.id = id;
		this.username = username;
		this.portrait = portrait;
		this.nameForShow = nameForShow;
	}
	
}

package com.gack.business.vo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Transient;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-6-1
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EnterpriseItemVO {

	private String id; //公司id
	private String name; //公司名称
	private Integer currentNumber; //公司当前人数
	private String status; //公司状态（0未认证，1已认证,2认证申请中，3认证失败，认证被拒绝，4公司已解散）
	@Transient
	private boolean hasIn; //是否在公司内
	
	@Transient
	private List<TopDepartmentAndUserItemVO> topDepartmentAndUserVOList = new ArrayList<>();
	
	public EnterpriseItemVO(String id, String name, Integer currentNumber, String status){
		this.id = id;
		this.name = name;
		this.currentNumber = currentNumber;
		this.status = status;
	}
	
}

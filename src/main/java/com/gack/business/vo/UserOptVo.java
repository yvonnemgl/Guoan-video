package com.gack.business.vo;

import java.math.BigDecimal;
import java.util.Date;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: UserOptVo 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月1日 下午2:25:00 
* @version V1.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserOptVo extends IdEntity{
	private String nickname; //昵称
	private String username; //用户名
	private BigDecimal expense_amount; //累计消费金额
	private Integer create_office_num; //创建共享办公次数
	private Integer join_video_num; //参加视频会议次数
	private boolean is_check; //是否认证
	private boolean status; //账号状态  
	private Date createtime; //注册时间
	
	public UserOptVo(String id,String nickname,String username,BigDecimal expense_amount,Integer create_office_num,Integer join_video_num,
			boolean is_check,boolean status,Date createtime){
		this.setId(id);
		this.setNickname(nickname);
		this.setUsername(username);
		this.setExpense_amount(expense_amount);
		this.setCreate_office_num(create_office_num);
		this.setJoin_video_num(join_video_num);
		this.set_check(is_check);
		this.setStatus(status);
		this.setCreatetime(createtime);
	}
}

package com.gack.business.vo;

import java.util.ArrayList;
import java.util.List;

import com.gack.business.model.PermissionGroup;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 管理员ItemVO
 * @author ws
 * 2018-6-2
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ManagerItemVO {

	private String userId; //用户id
	private String userNameForShow; //显示出来的用户名称
	private String portrait; //头像地址
	private String departmentName; //所属部门
	private String positionName; //职位名称
	private String username; //手机号
	private List<PermissionGroup> permissionGroupList = new ArrayList<>(); //用户权限组集合
	
}

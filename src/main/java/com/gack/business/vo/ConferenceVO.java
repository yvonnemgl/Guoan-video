package com.gack.business.vo;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ConferenceVO {
	private String id;
	private String name; //会议室名称
	private String topic; //会议室主题
	private String conferenceCode; //会议室编号 产品给规则生成
	private Integer people_num; //参加会议室人数
	private Integer people_all; //会议室上限人数 default 20
	private Date createTime; //会议室创建时间
}


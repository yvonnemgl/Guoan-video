package com.gack.business.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-6-1
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IndustryVO {

	private String id; //行业id
	private String name; //行业名称
	private List<IndustryVO> subIndustyList = new ArrayList<IndustryVO>(); //该行业的子行业
	private String parentId; //父行业id
	
	public IndustryVO(String id, String name, String parentId){
		this.id = id;
		this.name = name;
		this.parentId = parentId;
	}
	
}

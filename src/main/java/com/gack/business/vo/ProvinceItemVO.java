package com.gack.business.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-6-4
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProvinceItemVO {

	private String id; //省份id
	private String code; //code码
	private String name; //名称
	
	private List<CityItemVO> cityList = new ArrayList<>();
	
}

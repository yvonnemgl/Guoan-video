package com.gack.business.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-6-2
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TopDepartmentAndUserItemVO {

	private String departmentId; //部门id(只为第一级部门id)
	private String departmentName; //部门名称
	private List<UserItemVO> userItemVOList = new ArrayList<>(); //该部门下所有人员(包括子子部门的人)
	
	public TopDepartmentAndUserItemVO(String departmentId, String departmentName){
		this.departmentId = departmentId;
		this.departmentName = departmentName;
	}
	
}

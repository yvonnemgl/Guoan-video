package com.gack.business.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ConferenceUserVO {
	private String id;
	private String username; //用户名
	private String nickname; //昵称
	private String portrait; //头像地址
	private Integer isJoin; //是否在会议中
	

}

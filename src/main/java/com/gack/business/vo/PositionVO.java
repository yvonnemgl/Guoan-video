package com.gack.business.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-9-10
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PositionVO {
	
	private String id;//职位id
	private String name;//职位名称
	private String jobrankId;//职级id
	private String jobrankName; //职级名称
	
}

package com.gack.business.vo;

import java.util.ArrayList;
import java.util.List;

import com.gack.business.model.Area;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-6-4
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CityItemVO {

	private String id;//城市id
	private String code; //code码
	private String name; //名称
	private String provinceCode; //所属省份code码
	
	private List<Area> areaList = new ArrayList<>();//该城市所包含的城区
	
}

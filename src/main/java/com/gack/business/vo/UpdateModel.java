package com.gack.business.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UpdateModel {
	private String conferenceCode;
	private String name;
	private String topic;
	private String managerId;

}

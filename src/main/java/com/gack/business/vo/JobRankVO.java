package com.gack.business.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-9-10
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobRankVO {

	private String id;//职级id
	private String name;//职级名称
	private Integer level;//职级等级(1-99)
	private String enterpriseId;//公司id
	
}

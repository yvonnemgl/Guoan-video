package com.gack.business.vo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderVo {
	
	private String id;
	private String storeName;// 门店名称
	private Date createdTime;// 创建时间 
	private Date beginTime;// 订单开始使用时间
	private Date endTime;// 结束时间
	private Integer amount;// 订单金额
	private String status;// 1>>预约  2>>主动取消预约订单关闭  3>>预约超时订单关闭  4>>使用中  5>>主动发起结束订单 6>>订单异常后台手动订单关闭  7>>用户主动支付订单

	Date nowTime;// 当前时间

	public OrderVo(String id, String storeName, Date createdTime, Date beginTime, Date endTime, Integer amount,
			String status) {
		super();
		this.id = id;
		this.storeName = storeName;
		this.createdTime = createdTime;
		this.beginTime = beginTime;
		this.endTime = endTime;
		this.amount = amount;
		this.status = status;
	}
	
	public Date getNowTime(){
		return new Date();
	}
	
}
  
    
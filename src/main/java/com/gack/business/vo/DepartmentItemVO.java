package com.gack.business.vo;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-5-30
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentItemVO {

	private String id; //部门id
	private String name; //部门名称
	private Integer number; //部门人数
	private String  parentId; //父部门id  无父部门则为0
	private String level;//部门分级(从0开始)
	private String bigestLevel = "2";//最大部门分级
	private List<DepartmentItemVO> subDepartmentItemVOList = new ArrayList<>();//该部门的子部门
	
	public DepartmentItemVO(String id, String name, Integer number, String parentId, String level){
		this.id = id;
		this.name = name;
		this.number = number;
		this.parentId = parentId;
		this.level = level;
	}
	
	
}

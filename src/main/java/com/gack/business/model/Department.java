package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: VideoDepartment.java 
* @Description: 部门表
* @author Cancerl
* @date 2018年5月21日
*  
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="department")
@Entity
public class Department extends IdEntity{
	private String no;	//部门编号(一级部门编号:00   二级部门编号:父部门编号+00   三级部门编号:父部门编号+00)
	private String name;	//部门名称
	private String enterpriseId;	//部门对应公司id
	private String level;	//部门要分层,层级不写死,第一次0,树形结构添加.根据上一级目录添加下层部门
	private Integer number;	//当前部门人数  初始为0
	private String parentId; //父部门id  无父部门则为0
	private Integer surplus_amount;	//部门剩余可用额度(分）
	private Integer used_amount;	//部门已用额度(分)
	private Integer status;	//部门状态(0:已删除   1:未删除)
	private Date updateNameTime;// 更改部门名称时间
	private Date deleteTime;// 删除部门时间
	
}

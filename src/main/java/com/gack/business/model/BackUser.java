package com.gack.business.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "backiuser")
public class BackUser extends IdEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String username;
	private String password;
	private String department;
	private String role;
	private Date creattime;
	private String name;
	private int status; // 0---启用 1---禁用
	private String storename;
	private String phone;
	private String email;
    private String gender;//0--男  1--女
	
	@Transient
	private String rolename;
	private String pids;
	private String storeid;
	
}

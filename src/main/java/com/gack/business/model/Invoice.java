package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class) 
@Table(name = "invoice")
public class Invoice extends IdEntity{
	private String uid;// 关联用户ID
	private String oid;// 关联订单ID
	private String uuid;// 用于分组的ID
	private Date orderTime;// 订单时间
	private String name;// 门店信息/商务卡信息
	private String info;// 使用时间/商务卡URL
	private Integer amout;// 开票金额
	private String kind;// 发票类型 1门店发票 2商务卡发票
	private String type;// 类型 0 个人发票 1企业发票
	private String content;// 内容
	private String head;// 抬头
	private String number;// 税号
	private String username;// 姓名
	private String phone;// 联系方式
	private String address;// 地址
	private String detail;// 详细地址
	@CreatedDate
	private Date createTime;// 创建时间(默认创建订单时间)
	@LastModifiedDate
	private Date modifiedTime;// 开票时间
	private boolean used;// 是否使用
	
	public boolean getUsed(){
		return 	used;
	}
}
  
    
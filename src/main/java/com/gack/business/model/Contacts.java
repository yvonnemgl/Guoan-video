package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/*联系人列表*/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="contacts")
public class Contacts extends IdEntity{

	private String userUsername;
	private String useridFriendId;
	private Date friendAddtime;//添加时间
	private Date friendPasstime;//通过时间
	private String friendRemarks;//好友备注

	private Integer type;//0是发出消息的初始状态，1是确认，2忽略
	private String userUserid;//发起者id
	private String useridFriendUsername;//接受者的手机号


}


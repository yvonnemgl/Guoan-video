package com.gack.business.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-8-2
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UrlAndPermission {

	private String url;
	private String method;
	private String permission;
	
}

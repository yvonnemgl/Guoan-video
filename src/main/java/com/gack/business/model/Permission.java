package com.gack.business.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 基础权限表
 * @author ws
 * 2018-8-14
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="permission")
@Entity
public class Permission extends IdEntity{
	private String name;	//权限名称
	private String description;	//英文描述
	private Integer type; //基本权限分类
	@Transient
	private boolean isOpen; //权限是否开启
	
	public Permission(String id, String name, String description, Integer type, boolean isOpen){
		this.setId(id);
		this.name = name;
		this.description = description;
		this.type = type;
		this.isOpen = isOpen;
	}
	
}

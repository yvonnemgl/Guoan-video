package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "depositrecord")
@EntityListeners(AuditingEntityListener.class) 
public class DepositRecord extends IdEntity{
	
	private String oid;// 订单编号
	private String uid;// 用户ID
	private String type;// 类型(0付款，1退款)
	private String channel;// 支付渠道
	private Integer amount;// 支付金额
	@CreatedDate
	private Date createTime; 
	
	public DepositRecord(DepositRecord depositRecord){
		this.oid = depositRecord.getOid();
		this.amount = depositRecord.getAmount();
		this.uid = depositRecord.getUid();
		this.channel = depositRecord.getChannel();
		this.type = "1";
	}
	
}

package com.gack.business.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * 
 * 
* @ClassName: VideoUser.java 
* @Description: 用户实体
* @author Cancerl
* @date 2018年3月20日
*  
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="user")
public class VideoUser extends IdEntity{
	String username;
	String password;
	String portait;		//头像url
	String nickname;	//昵称
	Date createtime;	//创建时间
//	String reservation; //用户是否已预约
	
//	@JsonIgnore
//	@OneToMany(mappedBy="user",fetch=FetchType.LAZY,cascade={CascadeType.ALL})
//	private Set<VideoOpinion> opinions;
	
	@JsonIgnore
	@OneToMany(mappedBy="user",fetch=FetchType.LAZY,cascade={CascadeType.ALL})
	private Set<VideoMessage> messages;
	
	@JsonIgnore
	@OneToMany(mappedBy="user",fetch=FetchType.LAZY,cascade={CascadeType.ALL})
	private Set<VideoOrderform> orderforms;
	
	@JsonIgnore
	@OneToMany(mappedBy="user",fetch=FetchType.LAZY,cascade={CascadeType.ALL})
	private Set<VideoUserLogin> userLogins;
}



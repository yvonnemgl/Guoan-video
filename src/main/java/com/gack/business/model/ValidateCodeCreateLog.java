package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: ValidateCodeCreateLog 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月12日 下午3:39:37 
* @version V1.0
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="validate_code_create_log")
public class ValidateCodeCreateLog extends IdEntity{
	private String code;		//验证码
	private Date createTime;	//创建时间
	private String picName;		//oss文件存储名称
	private String picUrl;		//oss文件地址
}

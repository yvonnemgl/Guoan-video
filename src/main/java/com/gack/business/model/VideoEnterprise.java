package com.gack.business.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: VideoEnterprise.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年5月21日
*  
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="enterprise")
@Entity
public class VideoEnterprise extends IdEntity{
	String name;
	
}

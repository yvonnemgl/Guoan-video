package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-6-1
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "message")
public class Message extends IdEntity{

	private String userid; //接受者id
	private String message; //消息内容
	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime; //创建时间
	private String type; //二期废弃字段
	private String title; //标题
	private Integer mesType; //消息类型   0.回复消息   1.邀请加入公司    2.邀请加入好友   3.押金支付提醒     4.订单取消    5.系统消息   6.会议消息 7.费控宝推送消息
	private Integer state; //消息内容对应的状态 0:拒绝  1:同意  2:未处理    3:不可再次处理
	private String createId; //创建者ID
	private Integer sendWay; //发送方式 1 短信 2 站内信 3 push 4 邮件
	private Integer isRead; //已读 0否 1是
	private String detailId; //邀请消息所关联的邀请表id
	
}

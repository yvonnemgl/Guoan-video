package com.gack.business.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gack.helper.common.abstractobj.IdEntity;
import com.gack.helper.common.util.Arith;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "orders")
@EntityListeners(AuditingEntityListener.class)
public class Order extends IdEntity {

	private String orderId;// 订单ID
	private String userId;// 用户ID
	private String pingId;// 支付ID
	private String storeId;// 门店ID
	private String enterpriseId;// 企业ID
	private String departmentId;// 部门ID
	private String status;// 1>>预约 2>>主动取消预约订单关闭 3>>预约超时订单关闭 4>>使用中 5>>主动发起结束订单
							// 6>>订单异常后台手动订单关闭 7>>用户主动支付订单
	@CreatedDate
	private Date createdTime;// 创建时间
	private Date beginTime;// 订单开始使用时间
	private Date endTime;// 结束时间
	@LastModifiedDate
	private Date payTime;// 支付时间
	private Integer amount;// 订单金额
	private Integer payAmount;// 实际支付金额
	private Integer cardAmount;// 优惠卷支付金额
	private Integer businessCardAmount;// 商务卡支付金额
	private String type;// 支付类型:
						// 微信扫码(wx_pub_qr)，微信app(wx),支付宝PC(alipay_pc_direct)，支付宝app(alipay),kq(快钱)，kq_app(快钱APP)，free(商务卡全额支付)
	private String cancelReason;// 取消原因

	@Transient
	private VideoStores store; // 门店
	@Transient
	private double amountYuan;// 订单金额
	@Transient
	private double payAmountYuan;// 实际支付金额
	@Transient
	private double cardAmountYuan;// 优惠卷支付金额
	@Transient
	private double businessCardAmountYuan;// 商务卡支付金额

	@Transient
	Date nowTime; // 当前系统时间

	public Date getNowTime() {
		return new Date();
	}

	public double getAmountYuan() {
		return (amount == null ? 0 : Arith.div(100.0 , (amount == null ? 0.00 : amount)));
	}

	public double getPayAmountYuan() {
		return (payAmount == null ? 0 : Arith.div(100.0 ,(payAmount == null ? 0.00 : payAmount)));
	}

	public double getCardAmountYuan() {
		return (cardAmount == null ? 0 : Arith.div(100.0 ,(cardAmount == null ? 0.00 : cardAmount)));
	}

	public double getBusinessCardAmountYuan() {
		return (businessCardAmount == null ? 0 : Arith.div(100.0 ,(businessCardAmount == null ? 0.00 : businessCardAmount)));
	}
}

package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "join_conference_record")
public class JoinConferenceRecord extends IdEntity {
	private String uid;
	private Date joinTime;
	private String cid;
	private Integer channel; //入会渠道 1 android 2 ios 3 pc
	

}

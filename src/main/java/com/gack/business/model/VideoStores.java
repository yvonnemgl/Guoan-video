package com.gack.business.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="stores")
public class VideoStores extends IdEntity{
	
	private String  storeName;  //门店名称
	
	private String  storeAddress; //门店地址
	
	private Date  starttime;//营业起始时间
	
	private Date  endtime;//营业结束时间
	
	private String  supportfacity;//配套设施
	
	private String storeUser;//店长
	
	private String storePhone;//电话
	
	private String storeLandline;	//门店座机号码
	
	private String attention;//注意事项
	
	private String longitude;//经度
	
	private  String latitude;//纬度
	
	private  String storep_hotourl;//门店图片
	
	
	private String  onephotourl;//第一张图门店
	
	private String  twophoneurl;//第二张图门店
	
	private String  threephotourl;//第三张图门店
	
	private String  fourphotourl;//第四张图门店
	
	private String  fivephotourl;//第五张图门店
	
	private String  sixphotourl;//第六张图门店
	
	private Integer storeArea;//面积
	
	private String storeService;//店铺服务

	private Integer storeDeposit;	//门店押金
	
	private Integer storePrice;//价格
	
	private Integer status; //店铺状态 ： 0>>未预定  1>>被预约中   2>>被使用中
	
	private Date createtime;	//门店创建时间
	
	private Integer isUse;// 门店可用状态)是否可被使用  0可被使用  1不可使用（删除）  2不可使用（冻结）
	
	private String province;	//门店所属省份
	
	private String city;		//门店所属城市
	
	private String area;		//门店所属区域	
	
	
	@Transient
	private StoreCameras cameras;		//摄像头列表
	
	@Transient
	private List<StoreSupportFacity> facities;	// 配套设施列表
	
	@Transient
	private List<StoreSupportService> services;	// 配套服务列表
	
}

package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "conference_remind")
public class ConferenceRemind extends IdEntity{
	private String conferenceId; //关联的会议室id
	private Integer isSwitch; //提醒开关 0关闭 1打开
	@Temporal(TemporalType.TIMESTAMP)
	private Date beginTime; //会议开始时间
	private Long remindTime; //提醒时间
	private Integer isPush; //是否已经推送
	@Temporal(TemporalType.TIMESTAMP)
	private Date pushTime; //推送时间
	
	


}

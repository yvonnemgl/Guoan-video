package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: VideoEnterprisePosition.java 
* @Description: 公司职位实体
* @author Cancerl
* @date 2018年5月25日
*  
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="position")
@Entity
public class Position extends IdEntity{
	private String name;	//职位名称
	private String enterpriseId;	//公司id
	private String jobrankId; //职级id
	private Integer status; //职位状态(0:已删除   1:未删除)
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime; //职位更改时间
	@Temporal(TemporalType.TIMESTAMP)
	private Date deleteTime; //删除职位时间
}

package com.gack.business.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 省份实体类
 * @author ws
 * 2018-5-29
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "province")
public class Province extends IdEntity{

	private String code; //code码
	private String name; //名称
	
}

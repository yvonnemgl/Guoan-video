
package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "user")
public class User extends IdEntity{
	
	private String username; //用户名
	private String password; //密码
	private String nickname; //昵称
	private String portrait; //头像地址
	@Temporal(TemporalType.TIMESTAMP)
	private Date createtime; //注册时间
	@Temporal(TemporalType.TIMESTAMP)
	private Date last_login_time; //上次登录时间
	private String register_source; //注册来源  android或ios
	private boolean is_check; //是否认证
	private Integer isPledge; //是否交纳押金 0未缴纳 1已缴纳 2退款中
	private String realName;//真实姓名
	@Temporal(TemporalType.TIMESTAMP)
	private Date check_time; //认证时间
	private boolean status; //账号状态  
	private Integer create_office_num; //创建共享办公次数
	private Integer create_meeting_num; //创建视频会议次数
	private Integer join_video_num; //参加视频会议次数
	private Long total_join_meeting_time; //参加视频会议总时长(秒为单位)
	private Long total_office_time; //参加共享办公总时长(秒为单位)
	private Integer useramount; //账号余额(分)
	private Integer expense_amount; //累计消费金额(分)
	private Integer expense_num; //累计消费次数
	private Integer orderState;// 用户订单状态 0未使用 1包含预约订单 2包含使用中订单 3包含待支付订单
	private Integer loginNum;	//登录次数
	@Temporal(TemporalType.TIMESTAMP)
	private Date reset_password_time;	//重置密码时间
	@Temporal(TemporalType.TIMESTAMP)
	private Date update_name_time;	//重置密码时间
	private String password_token;	// 密码token（每次更新密码刷新token  比较token判断密码是否刷新）
}

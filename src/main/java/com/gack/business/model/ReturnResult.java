package com.gack.business.model;

import lombok.Getter;
import lombok.Setter;
/**
 * 
* @ClassName: ReturnResult 
* @Description: 返回值实体,不同步数据库
* @author zj
*   
 */
@Getter
@Setter
public class ReturnResult {
	
	private String key;
	private Object value;

}

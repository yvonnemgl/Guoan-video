package com.gack.business.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: VideoOrderform.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月21日
*  
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="orderform")
public class VideoOrderform extends IdEntity{
	
	Date placeOrdertime;	//订单预定时间
	String orderStatus;		//订单状态		
	// 1>>预约  2>>主动取消预约订单关闭  3>>预约超时订单关闭  4>>使用中  5>>主动发起结束订单/提交预结算订单  6>>订单待付款  7>>用户订单已支付
	String orderNumber;		//订单编号
	Date orderEndtime;		//订单结束时间
	Date orderStarttime;    //订单开始时间
	
	@Transient
	Date nowTime;	//当前系统时间
	
	public Date getNowTime(){
		return new Date();
	}
	
	@JsonIgnore
	@ManyToOne(cascade=CascadeType.REFRESH,optional=false)
	@JoinColumn(name = "userid")
	private User user;
	
	@ManyToOne(cascade=CascadeType.REFRESH,optional=false)
	@JoinColumn(name = "storeid")
	private VideoStores store;
	
	@JsonIgnore
	@OneToOne(mappedBy="orderform")
	private VideoCancelOrderformReason cancelReason;
	
}

package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-8-22
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "manager_update_log")
@Entity
public class ManagerUpdateLog extends IdEntity{

	private String log; //日志内容
	private String enterpriseId; //公司id
	@Temporal(TemporalType.TIMESTAMP)
	private Date date; //记录发生日期
	
}

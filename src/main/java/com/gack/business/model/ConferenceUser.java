package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "conference_user")
@Entity
public class ConferenceUser extends IdEntity{
	private String cid;
	private String uid;
	private Date lastModifyTime;//用户最后修改会议室相关操作的时间

}

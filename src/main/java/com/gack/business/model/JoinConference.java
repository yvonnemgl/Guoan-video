package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Table(name = "join_conference")
public class JoinConference extends IdEntity{
	private String cid; //会议室ID
	private String uid; //用户ID
	private String participantid; //vidyo的ID
	private Date joinTime; //加入的时间
	private Date leaveTime; //离开的时间
	private Integer totalTime;  //总共的时间
	private Integer joinCount; //加入的次数
	private Integer isJoin; //是否在vidyo会议中 0 false 1true
	private String conferenceid; //vidyo的roomId
	

}

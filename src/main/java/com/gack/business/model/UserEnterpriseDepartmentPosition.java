package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-5-30
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_enterprise_department_position")
public class UserEnterpriseDepartmentPosition extends IdEntity{
	
	private String userId; //用户id
	private String enterpriseId; //公司id
	private String departmentId; //部门id
	private String positionId; //职位id
	private String enterpriseRemarks; //企业备注
	private Date updateTime;//修改部门、职位、企业备注的时间

}

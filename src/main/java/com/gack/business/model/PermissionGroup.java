package com.gack.business.model;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.TypeReference;
import com.gack.business.permisson.exception.PermissionArrayIsNullException;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 用以和前端交互数据
 * @author ws
 * 2018-8-14
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PermissionGroup {

	private String title; //权限组标题
	private List<Permission> permissionList; //权限组包含的基础权限 
	private boolean isOpen; //权限组是否开启
	
	/**
	 * 解析PermissionGroup成JSON串
	 * @param permissionGroup 权限组
	 * @return
	 */
	public static String parseToString(List<PermissionGroup> permissionGroupList){
		return JSONArray.toJSONString(permissionGroupList);
	}
	
	/**
	 * 解析字符串成PermissionGroup集合
	 * @param jsonStr json串
	 * @return
	 */
	public static List<PermissionGroup> parseToPermissionGroupList(String jsonStr){
		return JSONArray.parseObject(jsonStr, new TypeReference<ArrayList<PermissionGroup>>(){});
	}
	
	/**
	 * 校验权限组的open和权限的open是否匹配,若不匹配则抛出异常
	 * @return
	 */
	public boolean verifyOpen() throws PermissionArrayIsNullException{
		if(permissionList == null || permissionList.size() == 0){
			throw new PermissionArrayIsNullException("权限集合为空");
		}
		
		boolean flag = false;
		for(Permission element : permissionList){
			if(!isOpen){
				if(element.isOpen()){
					return false;
				}else{
					flag = true;
				}
			}else{
				if(element.isOpen()){
					flag = true;
				}
			}
		}

		return flag;
	}

	/**
	 * 权限组集合中,是否有一个权限组是开启状态
	 * @param permissionGroupList
	 * @return
	 */
	public static boolean hasOpenPermissionGroup(List<PermissionGroup> permissionGroupList){
		for(PermissionGroup element : permissionGroupList){
			if(element.isOpen){
				return true;
			}
		}
		return false;
	}
	
	public static PermissionGroup superManagerPermissionGroup(){
		PermissionGroup pg = new PermissionGroup();
		pg.setTitle("超级管理");
		pg.setPermissionList(new ArrayList<Permission>(){
			{
				add(new Permission("0", "超级权限", "SUPER_PERMISSION", 0, true));
			}
		});
		pg.setOpen(true);
		
		return pg;
	}
	
//	public static void main(String[] args){
//		List<PermissionGroup> list = new ArrayList<PermissionGroup>(){
//			{
//				add(PermissionGroup.superManagerPermissionGroup());
//			}
//		};
//		System.out.println(PermissionGroup.parseToString(list));
//		
//		PermissionGroup pg1 = new PermissionGroup();
//		pg1.setTitle("人员管理");
//		pg1.setPermissionList(new ArrayList<Permission>(){
//			{
//				add(new Permission("1", "分配职位", "ASSIGN_POSITION", 1, false));
//				add(new Permission("2", "分配部门", "ASSIGN_DEPARTMENT", 1, false));
//				add(new Permission("3", "设置企业备注", "SET_ENTERPRISE_REMARKS", 1, false));
//				add(new Permission("4", "删除员工", "DELETE_EMPLOYEE", 1, false));
//			}
//		});
//		pg1.setOpen(false);
//		
//		PermissionGroup pg2 = new PermissionGroup();
//		pg2.setTitle("公司配置管理");
//		pg2.setPermissionList(new ArrayList<Permission>(){
//			{
//				add(new Permission("5", "部门管理", "MANAGE_DEPARTMENT", 2, false));
//				add(new Permission("6", "职级职位管理", "MANAGE_RANK_POSITION", 2, false));
//				add(new Permission("8", "编辑公司信息", "UPDATE_ENTERPRISE_BASIC_INFO", 2, false));
//			}
//		});
//		pg2.setOpen(false);
//		
//		List<PermissionGroup> pList = new ArrayList<PermissionGroup>(){
//			{
//				add(pg1);
//				add(pg2);
//			}
//		};
//	
//		String jsonStr = PermissionGroup.parseToString(pList);
//		System.out.println(jsonStr);
//		
//		List<PermissionGroup> pList1 = PermissionGroup.parseToPermissionGroupList(jsonStr);
//		for(PermissionGroup element : pList1){
//			System.out.println(element.getTitle());
//			System.out.println(element.isOpen);
//		}
//		
//		//[{'open':false,'permissionList':[{'description':'ASSIGN_POSITION','id':'1','name':'分配职位'},{'description':'ASSIGN_DEPARTMENT','id':'2','name':'分配部门'},{'description':'SET_ENTERPRISE_REMARKS','id':'3','name':'设置企业备注'},{'description':'DELETE_EMPLOYEE','id':'4','name':'删除员工'}],'title':'人员管理'},{'open':false,'permissionList':[{'description':'MANAGE_DEPARTMENT','id':'5','name':'部门管理'},{'description':'MANAGE_POSITION','id':'6','name':'职位管理'},{'description':'UPDATE_ENTERPRISE_BASIC_INFO','id':'8','name':'编辑公司信息'}],'title':'公司配置管理'}]
//		
//		String s = "[{'open':false,'permissionList':[{'description':'ASSIGN_POSITION','id':'1','name':'分配职位'},{'description':'ASSIGN_DEPARTMENT','id':'2','name':'分配部门'},{'description':'SET_ENTERPRISE_REMARKS','id':'3','name':'设置企业备注'},{'description':'DELETE_EMPLOYEE','id':'4','name':'删除员工'}],'title':'人员管理'},{'open':false,'permissionList':[{'description':'MANAGE_DEPARTMENT','id':'5','name':'部门管理'},{'description':'MANAGE_POSITION','id':'6','name':'职位管理'},{'description':'UPDATE_ENTERPRISE_BASIC_INFO','id':'8','name':'编辑公司信息'}],'title':'公司配置管理'}]";
//		System.out.println(s.length());
//		
//	}
//	
}

package com.gack.business.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang3.StringUtils;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-8-14
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_enterprise_permissiongroup")
@Entity
public class UserEnterprisePermissionGroup extends IdEntity{

	private String userId; //用户id
	private String enterpriseId; //公司id
	private String permissionGroupsDescription; //权限组JSON格式的表示(当user_id、enterprise_id为-1时，此为默认权限组)
	@Temporal(TemporalType.TIMESTAMP)
	private Date managerTime; //什么时间被任命为管理员
	
	/**
	 * 获取开启的基本权限
	 * @return
	 */
	public List<Permission> getOpenPermission(){
		List<Permission> pList = new ArrayList<>();
		
		if(StringUtils.isBlank(permissionGroupsDescription)){
			return pList;
		}
		
		List<PermissionGroup> pgList = PermissionGroup.parseToPermissionGroupList(permissionGroupsDescription);
		for(PermissionGroup element : pgList){
			if(!element.isOpen()){
				continue;
			}
			
			for(Permission pe : element.getPermissionList()){
				if(pe.isOpen()){
					pList.add(pe);
				}
			}
			
		}

		return pList;
	}
	
	/**
	 * 获取开启的基本权限的英文描述
	 * @return
	 */
	public List<String> getOpenDescription(){
		List<String> dList = new ArrayList<>();
		
		for(Permission element : getOpenPermission()){
			dList.add(element.getDescription());
		}
		
		return dList;
	}
	
	/**
	 * 获取开启的基本权限的中文名称(title(name1、name2、name3),title(name1、name2、name3))
	 * @return
	 */
	public List<String> getOpenNameAndTile(){
		List<String> list = new ArrayList<>();
		
		if(StringUtils.isBlank(permissionGroupsDescription)){
			return list;
		}
		
		List<PermissionGroup> pgList = PermissionGroup.parseToPermissionGroupList(permissionGroupsDescription);
		for(PermissionGroup element : pgList){
			StringBuilder text = new StringBuilder();
			if(element.isOpen()){
				text.append(element.getTitle() + "(");
				for(Permission pElement : element.getPermissionList()){
					if(pElement.isOpen()){
						text.append(pElement.getName() + "、");
					}
				}
				text = new StringBuilder(text.substring(0, text.length() - "、".length()));
				text.append(")");
				list.add(text.toString());
			}
		}
		
		return list;
	}
	
	public static void main(String[] args){
		List<String> list = new ArrayList<>();
		List<PermissionGroup> pgList = PermissionGroup.parseToPermissionGroupList("[{\"open\":true,\"permissionList\":[{\"description\":\"ASSIGN_POSITION\",\"id\":\"1\",\"name\":\"分配职位\",\"type\":1},{\"description\":\"ASSIGN_DEPARTMENT\",\"id\":\"2\",\"name\":\"分配部门\",\"type\":1},{\"description\":\"SET_ENTERPRISE_REMARKS\",\"id\":\"3\",\"name\":\"设置企业备注\",\"type\":1},{\"description\":\"DELETE_EMPLOYEE\",\"id\":\"4\",\"name\":\"删除员工\",\"type\":1}],\"title\":\"人员管理\"},{\"open\":true,\"permissionList\":[{\"description\":\"MANAGE_DEPARTMENT\",\"id\":\"5\",\"name\":\"部门管理\",\"type\":2},{\"description\":\"MANAGE_POSITION\",\"id\":\"6\",\"name\":\"职位管理\",\"type\":2},{\"description\":\"UPDATE_ENTERPRISE_BASIC_INFO\",\"id\":\"8\",\"name\":\"编辑公司信息\",\"type\":2}],\"title\":\"公司配置管理\"}]");
		for(PermissionGroup element : pgList){
			StringBuilder text = new StringBuilder();
			if(element.isOpen()){
				text.append(element.getTitle() + "(");
				for(Permission pElement : element.getPermissionList()){
					text.append(pElement.getName() + "、");
				}
				text = new StringBuilder(text.substring(0, text.length() - "、".length()));
				text.append(")");
				list.add(text.toString());
			}
		}
		
		
		for(String element : list){
			System.out.println(element);
		}
	}
	
}

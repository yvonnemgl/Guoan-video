package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "history_conference_user")
@Entity
public class HistoryConferenceUser{
	@Id
	private String id;
	private String cid; //会议室id
	private String uid; //用户id
	private Date deleteTime; //离开会议时间

}

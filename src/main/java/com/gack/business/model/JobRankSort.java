package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 职级排序
 * @author ws
 * 2018-9-10
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "jobranksort")
public class JobRankSort extends IdEntity{

	private String sort;//排序(DESC---等级越小，权限越高、ASC---等级越大,权限越高)
	private String enterpriseId;//公司id
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;//创建时间
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;//更新时间
	
}

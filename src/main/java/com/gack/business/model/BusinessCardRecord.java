package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "businesscardrecord")
//@MappedSuperclass 
@EntityListeners(AuditingEntityListener.class) 
public class BusinessCardRecord extends IdEntity{

	private String cid;// 商务卡(商品)ID
	private String uid;// 用户ID
	private String eid;// 公司ID
	private String oid;// 订单ID
	private String plantid;// 平台编号
	private Integer number;// 商务卡购买数量
	private Integer amount;// 商务卡总金额
	private Integer payAmount;// 实际支付金额
	private boolean pay;// 支付状态
//	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	private Date createTime;// 创建时间(默认创建订单时间)
	
	public boolean getPay(){
		return pay;
	}
}

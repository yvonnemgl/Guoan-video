package com.gack.business.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * i商务下载地址
 * @author ws
 * 2018-6-27
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "downloadurl")
public class DownloadUrl extends IdEntity{

	private String url;
	
}

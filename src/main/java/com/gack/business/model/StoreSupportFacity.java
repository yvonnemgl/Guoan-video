package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: StoreSupportFacity 
* @Description: TODO(门店配套设施) 
* @author (ZhangXinYu)  
* @date 2018年6月4日 上午11:23:12 
* @version V1.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="store_support_facity")
public class StoreSupportFacity extends IdEntity{
	private String name;		//配套设施名称
//	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
//	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	private Date createtime;	//创建时间
	private Integer isUse;	//使用状态  0 未被使用  1 被使用中
	private String icon;	// icon图标的url
	private int use_template;	// 是否使用的数据库模板 0 未使用 1使用
}

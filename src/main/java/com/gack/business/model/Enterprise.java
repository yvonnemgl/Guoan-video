package com.gack.business.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: VideoEnterprise.java 
* @Description: 公司表
* @author Cancerl
* @date 2018年5月21日
*  
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="enterprise")
@Entity
public class Enterprise extends IdEntity{
	private String name;				//公司名称
	private String certifiedName;		//企业认证名称(与营业执照名称对应)
	private String contractName;		//公司联系人名称
	private String idNumber;			//身份证号码(公司认证时填写的身份证号码)
	private String unifiedSocialCreditCode;	//统一社会信用码
	private String positiveIdCard;		//身份证正面照片url
	private String negativeIdCard;		//身份证背面照片url
	private String businessLicense;		//营业执照照片url
	private String administratorJobCertificate;	//管理员在职证明url
	private String detailedAddress;		//公司详细地址
	private String numberRange;			//公司人数范围
	private Integer currentNumber;		//公司当前人数
	
	private String creator;				//公司创建人对应id;
	
	private String industry;			//所属行业
	private String location;			//所在地区
	private String status;				//公司状态：		（0未认证，1已认证,2认证申请中，3认证失败，认证被拒绝，4公司已解散）
	private String rejectingApprovalReason;	//拒绝公司审批理由	（如果公司审批被拒绝，添加公司审批被拒绝理由）
	private Date createTime;			//创建时间
	private Date certificationSubmissionTime;		//提交认证时间
	private Date certificationSuccessTime;		//成功认证时间
	private String architectureDiagram;	//公司架构图（架构图为url ）
	private Integer surplusAmount;// 剩余商务卡金额
	private Integer usedAmount;// 已使用商务卡金额
	private Date updateNameTime;// 公司更改名称时间
	private Date deleteTime;// 删除公司的时间
	@Transient
	private List<Department> departments;
	@Transient
	private List<User> departmentUsers;
}

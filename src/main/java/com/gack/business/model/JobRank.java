package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 职级类
 * @author ws
 * 2018-9-10
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "jobrank")
public class JobRank extends IdEntity{

	private String name;//职级名称
	private Integer level;//职级等级(1-99)
	private String enterpriseId;//公司id
	private Integer status; //职位状态(0:已删除   1:未删除)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime;//创建时间
	@Temporal(TemporalType.TIMESTAMP)
	private Date updateTime;//修改时间
	@Temporal(TemporalType.TIMESTAMP)
	private Date deleteTime;//删除时间
	
}

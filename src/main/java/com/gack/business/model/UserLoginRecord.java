package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-5-30
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "useloginrecord")
public class UserLoginRecord extends IdEntity{

	private String username; //用户名，即手机号
	@Temporal(TemporalType.TIMESTAMP)
	private Date login_time; //登录时间
	private String login_resource; //登录来源  PC或android或ios
	private String equipment; //设备号
	
}

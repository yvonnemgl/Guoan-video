package com.gack.business.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: VideoPermission.java 
* @Description: 权限
* @author Cancerl
* @date 2018年5月21日
*  
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name="permission")
@Entity
public class VideoPermission extends IdEntity{
	String name;
	String url;
	
}

package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="version_number")
public class VersionNumber extends IdEntity{

	private String versionNumber;
	
	private String versionDepict;
	
	private String title;
	
	private String force;//(1 强制更新  0 不强制)
	
	private Date createtime;
	
}

package com.gack.business.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: VideoCancelOrderformReason.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月21日
*  
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="cancel_orderform_reason")
public class VideoCancelOrderformReason extends IdEntity{
	String reason;	//取消订单原因
	
	@JsonIgnore
	@OneToOne(cascade=CascadeType.REFRESH,optional=false)
    @JoinColumn(name="orderform_id")
	VideoOrderform orderform;
	
}

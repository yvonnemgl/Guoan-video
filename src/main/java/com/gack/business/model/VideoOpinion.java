package com.gack.business.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * 
 * 
* @ClassName: VideoOpinion.java 
* @Description: 意见反馈实体
* @author Cancerl
* @date 2018年3月21日
*  
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="opinion")
public class VideoOpinion extends IdEntity{
	String opiniontext;
	
//	@ManyToOne(cascade=CascadeType.REFRESH,optional=false)
//	@JoinColumn(name = "userid")
//	private User user;
	
	String userid;
	
	Date createTime;		//提交日期
	Integer status;			//信息状态 0 未处理  1 处理中  2 已处理
	
	String source;		//意见反馈来源  app  /  pc
}

package com.gack.business.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 行业实体类
 * @author ws
 * 2018-5-29
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "industry")
public class Industry extends IdEntity{

	private String name; //行业名称
	private String parentId; //父行业id
	
}

package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "message_push")
public class MessagePush extends IdEntity{
	private String title; // 标题
	private String content; // 内容
	private Date sendTime; //发送时候
	private Integer type; //类型 1 短信 2 站内信 3 push
	private String mesKey; // 推送附加属性value key 为 11 value 为会议室cid
	private String mesValue;// 推送附加属性value key 为 11 value 为会议室cid 

	

}

package com.gack.business.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Table(name = "history_conference")
public class HistoryConference {
	@Id
	private String id;
	private String name; //会议室名称
	private String topic; //会议室主题
	private String conferenceCode; //会议室编号 产品给规则生成
	private String createId; //创建会议室的人
	private String manageId; //管理员ID
	private Integer people_num; //参加会议室人数
	private Integer people_all; //会议室上限人数 default 20
	private Date createTime; //会议室创建时间
	private Integer conferenceId; //vidyo会议室ID
	private String conferenceUrl; //vidyo的URL
	private String conferencePin; //vidyo的PIN码
	private Date deleteTime; //会议室解散时间
	

}

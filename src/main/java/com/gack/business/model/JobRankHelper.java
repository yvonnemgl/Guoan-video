package com.gack.business.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 *
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "jobrankhelper")
@Entity
public class JobRankHelper extends IdEntity{

	private String title;//文档标题
	private String message;//文档内容
	private Integer position;//文档位置
	
}

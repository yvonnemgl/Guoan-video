package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="meeting")
public class VideoMeeting extends IdEntity{
private String id;
private String name;
private Integer peopleAll;//所有人员数量
private Integer peopleNum;//进入数量
private String verifyPassword;//校验密码
private String roomid;//视频id；
private String roomUrl;//房间地址
private String showName;//列表显示名称

}

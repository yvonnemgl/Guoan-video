package com.gack.business.model;


import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="sensitiveword")
public class VideoSensitiveWord extends IdEntity{
	private String sensitivewordName;
	
	
	public String getSensitivewordName() {
		String specialChar="!@#$%^&*()_+=-~`,./<>?|{}[]，。？；‘：“”【】）（*&……%￥#@！~·";
		String str="";
		for (int i = 0; i < sensitivewordName.length(); i++) {
			char word = sensitivewordName.charAt(i);
			if(specialChar.indexOf(word+"")>0){
				continue;
			}
			str+=word;
		}
		return str;
	}

	public void setSensitivewordName(String sensitivewordName) {
		this.sensitivewordName = sensitivewordName;
	}
	
}

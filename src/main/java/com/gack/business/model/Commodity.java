package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "commodity")
public class Commodity extends IdEntity{
	
	private String number; //商品编号
	private Integer salesAmount; //销售数量
	private Integer faceValue; //面值 (分)
	private Integer salePrice; //销售价(分)
	private Integer status; //状态 0 下架 1 上架 2 删除
	private String category; //分类
	private Date shelTime; //上架时间
	private String photoUrl; //图片地址
	private String name;// 商品名称

}

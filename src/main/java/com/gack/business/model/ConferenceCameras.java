package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "conference_cameras")
public class ConferenceCameras extends IdEntity{
	private String cid; //会议室id
	private String scid; //门店摄像头Id
	private String participantID; //vidyo分配Id
	private Date joinTime;//加入时间
	private Date leaveTime; //离开时间
	private Integer isJoin; //是否已经加入 0 未加入 1 已经加入

}

package com.gack.business.model;

import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.Date;

import javax.persistence.Entity;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.NoArgsConstructor;

import lombok.AllArgsConstructor;

import lombok.Setter;

import lombok.Getter;

/**
 * 
* @ClassName: StoreSupportServiceIcon 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年9月7日 上午8:56:45 
* @version V1.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "store_support_service_icon")
public class StoreSupportServiceIcon extends IdEntity{
	private String name;	// 配套服务名称
	private String icon;	// icon的URL
	private int is_delete;		// 是否已删除 0未删除 1已删除
	@Temporal(TemporalType.TIMESTAMP)
	private Date update_time;	// 更新时间
}

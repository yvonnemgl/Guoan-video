package com.gack.business.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: StoreUniteInfo 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月7日 下午5:20:07 
* @version V1.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="store_unite_info")
public class StoreUniteInfo extends IdEntity{
	String uniteKey;			//信息名称
	String uniteValue;		//信息内容
}

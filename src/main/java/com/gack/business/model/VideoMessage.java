package com.gack.business.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: VideoMessage.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月21日
*  
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="message")
public class VideoMessage extends IdEntity{
	String message; //内容
	Date createtime;
	String type;	//消息类型  1>>预约消息
	String title; //标题
	Integer mesType; //消息类型 1 拉人入会 2 消息 3 预约消息 4 验证消息 5 审核消息 6 押金消息 7 商务卡消息
	Integer state; //消息内容对应的状态
	String createId; //创建人
	Integer sendWay; //发送方式 1 短信 2 站内信 3 push 4 邮件
	Integer isRead; //已读 0否 1是
	@JsonIgnore
	@ManyToOne(cascade=CascadeType.REFRESH,optional=false)
	@JoinColumn(name = "userid")
	private User user;
}

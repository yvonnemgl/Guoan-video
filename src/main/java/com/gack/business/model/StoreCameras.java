package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: StoreCameras 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年7月10日 下午5:47:40 
* @version V1.0
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "store_cameras")
public class StoreCameras extends IdEntity{
	private String storeId;			//门店id
	private String chineseName;		//中文名
	private String meetingRoomId;	//摄像头会议室id
	private Integer type;			//类型  1摄像头
	private String ip;				//ip地址
	private Date createTime;		//创建时间
	private Integer status;			//摄像头状态：  0 未使用  1 已使用
	private Integer isUse;			//使用状态： 0 可使用  1 暂停使用 2 已被删除
	
	private String meetingId;		//参加video会议室的id
	private String meetingName;		//参加video会议室的名称
	
	@Transient
	private String storeName;		//门店名称
}

package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-6-1
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "invitetoenterpriserecord")
public class InviteToEnterpriseRecord extends IdEntity{
	private String inviterId; //邀请人id
	private String inviteeId; //被邀请人id
	private String inviteeUsername; //被邀请人username,即手机号（若为inner，则为空; 若为outer则有值）
	private String enterpriseId; //公司id
	private String departmentId; //部门id
	private String positionId; //职位id
	@Temporal(TemporalType.TIMESTAMP)
	private Date createTime; //发起邀请时间
	private String inviteFrom; //(friend, tel_list, phone_number)
	private Integer status; //状态   0:拒绝  1:同意  2:未处理    3:不可再次处理
	private String remarks; //备注
}

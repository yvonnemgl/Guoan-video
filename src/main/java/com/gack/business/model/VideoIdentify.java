package com.gack.business.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* 
* 
* @ClassName: VideoIdentify.java 
* @Description: 短信验证码实体
* @author Cancerl
* @date 2018年3月20日
*  
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="identify")
public class VideoIdentify extends IdEntity{
	String username;
	String identifycode;	//短信验证码
	Date identifytime;	//短信验证码发送时间
}

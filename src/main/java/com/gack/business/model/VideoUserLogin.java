package com.gack.business.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: VideoUserLogin.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月22日
*  
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="user_login")
public class VideoUserLogin extends IdEntity{
	Date lasttime;
	
	@OneToOne(cascade=CascadeType.REFRESH,optional=false)
    @JoinColumn(name="userid")
	VideoUser user;
	
	String session;
	String type;
	String equipment;
}

package com.gack.business.sendscheduled;

import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gack.business.service.ConferenceServiceInterface;

//@EnableScheduling
@Component
public class UpdateJoinConference {
	@Autowired
	private ConferenceServiceInterface conferenceServiceInterface;
	
	@Scheduled(cron = "0/10 * * * * ?")
	public void updateJoinConference() throws DocumentException {
		conferenceServiceInterface.updateJoinConference();
	}

}

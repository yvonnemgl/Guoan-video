package com.gack.business.sendscheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gack.business.service.ConferenceServiceInterface;

//@Component
public class UpdateUserVideoDateScheduled {
	@Autowired
	private ConferenceServiceInterface conferenceServiceInterface;
	@Scheduled(cron = "0 0 * * * ?")//@Scheduled(cron = "0/10 * * * * ?")
	public void updateUserVideoDate() {
		System.out.println("更新了用户使用视频的数据");
		conferenceServiceInterface.updateUserVideoDate();
	}
	

}

//package com.gack.business.sendscheduled;
//
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import com.gack.business.model.Message;
//import com.gack.business.model.VideoCancelOrderformReason;
//import com.gack.business.model.VideoOrderform;
//import com.gack.business.model.VideoStores;
//import com.gack.business.repository.MessageRepository;
//import com.gack.business.repository.VideoCancelOrderformReasonRepository;
//import com.gack.business.repository.VideoOrderformRepository;
//import com.gack.business.repository.VideoStoresRepository;
//import com.gack.helper.common.util.SendCode;
//
///**
// * 
//* @ClassName: cancelOrderformScheduled.java 
//* @Description: 定时取消订单
//* @author Cancerl
//* @date 2018年3月28日
//*  
// */
//
//@Configuration
////@EnableScheduling
//@Component
//public class cancelOrderformScheduled {
//	
//	@Autowired
//	private VideoOrderformRepository orderformRepository;
//	@Autowired
//	private VideoCancelOrderformReasonRepository cancelOrderformReasonRepository;
//	@Autowired
//	private MessageRepository messageRepository;
//	@Autowired
//	private VideoStoresRepository storesRepository;
//	
//	/*每分钟获取所有预约订单   预定订单 超出30分钟   取消订单*/
//	@Scheduled(cron="0 */1 * * * ?")
//	//@Scheduled(cron="*/5 * * * * ?")
//	public void cancelOrderform(){
//		List<VideoOrderform> orderforms = orderformRepository.findByOrderStatus("1");
//		if(orderforms != null && orderforms.size() > 0){
//			//查询出预约订单信息
//			Date nowtime = new Date();
//			for (VideoOrderform orderform : orderforms) {
//				long TimeDiff = nowtime.getTime() - orderform.getPlaceOrdertime().getTime();
//				if(TimeDiff > 1000*60*30){
//					// 订单超时  判断当前订单状态是否已被更改
//					orderform = orderformRepository.findOne(orderform.getId());
//					if(orderform != null && "1".equals(orderform.getOrderStatus())){
//						//订单状态未被修改 修改订单状态
//						try{
//							//预约订单超时  修改订单状态
//							orderform.setOrderStatus("3");
//							orderform.setOrderEndtime(nowtime);
//							orderform = orderformRepository.save(orderform);
//							//添加预约订单原因
//							//如果不存在取消预约订单原因信息  增加 
//							List<VideoCancelOrderformReason> cancelOrderformReasons = cancelOrderformReasonRepository.findByOrderform(orderform);
//							if(cancelOrderformReasons == null || cancelOrderformReasons.size() == 0){
//								VideoCancelOrderformReason cancelOrderformReason = new VideoCancelOrderformReason();
//								cancelOrderformReason.setReason("预约订单超时");
//								cancelOrderformReason.setOrderform(orderform);
//								cancelOrderformReasonRepository.save(cancelOrderformReason);
//								//orderform.setCancelReason(cancelOrderformReason);
//								//orderform = orderformRepository.save(orderform);
//							}else{
//								continue;
//							}
//							//用户发送消息
//							Message message = new Message();
//							message.setMessage("你预约的会议室（"+orderform.getStore().getStoreName()+"） 订单超时，已自动取消");
//							message.setType("预约消息");
//							message.setMesType(5);
//							message.setSendWay(2);
//							message.setUserid(orderform.getUser().getId());
//							message.setCreatetime(new Date());
//							messageRepository.save(message);
//							//修改门店状态可用
//							VideoStores store = orderform.getStore();
//							if(store != null){
//								store.setStatus(1);
//								storesRepository.save(store);
//							}
//							//发送短信提示预约订单取消信息
//							SendCode.sendMessage(orderform.getUser().getUsername(), "您有预约订单超时，订单已被取消");
//							SendCode.sendMessage(orderform.getStore().getStorePhone(), "手机尾号为"+orderform.getUser().getUsername().substring(7)+"客户订单超时，已经自动取消预约订单。");
//						}catch (Exception e) {
//							e.printStackTrace();
//							System.out.println("取消超时预约订单操作失败");
//						}
//					}
//				}
//			}
//		}
//	}
//}

package com.gack.business.sendscheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gack.business.dao.UserLoginRecordDao;
import com.gack.business.repository.UserLoginRecordRepository;

/**
 * 
* @ClassName: AutoLogoutUsersByHeartbeat.java 
* @Description: 定时清除心跳超时user
* @author Cancerl
* @date 2018年4月18日
*  
 */

@Configuration
//@EnableScheduling
@Component
public class AutoLogoutUsersByHeartbeat {

	@Autowired
	private UserLoginRecordRepository userLoginRecordRepository;
	@Autowired
	private UserLoginRecordDao userLoginRecordDao;
	
	//@Scheduled(cron="0 */1 * * * ?")
	@Scheduled(cron="*/5 * * * * ?")
	public void autoLogoutUser(){
//		String login_resource = "pc";
//		Date now_date = new Date();
//		long now_time = now_date.getTime();
//		long d_value_max = 1000 * 60;	//心跳最大时间差
//		List<UserLoginRecord> userLogins = userLoginRecordRepository.findByLoginResource(login_resource);
//		if(userLogins != null && userLogins.size() > 0){
//			for (UserLoginRecord userLogin : userLogins) {
//				long last_time = userLogin.getLogin_time().getTime();
//				long d_value = now_time - last_time;
//				if(d_value >= d_value_max){
//					//当前时间到最后一次心跳时间大于等于 心跳最大时间差 默认连接中断   指定logout 抹除登录记录
//					try{
//						if(userLoginRecordRepository.findOne(userLogin.getId()) != null){
//							userLoginRecordRepository.delete(userLogin);
//						}
//					}catch (Exception e) {
//						System.err.println("心跳终止  清除用户登录记录失败   userid："+userLogin.getUsername());
//						e.printStackTrace();
//					}
//				}
//			}
//		}
		Integer timeout = 15;
		try{
			userLoginRecordDao.dealTimeoutBreatHeart(timeout);
		}catch (Exception e) {
			System.err.println("心跳终止  清除超时pc登录记录出错");
			e.printStackTrace();
		}
	}
}

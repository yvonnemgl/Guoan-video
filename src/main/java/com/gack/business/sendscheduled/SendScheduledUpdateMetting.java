package com.gack.business.sendscheduled;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gack.business.model.VideoMeeting;
import com.gack.business.service.VideoMeetingService;
import com.gack.helper.common.util.PackerUtil;
import com.gack.helper.common.util.WebServiceUtils;




@Component
public class SendScheduledUpdateMetting {
	
	
	@Autowired
	private  VideoMeetingService  videoMeetingService;
@Scheduled(cron = "0 55 01 * * ?")
	public void updateVideoMeeting() {
		List<VideoMeeting> meets=videoMeetingService.getAllVideoMeeting();
		
		for(VideoMeeting meet:meets){
			Map canshu=new HashMap<String,Object>();
			
			String name=meet.getName();
			String name1=name.substring(0,2);
			int name2=Integer.parseInt(name.substring(2));
			int i=name2+5;
			
			canshu.put("i",i);
			String romid=meet.getRoomid();
			/* Map canshu2=new HashMap<String,String>();
				PackerUtil packerUtil2=new PackerUtil();
				canshu2.put("roomID",meet.getRoomid());
				packerUtil2.packBody1("v1:DeleteRoomRequest", canshu2);
				String data2 = packerUtil2.xDoc.getDocument().asXML();
				System.out.println(data2);
				//调用webservice/删除room接口
				String xmlStr2=WebServiceUtils.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService",data2,"deleteRoom");*/
			//创建房间
			PackerUtil packerUtil=new PackerUtil();
			packerUtil.packBody("v1:AddRoomRequest", canshu);
			String data = packerUtil.xDoc.getDocument().asXML();
			System.out.println(data);
			//调用webservice创建room接口
			Map<String,String> fanhuimap=new HashMap<String, String>();
			String xmlStr=WebServiceUtils.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService",data,"addRoom");
			Document dom=null;
			try {
				dom = DocumentHelper.parseText(xmlStr);
			} catch (DocumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
			Element root=dom.getRootElement(); 
	        Element Body=root.element("Body");
	        Element AddRoomResponse=Body.element("AddRoomResponse");
	        String OK=AddRoomResponse.element("OK").getText();
	        if(OK.equals("OK")){
	        	//查询新创建的房间
	        	PackerUtil packerUtil1=new PackerUtil();
	    		packerUtil1.packBody3("v1:GetRoomsRequest", canshu);
	    		String data1 = packerUtil1.xDoc.getDocument().asXML();
	    		System.out.println(data);
	    		//调用webservice获取rooms接口
	    		/*Map<String,String> fanhuimap=new HashMap<String, String>();*/
	    		String xmlStr1=WebServiceUtils.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService",data1,"getRooms");
	    		Document dom1=null;
				try {
					dom1 = DocumentHelper.parseText(xmlStr1);
				} catch (DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
				Element root1=dom1.getRootElement();  
		        Element Body1=root1.element("Body");
		        Element AddRoomResponse1=Body1.element("GetRoomsResponse");
		        Element room=AddRoomResponse1.element("room");
		        String roomID=room.element("roomID").getText();
		        Element RoomMode=room.element("RoomMode");
		        
		        String roomURL=RoomMode.element("roomURL").getText();
		        String roomPIN=RoomMode.element("roomPIN").getText();
		        String url=StringEscapeUtils.unescapeXml(roomURL);
		        meet.setRoomid(roomID);
		        meet.setRoomUrl(url);
		        meet.setPeopleNum(0);
		        meet.setName("会议"+i);
		        videoMeetingService.updateVideoMeeting(meet);
		        //删除房间
		        Map canshu2=new HashMap<String,String>();
				PackerUtil packerUtil2=new PackerUtil();
				canshu2.put("roomID",romid);
				packerUtil2.packBody1("v1:DeleteRoomRequest", canshu2);
				String data2 = packerUtil2.xDoc.getDocument().asXML();
				System.out.println(data2);
				//调用webservice/删除room接口
				String xmlStr2=WebServiceUtils.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService",data2,"deleteRoom");
				continue;
	        }
		}
}
}

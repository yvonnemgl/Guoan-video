package com.gack.business.sendscheduled;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.aliyun.oss.OSSClient;
import com.gack.business.model.ValidateCodeCreateLog;
import com.gack.business.repository.ValidateCodeCreateLogRepository;
import com.gack.helper.common.util.OSSUnitHelper;

/**
 * 
* @ClassName: DealValidateCodeImage 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月13日 上午10:05:26 
* @version V1.0
 */
@Configuration
//@EnableScheduling
@Component
public class DealValidateCodeImage {

	@Autowired
	private ValidateCodeCreateLogRepository validateCodeCreateLogRepository;
//	@Scheduled(cron="0 */1 * * * ?")
	@Scheduled(cron="0 0 0 * * ?")
	public void dealValidateCodeImage(){
		OSSClient client=OSSUnitHelper.getOSSClient();
		String diskName = "datas/video/identify/";
        Date date=new Date();  
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(date);  
        calendar.add(Calendar.DAY_OF_MONTH, -1);  
        date = calendar.getTime();  
		
		List<ValidateCodeCreateLog> validateCodeCreateLogs = validateCodeCreateLogRepository.getValidateCodesBeforeTime(date);
		for (ValidateCodeCreateLog validateCodeCreateLog : validateCodeCreateLogs) {
			try{
				String key = validateCodeCreateLog.getPicName();
				OSSUnitHelper.deleteFile(client, diskName, key);
				validateCodeCreateLogRepository.delete(validateCodeCreateLog);
			}catch (Exception e) {
				System.err.println("定时删除验证码图片  错误");
				e.printStackTrace();
			}
		}
	}
}

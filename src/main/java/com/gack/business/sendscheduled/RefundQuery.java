package com.gack.business.sendscheduled;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gack.business.model.Message;
import com.gack.business.model.User;
import com.gack.business.repository.DepositRecordRepository;
import com.gack.business.repository.MessageRepository;
import com.gack.business.repository.UserRepository;
import com.gack.helper.common.config.PayConfig;
import com.gack.helper.common.util.HttpGetUtil;

@Configuration
//@EnableScheduling
@Component
public class RefundQuery {

	@Autowired
	private DepositRecordRepository depositRecordRepository;
	@Autowired
	private UserRepository userRepository;
	
	
	@Scheduled(cron = "0 */30 * * * ?")
	public void refundQuery() {
		depositRecordRepository.findOidByTypeAndUid().forEach(e -> {
			// 调用押金支付接口
			String baseUrl = PayConfig.REFUND_QUERY;
			StringBuffer url = new StringBuffer();
			url.append(baseUrl).append("?pingId=").append(e.getOid());
			String result = HttpGetUtil.httpGet(url.toString());
			if(result.equals("5")) {
				User user = userRepository.findOne(e.getUid());
				user.setIsPledge(0);
				userRepository.save(user);
			}
			
		});
	}

}

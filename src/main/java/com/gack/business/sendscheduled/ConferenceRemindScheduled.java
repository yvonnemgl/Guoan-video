package com.gack.business.sendscheduled;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gack.business.service.ConferenceServiceInterface;

//@EnableScheduling
@Component
public class ConferenceRemindScheduled {
	
	@Autowired
	private ConferenceServiceInterface conferenceServiceInterface;
	
	@Scheduled(cron="0/5 * *  * * ? ")
	public void autoConferenceRemind() throws Exception {
		conferenceServiceInterface.autoRemind();
	}
}

package com.gack.business.permisson.exception;

/**
 * 2018-9-6
 * @author ws
 *
 */
public class OpenNotConsensusException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5873041696046959990L;

	public OpenNotConsensusException(){
		super();
	}
	
	public OpenNotConsensusException(String message){
		super(message);
	}
	
}

package com.gack.business.permisson.exception;

/**
 * 2018-9-6
 * @author ws
 *
 */
public class PermissionArrayIsNullException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9055374454633281782L;

	public PermissionArrayIsNullException(){
		super();
	}
	
	public PermissionArrayIsNullException(String message){
		super(message);
	}
	
}

package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.StoreAttention;

/**
 * 
* @ClassName: StoreAttentionRepository 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年9月6日 下午5:16:51 
* @version V1.0
 */
public interface StoreAttentionRepository extends JpaRepository<StoreAttention, String> , JpaSpecificationExecutor<StoreAttention>{

}

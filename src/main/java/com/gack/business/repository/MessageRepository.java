package com.gack.business.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.Message;


/**
 * 
 * @author ws
 * 2018-6-1
 */
@Repository
public interface MessageRepository extends JpaRepository<Message, String>, JpaSpecificationExecutor<Message>{
	
	Page<Message> findByUseridAndSendWay(String userid,Integer sendWay,Pageable pageable);
	@Query("select m2 from Message m2 where 1=1 and m2.userid = :username and sendWay in( '2' , '3')")
	Page<Message> getPageMessages(@Param("username") String username,Pageable pageable);
	
	@Query("select m2 from Message m2 where m2.mesType = 1 and m2.userid = :username and sendWay in( '2' , '3')")
	List<Message> findUserBeVisitedMessage(@Param("username") String username);
	
	@Query("select count(m.id) from Message m where 1=1 and m.userid = :userid and sendWay in ('2','3') and isRead = 0")
	int getUserStatus(@Param("userid") String userid);
	
	@Transactional
	@Modifying
	@Query("update Message set isRead = :is_read where 1=1 and userid = :userid and  sendWay in( '2' , '3')")
	int updateAllMessageIsRead(@Param("userid")String userid,@Param("is_read")Integer is_read);
}

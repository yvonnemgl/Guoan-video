package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.MessagePushUser;

public interface MessagePushUserRepository extends JpaRepository<MessagePushUser, String>, JpaSpecificationExecutor<MessagePushUser> {

}

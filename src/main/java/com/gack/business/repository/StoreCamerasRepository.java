package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.StoreCameras;

/**
 * 
* @ClassName: StoreCamerasRepository 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年7月10日 下午5:54:56 
* @version V1.0
 */
public interface StoreCamerasRepository extends JpaRepository<StoreCameras, String>{
	
	@Query("select c from StoreCameras c where 1=1 and id = :id and isUse != 2")
	StoreCameras getNoDealCamers(@Param("id") String id);
}

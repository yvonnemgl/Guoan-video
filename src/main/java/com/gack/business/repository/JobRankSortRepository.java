package com.gack.business.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.JobRankSort;

/**
 * 职级排序表
 * @author ws
 * 2018-9-10
 */
@Repository
public interface JobRankSortRepository extends JpaRepository<JobRankSort, String>, JpaSpecificationExecutor<JobRankSort>{

	@Modifying
	@Query("update JobRankSort set sort=:sort, updateTime=:date where enterpriseId=:enterpriseId")
	int updateSort(@Param("enterpriseId")String enterpriseId, @Param("sort")String sort, @Param("date") Date date);
	
	@Query("from JobRankSort where enterpriseId=:enterpriseId")
	JobRankSort findByEnterpriseId(@Param("enterpriseId") String enterpriseId);//查询公司职级排序
	
}

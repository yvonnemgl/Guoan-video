package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.gack.business.model.DepositRecord;

public interface DepositRecordRepository
		extends JpaRepository<DepositRecord, String>, JpaSpecificationExecutor<DepositRecord> {


	List<DepositRecord> findByUidOrderByCreateTimeDesc(String uid);
	
	@Query(value = "SELECT d.oid,d.uid,d.id,d.type,d.channel,d.amount,d.create_time from depositrecord d where d.uid in (SELECT u.id FROM `user` u WHERE u.is_pledge = '2') and d.type = '1'",nativeQuery = true)
	List<DepositRecord> findOidByTypeAndUid();
}

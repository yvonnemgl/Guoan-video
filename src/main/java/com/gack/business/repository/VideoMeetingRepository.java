package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.VideoMeeting;




public interface VideoMeetingRepository extends JpaRepository<VideoMeeting, String>, JpaSpecificationExecutor<VideoMeeting>{
	  @Query("select t from  VideoMeeting t  where t.roomid =:roomid")
	  VideoMeeting   selectByOneVideoMeeting(@Param("roomid") String roomid);
}

package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.VideoIdentify;

/**
 * 
 * 
 * 
* @ClassName: VideoIdentifyRepository.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月20日
*  
 */
public interface VideoIdentifyRepository extends JpaRepository<VideoIdentify, String>,JpaSpecificationExecutor<VideoIdentify>{
	
	//根据用户名查询发送短信验证码
	List<VideoIdentify> findByUsername(String username);
}

package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.Area;

@Repository
public interface AreaRepository extends JpaRepository<Area, String>, JpaSpecificationExecutor<Area>{

	@Query("from Area where cityCode = (select code from City where id=:cityId )")
	List<Area> findByCityId(@Param("cityId") String cityId);
	
}

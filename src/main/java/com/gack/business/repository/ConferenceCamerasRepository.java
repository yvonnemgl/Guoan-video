package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.ConferenceCameras;

public interface ConferenceCamerasRepository extends JpaRepository<ConferenceCameras, String>, JpaSpecificationExecutor<ConferenceCameras> {
	List<ConferenceCameras> findByScidAndIsJoin(@Param("scid")String scid, @Param("isJoin") Integer isJoin);
	//根据会议室ID和是否已经加入查找会议室摄像头，并且安卓加入时间倒序查找
	List<ConferenceCameras> findByCidAndIsJoinOrderByJoinTimeDesc(@Param("cid")String cid, @Param("isJoin")Integer isJoin);

}

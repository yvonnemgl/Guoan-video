package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.VideoUser;
import com.gack.business.model.VideoUserLogin;

/**
 * 
* @ClassName: VideoUserLoginRepository.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月22日
*  
 */
public interface VideoUserLoginRepository extends JpaRepository<VideoUserLogin, String>,JpaSpecificationExecutor<VideoUserLogin>{
	List<VideoUserLogin> findByUser(VideoUser user);
	List<VideoUserLogin> findBySessionAndUser(String session,VideoUser user);
	List<VideoUserLogin> findBySession(String session);
	List<VideoUserLogin> findByUserAndType(VideoUser user,String type);
	List<VideoUserLogin> findByType(String type);
	List<VideoUserLogin> findByUserAndTypeAndEquipment(VideoUser user,String type,String equipment);
}

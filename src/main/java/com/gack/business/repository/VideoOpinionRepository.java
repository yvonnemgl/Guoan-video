package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.VideoOpinion;

/**
 * 
 * 
 * 
* @ClassName: VideoOpinionRepository.java 
* @Description: 意见反馈映射repository
* @author Cancerl
* @date 2018年3月21日
*  
 */
public interface VideoOpinionRepository extends JpaRepository<VideoOpinion, String>,JpaSpecificationExecutor<VideoOpinion>{
	
}

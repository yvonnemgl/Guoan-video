package com.gack.business.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.JobRank;
import com.gack.business.vo.JobRankVO;

/**
 * 
 * @author ws
 * 2018-9-10
 */
@Repository
public interface JobRankRepository extends JpaRepository<JobRank, String>, JpaSpecificationExecutor<JobRank>{

	@Query("from JobRank where id=:id and status=1")
	JobRank findById(@Param("id") String id);//查询未被删除的职级
	
	@Query("from JobRank where enterpriseId=:enterpriseId and name=:name and status=1")
	JobRank findByEnterpriseIdAndName(@Param("enterpriseId") String enterpriseId, @Param("name") String name);//查询未被删除的职级
	
	@Query("from JobRank where id=:id and enterpriseId=:enterpriseId and status=1")
	JobRank findByEnterpriseIdAndId(@Param("enterpriseId") String enterpriseId, @Param("id") String id);//查询未被删除的职级
	
	@Modifying()
	@Query("update JobRank set status=0, deleteTime=:date where id=:id")
	int deleteById(@Param("id") String id, @Param("date") Date date);//删除职级
	
	@Modifying()
	@Query("update JobRank set status=0, deleteTime=:date where enterpriseId=:enterpriseId")
	int deleteByEnterpriseId(@Param("enterpriseId") String enterpriseId, @Param("date") Date date);//删除职级
	
	@Query("select new com.gack.business.vo.JobRankVO(id, name, level, enterpriseId)"
			+ " from JobRank"
			+ " where enterpriseId=:enterpriseId"
			+ " and status=1"
			+ " order by level ASC, createTime ASC")
	List<JobRankVO> findVOByEnterpriseId(@Param("enterpriseId") String enterpriseId);//查询未被删除的职级VO
	
	@Query("select name from JobRank where id=:id")
	String findNameById(@Param("id") String id);
}

package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.City;

@Repository
public interface CityRepository extends JpaRepository<City, String>, JpaSpecificationExecutor<City>{

	@Query("from City where provinceCode = (select code from Province where id=:provinceId)")
	List<City> findByProvinceId(@Param("provinceId")String provinceId);
	
}

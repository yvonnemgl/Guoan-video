package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gack.business.model.PersonalAuthentication;

/**
 * 
* @ClassName: PersonalAuthenticationRepository 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月20日 下午4:00:00 
* @version V1.0
 */
public interface PersonalAuthenticationRepository extends JpaRepository<PersonalAuthentication, String>{
	
}

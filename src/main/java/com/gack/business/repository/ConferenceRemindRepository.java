package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.ConferenceRemind;

public interface ConferenceRemindRepository extends JpaRepository<ConferenceRemind, String>, JpaSpecificationExecutor<ConferenceRemind> {
	List<ConferenceRemind> findByIsPush(@Param("isPush") Integer isPush);
	List<ConferenceRemind> findByConferenceId(@Param("conferenceId") String conferenceId);

}

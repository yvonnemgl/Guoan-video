package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.ManagerUpdateLog;

/**
 * 
 * @author ws
 * 2018-8-22
 */
public interface ManagerUpdateLogRepository extends JpaRepository<ManagerUpdateLog, String>, JpaSpecificationExecutor<ManagerUpdateLog>{

}

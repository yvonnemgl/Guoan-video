package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.VideoUser;

/**
 * 
 * 
 * 
* @ClassName: VideoUserRepository.java 
* @Description: video_user_repository
* @author Cancerl
* @date 2018年3月20日
*  
 */
public interface VideoUserRepository extends JpaRepository<VideoUser, String>,JpaSpecificationExecutor<VideoUser>{
	List<VideoUser> findByUsername(String username);
}

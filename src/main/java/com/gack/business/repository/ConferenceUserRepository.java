package com.gack.business.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.ConferenceUser;


public interface ConferenceUserRepository extends JpaRepository<ConferenceUser, String>, JpaSpecificationExecutor<ConferenceUser> {
	List<ConferenceUser> findByUid(@Param("uid") String uid);
	Page<ConferenceUser> findByUid(@Param("uid") String uid, Pageable pageable);
	List<ConferenceUser> findByCid(@Param("cid") String cid);
	ConferenceUser findByUidAndCid(@Param("uid") String uid, @Param("cid") String cid);
	Page<ConferenceUser> findByUidOrderByLastModifyTimeDesc(@Param("uid") String uid, Pageable pageable);
	List<ConferenceUser> findByUidOrderByLastModifyTimeDesc(@Param("uid") String uid);

}

package com.gack.business.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.Order;
import com.gack.business.vo.OrderVo;
@Repository
public interface OrderRepository extends JpaRepository<Order, String>, JpaSpecificationExecutor<Order>{

	Order findByPingId(String pingId);
	
	List<Order> findByBusinessCardAmountGreaterThan(Integer amount);
	
	@Query("select new com.gack.business.vo.OrderVo(o.id,(SELECT storeName from VideoStores s where s.id = o.storeId) as storeName,o.createdTime,o.beginTime,o.endTime,o.amount,o.status) from Order o where o.userId = :userId")
	List<OrderVo> findOrdersByUserId(@Param("userId")String userId);
	
	/**
	 *  获取预约或使用中的订单
	 */
	@Query("select o from Order o where (o.status in ('1','4','5','6','7')) and o.userId = :userid")
	List<Order> getUserUsingOrReservedOrder(@Param("userid")String userid);
	
	@Query(nativeQuery = true, value = "select `id`, `order_id`, `user_id`, `ping_id`, `store_id`, `enterprise_id`, `department_id`, `status`, `created_time`, `begin_time`, `end_time`, `pay_time`, `amount`, `pay_amount`, `card_amount`, `business_card_amount`, `type`, `cancel_reason` from orders where user_id = :userId and status in ('1', '4', '5', '6', '7') ORDER BY created_time DESC LIMIT 1")
	Order findByUserIdOrderByCreatedTimeDesc(@Param("userId")String userId);
	
	@Query(nativeQuery = true, value = "select `id`, `order_id`, `user_id`, `ping_id`, `store_id`, `enterprise_id`, `department_id`, `status`, `created_time`, `begin_time`, `end_time`, `pay_time`, `amount`, `pay_amount`, `card_amount`, `business_card_amount`, `type`, `cancel_reason` from orders where store_id = :storeId ORDER BY created_time DESC LIMIT 1")
	Order findByStoreId(@Param("storeId")String storeId);
}
  
    
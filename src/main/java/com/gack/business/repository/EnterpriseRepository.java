package com.gack.business.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.Enterprise;
import com.gack.business.vo.EnterpriseItemVO;

@Repository
public interface EnterpriseRepository extends JpaRepository<Enterprise, String>, JpaSpecificationExecutor<Enterprise>{

	@Modifying
	@Query("update Enterprise set currentNumber=currentNumber+1 where id=:enterpriseId")
	int addCurrentNumber(@Param("enterpriseId") String enterpriseId);//将公司人数加一
	
	@Modifying
	@Query("update Enterprise set currentNumber=currentNumber-1 where id=:enterpriseId")
	int minusCurrentNumber(@Param("enterpriseId") String enterpriseId);//将公司人数减一
	
	@Query("select new com.gack.business.vo.EnterpriseItemVO(e.id, e.name, e.currentNumber, e.status)"
			+ " from Enterprise e"
			+ " where e.id in (select distinct uedp.enterpriseId"
								+ " from UserEnterpriseDepartmentPosition uedp"
								+ " where uedp.userId=:userId)"
			+ " and e.status != '4'"
			+ " order by e.createTime Desc")
	List<EnterpriseItemVO> findEnterpriseItemVOListByUserId(@Param("userId") String userId);//查询某用户所在的公司,并按创建时间,降序排序
	
	@Query("select new com.gack.business.vo.EnterpriseItemVO(e.id, e.name, e.currentNumber, e.status)"
			+ " from Enterprise e"
			+ " where e.id=:enterpriseId ")
	EnterpriseItemVO findEnterpriseItemVOByEnterpriseId(@Param("enterpriseId") String enterpriseId);//查询某公司ItemVO
	
	@Query("select e.name"
			+ " from Enterprise e"
			+ " where e.id=:enterpriseId")
	String findNameById(@Param("enterpriseId") String enterpriseId);//根据id获取公司名称
	
	@Modifying
	@Query("update Enterprise set name=:name, updateNameTime=:date where id=:enterpriseId")
	int updateEnterpriseName(@Param("enterpriseId") String enterpriseId, @Param("name") String name, @Param("date")Date date);//修改公司名称
	
	@Modifying
	@Query("update Enterprise"
			+ " set surplusAmount=surplusAmount+(select surplus_amount"
												+ " from Department"
												+ " where id=:departmentId)"
			+ " where id=(select enterpriseId"
						+ " from Department"
						+ " where id=:departmentId)")
	int updateEnterpriseSurplusAmountByDeleteDepartment(@Param("departmentId") String departmentId);//删除某部门后,归还剩余额度
	
	@Query("select count(e.id) from Enterprise e where e.id=:enterpriseId"
			+ " and e.status != '4'")
	int countEnterpriseByEnterpriseId(@Param("enterpriseId") String enterpriseId);//根据公司id,查询未删除公司数量(判断该公司是否存在)
	
	@Query("from Enterprise"
			+ " where id=:enterpriseId"
			+ " and status != '4'")
	Enterprise findById(@Param("enterpriseId") String enterpriseId);
	
//	@Modifying
//	@Query("update Enterprise"
//			+ " set status=:status"
//			+ " where id=:enterpriseId")
//	int updateStatus(@Param("enterpriseId")String enterpriseId, @Param("status")String status);
	
	@Modifying
	@Query("update Enterprise"
			+ " set status='4',"
			+ " deleteTime=:date"
			+ " where id=:enterpriseId")
	int deleteByEnterpriseId(@Param("enterpriseId")String enterpriseId, @Param("date")Date date);
	
	@Modifying
	@Query("update Enterprise"
			+ " set industry=:industryName"
			+ " where id=:enterpriseId")
	int updateIndustryByEnterpriseId(@Param("enterpriseId")String enterpriseId, @Param("industryName")String industryName);
	
	@Modifying
	@Query("update Enterprise"
			+ " set numberRange=:personNumRangeName"
			+ " where id=:enterpriseId")
	int updatePersonNumRangeByEnterpriseId(@Param("enterpriseId")String enterpriseId, @Param("personNumRangeName")String personNumRangeName);
	
	@Modifying
	@Query("update Enterprise"
			+ " set location=:locationName"
			+ " where id=:enterpriseId")
	int updateLocationByEnterpriseId(@Param("enterpriseId")String enterpriseId, @Param("locationName")String locationName);
}

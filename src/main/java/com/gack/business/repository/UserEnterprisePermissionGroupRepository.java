package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.UserEnterprisePermissionGroup;

/**
 * 
 * @author ws
 * 2018-8-14
 */
@Repository
public interface UserEnterprisePermissionGroupRepository extends JpaRepository<UserEnterprisePermissionGroup, String>, JpaSpecificationExecutor<UserEnterprisePermissionGroup>{

	@Query("from UserEnterprisePermissionGroup where userId=:userId and enterpriseId=:enterpriseId")
	UserEnterprisePermissionGroup findByUserIdAndEnterpriseId(@Param("userId")String userId, @Param("enterpriseId")String enterpriseId);
	
	@Query("from UserEnterprisePermissionGroup"
			+ " where enterpriseId=:enterpriseId")
	List<UserEnterprisePermissionGroup> findManager(@Param("enterpriseId")String enterpriseId);
	
	@Modifying
	@Query("delete UserEnterprisePermissionGroup"
			+ " where userId=:userId and enterpriseId=:enterpriseId")
	int deleteByUserIdAndEnterpriseId(@Param("userId")String userId, @Param("enterpriseId")String enterpriseId);
	
	@Modifying
	@Query("delete UserEnterprisePermissionGroup"
			+ " where enterpriseId=:enterpriseId")
	int deleteByEnterpriseId(@Param("enterpriseId")String enterpriseId);
}

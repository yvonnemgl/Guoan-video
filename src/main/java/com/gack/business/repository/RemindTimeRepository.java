package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.RemindTime;

public interface RemindTimeRepository extends JpaRepository<RemindTime, String>, JpaSpecificationExecutor<RemindTime> {
	@Query("SELECT r FROM RemindTime r  ORDER BY r.time ")
	List<RemindTime> findAll();

}

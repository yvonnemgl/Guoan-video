package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.MessagePush;

public interface MessagePushRepository extends JpaRepository<MessagePush, String>, JpaSpecificationExecutor<MessagePush> {

}

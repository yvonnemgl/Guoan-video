package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.VideoSensitiveWord;

public interface VideoSensitiveWordRepository extends JpaRepository<VideoSensitiveWord, String>, JpaSpecificationExecutor<VideoSensitiveWord> {

}

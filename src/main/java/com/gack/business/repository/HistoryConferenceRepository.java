package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.HistoryConference;

public interface HistoryConferenceRepository extends JpaRepository<HistoryConference, String>, JpaSpecificationExecutor<HistoryConference> {

}

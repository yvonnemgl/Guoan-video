package com.gack.business.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.BackCode;


@Repository
public interface BackCodeRepository extends JpaRepository<BackCode, String>, JpaSpecificationExecutor<BackCode>  {
	
	
	@Query("select c from BackCode c where c.phone=:phone and c.codetext=:codetext and  c.createdate>=:createdate")
	public  BackCode  selectByPhoneAndCodetextAndCreatedate(@Param("phone") String phone,@Param("codetext") String codetext,@Param("createdate") Date createdate);
	
	
	@Query("select c from BackCode c where c.phone=:phone order by c.createdate desc")
	public List<BackCode> findCodeByPhone(@Param("phone")String phone);

}

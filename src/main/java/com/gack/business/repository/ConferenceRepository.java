package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.gack.business.model.Conference;

public interface ConferenceRepository extends JpaRepository<Conference, String>, JpaSpecificationExecutor<Conference> {
	public List<Conference> findByConferenceCode(@Param("conference") String conferenceCode);
	public List<Conference> findById(@Param("id") String id);
	public List<Conference> findByCreateId(@Param("createId") String createId);
	public List<Conference> findByTypeAndId(@Param("type") Integer type, @Param("id") List<String> id);
//	public List<Conference> findByTypeAndPeople_num(@Param("type")Integer type, @Param("people_num")Integer people_num);
	@Transactional
	@Modifying
	@Query("UPDATE  Conference SET manageId =:uid where id=:id")
	public void updateManager(@Param("uid") String uid, @Param("id") String id);

}

package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.gack.business.model.UserLoginRecord;

/**
 * 
 * @author ws
 * 2018-5-30
 */
@Repository
public interface UserLoginRecordRepository extends JpaRepository<UserLoginRecord, String>, JpaSpecificationExecutor<UserLoginRecord>{
	
	@Query("select u from UserLoginRecord u where u.username=:username and u.login_resource=:loginResource")
	List<UserLoginRecord> findByUsernameAndLoginResource(@Param("username") String username,@Param("loginResource") String login_resource);
	
	@Query("select u from UserLoginRecord u where u.login_resource=:loginResource")
	List<UserLoginRecord> findByLoginResource(@Param("loginResource") String login_resource);
	
	/**
	 * 查询  android  ios的登录记录
	 */
	@Query("select count(u.id) from UserLoginRecord u where (u.login_resource = 'android' or u.login_resource = 'ios') and u.username = :userid ")
	int getAndroidOrIosLoginCount(@Param("userid")String userid);
	/**
	 * 查询 pc登录记录
	 */
	@Query("select count(u.id) from UserLoginRecord u where u.login_resource = 'pc' and u.username = :userid ")
	int getPcLoginCount(@Param("userid")String userid);
	
	@Query("select u from UserLoginRecord u where (u.login_resource = 'android' or u.login_resource = 'ios') and u.username = :userid ")
	List<UserLoginRecord> getAndroidOrIosLoginRecode(@Param("userid")String userid);
	
	/**
	 * 清除 android   ios的登录记录
	 */
	@Transactional
	@Modifying
	@Query("delete from UserLoginRecord u where (u.login_resource = 'android' or u.login_resource = 'ios') and u.username = :userid")
	int dealAndroidOrIosLoginRecord(@Param("userid")String userid);
	
//	@Query("select u from UserLoginRecord u where u.username = :userid and u.login_source = :source and u.equipment = :equipment")
//	List<UserLoginRecord> getUserLoginStatus(@Param("userid")String userid,
//			@Param("source")String source,
//			@Param("equipment")String equipment);
}

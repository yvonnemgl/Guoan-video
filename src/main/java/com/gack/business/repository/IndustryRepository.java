package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.Industry;
import com.gack.business.vo.IndustryVO;

@Repository
public interface IndustryRepository extends JpaRepository<Industry, String>, JpaSpecificationExecutor<Industry>{

	@Query("select new com.gack.business.vo.IndustryVO(i.id, i.name, i.parentId)"
			+ " from Industry i")
	List<IndustryVO> findAllIndustryVO();//查询所有行业，并嵌套起来
	
	@Query("select new com.gack.business.vo.IndustryVO(i.id, i.name, i.parentId)"
			+ " from Industry i"
			+ " where i.name like %:inputName%"
			+ " and i.parentId != 0")
	List<IndustryVO> findIndustryByInputName(@Param("inputName") String inputName);//按照输入,查询行业
	
}


package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice, String>, JpaSpecificationExecutor<Invoice> {

	List<Invoice> findByUidAndKindAndUsed(String uid,String kind,boolean used);
	
//	@Query(name = "SELECT SUM(amout) as amout,kind,type,create_time,name,info FROM invoice WHERE uid = :uid GROUP BY uuid ORDER BY modified_time DESC", nativeQuery = true)
//	List<Invoice> findRecordByuserId(@Param("uid")String uid);
}

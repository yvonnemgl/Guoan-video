package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.Permission;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, String>, JpaSpecificationExecutor<Permission>{
	
	List<Permission> findByType(@Param("type")Integer type);
	
	@Query("from Permission"
			+ " where description=:description")
	Permission findByDescription(@Param("description")String description);
	
	@Query("select description"
			+ " from Permission")
	List<String> findAllDescription();
	
}

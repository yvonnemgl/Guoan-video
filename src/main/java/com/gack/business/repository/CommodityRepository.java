package com.gack.business.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.Commodity;

public interface CommodityRepository  extends JpaRepository<Commodity, String>,JpaSpecificationExecutor<Commodity>{
	Commodity findByStatusAndId(@Param("status")Integer status, @Param("Id")String Id);
	@Modifying
	@Transactional
	@Query("UPDATE Commodity SET status = 0 where id =:id")
	void dropOffCommodity(@Param("id") String id);
	@Modifying
	@Transactional
	@Query("UPDATE Commodity SET status = 2 where id =:id")
	void deleteCommodity(@Param("id") String id);

}

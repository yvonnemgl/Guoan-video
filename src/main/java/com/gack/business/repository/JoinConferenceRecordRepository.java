package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.JoinConferenceRecord;

public interface JoinConferenceRecordRepository extends JpaRepository<JoinConferenceRecord, String>, JpaSpecificationExecutor<JoinConferenceRecord> {

}

package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gack.business.model.StoreSupportFacity;

/**
 * 
* @ClassName: StoreSupportFacityRepository 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月4日 上午11:25:38 
* @version V1.0
 */
public interface StoreSupportFacityRepository extends JpaRepository<StoreSupportFacity, String>{
	public List<StoreSupportFacity> findByName(String facityName);
}

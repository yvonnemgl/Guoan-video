package com.gack.business.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.ValidateCodeCreateLog;

/**
 * 
* @ClassName: ValidateCodeCreateLogRepository 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月12日 下午4:18:09 
* @version V1.0
 */
public interface ValidateCodeCreateLogRepository extends JpaRepository<ValidateCodeCreateLog, String>{
	
	@Query("select v from ValidateCodeCreateLog v where v.createTime < :tfhtime")
	List<ValidateCodeCreateLog> getValidateCodesBeforeTime(@Param("tfhtime") Date time);
	
}

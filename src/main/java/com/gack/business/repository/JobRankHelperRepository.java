package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.gack.business.model.JobRankHelper;

@Repository
public interface JobRankHelperRepository extends JpaRepository<JobRankHelper, String>, JpaSpecificationExecutor<JobRankHelper>{

	@Query("from JobRankHelper order by position asc")
	List<JobRankHelper> findAll(); 
	
}

package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.VideoCancelOrderformReason;
import com.gack.business.model.VideoOrderform;

/**
 * 
* @ClassName: VideoCancelOrderformReasonRepository.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月21日
*  
 */
public interface VideoCancelOrderformReasonRepository extends JpaRepository<VideoCancelOrderformReason, String>,JpaSpecificationExecutor<VideoCancelOrderformReason>{
	List<VideoCancelOrderformReason> findByOrderform(VideoOrderform orderform);
}

package com.gack.business.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import com.gack.business.model.VersionNumber;


public interface VersionNumberRepository extends JpaRepository<VersionNumber, String>,JpaSpecificationExecutor<VersionNumber>{

	@Query("from VersionNumber")
	List<VersionNumber> getVersionNumberByCreatetime(Pageable pageable);
}

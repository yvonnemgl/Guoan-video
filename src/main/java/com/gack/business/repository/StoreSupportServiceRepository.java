package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gack.business.model.StoreSupportService;

/**
 * 
* @ClassName: StoreSupportServiceRepository 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月4日 上午11:27:30 
* @version V1.0
 */
public interface StoreSupportServiceRepository extends JpaRepository<StoreSupportService, String>{
	public List<StoreSupportService> findByName(String serviceName);
}

package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.BackUser;





@Repository
public interface BackUserRepository extends JpaRepository<BackUser, String>, JpaSpecificationExecutor<BackUser>{
	
	@Query(" select  u  from BackUser u where u.username =:username")
	List<BackUser> findByUsername(@Param("username") String username);
	
	@Query(" select  u  from BackUser u where u.phone =:phone")
	List<BackUser> findByPhone(@Param("phone") String phone);
	
    @Query("select u from BackUser u  where u.username=:username")
	public List<BackUser> findUserByUsername(@Param("username") String username);
    
    @Query("select u from BackUser u where u.username=:username and u.password=:password")	
  	public BackUser  operatorLogin(@Param("username") String username,@Param("password") String password);
	
	public List<BackUser> findByUsernameAndIdNot(String username,String id);
	public List<BackUser> findByPhoneAndIdNot(String phone,String id);
	
	

}

package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.gack.business.model.BackRole;
@Repository
public interface BackRoleRepository extends JpaRepository<BackRole, String>, JpaSpecificationExecutor<BackRole> {

}

package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.gack.business.model.BackLog;

@Repository
public interface BackLogRepsitory extends JpaRepository<BackLog, String>, JpaSpecificationExecutor<BackLog>{

}

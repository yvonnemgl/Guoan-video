package com.gack.business.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.Message;

/**
 * 
* @ClassName: VideoMessageRepository.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月21日
*  
 */
public interface VideoMessageRepository extends JpaRepository<Message, String>,JpaSpecificationExecutor<Message>{
	public Page<Message> findAll(Pageable pageable);
	@Query("select"
			+ " m2"
		+ " from Message m2"
		+ " where 1 = 1"
			+ " and m2.userid = :userid"
			+ " and sendWay in( '2' , '3')"
			+ " and ("
					+ " m2.mesType in ( 0 , 1 , 3 , 4 , 5 , 6, 7)"
					+ " or ( m2.mesType = 2 and m2.state = 1 )"
				+ ")")
	public Page<Message> findByUserid(@Param("userid")String userid,Pageable pageable);
}

package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.PushSwitch;

public interface PushSwitchRepository extends JpaRepository<PushSwitch, String>, JpaSpecificationExecutor<PushSwitch> {

}

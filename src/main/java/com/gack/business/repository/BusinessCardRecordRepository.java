package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.gack.business.model.BusinessCardRecord;

@Repository
public interface BusinessCardRecordRepository extends JpaRepository<BusinessCardRecord, String>, JpaSpecificationExecutor<BusinessCardRecord>{

	BusinessCardRecord findByOid(String oid);
	
}

package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gack.business.model.InviteToEnterpriseRecord;

/**
 * 
 * @author ws
 * 2018-6-1
 */
@Repository
public interface InviteToEnterpriseRecordRepository extends JpaRepository<InviteToEnterpriseRecord, String>, JpaSpecificationExecutor<InviteToEnterpriseRecord>{

	@Modifying
	@Query("update InviteToEnterpriseRecord"
			+ " set status=:newStatus"
			+ " where enterpriseId=:enterpriseId"
			+ " and inviteeUsername=:inviteeUsername"
			+ " and status=:oldStatus")
	int updateStatusByEnterpriseIdAndInviteeUsername(@Param("oldStatus")int oldStatus, @Param("newStatus")int newStatus, @Param("enterpriseId")String enterpriseId, @Param("inviteeUsername")String inviteeUsername);
	
	@Query("select id"
			+ " from InviteToEnterpriseRecord"
			+ " where status=:status"
			+ " and enterpriseId=:enterpriseId"
			+ " and inviteeUsername=:inviteeUsername")
	List<String> findIdByEnterpriseIdAndInviteeUsername(@Param("status")int status, @Param("enterpriseId")String enterpriseId, @Param("inviteeUsername")String inviteeUsername);
	
	@Query("select count(iter.id)"
			+ " from InviteToEnterpriseRecord iter"
			+ " where iter.status = 2"
			+ " and iter.enterpriseId=:enterpriseId"
			+ " and iter.departmentId=:departmentId"
			+ " and iter.positionId=:positionId"
			+ " and iter.inviterId=:inviterId"
			+ " and iter.inviteeId=:inviteeId"
			+ " and iter.inviteFrom='inner'")
	int countSameInnerRecord(@Param("enterpriseId") String enterpriseId, @Param("departmentId") String departmentId, @Param("positionId") String positionId, @Param("inviterId") String inviterId, @Param("inviteeId") String inviteeId);//查询未处理的相同内部邀请数量(判断是否已发出邀请,且尚未被处理)
	
	@Query("select count(iter.id)"
			+ " from InviteToEnterpriseRecord iter"
			+ " where iter.status = 2"
			+ " and iter.enterpriseId=:enterpriseId"
			+ " and iter.departmentId=:departmentId"
			+ " and iter.positionId=:positionId"
			+ " and iter.inviterId=:inviterId"
			+ " and iter.inviteeId=:inviteeId"
			+ " and iter.inviteFrom='outer'")
	int countSameOuterRecordWithId(@Param("enterpriseId") String enterpriseId, @Param("departmentId") String departmentId, @Param("positionId") String positionId, @Param("inviterId") String inviterId, @Param("inviteeId") String inviteeId);//查询未处理的相同外部邀请数量(判断是否已发出邀请,且尚未被处理)
	
	@Query("select count(iter.id)"
			+ " from InviteToEnterpriseRecord iter"
			+ " where iter.status = 2"
			+ " and iter.enterpriseId=:enterpriseId"
			+ " and iter.departmentId=:departmentId"
			+ " and iter.positionId=:positionId"
			+ " and iter.inviterId=:inviterId"
			+ " and iter.inviteeUsername=:inviteeUsername"
			+ " and iter.inviteFrom='outer'")
	int countSameOuterRecordWithUsername(@Param("enterpriseId") String enterpriseId, @Param("departmentId") String departmentId, @Param("positionId") String positionId, @Param("inviterId") String inviterId, @Param("inviteeUsername") String inviteeUsername);//查询未处理的相同外部邀请数量(判断是否已发出邀请,且尚未被处理)
	
}

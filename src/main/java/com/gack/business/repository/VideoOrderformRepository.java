package com.gack.business.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.User;
import com.gack.business.model.VideoOrderform;
import com.gack.business.model.VideoUser;

/**
 * 
* @ClassName: VideoOrderformRepository.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月21日
*  
 */
public interface VideoOrderformRepository extends JpaRepository<VideoOrderform, String>,JpaSpecificationExecutor<VideoOrderform>{
	public Page<VideoOrderform> findAll(Pageable pageable);
	public Page<VideoOrderform> findByUserAndOrderStatusIsNot(User user,String orderStatus,Pageable pageable);
	
	//获取用户预约中的门店订单
	public List<VideoOrderform> findByUserAndOrderStatus(User user,String status);
	
	//通过用户id获取用户可用订单
	public List<VideoOrderform> findByUserAndOrderStatusIsNotAndOrderStatusIsNot(User user, String status1 , String status2);
	
	//获取所有预约中的门店订单
	public List<VideoOrderform> findByOrderStatus(String status);
}

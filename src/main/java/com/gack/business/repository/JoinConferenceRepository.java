package com.gack.business.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.query.Param;

import com.gack.business.model.JoinConference;

public interface JoinConferenceRepository extends JpaRepository<JoinConference, String>, JpaSpecificationExecutor<JoinConference> {
	JoinConference findByCidAndUid(@Param("cid") String cid, @Param("uid") String uid);
	List<JoinConference> findByUid(@Param("uid") String uid);
	List<JoinConference> findByIsJoin(@Param("isJoin") Integer isJoin);

}

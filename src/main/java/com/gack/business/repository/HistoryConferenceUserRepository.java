package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.business.model.HistoryConferenceUser;

public interface HistoryConferenceUserRepository extends JpaRepository<HistoryConferenceUser, String>, JpaSpecificationExecutor<HistoryConferenceUser> {

}

package com.gack.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.gack.business.model.DownloadUrl;

/**
 * 
 * @author ws
 * 2018-6-27
 */
@Repository
public interface DownloadUrlRepository extends JpaRepository<DownloadUrl, String>, JpaSpecificationExecutor<DownloadUrl>{

}

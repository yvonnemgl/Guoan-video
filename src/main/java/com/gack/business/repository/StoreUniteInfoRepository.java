package com.gack.business.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.gack.business.model.StoreUniteInfo;

/**
 * 
* @ClassName: StoreUniteInfoRepository 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月7日 下午5:22:09 
* @version V1.0
 */
public interface StoreUniteInfoRepository extends JpaRepository<StoreUniteInfo, String>{
	@Query("select s from StoreUniteInfo s where s.uniteKey = :uniteKey")
	public StoreUniteInfo findUniteInfoByKey(@Param("uniteKey") String uniteKey);
	
	@Query("select COUNT(s.id) from StoreUniteInfo s where s.uniteKey = :uniteKey")
	public Integer findUniteInfoKeyCount(@Param("uniteKey") String uniteKey);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM StoreUniteInfo s where s.id = :id")
	public Integer dealUniteInfoById(@Param("id")String id);
	
	@Transactional
	@Modifying
	@Query("DELETE FROM StoreUniteInfo s where s.uniteKey = :uniteKey")
	public Integer dealUniteInfoByKey(@Param("uniteKey")String uniteKey);
	
	Page<StoreUniteInfo> findAll(Pageable pageable);

}

package com.gack.business.asyncTest;

import javax.annotation.Resource;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gack.business.model.UserLoginRecord;
import com.gack.business.repository.UserLoginRecordRepository;

@Component
public class AsyncUser {

	@Resource
	private UserLoginRecordRepository userLoginRepository;
	
	@Async("myExecutor")
	@Transactional
	public void saveUserLogin(UserLoginRecord userLoginRecord){
		System.out.println("ThreadName2====" + Thread.currentThread().getName());
		userLoginRepository.save(userLoginRecord);
	}
	
}

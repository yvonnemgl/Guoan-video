package com.gack.business.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import io.jsonwebtoken.lang.Collections;

/**
 * 
 * @author ws
 * 2018-8-17
 */
@Repository
public class UserEnterprisePermissionGroupDao {

	@Autowired
	private EntityManager em;
	
	/**
	 * 删除某些用户在某个公司的权限组
	 * @param userIdList 用户id集合
	 * @param enterpriseId 公司id
	 * @return
	 */
	public int deleteByUserIdListAndEnterpriseId(List<String> userIdList, String enterpriseId){
		
		if(Collections.isEmpty(userIdList)){
			return 0;
		}
		
		String jpql = "delete from UserEnterprisePermissionGroup where enterpriseId=:enterpriseId and userId in ";
		
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for(String t : userIdList){
			 sb.append(" '" + t + "',");
		}
		sb = new StringBuilder(sb.substring(0, sb.length()-1));//去掉最后的","
		sb.append(")");
		
		jpql += sb.toString();
		
		Query query = em.createQuery(jpql);
		query.setParameter("enterpriseId", enterpriseId);
		return query.executeUpdate();
	}
	
}

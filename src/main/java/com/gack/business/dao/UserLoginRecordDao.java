package com.gack.business.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 
* @ClassName: UserLoginRecordDao 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月28日 下午4:07:56 
* @version V1.0
 */
@Repository
public class UserLoginRecordDao {
	@Autowired
	private EntityManager entityManager;
	
	/**
	 * 删除超时pc登录记录
	 */
	@Transactional
	public void dealTimeoutBreatHeart(Integer timeout){
		timeout = timeout == null ? 10 : timeout;
		String jpql = "delete"
					+ " from useloginrecord"
					+ " where"
						+ " login_resource = 'pc'"
						+ " and IFNULL((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(login_time)),0) > :timeout";
		Query query = entityManager.createNativeQuery(jpql);
		query.setParameter("timeout", timeout);
		query.executeUpdate();
	}
	
	/**
	 * 修改pc登录时间为当前时间
	 */
	@Transactional
	public void updatePCLoginTime(String userId) {
		String jpql = "UPDATE useloginrecord SET login_time = NOW() WHERE login_resource = 'pc' AND username = :username";
		Query query = entityManager.createNativeQuery(jpql);
		query.setParameter("username", userId);
		query.executeUpdate();
	}
}

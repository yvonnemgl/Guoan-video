package com.gack.business.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import io.jsonwebtoken.lang.Collections;

/**
 * 
 * @author ws
 * 2018-6-1
 */
@Repository
@Transactional
public class DepartmentDao {

	@PersistenceContext
	private EntityManager em;
	
	/**
	 * 将某些部门的人数加一
	 * @param departmentIdList
	 * @return
	 */
	public int addDepartmentNumber(List<String> departmentIdList){
		
		if(Collections.isEmpty(departmentIdList)){
			return 0;
		}
		
		String jpql = "update Department d set d.number=d.number+1 where d.id in ";
		
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for(String t : departmentIdList){
			 sb.append(" '" + t + "',");
		}
		sb = new StringBuilder(sb.substring(0, sb.length()-1));//去掉最后的","
		sb.append(")");
		
		jpql += sb.toString();
		
		Query query = em.createQuery(jpql);
		return query.executeUpdate();
	}
	
	/**
	 * 将某些部门的人数减一
	 * @param departmentIdList
	 * @return
	 */
	public int minusDepartmentNumber(List<String> departmentIdList){
		
		if(Collections.isEmpty(departmentIdList)){
			return 0;
		}
		
		String jpql = "update Department d set d.number=d.number-1 where d.id in ";
		
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for(String t : departmentIdList){
			 sb.append(" '" + t + "',");
		}
		sb = new StringBuilder(sb.substring(0, sb.length()-1));//去掉最后的","
		sb.append(")");
		
		jpql += sb.toString();
		
		Query query = em.createQuery(jpql);
		return query.executeUpdate();
	}
	
	/**
	 * 将某些部门的人数减n
	 * @param departmentIdList
	 * @return
	 */
	public int minusNDepartmentNumber(List<String> departmentIdList, Integer num){
		
		if(Collections.isEmpty(departmentIdList)){
			return 0;
		}
		
		String jpql = "update Department d set d.number=d.number-:num where d.id in ";
		
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for(String t : departmentIdList){
			 sb.append(" '" + t + "',");
		}
		sb = new StringBuilder(sb.substring(0, sb.length()-1));//去掉最后的","
		sb.append(")");
		
		jpql += sb.toString();
		
		Query query = em.createQuery(jpql);
		query.setParameter("num", num);
		return query.executeUpdate();
	}
	
	/**
	 * 删除某些部门
	 * @param departmentIdList
	 * @return
	 */
	public int deleteDepartment(List<String> departmentIdList){
		
		if(Collections.isEmpty(departmentIdList)){
			return 0;
		}
		
		String jpql = "update Department d set d.status=0, d.deleteTime=:date where d.id in";
		
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for(String t : departmentIdList){
			 sb.append(" '" + t + "',");
		}
		sb = new StringBuilder(sb.substring(0, sb.length()-1));//去掉最后的","
		sb.append(")");
		
		jpql += sb.toString();
		
		Query query = em.createQuery(jpql);
		query.setParameter("date", new Date());
		return query.executeUpdate();
	}

	/**
	 * 查询部门id对应的部门名称
	 * @param departmentIdList 部门id集合
	 * @return
	 */
	public List<Object[]> findDepartmentIdAndName(List<String> departmentIdList){
		
		if(Collections.isEmpty(departmentIdList)){
			return new ArrayList<Object[]>();
		}
		
		String jpql = "select d.id, d.name"
				+ " from Department d"
				+ " where d.id in ";
		
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for(String t : departmentIdList){
			 sb.append(" '" + t + "',");
		}
		sb = new StringBuilder(sb.substring(0, sb.length()-1));//去掉最后的","
		sb.append(")");
		
		jpql += sb.toString();
		
		Query query = em.createQuery(jpql);
		List<Object[]> list = query.getResultList();
		
		return list;
	}
	
	/**
	 * 查询某些部门的人数
	 * @param departmentIdList 部门id集合
	 * @return
	 */
	public int findUserNum(List<String> departmentIdList){
		
		if(Collections.isEmpty(departmentIdList)){
			return 0;
		}
		
		String jpql = "select count(uedp.userId)"
				+ " from UserEnterpriseDepartmentPosition uedp"
				+ " where uedp.departmentId in ";
		
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		for(String t : departmentIdList){
			 sb.append(" '" + t + "',");
		}
		
		sb = new StringBuilder(sb.substring(0, sb.length()-1));//去掉最后的","
		sb.append(")");
		
		jpql += sb.toString();
		
		Query query = em.createQuery(jpql);
		long num = (long)query.getSingleResult();
		
		return (int)num;
	}
	
}

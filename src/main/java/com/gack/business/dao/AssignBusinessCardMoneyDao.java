package com.gack.business.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gack.business.controller.AssignBusinessCardMoneyController;

/**
 * 
 * @author ws
 * 2018-6-20
 */
@Repository
public class AssignBusinessCardMoneyDao {

	@Autowired
	private EntityManager em;
	
	/**
	 * 根据公司id,获取某公司的一级部门的名称以及剩余金额
	 * @param enterpriseId 公司id
	 * @return topDepartmentList:一级部门的名称以及剩余金额实体集合
	 */ 
	public List<AssignBusinessCardMoneyController.TopDepartment> findTopDepartmentList(String enterpriseId){
		String jpql = "select new com.gack.business.controller.AssignBusinessCardMoneyController$TopDepartment("
				+ " d.id, d.name, d.surplus_amount)"
				+ " from Department d"
				+ " where d.enterpriseId=:enterpriseId"
				+ " and d.parentId='0'"
				+ " and d.status=1";
		
		Query query = em.createQuery(jpql, AssignBusinessCardMoneyController.TopDepartment.class);
		query.setParameter("enterpriseId", enterpriseId);
		
		return query.getResultList();
	}
	
	/**
	 * 查询某公司下是否有此一级部门
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @return
	 */
	public long isTopDepartmentInEnterprise(String enterpriseId, String departmentId){
		String  jpql = "select count(d.id)"
				+ " from Department d"
				+ " where d.enterpriseId=:enterpriseId"
				+ " and d.parentId='0'"
				+ " and d.status=1"
				+ " and d.id=:departmentId";
		
		Query query = em.createQuery(jpql);
		query.setParameter("enterpriseId", enterpriseId);
		query.setParameter("departmentId", departmentId);
		if(query.getResultList().size() == 0){
			return 0;
		}
		
		return (long)query.getSingleResult();
	}
	
	/**
	 * 根据公司id和部门id获取该部门可调配金额的最大值(即公司剩余可用额度+部门剩余可用额度)
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @return
	 */
	public List<Object[]> findMaxCouldAssignedMoney(String enterpriseId, String departmentId){
		String jpql = "select ifnull((select e.surplus_amount"
								+ " from enterprise e"
								+ " where e.id=:enterpriseId"
								+ " and e.status!='4'), 0), ifnull((select d.surplus_amount"
													+ " from department d"
													+ " where d.enterprise_id=:enterpriseId"
													+ " and d.id=:departmentId"
													+ " and d.parent_id='0'"
													+ " and d.status=1), 0)";
		
		Query query = em.createNativeQuery(jpql);
		query.setParameter("enterpriseId", enterpriseId);
		query.setParameter("departmentId", departmentId);
		
		return (List<Object[]>)query.getResultList();
	}
	
	/**
	 * 设置某公司的某一级部门的剩余可用额度(在更新数据库前,需判定公司可用金额以及部门可用金额之和是否大于等于设定金额)
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @param money 金额(分)
	 * @return
	 */
	public int setDepartmentSurplusAmount(String enterpriseId, String departmentId, Integer money){
		String jpql = "update Department set surplus_amount=:money"
				+ " where enterpriseId=:enterpriseId"
				+ " and id=:departmentId"
				+ " and parentId='0'"
				+ " and status=1";
		
		Query query = em.createQuery(jpql);
		query.setParameter("enterpriseId", enterpriseId);
		query.setParameter("departmentId", departmentId);
		query.setParameter("money", money);
		
		return query.executeUpdate();
	}
	
	/**
	 * 设置公司的剩余可用额度
	 * @param enterpriseId 公司id
	 * @param money 金额(分)
	 * @return
	 */
	public int setEnterpriseSurplusAmount(String enterpriseId, Integer money){
		String jpql = "update Enterprise set surplusAmount=:money"
				+ " where id=:enterpriseId"
				+ " and status != '4'";
		
		Query query = em.createQuery(jpql);
		query.setParameter("enterpriseId", enterpriseId);
		query.setParameter("money", money);
		
		return query.executeUpdate();
	}
	
}

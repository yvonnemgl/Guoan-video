package com.gack.business.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gack.business.model.StoreSupportFacity;
import com.gack.business.model.StoreSupportService;

/**
 * 
* @ClassName: StoreSupportFacityAndSupportServiceDao 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年9月19日 下午3:59:42 
* @version V1.0
 */
@Repository
public class StoreSupportFacityAndSupportServiceDao {

	@Autowired
	private EntityManager entityManager;
	
	// 根据门店id查询门店配套设施
	@SuppressWarnings("unchecked")
	public List<StoreSupportFacity> facities(String storeId) {
		List<StoreSupportFacity> facities = null;
		String jpql = "SELECT * from("
						+ " SELECT "
						+ " suf.id as id,"
						+ " suf.`name` as name,"
						+ " suf.createtime as createtime,"
						+ " suf.is_use as is_use,"
						+ " suf.icon as icon,"
						+ " suf.use_template as use_template"
					+ " FROM store_support_facity suf"
					+ " WHERE 1=1 AND suf.use_template = 0 AND suf.is_use <> 2"
					+ " UNION"
					+ " SELECT"
						+ " suf2.id as id,"
						+ " suf2.`name` as name,"
						+ " suf2.createtime as createtime,"
						+ " suf2.is_use as is_use,"
						+ " ssfi.icon as icon,"
						+ " suf2.use_template as use_template"
					+ " FROM store_support_facity suf2"
					+ " LEFT JOIN store_support_facity_icon ssfi"
					+ " ON suf2.icon = ssfi.id"
					+ " WHERE 1=1 and suf2.use_template = 1 AND suf2.is_use <> 2) AS f"
				+ " WHERE f.id in (SELECT facityid FROM store_and_facity WHERE storeid = :storeId)";
		Query query = entityManager.createNativeQuery(jpql, StoreSupportFacity.class);
		query.setParameter("storeId", storeId);
		facities = query.getResultList();
		return facities;
	}
	
	// 根据门店id获取门店配套服务 包括icon
	@SuppressWarnings("unchecked")
	public List<StoreSupportService> services(String storeId) {
		List<StoreSupportService> services = null;
		String jpql = "SELECT * from("
						+ " SELECT "
						+ " sus.id as id,"
						+ " sus.`name`,"
						+ " sus.createtime,"
						+ " sus.is_use,"
						+ " sus.icon,"
						+ " sus.use_template"
					+ " FROM store_support_service sus"
					+ " WHERE 1=1 AND sus.use_template = 0 AND sus.is_use <> 2"
					+ " UNION"
					+ " SELECT"
						+ " sus2.id as id,"
						+ " sus2.`name`,"
						+ " sus2.createtime,"
						+ " sus2.is_use,"
						+ " sssi.icon,"
						+ " sus2.use_template"
					+ " FROM store_support_service sus2"
					+ " LEFT JOIN store_support_service_icon sssi"
					+ " ON sus2.icon = sssi.id"
					+ " WHERE 1=1 and sus2.use_template = 1 AND sus2.is_use <> 2) AS s"
				+ " WHERE s.id in (SELECT serviceid FROM store_and_service WHERE storeid = :storeId)";
		Query query = entityManager.createNativeQuery(jpql, StoreSupportService.class);
		query.setParameter("storeId", storeId);
		services = query.getResultList();
		return services;
	}
}

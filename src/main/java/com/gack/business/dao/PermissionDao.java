package com.gack.business.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gack.business.model.Permission;

import io.jsonwebtoken.lang.Collections;

/**
 * 
 * @author ws
 * 2018-8-3
 */
@Repository
public class PermissionDao {

	@Autowired
	private EntityManager em;
	
	/**
	 * 根据权限组id获取对应的permissionId(不可重复)
	 * @param roleGroupIds 权限组id(A权限组id;B权限组id;C权限组id)
	 * @return
	 */
	public List<String> findPermissionIdByRoleGroupIds(String roleGroupIds){
		
		if(StringUtils.isBlank(roleGroupIds)){
			return new ArrayList<String>();
		}
		
		String[] roleGroupIdArray = roleGroupIds.split(";");
		if(roleGroupIdArray.length == 0){
			return new ArrayList<String>();
		}
		
		String jpql = "select permissionIds"
				+ " RoleGroup"
				+ " where id in ";
		
		StringBuilder sb = new StringBuilder("(");
		for(String t : roleGroupIdArray){
			sb.append("'" + t + "', ");
		}
		sb = new StringBuilder(sb.substring(0, sb.length() - ", ".length()));
		sb.append(")");
		jpql += sb.toString();
		
		Query query = em.createQuery(jpql);
		List<String> permissionIdsList = query.getResultList();
		Set<String> permissionIdSet = new HashSet<>();
		for(String t : permissionIdsList){
			if(t != null){
				permissionIdSet.addAll(Arrays.asList(t.split(";")) );
			}
		}
		
		List<String> permissionIdList = new ArrayList<>(permissionIdSet);
		return permissionIdList;
	}
	
	/**
	 * 根据基本权限id集合查找对应的英文描述集合
	 * @param permissionIdList 基本权限id集合
	 * @return
	 */
	public List<String> findDescriptionByPermissionIdList(List<String> permissionIdList){
		
		if(Collections.isEmpty(permissionIdList)){
			return new ArrayList<>();
		}
		
		String jpql = "select description"
				+ " from Permission"
				+ " where id in ";
		
		StringBuilder sb = new StringBuilder("(");
		for(String t : permissionIdList){
			sb.append("'" + t + "', ");
		}
		sb = new StringBuilder(sb.substring(0, sb.length() - ", ".length()));
		sb.append(")");
		jpql += sb.toString();
		
		Query query = em.createQuery(jpql);
		List<String> descriptionList = query.getResultList();
		return descriptionList;
	}
	
	/**
	 * 根据基本权限id,查找对应的基本权限对象集合
	 * @param permissionIdList 基本权限id
	 * @return
	 */
	public List<Permission> findPermission(List<String> permissionIdList){
		
		if(Collections.isEmpty(permissionIdList)){
			return new ArrayList<>();
		}
		
		String jpql = "select *"
				+ " from Permission"
				+ " where id in ";
		
		StringBuilder sb = new StringBuilder("(");
		for(String t : permissionIdList){
			sb.append("'" + t + "', ");
		}
		sb = new StringBuilder(sb.substring(0, sb.length() - ", ".length()));
		sb.append(")");
		jpql += sb.toString();
		
		Query query = em.createQuery(jpql);
		List<Permission> permissionList = query.getResultList();
		return permissionList;
	}
	
}

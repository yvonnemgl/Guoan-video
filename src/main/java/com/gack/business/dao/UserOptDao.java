package com.gack.business.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.gack.business.vo.UserOptVo;

/**
 * 
* @ClassName: UserOptDao 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月1日 下午4:26:12 
* @version V1.0
 */
@Repository
public class UserOptDao {
	@Autowired
	private EntityManager entityManager;
	
	/***
	private String nickname; //昵称
	private String username; //用户名
	private BigDecimal expense_amount; //累计消费金额
	private Integer create_office_num; //创建共享办公次数
	private Integer join_video_num; //参加视频会议次数
	private boolean is_check; //是否认证
	private boolean status; //账号状态  
	private Date createtime; //注册时间
	*/
	//获取所有用户
	public List<UserOptVo> getUsers(Integer currentPage,Integer pageSize){
		List<UserOptVo> users = null;
		String jpql = "select new com.gack.business.vo.UserOptVo(u.id,u.nickname,u.username,u.expense_amount,u.create_office_num"
				+",u.join_video_num,u.is_check,u.status,u.createtime) from User u ORDER BY u.createtime DESC";
		Query query = entityManager.createQuery(jpql);
		users = query.setFirstResult(currentPage*pageSize).setMaxResults(pageSize).getResultList();
		return users;
	}
	
	//通过nickname获取用户
	public List<UserOptVo> getUsersByNickname(String nickname,Integer currentPage,Integer pageSize){
		List<UserOptVo> users = null;
		String jpql = "select new com.gack.business.vo.UserOptVo(u.id,u.nickname,u.username,u.expense_amount,u.create_office_num"
				+",u.join_video_num,u.is_check,u.status,u.createtime) from User u where u.nickname =:nickname ORDER BY u.createtime DESC";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("nickname", nickname);
		users = query.setFirstResult(currentPage*pageSize).setMaxResults(pageSize).getResultList();
		return users;
	}
	
	//通过订单号码获取用户
	public List<UserOptVo> getUserByOrderformId(String orderId,Integer currentPage,Integer pageSize){
		List<UserOptVo> users = null;
		String jpql = "select new com.gack.business.vo.UserOptVo(u.id,u.nickname,u.username,u.expense_amount,u.create_office_num"
				+",u.join_video_num,u.is_check,u.status,u.createtime) from VideoOrderform o LEFT JOIN User u on o.userid = u.id where o.id =:orderId ORDER BY u.createtime DESC";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("orderId", orderId);
		users = query.setFirstResult(currentPage*pageSize).setMaxResults(pageSize).getResultList();
		return users;
	}
	
	// 通过消费金额查询用户
	public List<UserOptVo> getUserByExpenseAmount(BigDecimal minAmount,BigDecimal maxAmount,Integer currentPage,Integer pageSize){
		List<UserOptVo> users = null;
		String jpql = "select new com.gack.business.vo.UserOptVo(u.id,u.nickname,u.username,u.expense_amount,u.create_office_num"
				+",u.join_video_num,u.is_check,u.status,u.createtime) from User u "
				+" where u.expense_amount between(:minAmount,:maxAmount)"
				+" ORDER BY u.createtime DESC";
		Query query = entityManager.createQuery(jpql);
		query.setParameter("minAmount", minAmount);
		query.setParameter("maxAmount", maxAmount);
		users = query.setFirstResult(currentPage*pageSize).setMaxResults(pageSize).getResultList();
		return users;
	}
}

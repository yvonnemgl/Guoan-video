package com.gack.business.dao;

import java.math.BigInteger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 
* @ClassName: EnterpriseInfoDao 
* @Description: TODO(公司详情  dao) 
* @author (ZhangXinYu)  
* @date 2018年5月31日 下午2:27:46 
* @version V1.0
 */
@Repository
public class EnterpriseInfoDao {
	@Autowired
	private EntityManager entityManager;
	
//	//验证用户是否有权限去修改公司名称   既用户是否有超级管理员权限
//	public boolean validateIsCanEnterpriseSuperAdmin(String userid,String enterpriseid){
//		boolean flag = false;
//		String jpql = "select ue.id from UserEnterpriseDepartmentRole as ue where ue.roleId in (select id from Role where level = 1)"
//				+" AND ue.userId =:userid and ue.enterpriseId =:enterpriseid";
//		Query query = entityManager.createQuery(jpql);
//		query.setParameter("userid", userid);
//		query.setParameter("enterpriseid", enterpriseid);
//		if(query.getResultList().size() > 0){
//			flag = true;
//		}
//		return flag;
//	}
	
	public BigInteger getSameCertifiedEnterpriseNameCount(String ename){
		BigInteger count = new BigInteger("0");
		String jpql = "SELECT COUNT(e3.name) from (" 
						+ " SELECT e1.name as name from enterprise e1 where e1.status = '1'"
						+ " UNION ALL"
						+ " SELECT e2.certified_name as name from enterprise e2 where e2.status = '2') e3"
						+ " WHERE e3.name = :ename";
		Query query = entityManager.createNativeQuery(jpql);
		query.setParameter("ename", ename);
		count = (BigInteger)query.getSingleResult();
		return count;
	}
	
}

package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.StoreCameraServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: StoreCameraController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年7月11日 下午2:51:46 
* @version V1.0
 */
@RestController
@RequestMapping(ApiController.CAMERA_URL)
public class StoreCameraController {
	
	@Autowired
	private StoreCameraServiceInterface cameraService;
	
	@ApiOperation(value="根据用户id查询使用中门店的摄像头",notes="根据用户id查询使用中门店的摄像头")
	@PostMapping("getCamerasByUser")
	public AjaxJson getCamerasByUser(String userid){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setData(cameraService.getCamerasByUser(userid));
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="根据用户id查询使用中门店的摄像头",notes="根据用户id查询使用中门店的摄像头")
	@PostMapping("getStoreCameras")
	public AjaxJson getStoreCameras(String storeid){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setData(cameraService.getStoreCameras(storeid));
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="查询摄像头使用状态",notes="查询摄像头使用状态")
	@PostMapping("getCameraStatus")
	public AjaxJson getCameraStatus(String userid){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setData(cameraService.getCameraStatus(userid));
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败");
		}
		return ajaxJson;
	}
}

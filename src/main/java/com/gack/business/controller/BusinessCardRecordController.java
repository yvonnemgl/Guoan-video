package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gack.business.service.BusinessCardRecordServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

@RestController
@RequestMapping(ApiController.CARD)
public class BusinessCardRecordController {

	@Autowired
	BusinessCardRecordServiceInterface cardRecordServiceInterface;
	/**
	 * 
	 * @Title: findEnterprisesIdsByUserId
	 * @Description: 根据用户ID获取用户所属公司ID
	 * @param userId 用户ID
	 * @return AjaxJson 
	 * @throws
	 */
	@GetMapping("/user")
	public AjaxJson findEnterprisesIdsByUserId(String userId){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(cardRecordServiceInterface.findEnterprisesIdsByUserId(userId));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("ERROR");
		}
		return ajaxJson;
	}
	
	
	
	/**
	 * 
	 * @Title: save 
	 * @Description: 保存商务卡订单 
	 * @param uid 用户ID 
	 * @param cid 商品ID 
	 * @param eid 公司ID 
	 * @param number 商品数量 
	 * @param channel 支付渠道 
	 * @param back_url 支付同步回调地址(支付宝PC/快钱PC) 
	 * @return AjaxJson 
	 * @throws
	 */
	@PostMapping
	public AjaxJson save(String uid, String cid, String eid, int number, String channel, String back_url) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(cardRecordServiceInterface.save(uid, cid, eid, number, channel, back_url));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("ERROR");
		}
		return ajaxJson;
	}

	/**
	 * 
	 * @Title: update 
	 * @Description: 支付系统回调 
	 * @param oid 
	 * @return AjaxJson 
	 * @throws
	 */
	@GetMapping("/updateState")
	public String update(String oid) {
		return cardRecordServiceInterface.updateByOid(oid) ? "SUCCESS" : "FAIL";

	}

}

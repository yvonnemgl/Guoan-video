package com.gack.business.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gack.helper.common.util.JDeviceRestAPI;
import com.gack.helper.common.util.JPushRestAPI;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: JPushTestController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年8月24日 下午1:50:15 
* @version V1.0
 */
@RestController
@RequestMapping("JPushTest")
public class JPushTestController {

	@ApiOperation(value="未指定别名推送信息",notes="为指定别名设备推送信息")
	@PostMapping("push")
	public void pushInfo(String alias,
			@RequestParam(value="content",defaultValue="测试-为指定别名设备推送") String content){
		JPushRestAPI jPushRestAPI = new JPushRestAPI();
		jPushRestAPI.setApns_production(false);
		jPushRestAPI.SendPush(content, null, null, "", jPushRestAPI.jsonDataByOneAlias(alias));
	}
	
	@ApiOperation(value="移除指定别名",notes="移除指定别名")
	@PostMapping("removeAlias")
	public void removeAlias(String alias){
		JDeviceRestAPI jDeviceRestAPI = new JDeviceRestAPI();
   	 	jDeviceRestAPI.setApns_production(false);
   	 	jDeviceRestAPI.removeAlias(alias);
	}
	
	@ApiOperation(value="移除别名后推送",notes="移除别名后推送")
	@PostMapping("removeAliasAndPush")
	public void removeAliasAndPush(String alias,String content){
   	 	
   	 	JPushRestAPI jPushRestAPI = new JPushRestAPI();
		jPushRestAPI.setApns_production(false);
		jPushRestAPI.SendPush(content, null, null, "", jPushRestAPI.jsonDataByOneAlias(alias));

		JDeviceRestAPI jDeviceRestAPI = new JDeviceRestAPI();
		jDeviceRestAPI.setApns_production(false);
		jDeviceRestAPI.removeAlias(alias);
	}
}

package com.gack.business.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.InviteToEnterpriseServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-8-20
 */
@RestController
@RequestMapping(ApiController.INVITE_TO_ENTERPRISE_URL)
public class InviteToEnterpriseController {

	@Autowired
	private InviteToEnterpriseServiceInterface inviteToEnterpriseService;
	
	/**
	 * 好友邀请列表
	 * @param inviterId 操作者id
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "好友邀请列表")
	@PostMapping("friendList")
	public AjaxJson friendListForInvitation(String inviterId, String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();

		if(StringUtils.isBlank(inviterId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(inviteToEnterpriseService.friendListForInvitation(inviterId.trim(), enterpriseId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 通过好友邀请渠道,发送邀请 
	 * @param inviterId 邀请者id
	 * @param enterpriseId 公司id
	 * @param inviteeUserIds 被邀请者id(id1;id2;id3)
	 * @return
	 */
	@ApiOperation(value = "通过好友邀请渠道,发送邀请")
	@PostMapping("inviteByFriend")
	public AjaxJson inviteByFriend(String inviterId, String enterpriseId, String inviteeUserIds){
		AjaxJson ajaxJson = new AjaxJson();

		if(StringUtils.isBlank(inviterId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(inviteeUserIds)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("选择为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(inviteToEnterpriseService.inviteByFriend(inviterId.trim(), enterpriseId.trim(), inviteeUserIds.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * ①获取已加入公司的人员手机号②获取已被该用户邀请加入该公司的手机号
	 * @param inviterId 邀请者id
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "标记通讯录属性")
	@PostMapping("markContactsAttribute")
	public AjaxJson markContactsAttribute(String inviterId, String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();

		if(StringUtils.isBlank(inviterId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(inviteToEnterpriseService.markContactsAttribute(inviterId.trim(), enterpriseId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 通过通讯录邀请渠道,发送邀请 
	 * @param inviterId 邀请者id
	 * @param enterpriseId 公司id
	 * @param inviteeUsernames 被邀请者手机号(手机号1;手机号2;手机号3)
	 * @return
	 */
	@ApiOperation(value = "通过通讯录邀请渠道,发送邀请")
	@PostMapping("inviteByTelList")
	public AjaxJson inviteByTelList(String inviterId, String enterpriseId, String inviteeUsernames){
		AjaxJson ajaxJson = new AjaxJson();

		if(StringUtils.isBlank(inviterId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(inviteeUsernames)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("选择为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(inviteToEnterpriseService.inviteByTelList(inviterId.trim(), enterpriseId.trim(), inviteeUsernames.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 通过手机号邀请渠道,发送邀请 
	 * @param inviterId 邀请者id
	 * @param enterpriseId 公司id
	 * @param inviteeUsername 被邀请者手机号
	 * @param remarks 备注
	 * @return
	 */
	@ApiOperation(value = "通过手机号邀请渠道,发送邀请 ")
	@PostMapping("inviteByPhoneNumber")
	public AjaxJson inviteByPhoneNumber(String inviterId, String enterpriseId, String inviteeUsername, String remarks){
		AjaxJson ajaxJson = new AjaxJson();

		if(StringUtils.isBlank(inviterId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(inviteeUsername)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("选择为空").setData(null);
		}
		if(StringUtils.isBlank(remarks)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("姓名为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(inviteToEnterpriseService.inviteByPhoneNumber(inviterId.trim(), enterpriseId.trim(), inviteeUsername.trim(), remarks.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
}

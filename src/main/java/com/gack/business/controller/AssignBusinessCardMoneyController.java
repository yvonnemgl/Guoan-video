package com.gack.business.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.AssignBusinessCardMoneyServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-6-19
 */
@RestController
@RequestMapping(ApiController.ASSIGN_BUSINESS_CARD_MONEY_URL)
public class AssignBusinessCardMoneyController {

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	public static class TopDepartment{
		private String id;
		private String name;
		private Integer surplus_amount;	//部门剩余可用额度(分）
	}
	
	@Autowired
	private AssignBusinessCardMoneyServiceInterface assignBusinessCardMoneyService;
	
	/**
	 * 根据公司id,查询公司的一级部门名称以及部门剩余可用额度
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "查询公司的一级部门名称以及部门剩余可用额度", notes = "查询公司的一级部门名称以及部门剩余可用额度")
	@PostMapping("findTopDepartmentList")
	public AjaxJson findTopDepartmentList(String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(enterpriseId == null || enterpriseId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(assignBusinessCardMoneyService.findTopDepartmentList(enterpriseId));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 根据公司id,查询公司的一级部门名称以及部门剩余可用额度(剩余可用额度大于0的一级部门)
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "查询公司的一级部门名称以及部门剩余可用额度(剩余可用额度大于0的一级部门)", notes = "查询公司的一级部门名称以及部门剩余可用额度(剩余可用额度大于0的一级部门)")
	@PostMapping("findAssignedTopDepartmentList")
	public AjaxJson findAssignedTopDepartmentList(String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(enterpriseId == null || enterpriseId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(assignBusinessCardMoneyService.findAssignedTopDepartmentList(enterpriseId));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 根据公司id和部门id获取该部门可调配金额的最大值(即公司剩余可用额度+部门剩余可用额度)
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @return
	 */
	@ApiOperation(value = "根据公司id和部门id获取该部门可调配金额的最大值", notes = "根据公司id和部门id获取该部门可调配金额的最大值")
	@PostMapping("findMaxCouldAssignedMoney")
	public AjaxJson findMaxCouldAssignedMoney(String enterpriseId, String departmentId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(enterpriseId == null || enterpriseId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(departmentId == null || departmentId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("部门为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(assignBusinessCardMoneyService.findMaxCouldAssignedMoney(enterpriseId, departmentId));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 设置某公司的某一级部门的剩余可用额度(在更新数据库前,需判定公司可用金额以及部门可用金额之和是否大于等于设定金额)
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @param money 金额(分)
	 * @param userId 操作者id
	 * @return
	 */
	@ApiOperation(value = "设置某公司的某一级部门的剩余可用额度", notes = "设置某公司的某一级部门的剩余可用额度")
	@PostMapping("setDepartmentAssignedMoney")
	public AjaxJson setDepartmentAssignedMoney(String enterpriseId, String departmentId, String money, String userId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(enterpriseId == null || enterpriseId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(departmentId == null || departmentId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("部门为空").setData(null);
		}
		if(money == null || money.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("金额为空").setData(null);
		}
		if(userId == null || userId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(assignBusinessCardMoneyService.setDepartmentSurplusAmount(enterpriseId, departmentId, money));
		}catch(Exception e){
			if(e.getMessage().contains("---msg:")){			
				Map<String, Object> map = new HashMap<>();
				map.put("status", "error");
				map.put("msg", "操作失败");
				return ajaxJson.setSuccess(true).setStatus(200).setData(map);
			}
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
}

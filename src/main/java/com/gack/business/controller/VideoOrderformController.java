package com.gack.business.controller;

import java.util.concurrent.Semaphore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.VideoOrderformServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: VideoOrderformController.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月21日
*  
 */
@RestController
@RequestMapping(ApiController.ORDERFORM_URL)
public class VideoOrderformController {
	
	Semaphore semaphore=new Semaphore(1);  //定义资源的总数量
	
	@Autowired
	private VideoOrderformServiceInterface orderformService;
	
	@ApiOperation(value="门店预约",notes="门店预约")
	@PostMapping("reservateStore")
	public AjaxJson reservateStore(String userid,String storeid,
			@RequestParam(value="operation",defaultValue="iosORpc") String operation){
		AjaxJson ajaxJson = new AjaxJson();
		try {
//			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderformService.reservateStore(userid,storeid));
			try {
	            semaphore.acquire();  //请求占用一个资源
	            ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderformService.reservateStore(userid,storeid,operation));
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	            ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
	        }finally{
	            semaphore.release();//释放一个资源
	        }
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="取消预约",notes="取消预约")
	@PostMapping("cancelReservation")
	public AjaxJson cancelReservation(String id,String reason){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderformService.cancelReservation(id,reason.trim()));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="开始使用订单",notes="开始使用订单")
	@PostMapping("startToUseOrderform")
	public AjaxJson startToUseOrderform(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderformService.startToUseOrderform(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="通过用户id开始使用订单",notes="通过用户id开始使用订单")
	@PostMapping("startToUseOrderformByUserid")
	public AjaxJson startToUseOrderformByUserid(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderformService.startToUseOrderformByUserid(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="结束订单",notes="结束订单")
	@PostMapping("endToUseOrderform")
	public AjaxJson endToUseOrderform(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderformService.endToUseOrderform(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="通过用户id结束订单",notes="通过用户id结束订单")
	@PostMapping("endToUseOrderformByUserid")
	public AjaxJson endToUseOrderformByUserid(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderformService.endToUseOrderformByUserid(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="获取订单列表",notes="获取订单列表")
	@PostMapping("findAllOrderform")
	public AjaxJson findAllOrderform(String id,Integer currentPage,Integer pageSize){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderformService.findAllOrderform(id,currentPage,pageSize));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="获取订单详情",notes="获取订单详情")
	@PostMapping("findOrderformInfo")
	public AjaxJson findOrderformInfo(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderformService.findOrderformInfo(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="通过用户id查询预定或使用中中订单",notes="通过用户id查询预定或使用中详情")
	@PostMapping("findOrderformInfoByUserId")
	public AjaxJson findOrderformInfoByUserId(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderformService.findOrderformInfoByUserId(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}

}

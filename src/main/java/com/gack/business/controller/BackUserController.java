package com.gack.business.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.gack.business.model.BackLog;
import com.gack.business.model.BackRole;
import com.gack.business.model.BackUser;
import com.gack.business.model.ReturnResult;

import com.gack.business.repository.BackLogRepsitory;
import com.gack.business.repository.BackRoleRepository;
import com.gack.business.repository.BackUserRepository;
import com.gack.business.service.BackUserService;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author zj
 *
 */
@RestController
@RequestMapping(ApiController.USER_URL)
public class BackUserController {
	
	
	@Autowired
	private  BackUserService  backUserService;
	
	@Autowired
	private   BackRoleRepository  backRoleRepository;
	
	@Autowired
	private  BackLogRepsitory backLogRepsitory;
	
	@Autowired
	private   BackUserRepository   backUserRepository;
	
	/**
	 * 
	 * @param username
	 * @param name
	 * @param gender
	 * @param department
	 * @param roleid
	 * @param storename
	 * @param userPhone
	 * @param email
	 * @param status
	 * @return
	 */
	@ApiOperation(value = "新建账号", notes = "新建账号")
	@RequestMapping(value ="insertBackIUser",method = RequestMethod.POST)
	public  AjaxJson  insertBackIUser(String userid,String username,String name,String gender,String department,String roleid,
			String storename,String userPhone,String email,Integer status,String storeid){
		AjaxJson ajaxJson=new AjaxJson();
		ReturnResult returnResult=new ReturnResult();
		try {
			returnResult=backUserService.insertBackIUser(userid,username,name,gender,department,roleid,storename,userPhone,email,status,storeid);
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(returnResult);
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(returnResult);
		}
		return ajaxJson;
		
	}
	/**
	 * 
	 * @param id
	 * @return
	 */ 
	@ApiOperation(value = "查询账号详情")
	@RequestMapping(value ="queryBackUserinfo",method = RequestMethod.POST)
	public  AjaxJson  queryBackUserinfo(String id){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(backUserService.queryBackUserinfo(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
		
	}
	
	
	/**
	 * 
	 * @param ids
	 * @return
	 */
	@ApiOperation(value = "删除账号", notes = "删除单个用户或者多个用户")
	@RequestMapping(value = "deleteForOiuser",method = RequestMethod.POST)
	public  AjaxJson  deleteForOiuser(String ids,String userid){
		AjaxJson ajaxJson=new AjaxJson();
		ReturnResult returnResult=new ReturnResult();
		try {
			returnResult=backUserService.deleteForOiuser(ids,userid);
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(returnResult);
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(returnResult);
		}
		return ajaxJson;
		
	}
	/**
	 * 
	 * @param id
	 * @param username
	 * @param name
	 * @param gender
	 * @param department
	 * @param roleid
	 * @param storename
	 * @param userPhone
	 * @param email
	 * @param status
	 * @return
	 */
	@ApiOperation(value = "修改账号")
	@RequestMapping(value ="editForOiuser",method = RequestMethod.POST)
	public AjaxJson editForOiuser(String userid,String id,String username,String name,String gender,String department,String roleid,
			String storename,String userPhone,String email,Integer status,String storeid){
		AjaxJson ajaxJson=new AjaxJson();
		ReturnResult returnResult=new ReturnResult();
		try {
			returnResult=backUserService.editOiSysuer(userid,id,username,name,gender,department,roleid,storename,userPhone,email,status,storeid);
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(returnResult);
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(returnResult);
		}
		
		
		return ajaxJson;
	}
	
	/**
	 * 
	 * @param page
	 * @param size
	 * @param name
	 * @param phone
	 * @param status
	 * @return
	 */
	@ApiOperation(value = "多条件查询账号列表")
	@RequestMapping(value ="multiconditioniQuery",method = RequestMethod.POST)
	public AjaxJson multiconditioniQuery(@RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,String name,String phone,Integer status){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(backUserService.multiconditioniQuery(page,size,name,phone,status));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @param page
	 * @param size
	 * @return
	 */
	@ApiOperation(value = "角色列表")
	@RequestMapping(value ="queryRoles",method = RequestMethod.POST)
	public AjaxJson queryRoles(@RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(backUserService.queryRoles(page,size));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	
	@ApiOperation(value = "新建角色", notes = "新建角色测试用")
	@RequestMapping(value ="insertrole",method = RequestMethod.POST)
	public  AjaxJson  insertrole(String rolename,String permission,String pids){
		AjaxJson ajaxJson=new AjaxJson();
		ReturnResult returnResult=new ReturnResult();
		try {
			BackRole  backrole=new BackRole();
			backrole.setRolename(rolename);
			backrole.setPermission(permission);
			backrole.setPids(pids);
			backRoleRepository.save(backrole);
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData("success");
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(returnResult);
		}
		return ajaxJson;
		
	}
	
	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	
	@ApiOperation(value = "后台登录接口", notes = "登录判断账号是否正常")
	@RequestMapping(value = "/operatorLogin",method = RequestMethod.POST)
	public AjaxJson backLogin(String username, String password) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(backUserService.validatUandP(username, password));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}


/**
 * 
 * @param account
 * @return
 */
	// 发送验证码
	@ApiOperation(value = "发送短信接口", notes = "发送短信")
	@RequestMapping(value ="/sendVeryMessage",method = RequestMethod.POST)
	public AjaxJson sendVeryMessage(String account) {
		AjaxJson ajaxJson=new AjaxJson();
		ReturnResult returnResult = new ReturnResult();
		try {
			returnResult = backUserService.sendVeryMessage(account);
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(returnResult);
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(returnResult);
		}

		return ajaxJson;

	}

	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */

	// 忘记密码
	@ApiOperation(value = "忘记密码接口", notes = "设置用户密码")
	@RequestMapping(value ="/forgetPassword",method = RequestMethod.POST)
	public AjaxJson forgetPassword(String username, String password) {
		AjaxJson ajaxJson=new AjaxJson();
		ReturnResult returnResult = new ReturnResult();
		try {
			returnResult = backUserService.forgetPassword(username, password);
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(returnResult);
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(returnResult);
		}
		return ajaxJson;
	}
/**
 * 
 * @param username
 * @return
 */
	@ApiOperation(value = "验证账号是否存在")
	@RequestMapping(value = "/validateUsernameIs",method = RequestMethod.POST)
	public AjaxJson validateUsernameIs(String username) {
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(backUserService.validateUsernameIs(username));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @param username
	 * @param code
	 * @return
	 */
	@ApiOperation(value = "验证验证码存在接口")
	@RequestMapping(value = "/vlidateCodeIsexist",method = RequestMethod.POST)
	public AjaxJson vlidateCodeIsexist(String username,String code) {
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(backUserService.vlidateCodeIsexist(username,code));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	//操作日志记录查询加条件
	/**
	 * 
	 * @param page
	 * @param size
	 * @param type         //传  后台  或者  门店
	 * @param datestr      //全部    今天    本周    本月    本季度
	 * @param name         //名字
	 * @return
	 */
	@ApiOperation(value = "操作日志记录查询加条件接口")
	@RequestMapping(value = "/queryLogsList",method = RequestMethod.POST)
	public AjaxJson queryLogsList(@RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size,String type,String datestr,String name) {
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(backUserService.queryLogsList(page,size,type,datestr,name));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	
	//操作日志保存测试接口
	
	@ApiOperation(value = "新建日志", notes = "新建日志")
	@RequestMapping(value ="insertLog",method = RequestMethod.POST)
	public  AjaxJson  insertLog(String name,String type,String details){
		AjaxJson ajaxJson=new AjaxJson();
		ReturnResult returnResult=new ReturnResult();
		try {
			BackLog backLog =new BackLog();
			backLog.setOperationdate(new Date());
			backLog.setDetails("增加。。。。。。。什么或者  编辑。。。。什么   或者  sha");
			/*BackUser  backUser=	backUserRepository.findOne(id);
			if(backUser!=null){
				backLog.setName(backUser.getName());
				if(backUser.getPids().equals("1")&&backUser.getPids()!=null&&!backUser.getPids().equals("")){
					backLog.setStatus("1");
					backLog.setType("后台");
				}else if(backUser.getPids().equals("2")&&backUser.getPids()!=null&&!backUser.getPids().equals("")){
					backLog.setStatus("2");
					backLog.setType("门店");
				}
			}*/
			backLogRepsitory.save(backLog);
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData("success");
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(returnResult);
		}
		return ajaxJson;
		
	}
	
	
	/**
	 * 
	 * @param name
	 * @param email
	 * @param userid
	 * @return
	 */
	@ApiOperation(value = "个人账号设置")
	@RequestMapping(value ="personalAccountSetting",method = RequestMethod.POST)
	public AjaxJson personalAccountSetting(String name,String email,String userid){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(backUserService.personalAccountSetting(name,email,userid));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @param userid
	 * @return
	 */
	@ApiOperation(value = "个人账号设置信息回显")
	@RequestMapping(value ="personalAccountSettinginfo",method = RequestMethod.POST)
	public AjaxJson personalAccountSettinginfo(String userid){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(backUserService.personalAccountSettinginfo(userid));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value = "个人账号设置修改密码")
	@RequestMapping(value ="personalAccountSettingPwd",method = RequestMethod.POST)
	public AjaxJson personalAccountSettingPwd(String password,String userid){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(backUserService.personalAccountSettingPwd(password,userid));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	
	
	

}

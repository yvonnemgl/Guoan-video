package com.gack.business.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.StoresOptServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: StoresOptController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月4日 下午3:04:25 
* @version V1.0
 */
@RestController
@RequestMapping(ApiController.STORES_OPERATOR_URL)
public class StoresOptController {
	
	@Autowired
	private StoresOptServiceInterface storesOptService;
	
	/**
	 * 
	* @Title: getStores 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param searchType	 查询方式  1 门店名称查询  2门店面积查询  3门店使用价格查询
	* @param @param searchKey
	* @param @param currentPage
	* @param @param pageSize
	* @param @return    入参
	* @return AjaxJson    返回类型
	* @author （ZhangXinYu） 
	* @throws
	* @date 2018年6月5日 上午9:03:16 
	* @version V1.0
	 */
	@ApiOperation(value="门店信息查询",notes="门店信息查询")
	@PostMapping("getStores")
	public AjaxJson getStores(Integer searchType,String searchKey,Integer currentPage,Integer pageSize){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(storesOptService.getStores(searchType, searchKey, currentPage, pageSize));
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(true).setStatus(500).setMsg("门店查询操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="查询所有门店信息",notes="分页查询所有门店信息")
	@PostMapping("getAllStores")
	public AjaxJson getAllStores(Integer currentPage,Integer pageSize){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(storesOptService.getAllStores(currentPage, pageSize));
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(true).setStatus(500).setMsg("门店查询操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="查询所有门店",notes="查询所有门店")
	@PostMapping("getAllStoresInfo")
	public AjaxJson getAllStoresInfo(){
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.setSuccess(true).setStatus(200).setData(storesOptService.getAllStores());
		return ajaxJson;
	}
	
	@ApiOperation(value="门店信息导出",notes="门店信息导出")
	@GetMapping("exportStores")
	public void exportStores(HttpServletResponse response){
		try (OutputStream out = response.getOutputStream(); Workbook wb = storesOptService.exportStores();) {
			response.setContentType("application/octet-stream;charset=utf-8");  
			String  fileName ="商户信息";
			response.setHeader("Content-Disposition", "attachment;filename*=utf-8'zh_cn'"+URLEncoder.encode(fileName, "UTF-8") + ".xls"); 
			wb.write(out);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@ApiOperation(value="查询门店详情",notes="查询门店详情")
	@PostMapping("getStoresInfo")
	public AjaxJson getStoresInfo(String storesid){
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.setSuccess(true).setStatus(200).setData(null);
		return ajaxJson;
	}

}

package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.InvoiceServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

@RestController
@RequestMapping(ApiController.INVOICE)
public class InvoiceController {

	@Autowired
	InvoiceServiceInterface invoiceServiceInterface;
	
	/**
	 * 
	 * @Title: save
	 * @Description: 开发票接口
	 * @param userId 用户ID
	 * @param ids 数组 发票ID
	 * @param amount 开票总金额
	 * @param type 类型 0 个人发票 1企业发票
	 * @param content 发票内容
	 * @param head 发票抬头
	 * @param number 税号
	 * @param username 收件人
	 * @param phone 收件人手机号
	 * @param address 收件人地址
	 * @return AjaxJson 
	 * @throws
	 */
	@PostMapping
	public AjaxJson save(String userId,String[] ids,int amount,String type,String content,String head,String number,String username,String phone,String address,String detail){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(invoiceServiceInterface.save(userId, ids, amount, type, content, head, number, username, phone, address, detail)
					);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("ERROR");
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @Title: get
	 * @Description: 获取待开发票信息
	 * @param userId 用户ID
	 * @param kind 发票类型 1商务卡发票 2门店发票
	 * @return AjaxJson 
	 * @throws
	 */
	@GetMapping
	public AjaxJson get(String userId,String kind){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(invoiceServiceInterface.get(userId, kind));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("ERROR");
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @Title: getList
	 * @Description: 获取已开发票详细信息
	 * @param userId 用户ID
	 * @return AjaxJson 
	 * @throws
	 */
	@GetMapping("/list")
	public AjaxJson getList(String userId){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(invoiceServiceInterface.getList(userId));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("ERROR");
		}
		return ajaxJson;
	}
}

package com.gack.business.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.UserLoginAndRegisterService;
import com.gack.business.service.UserLoginAndRegisterServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-5-25
 */

@RestController
@RequestMapping(ApiController.USER_LOGIN_AND_REGISTER_URL)
public class UserLoginAndRegisterController {

	@Autowired
	private UserLoginAndRegisterServiceInterface userLoginAndRegisterService;
	
	/**
	 * 注册
	 * @param username 用户名，即手机号
	 * @param password 密码
	 * @param identifycode 短信验证码
	 * @param register_source 注册来源 android或ios
	 * @return
	 */
	@ApiOperation(value="用户注册",notes="用户注册")
	@PostMapping("registUser")
	public AjaxJson registerUser(String username, String password, String identifycode, String register_source){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(username == null || username.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户名为空").setData(null);
		}
		if(password == null || password.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("密码为空").setData(null);
		}
		if(identifycode == null || identifycode.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("验证码为空").setData(null);
		}
		if(register_source == null || register_source.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("注册来源为空").setData(null);
		}
		if(!register_source.equalsIgnoreCase("android") && !register_source.equalsIgnoreCase("ios")){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("注册来源不正确").setData(null);
		}
		
		try {
            ajaxJson.setSuccess(true).setStatus(200).setData(userLoginAndRegisterService.registUser(username.trim(), password.trim(), identifycode.trim(), register_source.trim()));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	/**
	 * 用户名密码登录
	 * @param username 用户名
	 * @param password 密码
	 * @param login_resource 登录来源   pc或android或ios
	 * @param equipment 设备号
	 * @return
	 */
	@ApiOperation(value="用户名/密码登录",notes="用户名/密码登录")
	@PostMapping("loginByPassword")
	public AjaxJson loginByPassword(String username, String password, String login_resource, String equipment){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(username == null || username.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户名为空").setData(null);
		}
		if(password == null || password.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("密码为空").setData(null);
		}
		if(login_resource == null || login_resource.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("登录来源为空").setData(null);
		}
		if(!login_resource.equalsIgnoreCase("pc") && !login_resource.equalsIgnoreCase("android") && !login_resource.equalsIgnoreCase("ios")){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("登录来源不正确").setData(null);
		}
		if(equipment == null || equipment.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("设备号为空").setData(null);
		}
		
		try {
            ajaxJson.setSuccess(true).setStatus(200).setData(userLoginAndRegisterService.loginByPassword(username.trim(), password.trim(), login_resource.trim(), equipment.trim()));
        } catch (Exception e) {
            e.printStackTrace();
            ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
        }
		return ajaxJson;
	} 
	
	/**
	 * 用户名短信验证码登录
	 * @param username 用户名
	 * @param identifyCode 短信验证码
	 * @param login_resource 登录来源   pc或android或ios
	 * @param equipment 设备号
	 * @return
	 */
	@ApiOperation(value="用户名/验证码登录",notes="用户名/验证码登录")
	@PostMapping("loginByIdentifyCode")
	public AjaxJson loginByIdentifyCode(String username, String identifyCode, String login_resource, String equipment){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(username == null || username.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户名为空").setData(null);
		}
		if(login_resource == null || login_resource.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("登录来源为空").setData(null);
		}
		if(!login_resource.equalsIgnoreCase("pc") && !login_resource.equalsIgnoreCase("android") && !login_resource.equalsIgnoreCase("ios")){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("登录来源不正确").setData(null);
		}
		if(equipment == null || equipment.length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("设备号为空").setData(null);
		}
		
		try {
	        ajaxJson.setSuccess(true).setStatus(200).setData(userLoginAndRegisterService.loginByIdentifyCode(username.trim(), identifyCode.trim(), login_resource.trim(), equipment.trim()));
	    } catch (Exception e) {
	        e.printStackTrace();
	        ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
	    }
		
		return ajaxJson;
	}
	
	@ApiOperation(value="发送短信验证码",notes="发送短信验证码")
	@PostMapping("sendIdentifyCode")
	public AjaxJson sendIdentifyCode(String username){
		AjaxJson ajaxJson = new AjaxJson();
		
		return ajaxJson.setSuccess(true).setStatus(200).setData(userLoginAndRegisterService.sendIdentifyCode(username.trim()));
	}
	
}

package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.UserHomeServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: UserHomeController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月12日 下午2:45:21 
* @version V1.0
 */
@RestController
@RequestMapping(ApiController.USER_HOME_URL)
public class UserHomeController {
	
	@Autowired
	private UserHomeServiceInterface userHomeService;

	@ApiOperation(value="发送短信验证码",notes="发送短信验证码")
	@PostMapping("sendIdentifyCode")
	public AjaxJson sendIdentifyCode(String username){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(userHomeService.sendIdentifyCode(username.trim()));
	}
	
	@ApiOperation(value="获取图片验证码",notes="获取图片验证码")
	@PostMapping("getValidateCodePic")
	public AjaxJson getValidateCodePic(){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setData(userHomeService.getIdentifyPic());
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("获取图片验证码操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="首页登录",notes="首页登录")
	@PostMapping("homeLogin")
	public AjaxJson homeLogin(String username,String password,String code,String validateid){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setData(userHomeService.homeLogin(username, password, code, validateid));
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("首页登录操作失败");
		}
		return ajaxJson;
	}
	
	/**
	 * 
	* @Title: addOpinionUnLogin 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param opiniontext		意见内容
	* @param @param username		用户id  已登录为用户id  未登录为用户输入手机号
	* @param @param code			验证码
	* @param @param validateid		验证码记录id
	* @param @param type			类型 （0 未登录  1已登录）
	* @param @return    入参
	* @return AjaxJson    返回类型
	* @author （ZhangXinYu） 
	* @throws
	* @date 2018年6月12日 下午5:50:51 
	* @version V1.0
	 */
	@ApiOperation(value="问题反馈-首页",notes="问题反馈-首页")
	@PostMapping("addHomeOpinion")
	public AjaxJson addHomeOpinion(String opiniontext,String username,String code,Integer type){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setData(userHomeService.addHomeOpinion(opiniontext, username, code, type));
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("意见反馈操作失败");
		}
		return ajaxJson;
	}
	
}

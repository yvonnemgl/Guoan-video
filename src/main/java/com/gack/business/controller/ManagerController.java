package com.gack.business.controller;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.gack.business.model.PermissionGroup;
import com.gack.business.service.ManagerServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.jsonwebtoken.lang.Collections;
import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-6-2
 */
@RestController
@RequestMapping(ApiController.MANAGER_URL)
public class ManagerController {

	@Autowired
	private ManagerServiceInterface managerService;
	
	/**
	 * 查找公司的所有管理员(包含主管理员)
	 * @param permissionEnterpriseId 公司id
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "查找公司的管理员", notes = "查找公司的管理员")
	@PostMapping("findAllManagerAndNeepPermission")
	public AjaxJson findAllManager(String permissionEnterpriseId, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(managerService.findAllManager(permissionEnterpriseId.trim(), permissionUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 删除管理员
	 * @param userId 用户id(id1;id2;id3)
	 * @param permissionEnterpriseId 公司id
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "删除管理员", notes = "删除管理员")
	@PostMapping("deleteManagerAndNeedPermission")
	public AjaxJson deleteManager(String userId, String permissionEnterpriseId, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(managerService.deleteManager(userId.trim(), permissionEnterpriseId.trim(), permissionUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}

	/**
	 * 根据用户id、公司id查找权限组(若无权限组,则返回默认权限组)
	 * @param userId 用户id
	 * @param permissionEnterpriseId 公司id
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "根据用户id、公司id查找权限组")
	@PostMapping("findPermissionGroupListAndNeedPermission")
	public AjaxJson findPermissionGroupList(String userId, String permissionEnterpriseId, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(managerService.findPermissionGroupList(userId.trim(), permissionEnterpriseId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 保存权限组
	 * @param userId 用户id
	 * @param permissionEnterpriseId 公司id
	 * @param permissionGroupList 权限组集合
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "保存某公司中某用户的权限组")
	@PostMapping("savePermissionGroupAndNeedPermission")
	public AjaxJson savePermissionGroup(String userId, String permissionEnterpriseId, @RequestBody List<PermissionGroup> permissionGroupList, String permissionUserId) {
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId) || userId.equals("-1")){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId) || permissionEnterpriseId.equals("-1")){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(Collections.isEmpty(permissionGroupList)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("权限组为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("权限组为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(managerService.savePermissionGroup(userId.trim(), permissionEnterpriseId.trim(), permissionGroupList, permissionUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 保存权限组 (for-IOS)
	 * @param userId 用户id
	 * @param permissionEnterpriseId 公司id
	 * @param permissionGroupList 权限组集合
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "保存某公司中某用户的权限组")
	@PostMapping("savePermissionGroupForIOSAndNeedPermission")
	public AjaxJson savePermissionGroupForIOS(String userId, String permissionEnterpriseId, String permissionGroupList, String permissionUserId) {
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId) || userId.equals("-1")){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId) || permissionEnterpriseId.equals("-1")){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(permissionGroupList)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("权限组为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("权限组为空").setData(null);
		}
		
		//解析字符串
		List<PermissionGroup> list = JSON.parseObject(permissionGroupList, new TypeReference<List<PermissionGroup>>() {});
		
		if(null == list || list.size() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("权限组集合为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(managerService.savePermissionGroup(userId.trim(), permissionEnterpriseId.trim(), list, permissionUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查找某人所具有的权限
	 * @param userId 被查看者的用户id
	 * @param permissionEnterpriseId 公司id
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "查找某人所具有的权限")
	@PostMapping("findPermissionByUserIdAndNeedPermission")
	public AjaxJson findPermissionByUserId(String userId, String permissionEnterpriseId, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(managerService.findPermissionByUserId(permissionEnterpriseId.trim(), userId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
//	/**
//	 * 查找公司超级管理员的手机号
//	 * @param enterpriseId 公司id
//	 * @return
//	 */
//	@ApiOperation(value = "查找公司超级管理员的手机号")
//	@PostMapping("findSuperManagerUsername")
//	public AjaxJson findSuperManagerUsername(String enterpriseId){
//		AjaxJson ajaxJson = new AjaxJson();
//		
//		if(StringUtils.isBlank(enterpriseId)){
//			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
//		}
//		
//		try{
//			ajaxJson.setSuccess(true).setStatus(200).setData(managerService.findSuperManagerUsername(enterpriseId.trim()));
//		}catch(Exception e){
//			e.printStackTrace();
//			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
//		}
//		
//		return ajaxJson;	
//	}
	
	/**
	 * 发送短信验证码,用来更换超级管理员
	 * @param permissionUserId 操作者id
	 * @param permissionEnterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "发送短信验证码,用来更换超级管理员")
	@PostMapping("sendCodeForSetSuperManagerAndNeedPermission")
	public AjaxJson sendCodeForSetSuperManager(String permissionUserId, String permissionEnterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(managerService.sendCodeForSetSuperManager(permissionEnterpriseId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;	
	}
	
	/**
	 * 更换超级管理员
	 * @param permissionUserId 操作者id
	 * @param permissionEnterpriseId 公司id
	 * @param userId 用户id
	 * @param code 短信验证码
	 * @return
	 */
	@ApiOperation(value = "更换超级管理员")
	@PostMapping("setSuperManagerAndNeedPermission")
	public AjaxJson setSuperManager(String permissionUserId, String permissionEnterpriseId, String userId, String code){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(code)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("验证码为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(managerService.setSuperManager(permissionUserId.trim(), permissionEnterpriseId.trim(), userId.trim(), code.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;	
	}
	
}

package com.gack.business.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.JobRankServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-9-10
 */
@RestController
@RequestMapping(ApiController.JOB_RANK_URL)
public class JobRankController {

	@Autowired
	private JobRankServiceInterface jobRankService;
	
	/**
	 * 新增或修改职级
	 * @param permissionEnterpriseId 公司id
	 * @param permissionUserId 操作者id
	 * @param rankId 职级id(若有职级id,则为修改;若无职级id,则为新增)
	 * @param name 职级名称
	 * @param level 职级等级
	 * @return
	 */
	@ApiOperation(value = "新增或修改职级")
	@PostMapping("saveOrUpdateAndNeedPermission")
	public AjaxJson saveOrUpdate(String permissionEnterpriseId, String permissionUserId, String rankId, String name, Integer level){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(name)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职级名称为空").setData(null);
		}
		if(null == level || level.compareTo(1) < 0 || level.compareTo(99) > 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职级等级非法").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(jobRankService.saveOrUpdate(permissionEnterpriseId, rankId, name, level));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}

	/**
	 * 删除职级
	 * @param permissionEnterpriseId 公司id
	 * @param permissionUserId 操作者id
	 * @param rankId 职级id
	 * @return
	 */
	@ApiOperation(value = "删除职级")
	@PostMapping("deleteAndNeedPermission")
	public AjaxJson delete(String permissionEnterpriseId, String permissionUserId, String rankId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(rankId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职级为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(jobRankService.delete(permissionEnterpriseId, permissionUserId, rankId));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查找公司下所有职级
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "查找公司下所有职级")
	@PostMapping("findAll")
	public AjaxJson findAll(String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(jobRankService.findAll(enterpriseId));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}

	/**
	 * 查找职级下所有职位(未被删除)
	 * @param jobrankId 职级id
	 * @return
	 */
	@ApiOperation(value = "查找职级下所有职位(未被删除)")
	@PostMapping("findPosition")
	public AjaxJson findPosition(String jobrankId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(jobrankId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职级为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(jobRankService.findPosition(jobrankId));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查找"职级使用帮助"
	 * @return
	 */
	@ApiOperation(value = "查找\"职级使用帮助\"")
	@PostMapping("findHelper")
	public AjaxJson findHelper(){
		AjaxJson ajaxJson = new AjaxJson();
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(jobRankService.findHelper());
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
}

package com.gack.business.controller;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.OSSClient;
import com.gack.business.fkbModel.input.DepartmentInput;
import com.gack.business.fkbModel.input.EnterpriseInput;
import com.gack.business.fkbModel.input.PeopleInput;
import com.gack.business.fkbModel.input.PositionInput;
import com.gack.business.fkbModel.input.PushInput;
import com.gack.business.fkbModel.output.FKBOutput;
import com.gack.business.service.FKBServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;
import com.gack.helper.common.util.OSSUnitHelper;

import io.swagger.annotations.ApiOperation;
import net.sf.json.JSONObject;

/**
 * 费控宝接口(被费控宝调用)
 * @author ws
 *
 */
@RestController
@RequestMapping(ApiController.FKB_URL)
public class FKBController {

	@Autowired
	private FKBServiceInterface fkbService;
	
	@ApiOperation(value = "组织架构查询接口", notes = "组织架构查询接口")
	@PostMapping("findEnterprise")
	public FKBOutput findEnterprise(@RequestBody EnterpriseInput input){
		FKBOutput output = new FKBOutput();
		
		if(input == null || input.getData() == null){
			output.setStatus(0);
			output.setMessage("操作失败:输入参数为空或输入参数中的data");
			output.setData(null);
			return output;
		}
		
		try{
			output.setStatus(1);
			output.setMessage("操作成功");
			output.setData(fkbService.findEnterprise(input.getData().getOrgcode()));
			return output;
		}catch(Exception e){
			output.setStatus(0);
			output.setMessage("操作失败:查询时出现异常");
			output.setData(null);
			return output;
		}
	}
	
	@ApiOperation(value = "部门查询接口", notes = "部门查询接口")
	@PostMapping("findDepartment")
	public FKBOutput findDepartment(@RequestBody DepartmentInput input){
		FKBOutput output = new FKBOutput();
		
		if(input == null || input.getData() == null || input.getData().getOrgcode() == null || input.getData().getOrgcode().trim().length() == 0){
			output.setStatus(0);
			output.setMessage("操作失败:输入参数为空或输入参数中的data或输入参数中公司编码为空");
			output.setData(null);
			return output;
		}
		
		try{
			output.setStatus(1);
			output.setMessage("操作成功");
			output.setData(fkbService.findDepartment(input.getData().getOrgcode(), input.getData().getDeptcode()));
			return output;
		}catch(Exception e){
			output.setStatus(0);
			output.setMessage("操作失败:查询时出现异常");
			output.setData(null);
			return output;
		}
	}

	@ApiOperation(value = "职位职级查询接口", notes = "职位职级查询接口")
	@PostMapping("findPosition")
	public FKBOutput findPosition(@RequestBody PositionInput input){
		FKBOutput output = new FKBOutput();
		
		if(input == null || input.getData() == null || StringUtils.isBlank(input.getData().getOrgcode())){
			output.setStatus(0);
			output.setMessage("操作失败:输入参数为空或输入参数中的data或输入参数中公司编码为空");
			output.setData(null);
			return output;
		}
		
		try{
			output.setStatus(1);
			output.setMessage("操作成功");
			output.setData(fkbService.findPosition(input.getData().getOrgcode()));
			return output;
		}catch(Exception e){
			output.setStatus(0);
			output.setMessage("操作失败:查询时出现异常");
			output.setData(null);
			return output;
		}
	}
	
	@ApiOperation(value = "人员信息查询接口", notes = "人员信息查询接口")
	@PostMapping("findPeople")
	public FKBOutput findPeople(@RequestBody PeopleInput input){
		FKBOutput output = new FKBOutput();
		
		if(input == null || input.getData() == null || input.getData().getOrgcode() == null || input.getData().getOrgcode().trim().length() == 0){
			output.setStatus(0);
			output.setMessage("操作失败:输入参数为空或输入参数中的data或输入参数中公司编码为空");
			output.setData(null);
			return output;
		}
		
		try{
			output.setStatus(1);
			output.setMessage("操作成功");
			output.setData(fkbService.findPeople(input.getData().getOrgcode(), input.getData().getDeptcode(), input.getData().getPeoplecode()));
			return output;
		}catch(Exception e){
			output.setStatus(0);
			output.setMessage("操作失败:查询时出现异常");
			output.setData(null);
			return output;
		}
	}
	
	@ApiOperation(value = "移动消息推送接口", notes = "移动消息推送接口")
	@PostMapping("pushMessage")
	public FKBOutput pushMessage(@RequestBody PushInput input){
		FKBOutput output = new FKBOutput();
		
		if(input == null || input.getData() == null || input.getData().getOperatorNo() == null || input.getData().getOperatorNo().trim().length() == 0 || input.getData().getType() == null || input.getData().getType().trim().length() == 0){
			output.setStatus(0);
			output.setMessage("操作失败:输入参数为空或输入参数中的data或输入参数中通知对象为空或输入参数中执行动作类型为空");
			output.setData(null);
			return output;
		}
		
		try{
			JSONObject result = fkbService.pushMessage(input.getData().getOperatorNo(), input.getData().getRemarks(), input.getData().getH5url(), input.getData().getPCurl(), input.getData().getType());
			
			if(result.containsKey("error")){
				output.setStatus(0);
				output.setMessage("推送消息失败");
				output.setData(null);
				return output;
			}
			output.setStatus(1);
			output.setMessage("操作成功");
			output.setData(null);
			return output;
		}catch(Exception e){
			output.setStatus(0);
			output.setMessage("操作失败:查询时出现异常");
			output.setData(null);
			return output;
		}
	}
	
}

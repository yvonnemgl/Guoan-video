package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.VideoMeetingService;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;


import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(ApiController.VIDEO_MEETING)
public class VideoMeetingController {
	@Autowired
	private  VideoMeetingService  videoMeetingService;
	
	@ApiOperation(value = "查询会议列表")
	@RequestMapping(value ="VideoMeetinglist",method = RequestMethod.POST)
	public AjaxJson VideoMeetinglist(){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoMeetingService.getAllVideoMeeting());
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	/**
	 * 查询会议室存在人数
	 * @param roomId
	 * @return
	 */
	@ApiOperation(value = "查询会议室人数")
	@RequestMapping(value = "getParticipants", method = RequestMethod.POST)
	public AjaxJson getParticipants(String roomId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoMeetingService.getParticipants(roomId));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	/**
	 * 传入id 查询详情
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "查询会议室详情")
	@RequestMapping(value = "getParticipant", method = RequestMethod.POST)
	public AjaxJson getParticipant(String id) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoMeetingService.getParticipant(id));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value = "更新会议人数")
	@RequestMapping(value = "updateMeetParticipant", method = RequestMethod.POST)
	public AjaxJson updateMeetParticipant(String roomId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoMeetingService.updateVideoMeeting(roomId));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
}

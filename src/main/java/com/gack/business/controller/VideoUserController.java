package com.gack.business.controller;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.OSSClient;
import com.gack.business.service.VideoUserServerInterface;
import com.gack.helper.aspect.DuplicateSubmitException;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;
import com.gack.helper.common.util.OSSUnitHelper;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: VideoUserController.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月20日
*  
 */
@RestController
@RequestMapping(ApiController.USER_URL)
public class VideoUserController {
	
//	Semaphore semaphore=new Semaphore(1);  //定义资源的总数量
	
	@Autowired
	private VideoUserServerInterface videoUserService;
	
	@ApiOperation(value="发送短信验证码",notes="发送短信验证码")
	@PostMapping("sendIdentifyCode")
	public AjaxJson sendIdentifyCode(String username){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.sendIdentifyCode(username.trim()));
	}
	
	@ApiOperation(value="发送短信验证码-验证用户是否注册",notes="发送短信验证码-验证用户是否注册")
	@PostMapping("sendIdentifyCodeByRegistUser")
	public AjaxJson sendIdentifyCodeByRegistUser(String username){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.sendIdentifyCodeByRegistUser(username.trim()));
	}
	
	@ApiOperation(value="用户注册",notes="用户注册")
	@PostMapping("registUser")
	public AjaxJson resiterVideoUser(String username,String password,String identifycode,
			@RequestParam(value="source",defaultValue="android") String source){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.registUser(username.trim(),password.trim(),identifycode.trim(),source));
		} catch (DuplicateSubmitException e) {
        	e.printStackTrace();
        	ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setMsg("表单重复提交");
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="用户名/密码登录",notes="用户名/密码登录")
	@PostMapping("loginByPassword")
	public AjaxJson loginByPassword(String username,String password,
			@RequestParam(value="type",defaultValue="android") String type,
			@RequestParam(value="equipmentid",defaultValue="null") String equipmentid,
			@RequestParam(value="token",defaultValue="null")String token){
		AjaxJson ajaxJson = new AjaxJson();
		try {
            ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.testLoginByPassword(username.trim(),password.trim(),type.trim(),equipmentid.trim(),token.trim()));
		}catch (DuplicateSubmitException e) {
        	e.printStackTrace();
        	ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setMsg("表单重复提交");
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="用户名/验证码登录",notes="用户名/验证码登录")
	@PostMapping("loginByIdentifyCode")
	public AjaxJson loginByIdentifyCode(String username,String identifyCode,
			@RequestParam(value="type",defaultValue="android") String type,
			@RequestParam(value="equipmentid",defaultValue="null") String equipmentid){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.testLoginByIdentifyCode(username.trim(),identifyCode.trim(),type.trim(),equipmentid.trim()));
		}catch (DuplicateSubmitException e) {
        	e.printStackTrace();
        	System.out.println("表单重复");
        	ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setMsg("表单重复提交");
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="退出登录",notes="退出登录")
	@PostMapping("signOut")
	public AjaxJson signOut(String id,
			@RequestParam(value="type",defaultValue="android") String type){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.signOut(id.trim(),type));
	}
	
	@ApiOperation(value="找回密码",notes="找回密码")
	@PostMapping("resetPassword")
	public AjaxJson resetPassword(String username,String identifyCode,String password){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.resetPassword(username.trim(), identifyCode.trim(), password.trim()));
	}

	@ApiOperation(value="意见反馈",notes="意见反馈")
	@PostMapping("feedbackOpinion")
	public AjaxJson feedbackOpinion(String id,String opinion,
			@RequestParam(value="source",defaultValue="app") String source){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.feedbackOpinion(id.trim(), opinion.trim(),source.trim()));
	}
	
	@ApiOperation(value="修改昵称",notes="修改昵称")
	@PostMapping("resetNickname")
	public AjaxJson resetNickname(String id,String nickname){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.resetNickname(id.trim(), nickname.trim()));
	}
	
	@ApiOperation(value="忘记旧密码",notes="忘记旧密码")
	@PostMapping("forgetOldPassword")
	public AjaxJson forgetOldPassword(String id,String password,String identifyCode){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.forgetOldPassword(id.trim(), password.trim(), identifyCode.trim()));
	}
	
	@ApiOperation(value="获取个人信息",notes="获取个人信息")
	@PostMapping("getUserInfo")
	public AjaxJson getUserInfo(String id){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.getUserInfo(id.trim()));
	}
	
	@SuppressWarnings("unused")
	@ApiOperation(value="上传用户头像",notes="上传用户头像")
	@PostMapping("updateUserPortait")
	public AjaxJson updateUserProtait(MultipartFile contractlogo , HttpServletResponse response){
		AjaxJson ajaxJson=new AjaxJson();
		if(contractlogo!=null){
			OSSUnitHelper ossunit=new OSSUnitHelper();
			OSSClient client=OSSUnitHelper.getOSSClient();
			String diskName = "datas/video/portait/";
			String fileName = new Date().getTime() + (int) ((Math.random() * 9 + 1) * 100000)
					+ contractlogo.getOriginalFilename().substring(contractlogo.getOriginalFilename().indexOf("."));
	        String md5key = OSSUnitHelper.uploadObject2OSS(client, contractlogo, diskName,fileName);
	        String url="";
	        url=OSSUnitHelper.getUrl(client, diskName, fileName);
	        String urll=    url.substring(4, url.length());
	        url="https"+urll;
	        System.out.println(url+"5");
	        if(url!=null&&!url.equals("")){
	        	ajaxJson.setSuccess(true).setStatus(200).setData(url);
	        	response.setContentType(url);
	        } 
		 }
		 return ajaxJson;
	}
	
	@ApiOperation(value="修改头像",notes="修改头像")
	@PostMapping("resetUserPortait")
	public AjaxJson resetUserPortait(String id,String portait){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.resetUserPortait(id.trim(),portait.trim()));
	}
	
	@ApiOperation(value="pc心跳监控连接",notes="pc心跳监控连接")
	@PostMapping("heartbeat")
	public AjaxJson heartbeat(String id,
			@RequestParam(value="token",defaultValue="null")String token,
			@RequestParam(value="passwordToken",defaultValue="null") String passwordToken){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.heartbeat(id.trim(),token.trim(),passwordToken.trim()));
	}
	
	@ApiOperation(value="获取指定id用户昵称",notes="获取指定地用户昵称")
	@PostMapping("getUserNickname")
	public AjaxJson getUserNickname(String id){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.getUserNickname(id.trim()));
	}
	
	@ApiOperation(value="测试-账号密码登录-异地登录",notes="测试-账号密码登录-异地登录")
	@PostMapping("testLoginByPassword")
	public AjaxJson testLoginByPassword(String username,String password,
			@RequestParam(value="type",defaultValue="android") String type,
			@RequestParam(value="equipmentid",defaultValue="null") String equipmentid,
			@RequestParam(value="token",defaultValue="null")String token){
		AjaxJson ajaxJson = new AjaxJson();
		try {
            ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.testLoginByPassword(username.trim(),password.trim(),type.trim(),equipmentid.trim(),token.trim()));
		}catch (DuplicateSubmitException e) {
        	e.printStackTrace();
        	ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setMsg("表单重复提交");
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="测试-用户名/验证码登录-异地登录",notes="测试-用户名/验证码登录-异地登录")
	@PostMapping("testLoginByIdentifyCode")
	public AjaxJson testLoginByIdentifyCode(String username,String identifyCode,
			@RequestParam(value="type",defaultValue="android") String type,
			@RequestParam(value="equipmentid",defaultValue="null") String equipmentid){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.testLoginByIdentifyCode(username.trim(),identifyCode.trim(),type.trim(),equipmentid.trim()));
		}catch (DuplicateSubmitException e) {
        	e.printStackTrace();
        	System.out.println("表单重复");
        	ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setMsg("表单重复提交");
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="查询用户登录状态",notes="查询用户登录状态")
	@PostMapping("getUserLoginStatus")
	public AjaxJson getLoginStatus(String userid,
			@RequestParam(value="type",defaultValue="android") String type,
			@RequestParam(value="equipmentid",defaultValue="null") String equipmentid,
			@RequestParam(value="passwordToken",defaultValue="null") String passwordToken){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.getLoginStatus(userid,type,equipmentid, passwordToken));
		}catch (DuplicateSubmitException e) {
        	e.printStackTrace();
        	System.out.println("表单重复");
        	ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setMsg("表单重复提交");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="验证短信验证码",notes="验证短信验证码")
	@PostMapping("validateIdentifyCode")
	public AjaxJson validateIdentifyCode(String mobile,String code){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.validateIdentifyCode(mobile, code));
	}

	@ApiOperation(value="验证短信验证码",notes="验证短信验证码")
	@PostMapping("validateIdentifyCodeForForget")
	public AjaxJson validateIdentifyCodeForForget(String mobile,String code){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.validateIdentifyCodeForForget(mobile, code));
	}
	
	@ApiOperation(value="pc端注册",notes="pc端注册")
	@PostMapping("registByPc")
	public AjaxJson registByPc(String username,String nickname,String password,
			@RequestParam(value="source",defaultValue="pc")String source){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.registByPc(username, nickname, password, source));
	}
	
	@ApiOperation(value="为忘记密码下发短信验证码  有账号验证",notes="为忘记密码下发短信验证码  有账号存在验证")
	@PostMapping("sendIdentifyForForget")
	public AjaxJson sendIdentifyForForget(String username){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.sendIdentifyForForget(username.trim()));
	}

	@ApiOperation(value="为注册下发短信验证码  有账号验证",notes="为注册下发短信验证码  有账号存在验证")
	@PostMapping("sendIdentifyForRegist")
	public AjaxJson sendIdentifyForRegist(String username){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoUserService.sendIdentifyForRegist(username.trim()));
	}
}







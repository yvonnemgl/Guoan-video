package com.gack.business.controller;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.EnterpriseServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-5-29
 */
@RestController
@RequestMapping(ApiController.ENTERPRISE_URL)
public class EnterpriseController {

	@Autowired
	private EnterpriseServiceInterface enterpriseService;

	/**
	 * 某用户创建公司
	 * @param userId 用户id
	 * @param name 公司名称
	 * @param industryId 行业id
	 * @param personNumRangeId 人员范围id
	 * @param provinceId 省份id
	 * @param cityId 城市id
	 * @param areaId 城区id
	 * @return
	 */
	@ApiOperation(value = "创建公司", notes = "创建公司")
	@PostMapping("createEnterprise")
	public AjaxJson createEnterprise(String userId, String name, String industryId, String personNumRangeId, String provinceId, String cityId, String areaId){
		AjaxJson ajaxJson = new AjaxJson();

		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(name)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司名称为空").setData(null);
		}
		if(StringUtils.isBlank(industryId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("行业为空").setData(null);
		}
		if(StringUtils.isBlank(personNumRangeId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("人员范围为空").setData(null);
		}
		if(StringUtils.isBlank(provinceId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("省份为空").setData(null);
		}
		if(StringUtils.isBlank(cityId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("城市为空").setData(null);
		}
		if(StringUtils.isBlank(areaId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("城区为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseService.createEnterprise(userId.trim(), name.trim(), industryId.trim(), personNumRangeId.trim(), provinceId.trim(), cityId.trim(), areaId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询公司的组织架构(若公司id为空,则返回上次用户选择的公司的组织架构)
	 * @param enterpriseId 公司id
	 * @param userId 操作者id
	 * @return
	 */
	@ApiOperation(value = "查询公司的组织架构", notes = "查询公司的组织架构")
	@PostMapping("findEnterpriseFramework")
	public AjaxJson findEnterpriseFramework(String enterpriseId, String userId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseService.findEnterpriseFramework(enterpriseId, userId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 删除员工
	 * @param permissionEnterpriseId 公司id
	 * @param userId 用户id
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "删除员工", notes = "删除员工")
	@PostMapping("deleteUserFromEnterpriseAndNeedPermission")
	public AjaxJson deleteUserFromEnterprise(String permissionEnterpriseId, String userId, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseService.deleteUserFromEnterprise(permissionEnterpriseId.trim(), userId.trim(), permissionUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 修改公司名称
	 * @param permissionEnterpriseId 公司id
	 * @param enterpriseName 公司新名称
	 * @param permissionUserId 用户id
	 * @return
	 */
	@ApiOperation(value = "修改公司名称", notes = "修改公司名称")
	@PostMapping("updateEnterpriseNameAndNeedPermission")
	public AjaxJson updateEnterpriseName(String permissionEnterpriseId, String enterpriseName, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(enterpriseName)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司名称为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseService.updateEnterpriseName(permissionEnterpriseId.trim(), enterpriseName.trim(), permissionUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 修改公司基本信息(所属行业、人员数量、所在地区)
	 * @param permissionEnterpriseId 公司id
	 * @param whichOne industry:行业   personNumRange:人员数量  province;city;area:省份;城市;城区
	 * @param whichOneId 行业id或人员数量id或省份id;城市id;城区id
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "修改公司基本信息(所属行业、人员数量、所在地区)", notes = "修改公司基本信息(所属行业、人员数量、所在地区)")
	@PostMapping("updateEnterpriseBasicInfoAndNeedPermission")
	public AjaxJson updateEnterpriseBasicInfo(String permissionEnterpriseId, String whichOne, String whichOneId, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(whichOne)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作类型为空").setData(null);
		}
		if(StringUtils.isBlank(whichOneId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("id为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseService.updateEnterpriseBasicInfo(permissionEnterpriseId.trim(), whichOne.trim(), whichOneId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}

	/**
	 * 查看公司信息(公司名称,所属行业,人员数量,所在地区)
	 * @param enterpriseId 公司id
	 * @param userId 用户id
	 * @return
	 */
	@ApiOperation(value = "查看公司信息", notes = "查看公司信息")
	@PostMapping("findEnterpriseInfo")
	public AjaxJson findEnterpriseInfo(String enterpriseId, String userId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseService.findEnterpriseInfo(enterpriseId.trim(), userId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询某公司ItemVO
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "查询某公司ItemVO", notes = "查询某公司ItemVO")
	@PostMapping("findEnterpriseItemVOByEnterpriseId")
	public AjaxJson findEnterpriseItemVOByEnterpriseId(String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseService.findEnterpriseItemVOByEnterpriseId(enterpriseId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}

	/**
	 * 发送短信验证码,用来解散公司
	 * @param permissionEnterpriseId 公司id
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "发送短信验证码,用来解散公司")
	@PostMapping("sendCodeForDissolveEnterpriseAndNeedPermission")
	public AjaxJson sendCodeForDissolveEnterprise(String permissionEnterpriseId, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseService.sendCodeForDissolveEnterprise(permissionEnterpriseId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;	
	}
	
	/**
	 * 解散公司
	 * @param permissionUserId 操作者id
	 * @param permissionEnterpriseId 公司id
	 * @param code 短信验证码
	 * @return
	 */
	@ApiOperation(value="解散公司",notes="解散公司")
	@PostMapping("dissolveEnterpriseAndNeedPermission")
	public AjaxJson dissolveEnterprise(String permissionUserId, String permissionEnterpriseId, String code){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(code)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("验证码为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseService.dissolveEnterprise(permissionUserId.trim(), permissionEnterpriseId.trim(), code.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询操作者当前公司id
	 * @param userId 操作者id
	 * @return
	 */
	@ApiOperation(value = "查询当前公司id", notes = "查询当前公司id")
	@PostMapping("findCurrentEnterpriseId")
	public AjaxJson findCurrentEnterpriseId(String userId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseService.findCurrentEnterpriseId(userId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
}

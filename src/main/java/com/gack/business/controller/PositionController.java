package com.gack.business.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.PositionServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-6-1
 */
@RestController
@RequestMapping(ApiController.POSITION_URL)
public class PositionController {

	@Autowired
	private PositionServiceInterface positionService;
	
	/**
	 * 查询某公司下所有职位
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "查询某公司下所有职位", notes = "查询某公司下所有职位")
	@PostMapping("findAllPosition")
	public AjaxJson findAllPosition(String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(positionService.findAllPosition(enterpriseId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 新增某公司的职位   不可重名
	 * @param permissionEnterpriseId 公司id
	 * @param positionName 职位名称
	 * @param permissionUserId 操作者id
	 * @param rankId 职级id
	 * @return
	 */
	@ApiOperation(value = "新增某公司的职位", notes = "新增某公司的职位")
	@PostMapping("insertPositionAndNeedPermission")
	public AjaxJson insertPosition(String permissionEnterpriseId, String positionName, String permissionUserId, String rankId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(positionName)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职位为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(rankId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职级为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(positionService.insertPosition(permissionEnterpriseId.trim(), positionName.trim(), rankId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 修改职位   不可重名
	 * @param permissionEnterpriseId 公司id
	 * @param positionId 职位id
	 * @param positionName 职位名称
	 * @param permissionUserId 操作者id
	 * @param rankId 职级id
	 * @return
	 */
	@ApiOperation(value = "修改职位", notes = "修改职位")
	@PostMapping("updatePositionAndNeedPermission")
	public AjaxJson updatePosition(String permissionEnterpriseId, String positionId, String positionName, String permissionUserId, String rankId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(positionId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职位为空").setData(null);
		}
		if(StringUtils.isBlank(positionName)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职位名称为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(StringUtils.isBlank(rankId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职级为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(positionService.updatePosition(permissionEnterpriseId.trim(), positionId.trim(), positionName.trim(), permissionUserId.trim(), rankId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 批量删除相应职位,并将相应用户的职位id置为空
	 * @param permissionEnterpriseId 公司id
	 * @param positionIds 职位id集合
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "批量删除相应职位,并将相应用户的职位id置为空", notes = "删除相应职位,并将相应用户的职位id置为空")
	@PostMapping("deletePositionByBatchAndNeedPermission")
	public AjaxJson deletePositionByBatch(String permissionEnterpriseId, String[] positionIds, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(positionIds == null || positionIds.length == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职位为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		for(int i=0; i<positionIds.length; i++){
			positionIds[i] = positionIds[i].replaceAll("\"", "");
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(positionService.deletePositionByBatch(permissionEnterpriseId.trim(), positionIds, permissionUserId.trim()));
		}catch(Exception e){
			if(e.getMessage().contains("---msg:")){			
				Map<String, Object> map = new HashMap<>();
				map.put("status", "error");
				map.put("msg", e.getMessage().substring(e.getMessage().indexOf("---msg:") + "---msg:".length()));
				return ajaxJson.setSuccess(true).setStatus(200).setData(map);
			}
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
}

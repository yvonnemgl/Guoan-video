package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.RemindTimeServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(ApiController.REMIND_TIME)
public class RemindTimeController {
	@Autowired
	private RemindTimeServiceInterface remindTimeServiceInterface;
	
	@ApiOperation("保存提醒时间")
	@PostMapping("/saveRemindTime")
	public AjaxJson saveRemindTime(Integer time) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(remindTimeServiceInterface.saveTime(time));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("获取提醒时间")
	@PostMapping("/findAll")
	public AjaxJson findAll() {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(remindTimeServiceInterface.findAll());
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}

}

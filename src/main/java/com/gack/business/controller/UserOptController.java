package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.UserOptServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: UserOptController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月1日 下午2:18:41 
* @version V1.0
 */
@RestController
@RequestMapping(ApiController.USER_OPERATOR_URL)
public class UserOptController {
	
	@Autowired
	private UserOptServiceInterface userOptService;
	
	@ApiOperation(value="获取用户列表",notes="获取用户列表")
	@PostMapping("/getUsers")
	public AjaxJson getUsers(){
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.setSuccess(true).setStatus(200).setData(null);
		return ajaxJson;
	}
}

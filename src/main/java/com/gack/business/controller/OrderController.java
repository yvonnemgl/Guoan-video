package com.gack.business.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.OrderService;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(ApiController.ORDER_URL)
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	@Autowired
	private HttpServletRequest request;
	/**
	 * 
	 * @Title: reservation
	 * @Description: 预约门店
	 * @param userId 用户ID
	 * @param storeId 门店ID
	 * @param enterpriseId 公司ID
	 * @return AjaxJson 
	 * @throws
	 */
	@PostMapping
	public AjaxJson reservation(String userId,String storeId,String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.reservationLock(userId, storeId, enterpriseId));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}

	/**
	 * 
	 * @Title: cancelReservation
	 * @Description: 取消预约
	 * @param id 订单ID
	 * @param cancelReason 取消原因
	 * @return AjaxJson 
	 * @throws
	 */
	@PostMapping("/cancel")
	public AjaxJson cancelReservation(String id,String cancelReason){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.cancelReservation(id, cancelReason,"1"));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @Title: begin
	 * @Description: 开始使用门店订单
	 * @param userId 用户ID
	 * @return AjaxJson 
	 * @throws
	 */
	@PostMapping("/begin")
	public AjaxJson begin(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.begin(id));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	/**
	 * 
	 * @Title: end
	 * @Description: 订单关闭
	 * @param id 订单ID
	 * @return AjaxJson 
	 * @throws
	 */
	@PostMapping("end")
	public AjaxJson end(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.end(id));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @Title: prePay
	 * @Description: 发起预支付订单
	 * @param id 订单ID
	 * @param departmentId 部门ID
	 * @param surplusAmount 使用金额
	 * @throws
	 */
//	@PostMapping("prePay")
	public AjaxJson prePay(String id,String departmentId,Integer surplusAmount){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.prePay(id, departmentId, surplusAmount));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @Title: pay
	 * @Description: 发起预支付订单
	 * @param id 订单ID
	 * @param channel 支付渠道
	 * @return AjaxJson 
	 * @throws
	 */
	@PostMapping("pay")
	public AjaxJson pay(String id,String channel){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.pay(id, channel,request));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @Title: backPay
	 * @Description: 支付系统回调接口
	 * @param pingId
	 * @return AjaxJson 
	 * @throws
	 */
	@GetMapping("backPay")
	public AjaxJson backPay(@RequestParam("pingId") String pingId){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.backPay(pingId,request));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	/**
	 * 
	 * @Title: userOrders
	 * @Description: 用户订单信息
	 * @param userId 用户ID
	 * @param page 页数 0开始
	 * @param pageSize 页面展示数据
	 * @return AjaxJson 
	 * @throws
	 */
	@GetMapping("user/{userId}")
	public AjaxJson userOrders(@PathVariable(value = "userId") String userId,@RequestParam(value = "page", defaultValue = "0") int page,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.findOrderVoByUserId(userId,page,pageSize));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @Title: userOrder
	 * @Description: 用户订单信息
	 * @param userId 用户ID
	 * @return AjaxJson 
	 * @throws
	 */
	@GetMapping("userState/{userId}")
	public AjaxJson userOrder(@PathVariable(value = "userId") String userId){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.userOrder(userId));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @Title: orderDetails
	 * @Description: 查看订单详情
	 * @param id 订单ID
	 * @return AjaxJson 
	 * @throws
	 */
	@GetMapping
	public AjaxJson orderDetails(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.orderDetails(id));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="获取用户使用中或者预定中的订单",notes="获取用户使用中或者预定中的订单")
	@PostMapping("getUserUsingOrReservedOrder")
	public AjaxJson getUserUsingOrReservedOrder(String userid){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.getUserUsingOrReservedOrder(userid));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg(null);
		}
		return ajaxJson;
	}

	@PostMapping("getAmout")
	public AjaxJson getAmout(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.getMoney(id));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg(null);
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="通过用户id查询预定或使用中中订单",notes="通过用户id查询预定或使用中详情")
	@PostMapping("findOrderformInfoByUserId")
	public AjaxJson findOrderformInfoByUserId(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(orderService.findOrderformInfoByUserId(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	@PostMapping("closeOrderByStoreName")
	public AjaxJson closeOrderByStoreName(String storeName){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(orderService.closeeOrderByStoreName(storeName));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg(null);
		}
		return ajaxJson;
	}
}
  
    
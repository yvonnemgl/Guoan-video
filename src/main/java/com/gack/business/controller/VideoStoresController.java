package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.VideoStoresService;
import com.gack.helper.common.AjaxJson;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("videostores")
public class VideoStoresController {
	
	@Autowired
	private  VideoStoresService  videoStoresService;
	
	  //通过当前用户的经纬度，查询附近的店铺
    
    /**
     *   SQL语句：（传入用户当前距离：蓝色为纬度，红色为经度）

		 SELECT *,SQRT(
		
		    POW(111.2 * (lat - 40.0844020000), 2) +
		
		    POW(111.2 * (116.3483150000 - lng) * COS(lat / 57.3), 2)) AS distance
		
		 FROM map HAVING distance < 25 ORDER BY distance;
		
		  得到结果：distance字段为 当前位置 到 所有店铺 的直线距离，单位km
     */
	 /**
     * 
     *  一期先返回所有门店列表,经纬度不考虑
     * @param longitude   当前用户位置经度
     * @param latitude    当前用户位置纬度    
     * @return
     */
	@ApiOperation(value = "查询附近店铺列表")
	@RequestMapping(value="findNearStores",method = RequestMethod.POST)
	public AjaxJson findNearStores(String longitude,String latitude,String id){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoStoresService.findNearStores(longitude,latitude,id));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}  
	
	
	@ApiOperation(value="门店详情",notes="门店详情")
	@PostMapping("findStoreInfo")
	public AjaxJson findStoreInfo(String id){
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoStoresService.findStoreInfo(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	
	
	

}

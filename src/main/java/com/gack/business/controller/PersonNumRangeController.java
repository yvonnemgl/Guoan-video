package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.PersonNumRangeServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-5-29
 */
@RestController
@RequestMapping(ApiController.PERSON_NUM_RANGE_URL)
public class PersonNumRangeController {

	@Autowired
	private PersonNumRangeServiceInterface personNumRangeService;
	
	/**
	 * 查询所有人员范围
	 * @return
	 */
	@ApiOperation(value = "查询所有人员范围", notes = "查询所有人员范围")
	@PostMapping("findAllPersonNumRange")
	public AjaxJson findAllPersonNumRange(){
		AjaxJson ajaxJson = new AjaxJson();
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(personNumRangeService.findAll());
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
}

package com.gack.business.controller;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.OSSClient;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;
import com.gack.helper.common.util.OSSUnitHelper;

import io.swagger.annotations.ApiOperation;

/**   
* @Title: UniteController.java 
* @Package com.gack.business.controller 
* @Description: TODO(用一句话描述该文件做什么) 
* @author （ZhangXinYu）  
* @date 2018年9月28日 下午3:51:05 
* @version V1.0   
*/
@RestController
@RequestMapping(ApiController.UNITE_URL)
public class UniteController {
	
	@ApiOperation(value="上传图片",notes="上传图片")
	@PostMapping("putImage")
	public AjaxJson putImage(MultipartFile contractlogo , HttpServletResponse response){
		AjaxJson ajaxJson=new AjaxJson();
		if(contractlogo!=null){
			OSSUnitHelper ossunit=new OSSUnitHelper();
			OSSClient client=OSSUnitHelper.getOSSClient();
			String diskName = "datas/video/fkb/";
			String fileName = new Date().getTime() + (int) ((Math.random() * 9 + 1) * 100000)
					+ contractlogo.getOriginalFilename().substring(contractlogo.getOriginalFilename().indexOf("."));
	        String md5key = OSSUnitHelper.uploadObject2OSS(client, contractlogo, diskName,fileName);
	        String url="";
	        url=OSSUnitHelper.getUrl(client, diskName, fileName);
	        String urll=    url.substring(4, url.length());
	        url="https"+urll;
	        System.out.println(url+"5");
	        if(url!=null&&!url.equals("")){
	        	ajaxJson.setSuccess(true).setStatus(200).setData(url);
	        	response.setContentType(url);
	        } 
		 }
		 return ajaxJson;
	}
}

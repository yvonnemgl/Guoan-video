package com.gack.business.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.model.User;
import com.gack.business.service.ContactsServiceInterface;

import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(ApiController.CONTACTS)
public class ContactsController {
	
	@Autowired
	private  ContactsServiceInterface  contactsServiceInterface;
	
	
	@ApiOperation(value = "查询好友列表(在联系人中)")
	@RequestMapping(value ="contactslist",method = RequestMethod.POST)
	public AjaxJson contactslist(String username,String nickname){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(contactsServiceInterface.selectContactsList(username, nickname));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value = "查询添加好友列表(在注册人员中查询)")
	@RequestMapping(value ="users",method = RequestMethod.POST)
	public AjaxJson users(String username){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(contactsServiceInterface.selectUserName(username));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	
	@ApiOperation(value = "查询新的朋友申请")
	@RequestMapping(value ="addcontacts",method = RequestMethod.POST)
	public AjaxJson addcontacts(String username){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(contactsServiceInterface.selectUserName(username));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}

	
	/**
	 * 修改某用户的好友备注
	 * @param userId 被修改者id
	 * @param operatorId 操作者id
	 * @param friendRemarks 好友备注
	 * @return
	 */
	@ApiOperation(value = "修改某用户的好友备注", notes = "修改某用户的好友备注")
	@PostMapping("updateFriendRemarks")
	public AjaxJson updateFriendRemarks(String userId, String operatorId, String friendRemarks){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(userId == null || userId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("被修改者为空").setData(null);
		}
		if(operatorId == null || operatorId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(contactsServiceInterface.updateFriendRemarks(userId.trim(), operatorId.trim(), friendRemarks));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	

	/**
	 * userUsername 自己的手机号，useridFriendUsername 好友的手机号
	* @Title: deletecontacts 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param username
	* @param userUsername
	* @return    设定文件 
	* @return AjaxJson    返回类型 
	* @throws 
	 */
	@ApiOperation(value = "删除好友")
	@RequestMapping(value ="deletecontacts",method = RequestMethod.POST)
	public AjaxJson deletecontacts(String userUsername,String useridFriendUsername){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			contactsServiceInterface.delectAllBycitycode(userUsername,useridFriendUsername);
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功");
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value = "用户注册补充信息")
	@RequestMapping(value ="updatecontactsforInformation",method = RequestMethod.POST)
	public AjaxJson updatecontactsforInformation(String username){
		AjaxJson ajaxJson=new AjaxJson();
		try {
			
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(contactsServiceInterface.updateContastsInformation(username));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}

}

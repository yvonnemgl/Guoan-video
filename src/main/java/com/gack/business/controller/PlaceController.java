package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.PlaceServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-6-4
 */
@RestController
@RequestMapping(ApiController.PLACE_URL)
public class PlaceController {

	@Autowired
	private PlaceServiceInterface placeService;
	
	/**
	 * 查询所有地址 省 省下的城市  城市下的城区
	 * @return
	 */
	@ApiOperation(value = "查询所有地址 省 省下的城市  城市下的城区", notes = "查询所有地址 省 省下的城市  城市下的城区")
	@PostMapping("findAllPlace")
	public AjaxJson findAllPlace(){
		AjaxJson ajaxJson = new AjaxJson();
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(placeService.findProvinceItem());
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
}

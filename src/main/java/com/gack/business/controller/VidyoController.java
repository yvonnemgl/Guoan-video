package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.VidyoSerivceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(ApiController.VIDYO)
public class VidyoController {
	@Autowired
	private VidyoSerivceInterface vidyoSerivceInterface;
	
	@ApiOperation("获取会议人员详情")
	@PostMapping("/getParticipants")
	public AjaxJson getParticipants(String roomId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(vidyoSerivceInterface.getParticipants(roomId)).setMsg("操作成功");
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("离会")
	@PostMapping("/leaveConference")
	public AjaxJson leaveConference(String conferenceID, String participantID) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(vidyoSerivceInterface.leaveConference(conferenceID, participantID)).setMsg("操作成功");
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("呼叫终端")
	@PostMapping("/ininviteToConference")
	public AjaxJson ininviteToConference(String conferenceID, String invite) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(vidyoSerivceInterface.ininviteToConference(conferenceID, invite)).setMsg("操作成功");
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}

}

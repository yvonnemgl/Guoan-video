package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.model.VersionNumber;
import com.gack.business.service.VersionNumberServiceInteface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(ApiController.VERSIONNUMBER_URL)
public class VersionNumberController {

	
	@Autowired  
	private  VersionNumberServiceInteface versionNumberServiceInteface;
	
	@ApiOperation(value="查询版本号", notes="查询版本号")
	@RequestMapping(value = "getVersionNumber",method = RequestMethod.POST)
	public AjaxJson getVersionNumber(){
		AjaxJson ajaxJson=new AjaxJson();
		try{
			VersionNumber s = versionNumberServiceInteface.getVersionNumber();
			if(s==null){
				ajaxJson.setSuccess(true).setStatus(230).setMsg("无版本号").setData("");
			}
			else{
				ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(s);
			}
			
		}catch (Exception e) {
			ajaxJson.setSuccess(true).setStatus(250).setMsg("操作成功").setData("查询错误");
		}
		return ajaxJson;
	} 
	
}

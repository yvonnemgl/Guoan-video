package com.gack.business.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.VideoSensitiveWordServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;
/**
 * 
 * @author ws
 * @date 2017-11-27
 */

@RestController
@RequestMapping(ApiController.VIDEO_SENSITIVE_WORD)
public class VideoSensitiveWordController {

	@Resource
	private VideoSensitiveWordServiceInterface videoSensitiveWordServiceInterface;
	
	
	@ApiOperation(value="获取字符串敏感字",notes="获取字符串敏感字")
	@PostMapping("sensitiveWordVerification")
	public AjaxJson sensitiveWordVerification (String word){
		AjaxJson ajaxJson=new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setMsg("操作成功").setData(videoSensitiveWordServiceInterface.sensitiveWordVerification(word));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	/**
	 * @description 向表t_video_sensitiveword中保存或修改敏感字,不能包含重复敏感字
	 * @param word 敏感字
	 * @param id 敏感字id
	 * @return AjaxJson
	 */
	@ApiOperation(value = "新增或修改敏感字", notes = "若id为空则为新增，否则为修改")
	@RequestMapping(value = "saveOrUpdateSensitiveWord", method = RequestMethod.POST)
	public AjaxJson saveOrUpdateSensitiveWord(String word, String id){
		AjaxJson ajaxJson=new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setMsg("操作成功").setData(videoSensitiveWordServiceInterface.saveOrUpdateSensitiveWord(word, id));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	/**
	 * @description 根据id删除敏感字记录
	 * @param id 敏感字id
	 * @return AjaxJson
	 */
	@ApiOperation(value = "删除敏感字记录", notes = "根据id删除敏感字记录")
	@RequestMapping(value = "deleteSensitiveWord", method = RequestMethod.POST)
	public AjaxJson deleteSensitiveWord(String id){
		AjaxJson ajaxJson=new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setMsg("操作成功").setData(videoSensitiveWordServiceInterface.deleteSensitiveWord(id));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	/**
	 * @description 根据敏感字模糊查询
	 * @param word 敏感字
	 * @param page 当前页数,从0开始
	 * @param size 页面中可显示记录数
	 * @return AjaxJson
	 */
	@ApiOperation(value = "根据敏感字模糊分页查询", notes = "根据敏感字模糊分页查询")
	@RequestMapping(value = "findByWord", method = RequestMethod.POST)
	public AjaxJson findByWord(String word, String page, String size){
		AjaxJson ajaxJson=new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setMsg("操作成功").setData(videoSensitiveWordServiceInterface.findByWord(word, page, size));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
}

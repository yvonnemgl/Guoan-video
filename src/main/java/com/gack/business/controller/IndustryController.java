package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.IndustryServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-5-29
 */
@RestController
@RequestMapping(ApiController.INDUSTRY_URL)
public class IndustryController {

	@Autowired
	private IndustryServiceInterface industryService;
	
	/**
	 * 查询一级行业,并带有子行业的嵌套关系
	 * @return
	 */
	@ApiOperation(value = "查询一级行业,并带有子行业的嵌套关系", notes = "查询一级行业,并带有子行业的嵌套关系")
	@PostMapping("findTopIndustry")
	public AjaxJson findTopIndustry(){
		AjaxJson ajaxJson = new AjaxJson();
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(industryService.findTopIndustry());
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 按照输入,查询行业
	 * @param inputName 输入
	 * @return
	 */
	@ApiOperation(value = "按照输入,查询行业", notes = "按照输入,查询行业")
	@PostMapping("findIndustryByInputName")
	public AjaxJson findIndustryByInputName(String inputName){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(inputName == null || inputName.trim().length()==0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("输入为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(industryService.findIndustryByInputName(inputName.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
}
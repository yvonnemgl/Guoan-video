package com.gack.business.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.DepartmentServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-5-31
 */
@RestController
@RequestMapping(ApiController.DEPARTMENT_URL)
public class DepartmentController {

	@Autowired
	private DepartmentServiceInterface departmentService;
	
	/**
	 * 查询某部门的下一级子部门
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @return
	 */
	@ApiOperation(value = "查询某部门的下一级子部门", notes = "查询某部门的下一级子部门")
	@PostMapping("findNextDepartmentItem")
	public AjaxJson findNextDepartmentItem(String enterpriseId, String departmentId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(departmentId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("部门为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(departmentService.findNextDepartmentItem(enterpriseId.trim(), departmentId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 用户查询某部门下的子部门信息以及当前部门人员信息(不包含子部门中人员信息)
	 * @param departmentId 部门id
	 * @param userId 用户id
	 * @return
	 */
	@ApiOperation(value = "查询该部门下的子部门信息以及当前部门的人员信息", notes = "查询该部门下的子部门信息以及当前部门的人员信息")
	@PostMapping("findSubDepartmentItemAndUserItem")
	public AjaxJson findSubDepartmentItemAndUserItem(String departmentId, String userId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(departmentId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("部门为空").setData(null);
		}
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(departmentService.findSubDepartmentItemAndUserItem(departmentId.trim(), userId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询该公司的一级部门,并带有子部门的嵌套关系
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "查询该公司一级部门ItemVO,并带有子部门的嵌套关系", notes = "查询该公司一级部门ItemVO,并带有子部门的嵌套关系")
	@PostMapping("findAllDepartmentItem")
	public AjaxJson findAllDepartmentItem(String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(departmentService.findAllDepartmentItem(enterpriseId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询该公司的一级部门,不带有子部门的嵌套关系
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "查询该公司一级部门ItemVO,不带有子部门的嵌套关系", notes = "查询该公司一级部门ItemVO,不带有子部门的嵌套关系")
	@PostMapping("findTopDepartmentItem")
	public AjaxJson findTopDepartmentItem(String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(departmentService.findTopDepartmentItem(enterpriseId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 新增部门  若父部门id为空,则新增一级部门
	 * @param permissionEnterpriseId 公司id
	 * @param permissionDepartmentId 父部门id
	 * @param newDepartmentName 新部门名称
	 * @param permissionUserId 新增部门操作的操作者
	 * @return
	 */
	@ApiOperation(value = "新增部门", notes = "新增部门")
	@PostMapping("insertDepartmentAndNeedPermission")
	public AjaxJson insertDepartment(String permissionEnterpriseId, String permissionDepartmentId, String newDepartmentName, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(permissionDepartmentId)){
			permissionDepartmentId = "0";//若父部门id为空,则新增一级部门
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(departmentService.insertDepartment(permissionEnterpriseId.trim(), permissionDepartmentId, newDepartmentName.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}

	/**
	 * 批量修改部门名称   不能出现重名情况
	 * @param permissionEnterpriseId 公司id
	 * @param departmentIdAndNewName 部门id和新名称     格式为:部门id@;-;@新名称;=;=;部门id@;-;@新名称
	 * @param permissionUserId 操作者id
	 * @param 
	 * @return
	 */
	@ApiOperation(value = "批量修改部门名称", notes = "批量修改部门名称")
	@PostMapping("updateDepartmentNameByBatchAndNeedPermission")
	public AjaxJson updateDepartmentNameByBatch(String permissionEnterpriseId, String departmentIdAndNewName, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(departmentIdAndNewName)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("部门id和新名称为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(departmentService.updateDepartmentNameByBatch(permissionEnterpriseId.trim(), departmentIdAndNewName.trim(), permissionUserId.trim()));
		}catch(Exception e){
			if(e.getMessage().contains("---msg:")){			
				Map<String, Object> map = new HashMap<>();
				map.put("status", "error");
				map.put("msg", e.getMessage().substring(e.getMessage().indexOf("---msg:") + "---msg:".length()));
				return ajaxJson.setSuccess(true).setStatus(200).setData(map);
			}
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 批量删除某些部门及其下属子部门,并将相应人员所在部门置为null,将相应权限删除,再将其父部门及父父部门的人数相应减少
	 * @param permissionEnterpriseId 公司id
	 * @param permissionDepartmentIds 部门id集合
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "删除某部门及其下属子部门", notes = "删除某部门及其下属子部门")
	@PostMapping("deleteDepartmentByBatchAndNeedPermission")
	public AjaxJson deleteDepartment(String permissionEnterpriseId, String[] permissionDepartmentIds, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();

		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(permissionDepartmentIds == null || permissionDepartmentIds.length  == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("部门为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		for(int i=0; i<permissionDepartmentIds.length; i++){
			permissionDepartmentIds[i] = permissionDepartmentIds[i].replaceAll("\"", "");
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(departmentService.deleteDepartmentByBatch(permissionEnterpriseId.trim(), permissionDepartmentIds, permissionUserId.trim()));
		}catch(Exception e){
			if(e.getMessage().contains("---msg:")){			
				Map<String, Object> map = new HashMap<>();
				map.put("status", "error");
				map.put("msg", e.getMessage().substring(e.getMessage().indexOf("---msg:") + "---msg:".length()));
				return ajaxJson.setSuccess(true).setStatus(200).setData(map);
			}
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询某公司的第一级部门以及每个一级部门下的人员(包含子子部门的人员)
	 * @param enterpriseId 公司id
	 * @param userId 用户id
	 * @return status:error/success msg:错误或成功消息  topDepartmentAndUserVOList:返回集合,按部门名称排序
	 */
	@ApiOperation(value = "查询某公司的第一级部门以及每个一级部门下的人员(包含子子部门的人员)", notes = "查询某公司的第一级部门以及每个一级部门下的人员(包含子子部门的人员)")
	@PostMapping("findTopDepartmentAndUserItemVO")
	public AjaxJson findTopDepartmentAndUserItemVO(String enterpriseId, String userId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(departmentService.findTopDepartmentAndUserItemVO(enterpriseId.trim(), userId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
}

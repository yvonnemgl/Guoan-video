package com.gack.business.controller;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.repository.JobRankRepository;
import com.gack.business.repository.PositionRepository;
import com.gack.business.service.JobRankServiceInterface;
import com.gack.business.service.JobRankSortServiceInterface;
import com.gack.business.vo.JobRankVO;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class TestController {

	@Autowired
	private JobRankServiceInterface jobRankService;
	@Autowired
	private JobRankSortServiceInterface jobRankSortService;
	@Autowired
	private JobRankRepository jobRankRepository;
	@Autowired
	private PositionRepository positionRepository;
	
	/**
	 * 
	 * @param enterpriseIds 公司表中所有公司id
	 */
	@PostMapping("setDefaultJobRankAndJobRankSort")
	@ApiOperation(value = "设置公司默认职级以及默认职级排序")
	public void setDefaultJobRankAndJobRankSort(String enterpriseIds){
		enterpriseIds = enterpriseIds.replaceAll("[^a-zA-Z0-9;]", "");
		String[] array = enterpriseIds.split(";");
		for(String element : array){
			jobRankService.setDefault(element);
			jobRankSortService.setDefault(element);
			System.out.println("=============================================");
			System.err.println(element);
			System.out.println("=============================================");
			System.out.println();
		}
	}
	
	/**
	 * 
	 * @param enterpriseIds 公司表中所有公司id
	 */
	@PostMapping("setJobRankForPosition")
	@ApiOperation(value = "为公司职位添加职级id")
	@Transactional
	public void setJobRankForPosition(String enterpriseIds){
		enterpriseIds = enterpriseIds.replaceAll("[^a-zA-Z0-9;]", "");
		String[] array = enterpriseIds.split(";");
		Date date = new Date();
		for(String element : array){
			List<JobRankVO> jobRankVOList = jobRankRepository.findVOByEnterpriseId(element);
			String jobRankId = jobRankVOList.get(0).getId();
			positionRepository.updateJobRankIdAndUpdateTimeByEnterpriseId(element, jobRankId, date);
			positionRepository.updateJobRankIdAndDeleteTimeByEnterpriseId(element, jobRankId, date);
			System.out.println("=============================================");
			System.err.println(element);
			System.out.println("=============================================");
			System.out.println();
		}
	}
	
//	@PostMapping("deleteJobRank")
//	@ApiOperation(value = "删除应该被删除的职级")
//	@Transactional
//	操作数据库即可
//	update jobrank
//	set status=0, delete_time=NOW()
//	where enterprise_id in (select id from enterprise where status = '4')
	
}

package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.ConferenceServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @ClassName: VideoConferenceController.java 
 * @Description: 会议室相关接口
 * @author lsy
 * @date 2018年5月25日   
 */
@RestController
@RequestMapping(ApiController.VIDEO_CONFERENCE)
public class ConferenceController {

	@Autowired
	private ConferenceServiceInterface videoConferenceService;

	/**
	 * 
	 * @Title: createConference
	 * @Description: 创建会议室
	 * @param name	会议室名称
	 * @param topic	会议室主题
	 * @param uid	创建者id
	 * @param joinId[]	参会人id数组
	 * @param type	会议室类型 1创建会议室 2 俩人拉会
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws 
	 * @date	2018年5月25日
	 * 
	 */
	@ApiOperation("创建会议室")
	@PostMapping("/createConference")
	public AjaxJson createConference(String name, String topic, String uid, String[] joinId, Integer type) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.createConference(name, topic, uid, joinId, type));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}

	/**
	 * @Description: 查询会议室列表 自建会议只有受邀请的人才能在会议列表中展现
	 * @Title: findConferences
	 * @param uid
	 *            用户id
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月25日
	 * 
	 */
	@ApiOperation("会议室列表")
	@PostMapping("/findConference")
	public AjaxJson findConference(String uid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.findConferences(uid));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}

	/**
	 * @Description: 搜索会议室
	 * @Title: searchConference
	 * @param code
	 *            会议室编号id 唯一
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月31日
	 * 
	 */
	@ApiOperation("搜索会议室")
	@PostMapping("/searchConference")
	public AjaxJson searchConference(String code) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.searchConference(code.toUpperCase()));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}

	/**
	 * @Description: 查询会议室详情
	 * @Title: conferenceDetails
	 * @param id
	 *            会议室id
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月31日
	 * 
	 */
	@ApiOperation("查询会议室详情")
	@PostMapping("/conferenceDetails")
	public AjaxJson conferenceDetails(String id) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.conferenceDetail(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}

	/**
	 * @Description: 会议室参与人
	 * @Title: getParticipants
	 * @param id
	 *            会议室id
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月31日
	 * 
	 */
	@ApiOperation("会议参与人")
	@PostMapping("/getParticipants")
	public AjaxJson getParticipants(String id, String uid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoConferenceService.getParticipants(id, uid));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}

	/**
	 * @Description: 更换管理员
	 * @Title: exchangeManager
	 * @param uid
	 *            用户id
	 * @param cid
	 *            会议室id
	 * @param managerId
	 *            管理员id
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月31日
	 * 
	 */
	@ApiOperation("更换管理员")
	@PostMapping("/exchangeManager")
	public AjaxJson exchangeManager(String uid, String cid, String managerId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.exchangeManager(uid, cid, managerId));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;

	}

	/**
	 * @Description:
	 * @Title: joinConference
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月31日
	 * 
	 */
	@ApiOperation("加入会议")
	@PostMapping("/joinConference")
	public AjaxJson joinConference(String uid, String cid, Integer channel) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.joinConference(uid, cid,channel));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	@ApiOperation("检查入会条件")
	@PostMapping("/checkJoin")
	public AjaxJson checkJoin(String uid, String cid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
			.setData(videoConferenceService.checkJoin(uid, cid));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}

	/**
	 * @Description: 删除会议室
	 * @Title: removeConference
	 * @param id
	 *            会议室id
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月31日
	 * 
	 */
	@ApiOperation("删除会议室")
	@PostMapping("/removeConference")
	public AjaxJson removeConference(String id) {
		AjaxJson ajaxJson = new AjaxJson();

		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.removeConference(id));
		} catch (Exception e) {
			ajaxJson.setSuccess(true).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}

	/**
	 * @Description: 删除vidyo 会议
	 * @Title: removeConference
	 * @param roomId
	 *            房间id
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月31日
	 * 
	 */
	@ApiOperation("删除Vidyo")
	@PostMapping("/removeVidyo")
	public AjaxJson removeVidyo(String roomId) {
		AjaxJson ajaxJson = new AjaxJson();

		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoConferenceService.removeVidyo(roomId));
		} catch (Exception e) {
			ajaxJson.setSuccess(true).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}

	/**
	 * @Description: 加入会议室
	 * @Title: addParticipant
	 * @param cid
	 *            会议室id
	 * @param uids
	 *            用户id集合拼接字符串
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月31日
	 * 
	 */
	@ApiOperation("拉人入会")
	@PostMapping("/addParticipant")
	public AjaxJson addParticipant(String cid, String[] uids, String uid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.addParticipant(cid, uids, uid));
			videoConferenceService.PushAPI(cid, "12", null);
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}

	/**
	 * @Description: 踢人
	 * @Title: removeParticipant
	 * @param cid
	 *            会议室id
	 * @param uids
	 *            用户id集合拼接字符串
	 * @param managerId
	 *            管理员id
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月31日
	 * 
	 */
	@ApiOperation("踢人")
	@PostMapping("/removeParticipant")
	public AjaxJson removeParticipant(String cid, String[] uids, String managerId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			videoConferenceService.PushAPI(cid, "12", null);
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.removeParticipant(cid, uids, managerId));
			
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}

	/**
	 * @Description: ios切应用调用接口
	 * @Title: iosLeave
	 * @param cid
	 *            会议室id
	 * @param uids
	 *            用户id集合拼接字符串
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws @date
	 *             2018年5月31日
	 * 
	 */
	@ApiOperation("ios切掉应用应该调用的接口")
	@PostMapping("/iosLeave")
	public AjaxJson iosLeave(String cid, String uid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoConferenceService.iosLeave(cid, uid));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;

	}

	/**
	 * @Description: 参会提醒设置 
	 * TODO 等安卓写到这里在协商开发
	 * @Title: beginRemind
	 * @param conferenceId 会议室id
	 * @param firstRemind  第一次提醒时间
	 * @param secondRemind 第二次提醒时间
	 * @return AjaxJson 返回值
	 * @author lsy
	 * @throws 
	 * @date 2018年5月31日
	 */

	@ApiOperation("参会提醒设置")
	@PostMapping("/beginRemind")
	public AjaxJson beginRemind(String conferenceId, Integer isSwitch,String beginTime,String firstTime,String secondTime, String id) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoConferenceService.beginRemind(conferenceId, isSwitch, beginTime, firstTime, secondTime,id));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	/**
	 * 
	 * @param cid
	 * @param uid
	 * @return
	 */
	@ApiOperation("搜索会议室后点击加入会议室")
	@PostMapping("/enterConference")
	public AjaxJson enterConference(String cid, String uid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.enterConference(cid, uid));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	@ApiOperation("左划离开会议")
	@PostMapping("/leaveConference")
	public AjaxJson leaveConference(String uid, String cid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.leaveConference(uid, cid));
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
		
	}
	
	@ApiOperation("更新6～8PIN码")
	@PostMapping("/updateConferenceCode")
	public AjaxJson updateConferenceCode(String cid,String uid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.updateConferenceCode(cid, uid));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	
	
	@ApiOperation("测试推")
	@PostMapping("/testPush")
	public AjaxJson testPush() {
		AjaxJson ajaxJson = new AjaxJson(); 
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(videoConferenceService.updateUserVideoDate());
		} catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		return ajaxJson;
	}
	
	@ApiOperation("更新会议名称")
	@PostMapping("/updateName")
	public AjaxJson updateName(String uid, String cid, String name) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.updateName(uid, cid, name));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("更新会议室主题")
	@PostMapping("/updateTopic")
	public AjaxJson updateTopic(String uid, String cid, String topic) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.updateTopic(uid, cid, topic));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("查询会议提醒")
	@PostMapping("/findRemindTime")
	public AjaxJson findRemindTime(String cid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.findRemindTime(cid));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("PC端查询会议室接口")
	@PostMapping("/findConferenes")
	public AjaxJson findConferenes(String conferenceCode) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.findConferene(conferenceCode.toUpperCase()));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("PC端最近显示的10条会议室")
	@PostMapping("/recentlyConference")
	public AjaxJson recentlyConference(String uid, @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "size", defaultValue = "10") Integer size) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.recentlyConference(uid, page, size));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("PC搜索加入会议室")
	@PostMapping("/toEnterConference")
	public AjaxJson toEnterConference(String uid, String cid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.toEnterConference(uid, cid));
			videoConferenceService.PushAPI(cid, "12", null);
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("呼叫外部终端设备")
	@PostMapping("/inviteToConference")
	public AjaxJson inviteToConference(String conferenceID, String invite) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoConferenceService.inviteToConference(conferenceID, invite));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	/**
	 * 已经废弃使用的代码
	 * @param conferenceID
	 * @param invite
	 * @return
	 */
	@ApiOperation("临时呼叫外部终端设备")
	@PostMapping("/JoininviteToConference")
	public AjaxJson JoininviteToConference(String conferenceID, String invite) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoConferenceService.JoininviteToConference(conferenceID.trim(), invite.trim()));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	/**
	 * 已经废弃使用的代码
	 * @param conferenceID
	 * @param participantID
	 * @return
	 */
	@ApiOperation("临时踢出外部终端设备")
	@PostMapping("/leaveinviteToConference")
	public AjaxJson leaveinviteToConference(String conferenceID, String participantID) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功").setData(videoConferenceService.leaveinviteToConference(conferenceID.trim(), participantID.trim()));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("在门店使用详情页面呼叫摄像头")
	@PostMapping("/joinConferenceCameras")
	public AjaxJson joinConferenceCameras(String conferenceCode, String storeCamerasId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.joinConferenceCameras(conferenceCode, storeCamerasId));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("在门店使用详情页面推出摄像头")
	@PostMapping("/leaveConferenceCameras")
	public AjaxJson leaveConferenceCameras(String storeCamerasId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.leaveConferenceCameras(storeCamerasId));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("创建会议室的时候呼叫摄像头")
	@PostMapping("/createAndJoinConferenceCameras")
	public AjaxJson createAndJoinConferenceCameras(String cid, String[] storeCamerasId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.createAndJoinConferenceCameras(cid, storeCamerasId));
			videoConferenceService.PushAPI(cid, "12", null);
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("详情页管理员移除设备")
	@PostMapping("/detailLeaveConferenceCamera")
	public AjaxJson detailLeaveConferenceCamera(String cid, String[] storeCamerasId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.detailLeaveConferenceCamera(cid, storeCamerasId));
			videoConferenceService.PushAPI(cid, "12", null);
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation("获取详情页摄像头")
	@PostMapping("/getConferenceCamera")
	public AjaxJson getConferenceCamera(String cid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setData(videoConferenceService.getConferenceCamera(cid));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败");
		}
		return ajaxJson;
		
	}

	
	
	

}

package com.gack.business.controller;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.OSSClient;
import com.gack.business.service.EnterpriseInfoServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.util.OSSUnitHelper;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: EnterpriseInfoController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年5月31日 下午12:52:35 
* @version V1.0
 */
@RestController
@RequestMapping("/enterpriseInfo")
public class EnterpriseInfoController {
	
	@Autowired
	EnterpriseInfoServiceInterface enterpriseInfoService;
	
	@ApiOperation(value="获取公司信息",notes="获取公司信息")
	@PostMapping("/getEnterpriseInfo")
	public AjaxJson getEnterpriseInfo(String enterpriseid){
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseInfoService.getEnterpriseInfo(enterpriseid));
		return ajaxJson;
	}
	
//	@ApiOperation(value="修改公司名称",notes="修改公司名称")
//	@PostMapping("/resetEnterpriseName")
//	public AjaxJson resetEnterpriseName(String userid,String enterpriseid,String newname){
//		AjaxJson ajaxJson = new AjaxJson();
//		ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseInfoService.resetEnterpriseName(userid, enterpriseid, newname));
//		return ajaxJson;
//	}
	
//	@ApiOperation(value="解散公司",notes="解散公司")
//	@PostMapping("/dissolvedEnterprise")
//	public AjaxJson dissolvedEnterprise(String userid,String enterpriseid){
//		AjaxJson ajaxJson = new AjaxJson();
//		ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseInfoService.dissolvedEnterprise(userid, enterpriseid));
//		return ajaxJson;
//	}
	
	
	/**
	 * 
	* @Title: commitEnterpriseAuthentiaction 
	* @Description: TODO(公司认证) 
	* @param @param name						//公司名称
	* @param @param contractName				//公司联系人名称
	* @param @param idNumber					//身份证号码(公司认证时填写的身份证号码)
	* @param @param unifiedSocialCreditCode		//统一社会信用码
	* @param @param detailedAddress				//公司详细地址
	* @param @param positiveIdCard				//身份证正面照片url
	* @param @param negativeIdCard				//身份证背面照片url
	* @param @param businessLicense				//营业执照照片url
	* @param @param administratorJobCertificate	//管理员在职证明url
	* @param @return    入参
	* @return AjaxJson    返回类型
	* @author （ZhangXinYu） 
	* @throws
	* @date 2018年5月31日 下午3:19:44 
	* @version V1.0
	 */
	@ApiOperation(value="提交公司认证",notes="提交公司认证")
	@PostMapping("/commitEnterpriseAuthentiaction")
	public AjaxJson commitEnterpriseAuthentiaction(
			String userid,
			String enterpriseid,
			String name,
			String contractName,
			String idNumber,
			String unifiedSocialCreditCode,
			String detailedAddress,
			String positiveIdCard,
			String negativeIdCard,
			String businessLicense,
			String administratorJobCertificate){
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.setSuccess(true).setStatus(200).setData(enterpriseInfoService.commitEnterpriseAuthentiaction(userid,enterpriseid,name, contractName,
				idNumber, unifiedSocialCreditCode, detailedAddress, positiveIdCard, negativeIdCard, businessLicense,
				administratorJobCertificate));
		return ajaxJson;
	}
	
	@ApiOperation(value="上传公司图片",notes="上传公司图片")
	@PostMapping("/putEnterpriseImg")
	public AjaxJson putImg(MultipartFile contractlogo , HttpServletResponse response){
		AjaxJson ajaxJson=new AjaxJson();
		if(contractlogo!=null){
			OSSUnitHelper ossunit=new OSSUnitHelper();
			OSSClient client=OSSUnitHelper.getOSSClient();
			String diskName = "datas/video/enterprise/";
			String fileName = new Date().getTime() + (int) ((Math.random() * 9 + 1) * 100000)
					+ contractlogo.getOriginalFilename().substring(contractlogo.getOriginalFilename().indexOf("."));
	        String md5key = OSSUnitHelper.uploadObject2OSS(client, contractlogo, diskName,fileName);
	        String url="";
	        url=OSSUnitHelper.getUrl(client, diskName, fileName);
	        String urll=    url.substring(4, url.length());
	        url="https"+urll;
	        System.out.println(url+"5");
	        if(url!=null&&!url.equals("")){
	        	ajaxJson.setSuccess(true).setStatus(200).setData(url);
	        	response.setContentType(url);
	        } 
		 }
		 return ajaxJson;
	}
}

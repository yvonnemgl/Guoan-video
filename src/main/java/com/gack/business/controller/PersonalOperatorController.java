package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.gack.business.service.UserService;
import org.springframework.web.bind.annotation.RestController;

import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

@RestController
@RequestMapping(ApiController.USER_URL)
public class PersonalOperatorController {

	@Autowired
	private UserService userService;
	
	/**
	 * 
	 * @Title: payDeposit
	 * @Description: 用户支付押金
	 * @param userId 用户ID
	 * @param channel 支付渠道
	 * @param amount 支付金额
	 * @return AjaxJson 
	 * @throws
	 */
	@PostMapping("/payDeposit")
	public AjaxJson payDeposit(String userId, String channel, String amount) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(userService.payDeposit(userId, channel, amount));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	/**
	 * 
	 * @Title: refundDeposit
	 * @Description: 用户退押金
	 * @param userId 用户ID
	 * @return AjaxJson 
	 * @throws
	 */
	@PostMapping("/refundDeposit")
	public AjaxJson refundDeposit(String userId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(userService.refundDeposit(userId));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	/**
	 * 
	 * @Title: refundDeposit
	 * @Description: 支付回调接口
	 * @param userId 用户ID
	 * @return AjaxJson 
	 * @throws
	 */
	@RequestMapping("/payedDeposit")
	public String payedDeposit(String userId,String oid,String channel,Integer amount) {
		try{
			userService.payedDeposit(userId,oid,channel,amount);
			return "SUCCESS";
		} catch (Exception e) {
			return "FAIL";
		}
	}
	/**
	 * 
	 * @Title: depositRecord
	 * @Description: 用户押金支付记录表
	 * @param userId 用户ID
	 * @return AjaxJson 
	 * @throws
	 */
	@RequestMapping("/depositRecord")
	public AjaxJson depositRecord(String userId) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setSuccess(true).setStatus(200).setMsg("操作成功")
					.setData(userService.findAllByUser(userId));
		} catch (Exception e) {
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData("error");
		}
		return ajaxJson;
	}
	
	
}

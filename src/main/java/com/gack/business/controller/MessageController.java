package com.gack.business.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.MessageServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: MessageController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月1日 下午5:13:32 
* @version V1.0
 */
	
@RestController
@RequestMapping(ApiController.MESSAGE_URL)
public class MessageController {

	@Autowired
	private MessageServiceInterface messageService;
	
	/**
	 * 对"邀请加入公司"类型的消息的处理
	 * @param messageId 消息id
	 * @param operation "agree" "refuse"
	 * @param remarks 用户输入的姓名
	 * @return
	 */
	@ApiOperation(value = "对'邀请加入公司'类型的消息的处理", notes = "对'邀请加入公司'类型的消息的处理")
	@PostMapping("dealMessage")
	public AjaxJson dealMessage(String messageId, String operation, String remarks){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(messageId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("消息为空").setData(null);
		}
		if(!"agree".equals(operation) && !"refuse".equals(operation)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作不正确").setData(null);
		}
		if("agree".equals(operation) && StringUtils.isBlank(remarks)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("姓名为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(messageService.dealMessage(messageId.trim(), operation.trim(), remarks));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 获取"邀请加入公司"类型的消息的备注
	 * @param messageId 消息id
	 * @return
	 */
	@ApiOperation(value = "获取'邀请加入公司'类型的消息的备注")
	@PostMapping("getRemarksAboutInvitationToEnterprise")
	public AjaxJson getRemarksAboutInvitationToEnterprise(String messageId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(messageId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("消息为空").setData(null);
		}

		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(messageService.getRemarksAboutInvitationToEnterprise(messageId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 删除消息
	 * @param messageId 消息id
	 * @return
	 */
	@ApiOperation(value = "删除消息")
	@PostMapping("deleteMessage")
	public AjaxJson deleteMessage(String messageId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(messageId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("消息为空").setData(null);
		}

		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(messageService.deleteMessage(messageId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	@ApiOperation(value="分页获取消息",notes="分页获取消息")
	@PostMapping("/messages")
	public AjaxJson getMessages(String userid,Integer currentPage,Integer pageSize){
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.setSuccess(true).setStatus(200).setData(messageService.getUserMessages(userid.trim(), currentPage, pageSize));
		return ajaxJson;
	}
	
	@ApiOperation(value="修改消息状态",notes="修改消息状态")
	@PostMapping("/setMessageRead")
	public AjaxJson setMessageRead(String messageId,@RequestParam(value="is_read",defaultValue="1") Integer is_read){
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.setSuccess(true).setStatus(200).setData(messageService.setMessageRead(messageId, is_read));
		return ajaxJson;
	}
	
	/**
	 * 0已读   1存在未读
	 */
	@ApiOperation(value="查询消息状态",notes="查询消息状态")
	@PostMapping("/getMessageStatus")
	public AjaxJson getMessageStatus(String userid){
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.setSuccess(true).setStatus(200).setData(messageService.getUserStatus(userid));
		return ajaxJson;
	}
	
	@ApiOperation(value="修改所有消息状态",notes="修改所有消息状态")
	@PostMapping("updateAllMessageIsRead")
	public AjaxJson updateAllMessageIsRead(String userid,@RequestParam(value="is_read",defaultValue="1")Integer is_read){
		AjaxJson ajaxJson = new AjaxJson();
		ajaxJson.setSuccess(true).setStatus(200).setData(messageService.updateAllMessageIsRead(userid,is_read));
		return ajaxJson;
	}
}

package com.gack.business.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.UserServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-5-30
 */
@RestController
@RequestMapping(ApiController.USER_URL)
public class UserController {

	@Autowired
	private UserServiceInterface userService;
	
	/**
	 * 根据查询名称,查找当前公司的用户(输入的查询名称应匹配用户应显示的名称(好友备注名或企业备注名))
	 * 根据查询手机号,查找当前公司用户(模糊查询)
	 * @param userId 操作者id
	 * @param enterpriseId 公司id
	 * @param inputName 用户输入的查询名称
	 * @return
	 */
	@ApiOperation(value = "根据名称或手机号,查找当前公司的用户", notes = "根据名称或手机号,查找当前公司的用户")
	@PostMapping("findUserByInput")
	public AjaxJson findUserByInput(String userId, String enterpriseId, String inputName){
		AjaxJson ajaxJson = new AjaxJson();

		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(inputName)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("输入为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.findUserItemVOByInput(userId.trim(), enterpriseId.trim(), inputName.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 用户查询无部门的人员信息
	 * @param enterpriseId 企业id
	 * @param userId 用户id
	 * @return
	 */
	@ApiOperation(value = "用户查询无部门的人员信息", notes = "用户查询无部门的人员信息")
	@PostMapping("findNoDepartmentUser")
	public AjaxJson findNoDepartmentUser(String enterpriseId, String userId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null); 
		}
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.findNoDepartmentUserItemVO(enterpriseId.trim(), userId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}

	/**
	 * 查询某用户的角色等级
	 * @param userId 用户id
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @return
	 */
	@ApiOperation(value = "查询某用户的角色等级", notes = "查询某用户的角色等级")
	@PostMapping("findRoleLevelByUserIdAndEnterpriseIdAndDepartmentId")
	public AjaxJson findRoleLevelByUserIdAndEnterpriseIdAndDepartmentId(String userId, String enterpriseId, String departmentId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(departmentId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("部门为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.findLevelByUserIdAndEnterpriseIdAndDepartmentId(userId.trim(), enterpriseId.trim(), departmentId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询某用户所在的公司。公司按创建时间,降序排序
	 * @param userId 用户id
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "查询某用户所在的公司,并将当前公司放在第一位", notes = "查询某用户所在的公司,并将当前公司放在第一位")
	@PostMapping("findEnterpriseListByUserId")
	public AjaxJson findEnterpriseListByUserId(String userId, String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.findEnterpriseItemVOListByUserId(userId.trim(), enterpriseId));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}

	/**
	 * 查询某用户管理的公司(在该公司是管理员权限),并将当前公司放在第一位。其他公司按创建时间,降序排序
	 * @param userId 用户id
	 * @param enterpriseId 公司id 可为空
	 * @param inviteeId 被邀请者id 可为空
	 * @return
	 */
	@ApiOperation(value = "查询某用户管理的公司(在该公司是管理员权限)", notes = "查询某用户管理的公司(在该公司是管理员权限)")
	@PostMapping("findManageEnterpriseListByUserId")
	public AjaxJson findManageEnterpriseListByUserId(String userId, String enterpriseId, String inviteeId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.findManageEnterpriseListByUserId(userId.trim(), enterpriseId, inviteeId));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 为用户设置企业备注
	 * @param userId 用户id
	 * @param permissionEnterpriseId 公司id
	 * @param enterpriseRemarks 企业备注
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "为用户设置企业备注", notes = "为用户设置企业备注")
	@PostMapping("setEnterpriseRemarksAndNeedPermission")
	public AjaxJson setEnterpriseRemarks(String userId, String permissionEnterpriseId, String enterpriseRemarks, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(enterpriseRemarks)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("企业备注为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.setEnterpriseRemarks(userId.trim(), permissionEnterpriseId.trim(), enterpriseRemarks.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}

	/**
	 * 为用户设置职位
	 * @param userId 用户id
	 * @param permissionEnterpriseId 公司id
	 * @param positionId 职位id
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "为用户设置职位", notes = "为用户设置职位")
	@PostMapping("setPositionAndNeedPermission")
	public AjaxJson setPosition(String userId, String permissionEnterpriseId, String positionId, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(positionId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("职位为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.setPosition(userId.trim(), permissionEnterpriseId.trim(), positionId.trim(), permissionUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 为用户设置部门
	 * @param userId 用户id
	 * @param permissionEnterpriseId 公司id
	 * @param departmentId 部门id
	 * @param permissionUserId 操作者id
	 * @return
	 */
	@ApiOperation(value = "为用户设置部门", notes = "为用户设置部门")
	@PostMapping("setDepartmentAndNeedPermission")
	public AjaxJson setDepartment(String userId, String permissionEnterpriseId, String departmentId, String permissionUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(departmentId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("部门为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.setDepartment(userId.trim(), permissionEnterpriseId.trim(), departmentId.trim(), permissionUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询某个联系人的详情,从搜索进入
	 * @param username 用户手机号
	 * @param showToUserId 向谁展示的用户id
	 * @return
	 */
	@ApiOperation(value = "查询某个联系人的详情,从搜索进入", notes = "查询某个用户的详情,从搜索进入")
	@PostMapping("findContactsDetailFromSearch")
	public AjaxJson findContactsDetailFromSearch(String username, String showToUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(username == null || username.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户手机号为空").setData(null);
		}
		if(showToUserId == null || showToUserId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("show用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.findContactsDetailFromSearch(username.trim(), showToUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询某个联系人的详情,从好友列表进入
	 * @param username 用户手机号
	 * @param showToUserId 向谁展示的用户id
	 * @return
	 */
	@ApiOperation(value = "查询某个联系人的详情,从好友列表进入", notes = "查询某个用户的详情,从好友列表进入")
	@PostMapping("findContactsDetailFromContactsList")
	public AjaxJson findContactsDetailFromContactsList(String username, String showToUserId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(username == null || username.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户手机号为空").setData(null);
		}
		if(showToUserId == null || showToUserId.trim().length() == 0){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("show用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.findContactsDetailFromContactsList(username.trim(), showToUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询某个用户在某个公司下的详情
	 * @param enterpriseId 公司id
	 * @param username 用户手机号
	 * @param showToUserId 向谁展示的用户id
	 * @return
	 */
	@ApiOperation(value = "查询某个用户在某个公司下的详情", notes = "查询某个用户在某个公司下的详情")
	@PostMapping("findUserDetailVO")
	public AjaxJson findUserDetailVO(String enterpriseId, String username, String showToUserId){
		AjaxJson ajaxJson = new AjaxJson();

		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(username)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户手机号为空").setData(null);
		}
		if(StringUtils.isBlank(showToUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("show用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.findUserDetailVO(enterpriseId.trim(), username.trim(), showToUserId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询某人在某公司下的关于公司管理的管理范围
	 * @param enterpriseId 公司id
	 * @param userId 用户id
	 * @return
	 */
	@ApiOperation(value = "查询某人在某公司下的关于公司管理的管理范围", notes = "查询某人在某公司下的关于公司管理的管理范围")
	@PostMapping("findManageRangeAboutEnterpriseManage")
	public AjaxJson findManageRangeAboutEnterpriseManage(String enterpriseId, String userId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		if(StringUtils.isBlank(userId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("用户为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(userService.findManageRangeAboutEnterpriseManage(enterpriseId.trim(), userId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	@ApiOperation(value="查看用户信息",notes="查看用户信息")
	@PostMapping("selectUser")
	public AjaxJson selectUser(String id){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(userService.findUser(id));
	}
	
}

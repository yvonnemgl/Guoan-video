package com.gack.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.model.VideoMessage;
import com.gack.business.service.VideoMessageServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: VideoMessageController.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月21日
*  
 */
@RestController
@RequestMapping(ApiController.MESSAGE_URL)
public class VideoMessageController {
	
	@Autowired
	private VideoMessageServiceInterface videoMessageService;
	
	@ApiOperation(value="获取用户消息",notes="获取用户消息")
	@PostMapping("getUserMessage")
	public AjaxJson getUserMessage(String id,Integer currentPage,Integer pageSize){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoMessageService.getUserMessage(id.trim(),currentPage,pageSize));
	}
	
	/**
	 * 
	* @Title: addUserMessage 
	* @Description: TODO(添加用户消息) 
	* @param @param id
	* @param @param message
	* @param @param type
	* @param @return    入参
	* @return AjaxJson    返回类型
	* @author （ZhangXinYu） 
	* @throws
	* @date 2018年3月21日 下午2:38:10 
	* @version V1.0
	* 
	* type： 消息类型   暂定1>>预约消息
	 */
	@ApiOperation(value="添加用户消息",notes="添加用户消息")
	@PostMapping("addUserMessage")
	public AjaxJson addUserMessage(String id,String message,String type){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoMessageService.addUserMessage(id.trim(),message.trim(),type));
	}
	/**
	 * 联系人添加 type 1从注册人员里添加，2是外部添加，发短信邀请
	* @Title: addNewUserMessage 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param username
	* @param friendid
	* @return    设定文件 
	* @return AjaxJson    返回类型 
	* @throws 
	 */
	@ApiOperation(value="添加消息",notes="添加消息")
	@PostMapping("addNewUserMessage")
	public AjaxJson addNewUserMessage(String username,String friendid,String type){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoMessageService.addNewUserMessage(username,friendid,type));
	}
	/**
	 * status 1是确认  2是拒绝
	* @Title: addNewContacts 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param username
	* @param friendid
	* @param status
	* @return    设定文件 
	* @return AjaxJson    返回类型 
	* @throws 
	 */
	@ApiOperation(value="确认添加好友或拒绝操作",notes="确认添加好友或拒绝操作")
	@PostMapping("addNewContacts")
	public AjaxJson addNewContacts(String username,String friendid,String  status){
		AjaxJson ajaxJson = new AjaxJson();
		return ajaxJson.setSuccess(true).setStatus(200).setData(videoMessageService.updateNewUserMessage(username,friendid,status));
	}
}







package com.gack.business.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.business.service.JobRankSortServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * @author ws
 * 2018-9-10
 */
@RestController
@RequestMapping(ApiController.JOB_RANK_SORT_URL)
public class JobRankSortController {

	@Autowired
	private JobRankSortServiceInterface jobRankSortService;
	
	/**
	 * 设置职级排序
	 * @param permissionEnterpriseId 公司id
	 * @param permissionUserId 操作者id
	 * @param sort 排序(DESC---等级越小，权限越高、ASC---等级越大,权限越高)
	 * @return
	 */
	@ApiOperation(value = "设置职级排序")
	@PostMapping("setSortAndNeedPermission")
	public AjaxJson setSort(String permissionEnterpriseId, String permissionUserId, String sort){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(permissionEnterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		if(StringUtils.isBlank(permissionUserId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("操作者为空").setData(null);
		}
		if(!"DESC".equals(sort) && !"ASC".equals(sort)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("排序规则为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(jobRankSortService.setSort(permissionEnterpriseId.trim(), sort));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
	/**
	 * 查询职级排序(DESC---等级越小，权限越高、ASC---等级越大,权限越高)
	 * @param enterpriseId 公司id
	 * @return
	 */
	@ApiOperation(value = "查询职级排序")
	@PostMapping("findSort")
	public AjaxJson findSort(String enterpriseId){
		AjaxJson ajaxJson = new AjaxJson();
		
		if(StringUtils.isBlank(enterpriseId)){
			return ajaxJson.setSuccess(false).setStatus(200).setMsg("公司为空").setData(null);
		}
		
		try{
			ajaxJson.setSuccess(true).setStatus(200).setData(jobRankSortService.findSort(enterpriseId.trim()));
		}catch(Exception e){
			e.printStackTrace();
			ajaxJson.setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
		}
		
		return ajaxJson;
	}
	
}

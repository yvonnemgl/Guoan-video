package com.gack.business.controller;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.OSSClient;
import com.gack.business.service.CertifiedServiceInterface;
import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;
import com.gack.helper.common.util.OSSUnitHelper;

import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: CertifiedController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月14日 上午10:24:13 
* @version V1.0
 */
@RestController
@RequestMapping(ApiController.CERTIFIED_URL)
public class CertifiedController {
	
	@Autowired
	private CertifiedServiceInterface certifiedService;
	
	@ApiOperation("检索是否已经认证")
	@PostMapping("/getpersonalAuthentication")
	public AjaxJson getpersonalAuthentication(String uid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setStatus(200).setSuccess(true).setData(certifiedService.getpersonalAuthentication(uid));
		} catch (Exception e) {
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败");	
		}
		return ajaxJson;
	}
	
	@ApiOperation("检索是否已经缴纳押金")
	@PostMapping("/getpersonaIsPledge")
	public AjaxJson getpersonaIsPledge(String uid) {
		AjaxJson ajaxJson = new AjaxJson();
		try {
			ajaxJson.setStatus(200).setSuccess(true).setData(certifiedService.getpersonaIsPledge(uid));
		} catch (Exception e) {
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败");	
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="个人身份认证",notes="个人身份认证")
	@PostMapping("personalAuthentication")
	public AjaxJson personalAuthentication(String userid,String name, String identify,String positiveIdCard,String negativeIdCard){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setData(certifiedService.personalAuthentication(userid,name, identify,positiveIdCard,negativeIdCard));
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	/**
	 * 
	* @Title: enterpriseCertification 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param name		公司名称		
	* @param @param identity	企业统一社会信用码
	* @param @param legalPerson	法人
	* @param @param detailedAddress	详细地址
	* @param @param businessLicense	公司营业执照url
	* @param @param administratorJobCertificate	公司管理员在职证明url
	* @param @return    入参
	* @return AjaxJson    返回类型
	* @author （ZhangXinYu） 
	* @throws
	* @date 2018年6月25日 上午8:24:25 
	* @version V1.0
	 */
	@ApiOperation(value="企业认证",notes="企业认证")
	@PostMapping("enterpriseCertification")
	public AjaxJson enterpriseCertification(String userid,String enterpriseid,String name,String identity,String legalPerson,
			String detailedAddress,String businessLicense,String administratorJobCertificate){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ajaxJson.setStatus(200).setSuccess(true).setData(certifiedService.enterpriseCertification(userid,enterpriseid,name, identity, legalPerson, detailedAddress,businessLicense,administratorJobCertificate));
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setMsg("操作失败");
		}
		return ajaxJson;
	}
	
	@ApiOperation(value="个人认证图片上传",notes="个人认证图片上传")
	@PostMapping("uploadPersonalPic")
	public AjaxJson putImg(MultipartFile contractlogo , HttpServletResponse response){
		AjaxJson ajaxJson=new AjaxJson();
		if(contractlogo!=null){
			OSSUnitHelper ossunit=new OSSUnitHelper();
			OSSClient client=OSSUnitHelper.getOSSClient();
			String diskName = "datas/video/personalAuthentication/";
			String fileName = new Date().getTime() + (int) ((Math.random() * 9 + 1) * 100000)
					+ contractlogo.getOriginalFilename().substring(contractlogo.getOriginalFilename().indexOf("."));
	        String md5key = OSSUnitHelper.uploadObject2OSS(client, contractlogo, diskName,fileName);
	        String url="";
	        url=OSSUnitHelper.getUrl(client, diskName, fileName);
	        String urll=    url.substring(4, url.length());
	        url="https"+urll;
	        System.out.println(url+"5");
	        if(url!=null&&!url.equals("")){
	        	ajaxJson.setSuccess(true).setStatus(200).setData(url);
	        	response.setContentType(url);
	        } 
		 }
		 return ajaxJson;
	}
}

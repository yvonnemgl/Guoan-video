package com.gack.business.service;

import java.util.Map;

/**
 * 
 * @author ws
 * 2018-9-10
 */
public interface JobRankServiceInterface {

	/**
	 * 设置默认职级
	 * @param enterpriseId 公司id
	 */
	void setDefault(String enterpriseId);

	/**
	 * 保存或修改职级
	 * @param enterpriseId 公司id
	 * @param rankId 职级id
	 * @param name 职级名称
	 * @param levle 职级等级
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> saveOrUpdate(String enterpriseId, String rankId, String name, Integer level);
	
	/**
	 * 删除职级
	 * @param enterpriseId 公司id
	 * @param userId 操作者id
	 * @param rankId 职级id
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> delete(String enterpriseId, String userId, String rankId);
	
	/**
	 * 查找公司下所有职级
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息  jobRankList:职级集合
	 */
	Map<String, Object> findAll(String enterpriseId);
	
	
	/**
	 * 查找职级下所有职位(未被删除)
	 * @param jobrankId 职级id
	 * @return status:error/success  msg:错误或成功消息  PositionList:positionVO集合
	 */
	Map<String, Object> findPosition(String jobrankId);
	
	/**
	 * 查找"职级使用帮助"
	 * @return status:error/success  msg:错误或成功消息  messageList:JobRankHelper集合
	 */
	Map<String, Object> findHelper();
}

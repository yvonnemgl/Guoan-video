package com.gack.business.service;

import java.util.Map;

/**
 * 
 * @author ws
 * 2018-6-20
 */
public interface AssignBusinessCardMoneyServiceInterface {

	/**
	 * 根据公司id,获取某公司的一级部门的名称以及剩余金额
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息  topDepartmentList:一级部门的名称以及剩余金额实体集合
	 */ 
	Map<String, Object> findTopDepartmentList(String enterpriseId);
	
	/**
	 * 根据公司id,获取某公司的一级部门的名称以及剩余金额(剩余可用额度大于0的一级部门)
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息  topDepartmentList:一级部门的名称以及剩余金额实体集合
	 */ 
	Map<String, Object> findAssignedTopDepartmentList(String enterpriseId);
	
	/**
	 * 根据公司id和部门id获取该部门可调配金额的最大值(即公司剩余可用额度+部门剩余可用额度)
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @return status:error/success msg:错误或成功消息  money:可调配金额的最大值
	 */
	Map<String, Object> findMaxCouldAssignedMoney(String enterpriseId, String departmentId);
	
	/**
	 * 设置某公司的某一级部门的剩余可用额度(在更新数据库前,需判定公司可用金额以及部门可用金额之和是否大于等于设定金额)
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @param money 金额(分)
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> setDepartmentSurplusAmount(String enterpriseId, String departmentId, String money) throws Exception;
}

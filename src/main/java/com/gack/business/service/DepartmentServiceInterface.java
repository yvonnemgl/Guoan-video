package com.gack.business.service;

import java.util.List;
import java.util.Map;

/**
 * 
 * @author ws
 * 2018-5-31
 */
public interface DepartmentServiceInterface {

	/**
	 * 查询某部门的下一级子部门
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @return status:error/success msg:错误或成功消息  departmentItemList:部门ItemVO集合
	 */
	Map<String, Object> findNextDepartmentItem(String enterpriseId, String departmentId);
	
	/**
	 * 用户查询某部门下的子部门信息以及当前部门人员信息(不包含子部门中人员信息)
	 * @param departmentId 部门id
	 * @param userId 用户id
	 * @return status:error/success  msg:错误或成功消息   hasSubDepartment:true/false hasUserItemList:true/false subDepartmentItemList:子部门ItemVO集合    userItemList:部门下人员ItemVO集合
	 */
	Map<String, Object> findSubDepartmentItemAndUserItem(String departmentId, String userId);

	/**
	 * 获取某部门下所有子部门id(全部子部门包含：层层嵌套子部门)
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @return
	 */
	List<String> findAllSubDepartmentId(String enterpriseId, String departmentId);

	/**
	 * 获取某部门的一级部门id
	 * @param departmentId 部门id
	 * @return
	 */
	String findTopParentIdByDepartmentId(String departmentId);
	
	/**
	 * 获取某部门的父部门,及其父父部门的id
	 * @param departmentId 部门id
	 * @return 父部门id集合
	 */
	public List<String> findAllParentDepartmentId(String departmentId);
	
	/**
	 * 获取某公司的一级部门ItemVO,并带有子部门的嵌套关系
	 * @param enterpriseId 公司id
	 * @return status:error/success  msg:错误或成功消息   topDepartmentItemList:一级部门ItemVO集合
	 */
	Map<String, Object> findAllDepartmentItem(String enterpriseId);
	
	/**
	 * 获取某公司的一级部门ItemVO,不带有子部门的嵌套关系
	 * @param enterpriseId 公司id
	 * @return status:error/success  msg:错误或成功消息   topDepartmentItemList:一级部门ItemVO集合
	 */
	Map<String, Object> findTopDepartmentItem(String enterpriseId);
	
	/**
	 * 新增部门   不能出现同名情况
	 * @param enterpriseId 公司id
	 * @param parentDepartmentId 父部门id
	 * @param newDepartmentName 新部门名称
	 * @return status:error/success  msg:错误或成功消息(hasName)
	 */
	Map<String, Object> insertDepartment(String enterpriseId, String parentDepartmentId, String newDepartmentName);
	
	/**
	 * 批量修改部门名称
	 * @param enterpriseId 公司id
	 * @param departmentIdAndNewName 部门id和新名称     格式为:部门id($%-)新名称&*=-&部门id($%-)新名称
	 * @param operatorUserId 操作者id
	 * @return  status:error/success  msg:错误或成功消息(hasName)
	 */
	Map<String, Object >updateDepartmentNameByBatch(String enterpriseId, String departmentIdAndNewName, String operatorUserId) throws Exception;
		
	/**
	 * 修改部门名称  若存在重名情况,则不能修改
	 * @param enterpriseId 父部门id
	 * @param departmentId 部门id
	 * @param newDepartmentName 部门新名称
	 * @param operatorUserId 操作者id
	 * @return status:error/success  msg:错误或成功消息(hasName)
	 */
	Map<String, Object> updateDepartmentName(String enterpriseId, String departmentId, String newDepartmentName, String operatorUserId);
	
	/**
	 * 批量删除某些部门
	 * @param enterpriseId 公司id
	 * @param departmentIds 部门id集合
	 * @param operatorId 操作者id
	 * @return
	 */
	Map<String, Object> deleteDepartmentByBatch(String enterpriseId, String[] departmentIds, String operatorId) throws Exception;
	
	/**
	 * 删除某部门及其下属子部门,并将相应人员所在部门置为null,将相应权限删除,再将其父部门及父父部门的人数相应减少
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @param operatorId 操作者id
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> deleteDepartment(String enterpriseId, String departmentId, String operatorId);
	
	/**
	 * 查询某公司的第一级部门以及每个一级部门下的人员(包含子子部门的人员)
	 * @param enterpriseId 公司id
	 * @param userId 操作者id
	 * @return status:error/success msg:错误或成功消息  topDepartmentAndUserVOList:返回集合,按部门名称排序
	 */
	Map<String, Object> findTopDepartmentAndUserItemVO(String enterpriseId, String userId);
}

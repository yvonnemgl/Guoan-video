package com.gack.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.model.RemindTime;
import com.gack.business.repository.RemindTimeRepository;
import com.gack.helper.common.abstractobj.Result;

@Service
public class RemindTimeService implements RemindTimeServiceInterface {
	@Autowired
	private RemindTimeRepository remindTimeRepository;

	@Override
	public Result saveTime(Integer time) {
		Result result = new Result();
		RemindTime remindTime = new RemindTime(); 
		remindTime.setTime(time);
		RemindTime saveAfter = remindTimeRepository.save(remindTime);
		if (saveAfter != null) {
			result.setKey("success");
			result.setValue("保存成功");
			return result;
		}
		result.setKey("error");
		result.setValue("保存失败");
		
		return result;
	}

	@Override
	public List<RemindTime> findAll() {
		
		return remindTimeRepository.findAll();
	}

}

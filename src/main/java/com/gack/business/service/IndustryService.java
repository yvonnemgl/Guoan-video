package com.gack.business.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gack.business.repository.IndustryRepository;
import com.gack.business.vo.IndustryVO;

/**
 * 
 * @author ws
 * 2018-5-29
 */
@Service
@Transactional
public class IndustryService implements IndustryServiceInterface{

	@Autowired
	private IndustryRepository industryRepository;
	
	/**
	 * 查询一级行业,并带有子行业的嵌套关系
	 */
	@Override
	public List<IndustryVO> findTopIndustry() {
		List<IndustryVO> allIndustryVO = industryRepository.findAllIndustryVO();
		
		for(IndustryVO t : allIndustryVO){
			for(IndustryVO t1 : allIndustryVO){
				if(t1.getParentId().equals(t.getId())){
					t.getSubIndustyList().add(t1);
				}
			}
		}
		
		List<IndustryVO> topIndustryVOList = new ArrayList<>();
		for(IndustryVO t : allIndustryVO){
			if(t.getParentId().equals("0")){
				topIndustryVOList.add(t);
			}
		}
		
		return topIndustryVOList;
	}
	
	/**
	 * 按照输入,查询公司名称
	 * @param inputName 输入
	 * @return
	 */
	@Override
	public List<IndustryVO> findIndustryByInputName(String inputName) {
		
		return industryRepository.findIndustryByInputName(inputName);
		
	}
	
}

package com.gack.business.service;

import java.util.Map;

import com.gack.business.model.StoreAttention;

/**
 * 
* @ClassName: StoreAttentionServiceInterface 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年9月6日 下午5:20:18 
* @version V1.0
 */
public interface StoreAttentionServiceInterface {

	/**
	 * 编辑店注意事项
	 */
	Map<String, Object> edit(String attention);
	
	/**
	 * 查询门店注意事项
	 */
	Map<String, Object> get();
	
	/**
	 * 查询门店注意事项
	 */
	StoreAttention attention();
}

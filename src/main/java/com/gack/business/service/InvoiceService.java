package com.gack.business.service;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.dao.UserDao;
import com.gack.business.model.Invoice;
import com.gack.business.model.User;
import com.gack.business.repository.InvoiceRepository;
import com.gack.business.repository.UserRepository;
import com.gack.helper.common.config.PayConfig;
import com.gack.helper.common.util.HttpGetUtil;

@Service
public class InvoiceService implements InvoiceServiceInterface {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	InvoiceRepository invoiceRepository;
	
	@Autowired
	UserDao  userDao;
	
	@Override
	public String save(String userId,String[] ids, int amount, String type, String content, String head, String number,
			String username, String phone, String address,String detail) {

		List<Invoice> invoices = invoiceRepository.findAll(Arrays.asList(ids));
		User user = userRepository.findOne(userId);
		StringBuffer orderIds = new StringBuffer();
		String kind = null;
		int totalAmonut = amount;
		String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
		for (Invoice invoice : invoices) {
			if(!invoice.getUsed())
				return "包含已开票订单";
			if(!invoice.getUid().equals(userId))
				return "用户信息异常";
			if(kind != null && !kind.equals(invoice.getKind())){
				return "提交类型错误";
			} else {
				kind = invoice.getKind();
			}
				
			invoice.setType(type);
			invoice.setContent(content);
			invoice.setHead(head);
			invoice.setNumber(number);
			invoice.setUsername(username);
			invoice.setPhone(phone);
			invoice.setAddress(address);
			invoice.setDetail(detail);
			invoice.setUsed(true);
			invoice.setUuid(uuid);
			amount -= invoice.getAmout();
			orderIds.append(invoice.getOid() + ",");
		}
		// 金额正确
		if(amount == 0) {
			// 调用押金支付接口
			String baseUrl = PayConfig.CREATE_INVOICE;
			StringBuffer url = new StringBuffer();
			
			/**
			 * 
			 * @Title: save
			 * @Description: 开发票接口
			 * @param ids LIST 集合 发票ID
			 * @param amount 开票总金额
			 * @param type 类型 0 个人发票 1企业发票
			 * @param content 发票内容
			 * @param head 发票抬头
			 * @param number 税号
			 * @param username 收件人
			 * @param phone 收件人手机号
			 * @param address 收件人地址
			 * @return AjaxJson 
			 * @throws
			 */
			try{
				url.append(baseUrl)
				.append("?create_user=").append(user.getId())
				.append("&account_num=").append(user.getUsername())
				.append("&type=").append("0")
				.append("&consume_type=").append(kind)
				.append("&kind=").append(type)
				.append("&amount=").append(totalAmonut)
				.append("&head=").append(URLEncoder.encode(head,"UTF-8"))
				.append("&number=").append(number)
				.append("&contact=").append(URLEncoder.encode(username,"UTF-8"))
				.append("&contact_phone=").append(phone)
				.append("&contact_address=").append(URLEncoder.encode(address + detail,"UTF-8"))
				.append("&orderids=").append(orderIds);
				
			}catch (Exception e) {
				e.printStackTrace();
			}
			String result = HttpGetUtil.httpGet(url.toString());
			if(result != null && result.equals("SUCCESS")) {
				invoiceRepository.save(invoices);
				return "SUCCESS";
			}
		}
		return "FAIL";

	}

	@Override
	public List<Invoice> get(String userId, String kind) {
		return invoiceRepository.findByUidAndKindAndUsed(userId, kind,false);    
	}

	@Override
	public Object getList(String userId) {
		return userDao.findRecordByuserId(userId);
		    
	}

}

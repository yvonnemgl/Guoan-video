package com.gack.business.service;

import java.util.List;

import com.gack.business.model.RemindTime;
import com.gack.helper.common.abstractobj.Result;

public interface RemindTimeServiceInterface {
	
	Result saveTime(Integer time);
	List<RemindTime> findAll();

}

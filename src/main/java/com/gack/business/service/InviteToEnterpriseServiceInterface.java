package com.gack.business.service;

import java.util.Map;

/**
 * 
 * @author ws
 * 2018-8-20
 */
public interface InviteToEnterpriseServiceInterface {

	/**
	 * 好友邀请列表
	 * @param inviterId 邀请者id
	 * @param enterpriseId 公司id
	 * @return  status:error/success msg:错误或成功消息 friendList:好友列表
	 */
	Map<String, Object> friendListForInvitation(String inviterId, String enterpriseId);
	
	/**
	 * 通过好友邀请渠道,发送邀请 
	 * @param inviterId 邀请者id
	 * @param enterpriseId 公司id
	 * @param inviteeIds 被邀请者id(id1;id2;id3)
	 * @return status:error/success  msg:错误或成功消息
	 */
	Map<String, Object> inviteByFriend(String inviterId, String enterpriseId, String inviteeIds);

	/**
	 * ①获取已加入公司的人员手机号②获取已被该用户邀请加入该公司的人员手机号
	 * @param inviterId 邀请者id
	 * @param enterpriseId 公司id
	 * @return status:error/success  msg:错误或成功消息   employeeUsernameList:已加入公司的人员手机号   waitingForPassUsername:已被邀请的人员手机号
	 */
	Map<String, Object> markContactsAttribute(String inviterId, String enterpriseId);
	
	/**
	 * 通过通讯录邀请渠道,发送邀请 
	 * @param inviterId 邀请者id
	 * @param enterpriseId 公司id
	 * @param inviteeUsernames  被邀请者手机号(手机号1;手机号2;手机号3)
	 * @return status:error/success  msg:错误或成功消息
	 */
	Map<String, Object> inviteByTelList(String inviterId, String enterpriseId, String inviteeUsernames);
	
	/**
	 * 通过手机号邀请渠道,发送邀请 
	 * @param inviterId 邀请者id
	 * @param enterpriseId 公司id
	 * @param inviteeUsername 被邀请者手机号
	 * @param remarks 备注
	 * @return status:error/success  msg:错误或成功消息
	 */
	Map<String, Object> inviteByPhoneNumber(String inviterId, String enterpriseId, String inviteeUsername, String remarks);
}

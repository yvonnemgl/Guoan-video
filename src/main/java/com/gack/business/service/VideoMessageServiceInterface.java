package com.gack.business.service;
/**
 * 
* @ClassName: VideoMessageServiceInterface.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月21日
*  
 */

import com.gack.business.model.VideoMessage;
import com.gack.helper.common.abstractobj.Result;

public interface VideoMessageServiceInterface {
	public Result getUserMessage(String id,Integer currentPage,Integer pageSize);
	public Result addUserMessage(String id,String message,String type);
	Result addNewUserMessage(String username,String friendid,String type);
	Result updateNewUserMessage(String username,String friendid,String  status);
}

package com.gack.business.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.dao.EnterpriseInfoDao;
import com.gack.business.model.Enterprise;
import com.gack.business.model.PersonalAuthentication;
import com.gack.business.model.User;
import com.gack.business.repository.EnterpriseRepository;
import com.gack.business.repository.PersonalAuthenticationRepository;
import com.gack.business.repository.UserRepository;
import com.gack.helper.common.abstractobj.Result;
import com.gack.helper.shangshangqian.BestsignClientTest;

/**
 * 
* @ClassName: CertifiedService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月14日 上午10:41:17 
* @version V1.0
 */
@Service
public class CertifiedService implements CertifiedServiceInterface{
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PersonalAuthenticationRepository personalRepository;
	@Autowired
	private EnterpriseRepository enterpriseRepository;
	@Autowired
	private EnterpriseInfoDao enterpriseInfoDao;

	/**
	 * 个人认证
	 * @param @param name    姓名
	 * @param @param identity	身份证号码
	 */
	@Override
	public Map<String, Object> personalAuthentication(String userid,String name, String identify,String positiveIdCard,String negativeIdCard) {
		Map<String , Object> map = new HashMap<>();
		if(userid == null || userid.equals("")){
			map.put("key", "error");
			map.put("msg", "用户id不可为空");
			return map;
		}
		User user = userRepository.findOne(userid);
		if(user == null){
			map.put("key", "error");
			map.put("msg", "用户不存在，操作失败");
			return map;
		}
		if(user.is_check() == true){
			map.put("key", "error");
			map.put("msg", "个人用户已认证，请勿重复认证");
			return map;
		}
		String result = BestsignClientTest.personalAuthentication(name, identify);
		if(result == null || result.equals("")){
			map.put("key", "error");
			map.put("msg", "个人信息有误，认证失败");
			return map;
		}
		//个人认证提交记录
		PersonalAuthentication personalAuthentication = new PersonalAuthentication();
		personalAuthentication.setName(name);
		personalAuthentication.setIdentify(identify);
		personalAuthentication.setPositiveIdCard(positiveIdCard);
		personalAuthentication.setNegativeIdCard(negativeIdCard);
		personalAuthentication.setCreateTime(new Date());
		personalAuthentication = personalRepository.save(personalAuthentication);
		//添加个人认证信息
		user.set_check(true);
		user.setRealName(name);
		user.setCheck_time(new Date());
		user = userRepository.save(user);
		map.put("key", "success");
		map.put("value", personalAuthentication);
		map.put("msg", "认证信息提交成功");
		return map;
	}

	/**
	 * 企业认证
	 * @param @param name		企业名称
	 * @param @param identity	工商注册号或统一社会信用代码，限中国大陆企业
	 * @param @param legalPerson		法定代表人姓名
	 * @param @param legalPersonIdentity		法定代表人证件号，目前仅中国大陆身份证号可验证
	 */
	@Override
	public Map<String, Object> enterpriseCertification(String userid,String enterpriseid,String name,String identity,String legalPerson,
			String detailedAddress,String businessLicense,String administratorJobCertificate) {
		Map<String, Object> map = new HashMap<>();
		if(userid == null || userid.equals("")){
			map.put("key", "error");
			map.put("msg", "用户id不可为空");
			return map;
		}
		if(enterpriseid == null || enterpriseid.equals("")){
			map.put("key", "error");
			map.put("msg", "公司id不可为空");
			return map;
		}
		User user = userRepository.findOne(userid);
		if(user == null){
			map.put("key", "error");
			map.put("msg", "用户不存在，操作失败");
			return map;
		}
		Enterprise enterprise = enterpriseRepository.findOne(enterpriseid);
		if(enterprise == null){
			map.put("key", "error");
			map.put("msg", "公司不存在,操作失败");
			return map;
		}
		if(enterprise.getStatus().equals("1")){
			map.put("key", "error");
			map.put("msg", "企业已成功认证,请勿重复认证");
			return map;
		}
		if(enterprise.getStatus().equals("2")){
			map.put("key", "error");
			map.put("msg", "企业认证申请中,请耐心等待认证结果");
			return map;
		}
		if(enterpriseInfoDao.getSameCertifiedEnterpriseNameCount(name).intValue() > 0){
			map.put("key", "error");
			map.put("msg", "该名称已认证或认证审核中,请重新提交认证");
			return map;
		}
		//个人未认证 不允许操作
		if(!user.is_check()){
			map.put("key", "error");
			map.put("msg", "用户未认证,无法进行企业认证");
			return map;
		}
		//公司认证只有公司创建者 既超级管理员有权限认证   
		//确认认证权限
//		boolean isCan = enterpriseInfoDao.validateIsCanEnterpriseSuperAdmin(userid, enterpriseid);
//		if(!isCan){
//			map.put("key", "error");
//			map.put("msg", "权限不足,企业认证操作失败");
//			return map;
//		}
		//企业三要素认证
		String result = BestsignClientTest.enterpriseCertificationThreePoint(name, identity, legalPerson);
		if(result == null || result.equals("")){
			map.put("key", "error");
			map.put("msg", "企业信息有误,认证失败");
			return map;
		}
		enterprise.setCertifiedName(name);
		enterprise.setContractName(legalPerson);
//		enterprise.setIdNumber(idNumber);
		enterprise.setUnifiedSocialCreditCode(identity);
		enterprise.setDetailedAddress(detailedAddress);
//		enterprise.setPositiveIdCard(positiveIdCard);
//		enterprise.setNegativeIdCard(negativeIdCard);
		enterprise.setBusinessLicense(businessLicense);
		enterprise.setAdministratorJobCertificate(administratorJobCertificate);
		enterprise.setCertificationSubmissionTime(new Date());
		enterprise.setStatus("2");
		enterprise = enterpriseRepository.save(enterprise);
		map.put("key", "success");
		map.put("msg", "企业认证提交成功");
//		String result = BestsignClientTest.enterpriseCertification(name, identity, legalPerson, legalPersonIdentity);
		return map;
	}
	
	/**
	 * @param @param uid 用户id 
	 */
	@Override
	public Result getpersonalAuthentication(String uid) {
		Result result = new Result();
		User user = userRepository.findOne(uid);
		if (user != null) {
			result.setKey("success");
			result.setValue(user.is_check());
		}
		return result;
	}

	/**
	 * @param @param uid 用户id 
	 */
	@Override
	public Result getpersonaIsPledge(String uid) {
		Result result = new Result();
		User user = userRepository.findOne(uid);
		if (user != null) {
			result.setKey("success");
			result.setValue(user.getIsPledge());
		}
		return result;
	}
	
}

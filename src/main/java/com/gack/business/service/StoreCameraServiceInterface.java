package com.gack.business.service;

import java.util.Map;

/**
 * 
* @ClassName: StoreCameraServiceInterface 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年7月11日 下午2:53:59 
* @version V1.0
 */
public interface StoreCameraServiceInterface {
	
	public Map<String, Object> getCamerasByUser(String userid);
	public Map<String, Object> getStoreCameras(String storeid);
	public Map<String, Object> getCameraStatus(String userid);
}

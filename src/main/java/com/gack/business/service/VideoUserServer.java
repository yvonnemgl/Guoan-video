package com.gack.business.service;



import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gack.business.dao.UserDao;
import com.gack.business.dao.UserLoginRecordDao;
import com.gack.business.model.Message;
import com.gack.business.model.PushSwitch;
import com.gack.business.model.User;
import com.gack.business.model.UserLoginRecord;
import com.gack.business.model.VideoIdentify;
import com.gack.business.model.VideoOpinion;
import com.gack.business.repository.MessageRepository;
import com.gack.business.repository.PushSwitchRepository;
import com.gack.business.repository.UserLoginRecordRepository;
import com.gack.business.repository.UserRepository;
import com.gack.business.repository.VideoIdentifyRepository;
import com.gack.business.repository.VideoOpinionRepository;
import com.gack.helper.aspect.DuplicateSubmitToken;
import com.gack.helper.common.abstractobj.Result;
import com.gack.helper.common.util.JDeviceRestAPI;
import com.gack.helper.common.util.JPushRestAPI;
import com.gack.helper.common.util.MD5;
import com.gack.helper.common.util.SendCode;
import com.gack.helper.common.util.SystemHelper;
import com.gack.helper.common.util.TokenUtils;
import com.gack.helper.redis.RedisClient;

/**
 * 
* @ClassName: VideoUserServer.java 
* @Description: 用户操作类
* @author Cancerl
* @date 2018年3月20日
*  
 */
@Service
public class VideoUserServer implements VideoUserServerInterface {
	
	@Autowired
	private VideoIdentifyRepository identifyRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private VideoOpinionRepository opinionRepository;
	@Autowired
	private UserLoginRecordRepository userLoginRecordRepository;
	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private ContactsServiceInterface conferenceService;
	@Autowired
	private UserDao userDao;
	@Autowired
	private VideoSensitiveWordServiceInterface sensitiveWordService;
	@Autowired
	private RedisClient redisClient;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private PushSwitchRepository pushSwitchRepository;
	@Autowired
	private UserLoginRecordDao userLoginRecordDao;
	
	/**
	 * 发送短信验证码
	 * result:: 1>>成功  -1>>失败
	 */
	@Override
	public Result sendIdentifyCode(String username){
		Result result = new Result();
		List<VideoIdentify> identifys = identifyRepository.findByUsername(username);
		VideoIdentify identify = null;
		//用户名已存在  直接修改验证码   不存在  新添加验证信息
		if(identifys != null && identifys.size() > 0){
			identify = identifys.get(0);
		}else{
			identify = new VideoIdentify();
		}
		String code = SystemHelper.getSixCode();
		identify.setUsername(username);
		identify.setIdentifycode(code);
		identify.setIdentifytime(new Date());
//		String msg = "尊敬的用户您好，您的短信验证码为 "+code+"，有效时间为5分钟，请及时使用";
		String msg = "您好，您的验证码是："+code+" ，有效时间5分钟。";
		try{
			SendCode.sendMessage(username, msg);
			result.setKey("success");
			result.setValue("验证码发送成功");
		}catch (Exception e) {
			e.printStackTrace();
			result.setKey("error");
			result.setKey("验证码发送失败，请再次尝试");
		}
		identifyRepository.save(identify);
		return result;
	}
	
	
	/**
	 * 发送短信验证码
	 * result:: 1>>成功  -1>>失败
	 */
	@Override
	public Result sendIdentifyCodeByRegistUser(String username){
		Result result = new Result();
		List<User> users = userRepository.findByUsername(username);
		if(users == null || users.size() == 0){
			result.setKey("error");
			result.setValue("该用户未注册,验证码发送失败");
			return result;
		}
		List<VideoIdentify> identifys = identifyRepository.findByUsername(username);
		VideoIdentify identify = null;
		//用户名已存在  直接修改验证码   不存在  新添加验证信息
		if(identifys != null && identifys.size() > 0){
			identify = identifys.get(0);
		}else{
			identify = new VideoIdentify();
		}
		String code = SystemHelper.getSixCode();
		identify.setUsername(username);
		identify.setIdentifycode(code);
		identify.setIdentifytime(new Date());
//		String msg = "尊敬的用户您好，您的短信验证码为 "+code+"，有效时间为5分钟，请及时使用";
		String msg = "您好，您的验证码是："+code+" ，有效时间5分钟。";
		try{
			SendCode.sendMessage(username, msg);
			result.setKey("success");
			result.setValue("验证码发送成功");
		}catch (Exception e) {
			e.printStackTrace();
			result.setKey("error");
			result.setValue("验证码发送失败，请再次尝试");
		}
		identifyRepository.save(identify);
		return result;
	}
	
	/**
	 * 用户注册
	 * result:: 1>>注册成功  -1>>失败，用户名已存在  -2>>失败,验证码超时  -3>>失败，验证码错误  -4>>失败，注册操作失败
	 */
	@DuplicateSubmitToken(type = DuplicateSubmitToken.REQUEST)
	@Transactional
	@Override
	public Result registUser(String username, String password, String identifyCode,String source) {
		Result result = new Result();
		Result validateIdentifyResult = this.validateIdentify(username, identifyCode);
		if(username == null || username.equals("")){
			result.setKey("error");
			result.setValue("用户名不可为空");
			return result;
		}
		if(password == null || password.equals("")){
			result.setKey("error");
			result.setValue("密码不可为空");
			return result;
		}
		if("success".equals(validateIdentifyResult.getKey())){
			//短信码验证通过
			//验证用户名	用户名存在 注册  否则 不注册
			User user = null;
			List<User> users = userRepository.findByUsername(username);
			if(users != null && users.size() > 0){
				//用户已存在，无法注册
				result.setKey("error");
				result.setValue("该用户已存在，无法重复注册");
			}else{
				//用户不存在  注册
				user = new User();
				user.setUsername(username);
				//密码md5加密
				try {
					password = MD5.md5(new String(password.getBytes(), "utf-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				user.setPassword(password);
				user.setCreatetime(new Date());
				user.setLoginNum(0);
//				user.setNickname(username.substring(0, 3)+"****"+username.substring(7));	//设置默认昵称
				//user.setPortait("https://guoanchuangke2.oss-cn-beijing.aliyuncs.com/datas/video/portait/1521611496322.jpg?Expires=4675210641&OSSAccessKeyId=LTAILWi7cyx4nHeI&Signature=mF%2BTnl0n/Ih15E0448KoUxtIQOM%3D");	//设置默认头像url
				user.setPortrait("https://guoanchuangke2.oss-cn-beijing.aliyuncs.com/datas/video/portait/1522745203889.png?Expires=4676344717&OSSAccessKeyId=LTAILWi7cyx4nHeI&Signature=6bsYvd8PTjbilQbWwBfUO1spm6U%3D");
				user.setRegister_source(source);
				user.setStatus(true);
				user.setExpense_amount(0);
				user.setExpense_num(0);
				user.set_check(false);
				user.setIsPledge(0);
				user.setCreate_office_num(0);
				user.setCreate_meeting_num(0);
				user.setJoin_video_num(0);
				user.setTotal_join_meeting_time(new Long(0));
				user.setTotal_office_time(new Long(0));
				user.setOrderState(0);
				user.setReset_password_time(new Date());
				user.setPassword_token(TokenUtils.getUUID());
				user = userRepository.save(user);
				//查询用户被邀请信息
				//messageRepository.updateUserBeInvisitedMessage(username, user.getId());
				//userDao.updateBeInvisitedMessage(username, user.getId());
				List<Message> messages = messageRepository.findUserBeVisitedMessage(username);
				for (Message message : messages) {
					message.setUserid(user.getId());
				}
				//用户注册补充信息
				if(user != null && user.getUsername() != null){
					conferenceService.updateContastsInformation(user.getUsername());
				}
				
				// 注册成功发送短信提示
				String smsMessage = "尊敬的先生/女士，公司为您创建的i商务账号已经生成，账号"+username+",请及时登录i商务客户端修改密码 <国安创客>。";
				try {
					SendCode.sendMessage(username, smsMessage);
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("注册成功短息模板发送失败");
				}
				
				result.setKey("success");
				if (source != null && source.equals("android")) {
					result.setValue("注册成功");
				}else{
					result.setValue(user);
				}
			}
		}else{
			//短信码验证失败
			result = validateIdentifyResult;
		}
		return result;
	}
	
	/**
	 * 用户名/密码 登录
	 * result :: 1>>登录成功  -1>>用户名不存在  -2>>密码错误
	 */
	@DuplicateSubmitToken(type = DuplicateSubmitToken.REQUEST)
	@Override
	public Result loginByPassword(String username, String password,String type,String equipmentid) {
		Result result = new Result();
		List<User> users = userRepository.findByUsername(username);
		if(users != null && users.size() > 0){
			//密码md5加密
			try {
				password = MD5.md5(new String(password.getBytes(), "utf-8"));
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			User user = users.get(0);
			if(user.getPassword() == null || user.getPassword().equals("") ){
				result.setKey("error");
				result.setValue("密码错误");
				return result;
			}
			if( !user.getUsername().equals(username) || !user.getPassword().equals(password)){
				result.setKey("error");
				result.setValue("密码错误");
				return result;
			}
			if(!user.isStatus()){
				result.setKey("error");
				result.setValue("该账户已被冻结，无法登录");
				return result;
			}
			//如果app登录  pc无法登录
			if("pc".equals(type)&&userLoginRecordRepository.getAndroidOrIosLoginCount(user.getId()) > 0){
				result.setKey("error");
				result.setValue("该账户已在app端登录，无法再次登录，请先在APP端登出");
				return result;
			}
			//如果pc登录  限制app及ios登录
			if((type.equals("android") || type.equals("ios"))&&userLoginRecordRepository.getPcLoginCount(user.getId()) > 0){
				result.setKey("error");
				result.setValue("该账户已在PC客户端登录,无法再次登录，请现在客户端登出");
				return result;
			}
			//判断用户是否存在登录状态  存在登录状态 提示账号已在异地登录  无法登录
			List<UserLoginRecord> userLogins = userLoginRecordRepository.findByUsernameAndLoginResource(user.getId(),type);
			// 只判断pc是否重复登录 && "pc".equals(userLogins.get(0).getType())
			//if(userLogins != null && userLogins.size() > 0 && type.equals("pc")){
			if(userLogins != null && userLogins.size() > 0){
				//如果是pc
				if ("pc".equals(type)){
					result.setKey("error");
					result.setValue("该账户已登录，无法再次登录");
					return result;
				}else if (!"null".equals(userLogins.get(0).getEquipment()) && !equipmentid.equals(userLogins.get(0).getEquipment())){
					//如果是app登录 但是不是在同一台设备上登录  不允许同时登录
					//如果登录记录中设备号为null 放行
					result.setKey("error");
					result.setValue("该账户已登录，无法再次登录");
					return result;
				}
			}else if (userLoginRecordRepository.getAndroidOrIosLoginCount(user.getId()) > 0){
				// android  ios 只允许 登录一个
				result.setKey("error");
				result.setValue("该账户已登录，无法再次登录");
				return result;
			}
			if(user.getPassword() != null && username.equals(user.getUsername()) && password.equals(user.getPassword())){
				//验证成功   记录用户登录
				/*VideoUserLogin userLogin = new VideoUserLogin();
				userLogin.setUser(user);
				userLogin.setLasttime(new Date());
				userLoginRepository.save(userLogin);*/
				//用户名密码正确
				saveUserLoginInfo(user,type,equipmentid);
				//用户 登录次数+1
				user.setLast_login_time(new Date());
				user = userRepository.save(user);
				userRepository.addOneLoginNum(user.getId());
				user.setLoginNum(user.getLoginNum() + 1);
				//android用户与  ios、pc返回数据区别   android返回userid   ios、pc返回user实体
				if(type.equals("android")){
					result.setKey("success");
					result.setValue(user.getId());
				}else{
					result.setKey("success");
					result.setValue(user);
				}
			}else{
				//密码错误
				result.setKey("error");
				result.setValue("密码错误");
			}
		}else{
			//用户名不存在
			result.setKey("error");
			result.setValue("您输入的手机号未注册");
		}
		return result;
	}

	/**
	 * 用户名/验证码登录
	 * result :: 1>>登录成功  -1>>用户名不存在  -2>>验证码超时  -3>>验证码错误  -4>>失败，其他错误
	 */
	@DuplicateSubmitToken(type = DuplicateSubmitToken.REQUEST)
	@Transactional
	@Override
	public Result loginByIdentifyCode(String username, String identifyCode,String type,String equipmentid) {
		Result result = new Result();
		Result validateIdentifyResult = this.validateIdentify(username, identifyCode);
		if("success".equals(validateIdentifyResult.getKey())){
			//验证用户名存在
			List<User> users = userRepository.findByUsername(username);
			if(users != null && users.size() > 0){
				//验证成功  记录用户登录状态
				User user = users.get(0);
				if(!user.isStatus()){
					result.setKey("error");
					result.setValue("该账户已被冻结，无法登录");
					return result;
				}
				//如果app登录  pc无法登录
				if("pc".equals(type)&&userLoginRecordRepository.getAndroidOrIosLoginCount(user.getId()) > 0){
					result.setKey("error");
					result.setValue("该账户已在app端登录，无法再次登录，请先在APP端登出");
					return result;
				}
				//如果pc登录  限制app及ios登录
				if((type.equals("android") || type.equals("ios"))&&userLoginRecordRepository.getPcLoginCount(user.getId()) > 0){
					result.setKey("error");
					result.setValue("该账户已在PC客户端登录,无法再次登录，请现在客户端登出");
					return result;
				}
				//判断用户是否存在登录状态  存在登录状态 提示账号已在异地登录  无法登录
				List<UserLoginRecord> userLogins = userLoginRecordRepository.findByUsernameAndLoginResource(user.getId(), type);
				// 只判断pc是否重复登录 && "pc".equals(userLogins.get(0).getType())
				//if(userLogins != null && userLogins.size() > 0 && type.equals("pc")){
				if(userLogins != null && userLogins.size() > 0){
					//如果是pc
					if ("pc".equals(type)){
						result.setKey("error");
						result.setValue("该账户已登录，无法再次登录");
						return result;
					}else if (!"null".equals(userLogins.get(0).getEquipment()) && !equipmentid.equals(userLogins.get(0).getEquipment())){
						//如果是app登录 但是不是在同一台设备上登录  不允许同时登录
						//如果登录记录中设备号为null 放行
						result.setKey("error");
						result.setValue("该账户已登录，无法再次登录");
						return result;
					}
				}else if (userLoginRecordRepository.getAndroidOrIosLoginCount(user.getId()) > 0){
					// android  ios 只允许 登录一个
					result.setKey("error");
					result.setValue("该账户已登录，无法再次登录");
					return result;
				}
				saveUserLoginInfo(user,type,equipmentid);
				user.setLast_login_time(new Date());
				user = userRepository.save(user);
				//用户 登录次数+1
				userRepository.addOneLoginNum(user.getId());
				user = userRepository.findOne(user.getId());
				//用户存在   短信码通过   验证成功
//				result.setKey("success");
//				result.setValue(users.get(0));
				//android用户与  ios、pc返回数据区别   android返回userid   ios、pc返回user实体
				if(type.equals("android")){
					result.setKey("success");
					result.setValue(users.get(0).getId());
				}else{
					result.setKey("success");
					result.setValue(users.get(0));
				}
			}else{
				//用户名不存在
				//result.setKey("error");
				//result.setValue("该用户不存在，请确认用户号码是否已成功注册");
				//用户不存在   新注册用户
				try{
					User user = new User();
					user.setUsername(username);
					user.setCreatetime(new Date());
					user.setLoginNum(1);
//					user.setNickname(username.substring(0, 3)+"****"+username.substring(7));	//设置默认昵称
//					user.setPortrait("https://guoanchuangke2.oss-cn-beijing.aliyuncs.com/datas/video/portait/1521611496322.jpg?Expires=4675210641&OSSAccessKeyId=LTAILWi7cyx4nHeI&Signature=mF%2BTnl0n/Ih15E0448KoUxtIQOM%3D");	//设置默认头像url
					user.setPortrait("https://guoanchuangke2.oss-cn-beijing.aliyuncs.com/datas/video/portait/1522745203889.png?Expires=4676344717&OSSAccessKeyId=LTAILWi7cyx4nHeI&Signature=6bsYvd8PTjbilQbWwBfUO1spm6U%3D");	//设置默认头像url
					user.setStatus(true);
					user.setExpense_amount(0);
					user.setExpense_num(0);
					user.set_check(false);
					user.setIsPledge(0);
					user.setCreate_office_num(0);
					user.setCreate_meeting_num(0);
					user.setJoin_video_num(0);
					user.setTotal_join_meeting_time(new Long(0));
					user.setTotal_office_time(new Long(0));
					user.setOrderState(0);
					user.setRegister_source(type);
					user.setLast_login_time(new Date());
					user.setReset_password_time(new Date());
					user.setPassword_token(TokenUtils.getUUID());
					user = userRepository.save(user);
					//查询用户被邀请信息
					//messageRepository.updateUserBeInvisitedMessage(username, user.getId());
					//userDao.updateBeInvisitedMessage(username, user.getId());
					List<Message> messages = messageRepository.findUserBeVisitedMessage(username);
					for (Message message : messages) {
						message.setUserid(user.getId());
					}
					//记录用户登录状态
					saveUserLoginInfo(user,type,equipmentid);
//					user.setLast_login_time(new Date());
//					user = userRepository.save(user);
					//用户 登录次数+1
//					userRepository.addOneLoginNum(user.getId());
//					user = userRepository.findOne(user.getId());
//					result.setKey("success");
//					result.setValue(user);
					//android用户与  ios、pc返回数据区别   android返回userid   ios、pc返回user实体
					//用户注册补充信息
					if(user != null && user.getUsername() != null){
						conferenceService.updateContastsInformation(user.getUsername());
					}
					
					// 注册成功发送短信提示
					String smsMessage = "尊敬的先生/女士，公司为您创建的i商务账号已经生成，账号"+username+",请及时登录i商务客户端修改密码 <国安创客>。";
					try {
						SendCode.sendMessage(username, smsMessage);
					} catch (Exception e) {
						e.printStackTrace();
						System.out.println("注册成功短息模板发送失败");
					}
					
					if(type.equals("android")){
						result.setKey("success");
						result.setValue(user.getId());
					}else{
						result.setKey("success");
						result.setValue(user);
					}
				}catch (Exception e) {
					e.printStackTrace();
					result.setKey("error");
					result.setValue("登录失败，创建快捷账号失败");
				}
			}
		}else{
			//短信验证码  验证不通过
			result = validateIdentifyResult;
		}
		return result;
	}
	
	/**
	 * 保存用户登录记录
	 */
	public void saveUserLoginInfo(User user,String login_resource,String equipmentid){
		//List<VideoUserLogin> userLogins = userLoginRepository.findByUser(user);
		List<UserLoginRecord> userLogins = userLoginRecordRepository.findByUsernameAndLoginResource(user.getId(), login_resource);
		if(userLogins != null && userLogins.size() > 0){
			UserLoginRecord userLogin = userLogins.get(0);
			userLogin.setLogin_time(new Date());
//			userLogin.setSession(request.getSession().getId());
			userLogin.setLogin_resource(login_resource);
			userLogin.setEquipment(equipmentid);
			userLoginRecordRepository.save(userLogin);
		}else{
			UserLoginRecord userLogin = new UserLoginRecord();
			userLogin.setUsername(user.getId());
			userLogin.setLogin_time(new Date());
			userLogin.setLogin_resource(login_resource);
			userLogin.setEquipment(equipmentid);
			userLoginRecordRepository.save(userLogin);
		}
		if(login_resource !=null && login_resource.equals("pc")){
			redisClient.setex("login>>"+user.getId(), 7200 ,equipmentid);
		}
	}
	
	/**
	 * 验证短信验证码
	 * result ：： 1>>验证成功  -2>>验证超时  -3>>验证码错误  -4>>失败，其他错误
	 */
	public Result validateIdentify(String username,String identifyCode){
		Result result = new Result();
		List<VideoIdentify> identifys = identifyRepository.findByUsername(username);
		if(identifys != null && identifys.size() > 0 && identifys.get(0).getIdentifytime() != null){
			VideoIdentify identify = identifys.get(0);
			Date nowDate = new Date();
			long alongTime = 0;	//时间差
			alongTime = nowDate.getTime() - identify.getIdentifytime().getTime();
			if(alongTime > 1000*60*5){
				//登录超时  登录失败
				result.setKey("error");
				result.setValue("验证码超时，请重新获取验证码");
			}else if(!identifyCode.equals(identify.getIdentifycode())){
				//验证码错误
				result.setKey("error");
				result.setValue("验证码错误，请确认验证码");
			}else{
				//验证通过
				result.setKey("success");
				result.setValue("验证码验证通过");
			}
		}else{
			//该用户名不存在验证码信息   验证失败
			result.setKey("error");
			result.setValue("验证失败，请先点击获取短信验证码");
		}
		return result;
	}

	/**
	 * 找回密码/重置密码(登录外忘记密码/修改密码)
	 * result  :: 密码重置成功   验证码错误，  验证码超时   手机账户不存在  
	 */
	@Override
	public Result resetPassword(String username, String identifyCode, String password) {
		Result result = new Result();
		Result validateIdentifyResult = this.validateIdentify(username, identifyCode);
		if("success".equals(validateIdentifyResult.getKey())){
			//验证用户名存在
			List<User> users = userRepository.findByUsername(username);
			if(users != null && users.size() > 0){
				//用户存在   短信码通过   验证成功
				User user = users.get(0);
				//密码md5加密
				try {
					password = MD5.md5(new String(password.getBytes(), "utf-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				user.setPassword(password);
				user.setPassword_token(TokenUtils.getUUID());
				//设置密码修改时间
				user.setReset_password_time(new Date());
				user = userRepository.save(user);
				//清除  android   ios  登录记录  跳转登录 重新登录
				userLoginRecordRepository.dealAndroidOrIosLoginRecord(user.getId());
				redisClient.remove("login>>"+user.getId());
				//推送 密码被修改 重新登录
				//推送时  content 推送当前登录的设备号  如果推送接收用户id处于登录状态   提示 密码被修该  跳转登录
//				this.sendReLoginPush("密码已被修改，请重新登录", "resetPassword", user.getId(), user.getUsername());
				this.sendReLoginPush("", "23", user.getId(), user.getUsername());
				result.setKey("success");
				result.setValue("密码修改成功");
			}else{
				//用户名不存在
				result.setKey("error");
				result.setValue("您输入的手机号未注册");
			}
		}else{
			//短信验证码  验证不通过
			result = validateIdentifyResult;
		}
		return result;
	}

	/**
	 * 意见反馈
	 * 提交成功   提交失败
	 */
	@Override
	public Result feedbackOpinion(String id, String opiniontext,String source) {
		Result result = new Result();
		User user = userRepository.findOne(id);
		if(user != null){
			try{
				if(opiniontext == null || opiniontext.equals("")){
					result.setKey("error");
					result.setValue("意见反馈不可为空");
					return result;
				}
				Set<String> words = sensitiveWordService.sensitiveWordVerification(opiniontext);
				if(words != null && words.size() > 0){
					result.setKey("error");
					result.setValue("意见反馈存在敏感词汇");
					return result;
				}
				if(userDao.getUserTodayOpinionCount(user.getId(),source)>0){
					result.setKey("error");
					result.setValue("您今天的反馈次数已用完");
					return result;
				}
				VideoOpinion userOpinion = new VideoOpinion();
				userOpinion.setOpiniontext(opiniontext);
				userOpinion.setCreateTime(new Date());
				userOpinion.setStatus(0);
				userOpinion.setUserid(user.getId());
				userOpinion.setSource(source);
				opinionRepository.save(userOpinion);
				result.setKey("success");
				result.setValue("提交成功");
			}catch (Exception e) {
				e.printStackTrace();
				result.setKey("error");
				result.setValue("提交失败");
			}
		}else{
			result.setKey("error");
			result.setValue("用户不存在，意见反馈提交失败");
		}
		return result;
	}
	
	/**
	 * 修改昵称
	 * 修改成功   修改失败
	 */
	@Override
	public Result resetNickname(String id, String nickname) {
		Result result = new Result();
		User user = userRepository.findOne(id);
		if(user != null){
			try{
				user.setNickname(nickname);
				user.setUpdate_name_time(new Date());
				user = userRepository.save(user);
				result.setKey("success");
				List<UserLoginRecord> userLoginRecord = userLoginRecordRepository.getAndroidOrIosLoginRecode(user.getId());
				if(userLoginRecord != null && userLoginRecord.size() > 0 && userLoginRecord.get(0).getLogin_resource().equals("adroid")){
					result.setValue("昵称修改成功");
				}else{
					result.setValue(user);
				}
			}catch (Exception e) {
				e.printStackTrace();
				result.setKey("error");
				result.setValue("昵称修改失败");
				result.setMsg("昵称修改失败");
			}
		}else{
			result.setKey("error");
			result.setValue("用户不存在，昵称修改失败");
			result.setMsg("用户不存在，昵称修改失败");
		}
		return result;
	}

	/**
	 * 忘记旧密码(app登录后修改密码)
	 * result  :: 密码修改成功   验证码错误，  验证码超时   手机账户不存在  
	 */
	@Override
	public Result forgetOldPassword(String id, String password, String identifyCode) {
		Result result = new Result();
		User user = userRepository.findOne(id);
		if(user != null){
			Result validateIdentifyResult = this.validateIdentify(user.getUsername(), identifyCode);
			if("success".equals(validateIdentifyResult.getKey())){
				//验证码通过  修改密码
				try{
					//密码md5加密
					try {
						password = MD5.md5(new String(password.getBytes(), "utf-8"));
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					user.setPassword(password);
					user.setPassword_token(TokenUtils.getUUID());
					user.setReset_password_time(new Date());
					user = userRepository.save(user);
					//清除  android   ios  登录记录  跳转登录 重新登录
					userLoginRecordRepository.dealAndroidOrIosLoginRecord(id);
					redisClient.remove("login>>"+user.getId());
					result.setKey("success");
					result.setValue("密码修改成功");
				}catch (Exception e) {
					e.printStackTrace();
					result.setKey("error");
					result.setValue("密码修改失败");
				}
			}else{
				//短信验证码  验证不通过
				result = validateIdentifyResult;
			}
		}else{
			result.setKey("error");
			result.setValue("您输入的手机号未注册");
		}
		return result;
	}

	/**
	 * 获取用户信息
	 */
	@Override
	public Result getUserInfo(String id) {
		Result result = new Result();
		User user = userRepository.findOne(id);
		if (user != null) {
			result.setKey("success");
			result.setValue(user);
		}else{
			result.setKey("error");
			result.setValue(null);
		}
		return result;
	}

	/**
	 * 修改用户头像
	 */
	@Override
	public Result resetUserPortait(String id, String portait) {
		Result result = new Result();
		User user = userRepository.findOne(id);
		if(user != null){
			user.setPortrait(portait);
			user = userRepository.save(user);
			result.setKey("success");
			List<UserLoginRecord> userLoginRecord = userLoginRecordRepository.getAndroidOrIosLoginRecode(user.getId());
			if(userLoginRecord != null && userLoginRecord.size() > 0 && userLoginRecord.get(0).getLogin_resource().equals("adroid")){
				result.setValue("头像修改成功");
			}else{
				result.setValue(user);
			}
//			result.setValue(user);
		}else{
			result.setKey("error");
			result.setValue("用户不存在，头像修改失败");
			result.setMsg("用户不存在，头像修改失败");
		}
		return result;
	}

	/**
	 * 登出/退出登录
	 */
	@Override
	public Result signOut(String id,String login_resource) {
		Result result = new Result();
		User user = userRepository.findOne(id);
		if(user != null){
			List<UserLoginRecord> userLogins = userLoginRecordRepository.findByUsernameAndLoginResource(id, login_resource);
			if(userLogins != null && userLogins.size() > 0){
				UserLoginRecord userLogin = userLogins.get(0);
				userLoginRecordRepository.delete(userLogin);
				redisClient.remove("login>>"+user.getId());
				result.setKey("success");
				result.setValue("用户登出成功");
			}else{
//				result.setKey("error");
//				result.setValue("该用户未存在登录记录，登出操作失败");
				//登出妥协   不存在登录记录  允许登出成功
				result.setKey("success");
				result.setValue("用户登出成功");
				return result;
			}
		}else{
			result.setKey("error");
			result.setValue("该用户不存在，登出操作失败");
		}
		return result;
	}

	/**
	 * pc心跳监控
	 */
	@Override
	public Map<String, Object> heartbeat(String id, String token, String passwordToken) {
		Map<String, Object> map = new HashMap<>();
		String login_resource = "pc";
		User user = userRepository.findOne(id);
		if(user != null){
			/*
			 * 更新pc登录时间=>>用于心跳检测登录状态,是否程序崩溃等
			 * 暂时不上长连接,账户登录状态由登录时间来标记，定时任务定时清除pc登录超时记录
			 */
			userLoginRecordDao.updatePCLoginTime(user.getId());
			
			/*
			 *验证优先顺序：
			 *	密码是否修改（登出）
			 *	是否异地登录（登出） 
			 */
			// 验证是否密码修改
			if(user.getPassword_token() != null && !user.getPassword_token().equals(passwordToken)) {
				map.put("key", "success");
				map.put("value", "验证成功,密码已被修改");
				map.put("type", 1);
				map.put("resetPassword", true);
				return map;
			}
			
			// 验证是否异地登录
			String loginToken = redisClient.get("login>>"+user.getId());
			if(loginToken == null || loginToken.equals("")){
				List<UserLoginRecord> userLogins = userLoginRecordRepository.findByUsernameAndLoginResource(id, login_resource);
				if(userLogins != null && userLogins.size() > 0){
					UserLoginRecord userLogin = userLogins.get(0);
					loginToken = userLogin.getEquipment();
					redisClient.setex("login>>"+user.getId(),7200, token);
					if(token.equals(loginToken)){
						map.put("key", "success");
						map.put("value", "验证成功,本机登录");
						map.put("type", 2);
						map.put("login", true);
					}else{
						map.put("key", "success");
						map.put("value", "验证成功,异地登录");
						map.put("type", 2);
						map.put("login", false);
					}
				}else{
					map.put("key", "error");
					map.put("value", "验证失败，用户不存在登录记录");
				}
			}else{
				if(token.equals(loginToken)){
					map.put("key", "success");
					map.put("value", "验证成功,本机登录");
					map.put("type", 2);
					map.put("login", true);
				}else{
					map.put("key", "success");
					map.put("value", "验证成功,异地登录");
					map.put("type", 2);
					map.put("login", false);
				}
			}
		}else{
			map.put("key", "error");
			map.put("type", 0);
			map.put("value", "该用户不存在");
		}
		System.out.println(request.getSession().getId());
		return map;
	}

	@Override
	public Result getUserNickname(String id) {
		Result result = new Result();
		User user = userRepository.findOne(id);
		if(user != null){
			result.setKey("success");
			result.setValue(user.getNickname());
		}else{
			result.setKey("error");
			result.setValue("该用户不存在");
		}
		return result;
	}
	
	/**
	 * 测试-用户名/密码 登录
	 * result :: 1>>登录成功  -1>>用户名不存在  -2>>密码错误
	 */
	@DuplicateSubmitToken(type = DuplicateSubmitToken.REQUEST)
	@Override
	public Result testLoginByPassword(String username, String password,String type,String equipmentid,String token) {
		Result result = new Result();
		//判断字段规范   用户名不可为空   type和equimentid 在controller中设置有默认值 不需要作出判断
		if(username == null || username.equals("")){
			result.setKey("error");
			result.setValue("用户名不可为空");
			return  result;
		}
		List<User> users = userRepository.findByUsername(username);
		//判断账号是否存在
		if(users == null || users.size() == 0){
			result.setKey("error");
			result.setValue("用户未注册，请先进行注册后在登录");
			return result;
		}
		User user = users.get(0);
		//判断账号密码  密码正确后才会去判断账号的登录状态  账号的使用状态
		//密码md5加密
		try {
			password = MD5.md5(new String(password.getBytes(), "utf-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		//快捷登录不存在密码，没必要进行密码判断，返回提示
		if(user.getPassword() == null || user.getPassword().equals("")){
			result.setKey("error");
//			result.setValue("该账户未设置密码，请使用快捷登录");
			result.setValue("密码错误");
			return result;
		}
		//密码错误  返回提示
		if(!user.getPassword().equals(password)){
			result.setKey("error");
			result.setValue("密码错误");
			return result;
		}
		//账号冻结不允许登录
		if(!user.isStatus()){
			result.setKey("error");
			result.setValue("该账户已被冻结，无法登录");
			return result;
		}
		
		//app端 与 pc端 存在影响 
		//app登录后  pc无法再次登录
		// pc 端登录判断 
		if("pc".equals(type)){
			//限制pc端单登录
//			List<UserLoginRecord> userLogins = userLoginRecordRepository.findByUsernameAndLoginResource(user.getId(),type);
//			if(userLogins != null && userLogins.size() > 0){
//				result.setKey("error");
//				result.setValue("该账户已登录，无法再次登录");
//				return result;
//			}
			// 由于pc端判断与app不同  走完之前的通用判断   如果登录类型为pc 若存在app登录 做出提示  不存在则认定可以登录
			/*if(userLoginRecordRepository.getAndroidOrIosLoginCount(user.getId()) > 0){
				result.setKey("error");
				result.setValue("该账户已在app端登录，无法再次登录，请先在APP端登出");
				return result;
			}*/
			//刨除被拦截的登录失败状态  即为登录成功
			//记录用户登录状态
//			saveUserLoginInfo(user,type,equipmentid);
			saveUserLoginInfo(user,type,token);
			System.out.println(request.getSession().getId());
			//设置用户的最后一次登录时间 , 登录次数 +1
			user.setLast_login_time(new Date());
			user.setLoginNum(user.getLoginNum() + 1);
			result.setKey("success");
			result.setValue(user);
			return result;
		}
		
		//单点登录   android  ios 一端登录后  将原登录状态清空   重新添加登录状态  
		//给另一端发送推送   在线收到推送 登出  不在线  打开APP时调用  查询账户登录状态接口  返回登录超时错误或者异地登录异常  跳转登录  进行重新登录
		if(type.equals("android") || type.equals("ios")){
			//如果pc登录  限制app及ios登录
			/*if(userLoginRecordRepository.getPcLoginCount(user.getId()) > 0){
				result.setKey("error");
				result.setValue("该账户已在PC客户端登录,无法再次登录，请现在客户端登出");
				return result;
			}*/
			//查询账号的登录记录   判断账号是否需要发送推送
			//如果账号存在登录记录  并且 账号的登录设备不同   判断为异地登录   发送推送  
			boolean flag = false;	// 是否发异地登录的推送 
			List<UserLoginRecord> logins = userLoginRecordRepository.getAndroidOrIosLoginRecode(user.getId());
			if(logins != null && logins.size() > 0){
				UserLoginRecord loginRecord = logins.get(0);
				//判断  如果登录记录  登录源  或者 登录设备号不同 则认定  登录为异地登录   修改标记 为 true
				if(!loginRecord.getLogin_resource().equals(type) || !loginRecord.getEquipment().equals(equipmentid)){
					flag = true;
				}
			}
			//清空账号登录状态 
			userLoginRecordRepository.dealAndroidOrIosLoginRecord(user.getId());
			//记录用户登录状态
			saveUserLoginInfo(user,type,equipmentid);
			//设置用户的最后一次登录时间 , 登录次数 +1
			user.setLast_login_time(new Date());
			user.setLoginNum(user.getLoginNum() + 1);
			user = userRepository.save(user);
			//根据标记发推送
			if(flag){
				//异地登录推送时  content 推送当前登录的设备号  如果推送接收id与登录用户id相同   提示异地登录  并跳转登录
//				this.sendReLoginPush("测试--账号已在异地登录，请重新登录","remoteLogin",user.getId(),user.getUsername());
//				this.sendReLoginPush("测试--账号已在异地登录，请重新登录","21",user.getId()+"@;-;@"+equipmentid,user.getUsername());
				this.sendReLoginPush("","21",user.getId()+"@;-;@"+equipmentid,user.getUsername());
			}
			//android用户与  ios、pc返回数据区别   android返回userid   ios、pc返回user实体
			if(type.equals("android")){
				result.setKey("success");
				result.setValue(user.getId());
				return result;
			}else{
				result.setKey("success");
				result.setValue(user);
				return result;
			}
		}
		
		result.setKey("error");
		result.setValue("登录源异常，登录失败");
		return result;
	}
	
	@DuplicateSubmitToken(type = DuplicateSubmitToken.REQUEST)
	@Transactional
	@Override
	public Result testLoginByIdentifyCode(String username, String identifyCode, String type, String equipmentid) {
		Result result = new Result();
		//判断字段规范   用户名不可为空   type和equimentid 在controller中设置有默认值 不需要作出判断
		if(username == null || username.equals("")){
			result.setKey("error");
			result.setValue("用户名不可为空");
			return  result;
		}
		//验证短信验证码
		Result validateIdentifyResult = this.validateIdentify(username, identifyCode);
		if("error".equals(validateIdentifyResult.getKey())){
			//短信验证码验证失败   直接返回  验证失败信息
			return validateIdentifyResult;
		}
		List<User> users = userRepository.findByUsername(username);
		//判断账号是否存在
		if(users == null || users.size() == 0){
			//用户不存在   则为用户注册新的账户    快捷登录直接添加登录记录 默认用户登入
			try{
				User user = new User();
				user.setUsername(username);
				user.setCreatetime(new Date());
				user.setLoginNum(1);
//				user.setNickname(username.substring(0, 3)+"****"+username.substring(7));	//设置默认昵称
				user.setPortrait("https://guoanchuangke2.oss-cn-beijing.aliyuncs.com/datas/video/portait/1521611496322.jpg?Expires=4675210641&OSSAccessKeyId=LTAILWi7cyx4nHeI&Signature=mF%2BTnl0n/Ih15E0448KoUxtIQOM%3D");	//设置默认头像url
				user.setStatus(true);
				user.setExpense_amount(0);
				user.setExpense_num(0);
				user.set_check(false);
				user.setIsPledge(0);
				user.setCreate_office_num(0);
				user.setCreate_meeting_num(0);
				user.setJoin_video_num(0);
				user.setTotal_join_meeting_time(new Long(0));
				user.setTotal_office_time(new Long(0));
				user.setOrderState(0);
				user.setRegister_source(type);
				user.setLast_login_time(new Date());
				user.setReset_password_time(new Date());
				user = userRepository.save(user);
				//查询用户被邀请信息
				//messageRepository.updateUserBeInvisitedMessage(username, user.getId());
				//userDao.updateBeInvisitedMessage(username, user.getId());
				List<Message> messages = messageRepository.findUserBeVisitedMessage(username);
				for (Message message : messages) {
					message.setUserid(user.getId());
				}
				//记录用户登录状态
				saveUserLoginInfo(user,type,equipmentid);
				//android用户与  ios、pc返回数据区别   android返回userid   ios、pc返回user实体
				//用户注册补充信息
				if(user != null && user.getUsername() != null){
					conferenceService.updateContastsInformation(user.getUsername());
				}
				//android用户与  ios、pc返回数据区别   android返回userid   ios、pc返回user实体
				if(type.equals("android")){
					result.setKey("success");
					result.setValue(user.getId());
					return result;
				}else{
					result.setKey("success");
					result.setValue(user);
					return result;
				}
			}catch (Exception e) {
				e.printStackTrace();
				result.setKey("error");
				result.setValue("登录失败，创建快捷账号失败");
				return result;
			}
		}
		//用户存在  开始验证用户状态 
		User user = users.get(0);
		//账号冻结不允许登录
		if(!user.isStatus()){
			result.setKey("error");
			result.setValue("该账户已被冻结，无法登录");
			return result;
		}
		
		//app端 与 pc端 存在影响 
		//app登录后  pc无法再次登录
		// pc 端登录判断 
		if("pc".equals(type)){
			//限制pc端单登录
			List<UserLoginRecord> userLogins = userLoginRecordRepository.findByUsernameAndLoginResource(user.getId(),type);
			if(userLogins != null && userLogins.size() > 0){
				result.setKey("error");
				result.setValue("该账户已登录，无法再次登录");
				return result;
			}
			// 由于pc端判断与app不同  走完之前的通用判断   如果登录类型为pc 若存在app登录 做出提示  不存在则认定可以登录
			/*if(userLoginRecordRepository.getAndroidOrIosLoginCount(user.getId()) > 0){
				result.setKey("error");
				result.setValue("该账户已在app端登录，无法再次登录，请先在APP端登出");
				return result;
			}*/
			//刨除被拦截的登录失败状态  即为登录成功
			//记录用户登录状态
//			saveUserLoginInfo(user,type,equipmentid);
			saveUserLoginInfo(user,type,equipmentid);
			//设置用户的最后一次登录时间 , 登录次数 +1
			user.setLast_login_time(new Date());
			user.setLoginNum(user.getLoginNum() + 1);
			result.setKey("success");
			result.setValue(user);
			return result;
		}
		
		//单点登录   android  ios 一端登录后  将原登录状态清空   重新添加登录状态  
		//给另一端发送推送   在线收到推送 登出  不在线  打开APP时调用  查询账户登录状态接口  返回登录超时错误或者异地登录异常  跳转登录  进行重新登录
		if(type.equals("android") || type.equals("ios")){
			//如果pc登录  限制app及ios登录
			/*if(userLoginRecordRepository.getPcLoginCount(user.getId()) > 0){
				result.setKey("error");
				result.setValue("该账户已在PC客户端登录,无法再次登录，请现在客户端登出");
				return result;
			}*/
			//查询账号的登录记录   判断账号是否需要发送推送
			//如果账号存在登录记录  并且 账号的登录设备不同   判断为异地登录   发送推送  
			boolean flag = false;	// 是否发异地登录的推送 
			List<UserLoginRecord> logins = userLoginRecordRepository.getAndroidOrIosLoginRecode(user.getId());
			if(logins != null && logins.size() > 0){
				UserLoginRecord loginRecord = logins.get(0);
				//判断  如果登录记录  登录源  或者 登录设备号不同 则认定  登录为异地登录   修改标记 为 true
				if(!loginRecord.getLogin_resource().equals(type) || !loginRecord.getEquipment().equals(equipmentid)){
					flag = true;
				}
			}
			//清空账号登录状态 
			userLoginRecordRepository.dealAndroidOrIosLoginRecord(user.getId());
			//重新登录
			//记录用户登录状态
			saveUserLoginInfo(user,type,equipmentid);
			//设置用户的最后一次登录时间 , 登录次数 +1
			user.setLast_login_time(new Date());
			user.setLoginNum(user.getLoginNum() + 1);
			user = userRepository.save(user);
			//根据标记发推送
			if(flag){
				//异地登录推送时  content 推送当前登录的设备号  如果推送接收id与登录用户id相同   提示异地登录  并跳转登录
//				this.sendReLoginPush("测试--账号已在异地登录，请重新登录","remoteLogin",user.getId(),user.getUsername());
//				this.sendReLoginPush("测试--账号已在异地登录，请重新登录","21",user.getId()+"@;-;@"+equipmentid,user.getUsername());
				this.sendReLoginPush("","21",user.getId()+"@;-;@"+equipmentid,user.getUsername());
			}
			
			//android用户与  ios、pc返回数据区别   android返回userid   ios、pc返回user实体
			if(type.equals("android")){
				result.setKey("success");
				result.setValue(user.getId());
				return result;
			}else{
				result.setKey("success");
				result.setValue(user);
				return result;
			}
		}
		
		result.setKey("error");
		result.setValue("登录源异常，登录失败");
		return result;
	}

	/**
	 * 验证账号登录状态
	 * value :
	 * 	-1 错误
	 *  0 成功
	 *  1 异地登录（账户异地登录）
	 *  2 登录超时（账户登录超时，不存在登录记录或异地登录登出登录记录被清除）
	 *  3 密码修改（密码已被修改，重新登录）
	 */
	@Override
	public Result getLoginStatus(String userid, String type, String equimentid, String passwordToken) {
		Result result = new Result();
		if(userid == null || userid.equals("")){
			result.setKey("error");
			result.setValue(-1);
			result.setMsg("id不可为空");
			return result;
		}
		User user = userRepository.findOne(userid);
		if(user == null){
			result.setKey("error");
			result.setValue(-1);
			result.setMsg("用户不存在，查询失败");
			return result;
		}
		
		if(user.getPassword_token() != null && !user.getPassword_token().equals(passwordToken)) {
			result.setKey("reset_password");
			result.setValue(3);
			result.setMsg("密码已被修改，请重新登录");	// 密码被修改，重新登录
			return result;
		}
		
		List<UserLoginRecord> logins = userLoginRecordRepository.getAndroidOrIosLoginRecode(userid);
		if(logins == null || logins.size() == 0){
			result.setKey("error_timeout");
			result.setValue(2);
			result.setMsg("登录超时，请重新登录");	//登录记录不存在，需要重新登录
			return result;
		}
		UserLoginRecord loginRecord = logins.get(0);
		//登录源或登录设备号不同  判断用户为异地登录
		if(!loginRecord.getLogin_resource().equals(type) || !loginRecord.getEquipment().equals(equimentid)){
			result.setKey("error_remote");
			result.setValue(1);
			result.setMsg("该账户已在异地登录，请重新登录");
			return result;
		}
		result.setKey("success");
		result.setValue(0);
		result.setMsg("验证成功");
		return result;
	}
	
	// 发送异地登录推送
	private void sendReLoginPush(String alert,String mesType, String content,String username){
		JPushRestAPI jPushRestAPI = new JPushRestAPI();
		List<PushSwitch> list = pushSwitchRepository.findAll();
		PushSwitch pushSwitch  = list.get(0);
		if (pushSwitch.getIsSwitch() == 0) {
			jPushRestAPI.setApns_production(false);
		}
		jPushRestAPI.SendPush(alert, mesType, content, "", jPushRestAPI.jsonDataByOneAlias(username));
		
		//推送后移除别名
		JDeviceRestAPI jDeviceRestAPI = new JDeviceRestAPI();
		if (pushSwitch.getIsSwitch() == 0) {
			jDeviceRestAPI.setApns_production(false);
		}
		jDeviceRestAPI.removeAlias(username);
	}


	@Override
	public Map<String, Object> validateIdentifyCode(String mobile, String code) {
		Map<String, Object> map = new HashMap<>();
		List<VideoIdentify> identifys = identifyRepository.findByUsername(mobile);
		if(identifys != null && identifys.size() > 0 && identifys.get(0).getIdentifytime() != null){
			List<User> users = userRepository.findByUsername(mobile);
			if(users != null && users.size() > 0){
				map.put("key", "error");
				map.put("msg", "此手机号已注册");
				return map;
			}
			VideoIdentify identify = identifys.get(0);
			Date nowDate = new Date();
			long alongTime = 0;	//时间差
			alongTime = nowDate.getTime() - identify.getIdentifytime().getTime();
			if(alongTime > 1000*60*5){
				//登录超时  登录失败
				map.put("key", "error");
				map.put("msg", "验证码超时，请重新获取验证码");
			}else if(!code.equals(identify.getIdentifycode())){
				//验证码错误
				map.put("key", "error");
				map.put("msg", "验证码错误，请确认验证码");
			}else{
				//验证通过
				map.put("key", "success");
				map.put("msg", "验证码验证通过");
			}
		}else{
			//该用户名不存在验证码信息   验证失败
			map.put("key", "error");
			map.put("msg", "验证失败，请先点击获取短信验证码");
		}
		return map;
	}
	
	@Override
	public Map<String, Object> validateIdentifyCodeForForget(String mobile, String code) {
		Map<String, Object> map = new HashMap<>();
		List<VideoIdentify> identifys = identifyRepository.findByUsername(mobile);
		if(identifys != null && identifys.size() > 0 && identifys.get(0).getIdentifytime() != null){
			List<User> users = userRepository.findByUsername(mobile);
			if(users == null || users.size() == 0){
				map.put("key", "error");
				map.put("msg", "此手机号未注册，请前往注册或更换手机号");
				return map;
			}
			VideoIdentify identify = identifys.get(0);
			Date nowDate = new Date();
			long alongTime = 0;	//时间差
			alongTime = nowDate.getTime() - identify.getIdentifytime().getTime();
			if(alongTime > 1000*60*5){
				//登录超时  登录失败
				map.put("key", "error");
				map.put("msg", "验证码超时，请重新获取验证码");
			}else if(!code.equals(identify.getIdentifycode())){
				//验证码错误
				map.put("key", "error");
				map.put("msg", "验证码错误，请确认验证码");
			}else{
				//验证通过
				map.put("key", "success");
				map.put("msg", "验证码验证通过");
			}
		}else{
			//该用户名不存在验证码信息   验证失败
			map.put("key", "error");
			map.put("msg", "验证失败，请先点击获取短信验证码");
		}
		return map;
	}


	@Override
	public Map<String, Object> registByPc(String username, String nickname, String password, String source) {
		Map<String, Object> map = new HashMap<>();
		if(username == null || username.equals("")){
			map.put("key", "error");
			map.put("msg", "用户名不可为空");
			return map;
		}
		if(password == null || password.equals("")){
			map.put("key", "error");
			map.put("msg", "密码不可为空");
			return map;
		}
		//验证用户名	用户名存在 注册  否则 不注册
		User user = null;
		List<User> users = userRepository.findByUsername(username);
		if(users != null && users.size() > 0){
			//用户已存在，无法注册
			map.put("key", "error");
			map.put("msg", "此手机号已注册，请您登陆，或者更换手机号");
			return map;
		}
		
		//用户不存在  注册
		user = new User();
		user.setUsername(username);
		//密码md5加密
		try {
			password = MD5.md5(new String(password.getBytes(), "utf-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		user.setPassword(password);
		user.setNickname(nickname);
		user.setCreatetime(new Date());
		user.setLoginNum(0);
		user.setPortrait("https://guoanchuangke2.oss-cn-beijing.aliyuncs.com/datas/video/portait/1522745203889.png?Expires=4676344717&OSSAccessKeyId=LTAILWi7cyx4nHeI&Signature=6bsYvd8PTjbilQbWwBfUO1spm6U%3D");
		user.setRegister_source(source);
		user.setStatus(true);
		user.setExpense_amount(0);
		user.setExpense_num(0);
		user.set_check(false);
		user.setIsPledge(0);
		user.setCreate_office_num(0);
		user.setCreate_meeting_num(0);
		user.setJoin_video_num(0);
		user.setTotal_join_meeting_time(new Long(0));
		user.setTotal_office_time(new Long(0));
		user.setOrderState(0);
		user.setReset_password_time(new Date());
		user = userRepository.save(user);
		//查询用户被邀请信息
		//messageRepository.updateUserBeInvisitedMessage(username, user.getId());
		//userDao.updateBeInvisitedMessage(username, user.getId());
		List<Message> messages = messageRepository.findUserBeVisitedMessage(username);
		for (Message message : messages) {
			message.setUserid(user.getId());
		}
		//用户注册补充信息
		if(user != null && user.getUsername() != null){
			conferenceService.updateContastsInformation(user.getUsername());
		}
		
		// 注册成功发送短信提示
		String smsMessage = "尊敬的先生/女士，公司为您创建的i商务账号已经生成，账号"+username+",请及时登录i商务客户端修改密码 <国安创客>。";
		try {
			SendCode.sendMessage(username, smsMessage);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("注册成功短息模板发送失败");
		}
		
		map.put("key", "success");
		map.put("msg", "注册成功");
//		map.put("user", user);
		return map;
	}


	@Override
	public Map<String, Object> sendIdentifyForForget(String username) {
		Map<String, Object> map = new HashMap<>();
		List<User> users = userRepository.findByUsername(username);
		if(users == null || users.size() == 0){
			map.put("key", "error");
			map.put("msg", "此手机号未注册，请前往注册或更换手机号");
			return map;
		}
		List<VideoIdentify> identifys = identifyRepository.findByUsername(username);
		VideoIdentify identify = null;
		//用户名已存在  直接修改验证码   不存在  新添加验证信息
		if(identifys != null && identifys.size() > 0){
			identify = identifys.get(0);
		}else{
			identify = new VideoIdentify();
		}
		String code = SystemHelper.getSixCode();
		identify.setUsername(username);
		identify.setIdentifycode(code);
		identify.setIdentifytime(new Date());
//		String msg = "尊敬的用户您好，您的短信验证码为 "+code+"，有效时间为5分钟，请及时使用";
//		String msg = "您好，您的验证码是："+code+" ，有效时间5分钟。";
		// 发送短信验证码
		String smsMessage = "尊敬的"+users.get(0).getNickname()+"先生/女士，您好，您正在找回登录密码，验证码："+code+"，如非本人操作，请忽略本短信 <国安创客>。";
		try{
			SendCode.sendMessage(username, smsMessage);
			map.put("key", "success");
			map.put("msg", "验证码发送成功");
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("忘记密码短息模板发送失败");
			map.put("key", "error");
			map.put("msg", "验证码发送失败");
		}
		identifyRepository.save(identify);
		return map;
	}


	@Override
	public Map<String, Object> sendIdentifyForRegist(String username) {
		Map<String, Object> map = new HashMap<>();
		List<User> users = userRepository.findByUsername(username);
		if(users != null && users.size() > 0){
			map.put("key", "error");
			map.put("msg", "此手机号已注册，请您登录，或者更换手机号");
			return map;
		}
		List<VideoIdentify> identifys = identifyRepository.findByUsername(username);
		VideoIdentify identify = null;
		//用户名已存在  直接修改验证码   不存在  新添加验证信息
		if(identifys != null && identifys.size() > 0){
			identify = identifys.get(0);
		}else{
			identify = new VideoIdentify();
		}
		String code = SystemHelper.getSixCode();
		identify.setUsername(username);
		identify.setIdentifycode(code);
		identify.setIdentifytime(new Date());
//		String msg = "尊敬的用户您好，您的短信验证码为 "+code+"，有效时间为5分钟，请及时使用";
//		String msg = "您好，您的验证码是："+code+" ，有效时间5分钟。";
		// 发送短信验证码
//		String smsMessage = "尊敬的"+users.get(0).getNickname()+"先生/女士，您好，您正在找回登录密码，验证码："+code+"，如非本人操作，请忽略本短信 <国安创客>。";
		String smsMessage = "尊敬的用户您好，验证码为 "+code+"，有效时间为5分钟，请及时使用";
		try{
			SendCode.sendMessage(username, smsMessage);
			map.put("key", "success");
			map.put("msg", "验证码发送成功");
		}catch (Exception e) {
			e.printStackTrace();
			System.out.println("注册短息模板发送失败");
			map.put("key", "error");
			map.put("msg", "验证码发送失败");
		}
		identifyRepository.save(identify);
		return map;
	}
}

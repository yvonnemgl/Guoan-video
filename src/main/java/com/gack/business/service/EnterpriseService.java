package com.gack.business.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gack.business.dao.DepartmentDao;
import com.gack.business.model.Area;
import com.gack.business.model.City;
import com.gack.business.model.Enterprise;
import com.gack.business.model.Industry;
import com.gack.business.model.Message;
import com.gack.business.model.PermissionGroup;
import com.gack.business.model.PersonNumRange;
import com.gack.business.model.Province;
import com.gack.business.model.PushSwitch;
import com.gack.business.model.UserEnterpriseDepartmentPosition;
import com.gack.business.model.UserEnterprisePermissionGroup;
import com.gack.business.repository.AreaRepository;
import com.gack.business.repository.CityRepository;
import com.gack.business.repository.ContactsRepository;
import com.gack.business.repository.DepartmentRepository;
import com.gack.business.repository.EnterpriseRepository;
import com.gack.business.repository.IndustryRepository;
import com.gack.business.repository.JobRankRepository;
import com.gack.business.repository.MessageRepository;
import com.gack.business.repository.PersonNumRangeRepository;
import com.gack.business.repository.PositionRepository;
import com.gack.business.repository.ProvinceRepository;
import com.gack.business.repository.PushSwitchRepository;
import com.gack.business.repository.UserEnterpriseDepartmentPositionRepository;
import com.gack.business.repository.UserEnterprisePermissionGroupRepository;
import com.gack.business.repository.UserRepository;
import com.gack.business.vo.DepartmentItemVO;
import com.gack.helper.common.util.JPushRestAPI;
import com.gack.helper.common.util.MapAsReturn;
import com.gack.helper.common.util.Pinyin;
import com.gack.helper.common.util.SendCode;
import com.gack.helper.redis.RedisClient;
import com.gack.helper.redis.UserActionInfoRedis;

/**
 * 
 * @author ws
 * 2018-5-29
 */
@Service
@Transactional
public class EnterpriseService implements EnterpriseServiceInterface{

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private EnterpriseRepository enterpriseRepository;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private IndustryRepository industryRepository;
	@Autowired
	private PersonNumRangeRepository personNumRangeRepository;
	@Autowired
	private ProvinceRepository provinceRepository;
	@Autowired
	private CityRepository cityRepository;
	@Autowired
	private AreaRepository areaRepository;
	@Autowired
	private UserEnterpriseDepartmentPositionRepository userEnterpriseDepartmentPositionRepository;
	@Autowired
	private UserEnterprisePermissionGroupRepository userEnterprisePermissionGroupRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ContactsRepository contactsRepository;
	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private DepartmentServiceInterface departmentService;
	@Autowired
	private DepartmentDao departmentDao;
	@Autowired
	private UserActionInfoRedis userActionInfoRedis;
	@Autowired
	private PositionRepository positionRepository;
	@Autowired
	private ManagerServiceInterface managerService;
	@Autowired
	private RedisClient jedis;
	@Autowired
	private PushSwitchRepository pushSwitchRepository;
	@Autowired
	private UserServiceInterface userService;
	@Autowired
	private JobRankServiceInterface jobRankService;
	@Autowired
	private JobRankSortServiceInterface jobRankSortService;
	@Autowired	
	private JobRankRepository jobRankRepository;
	
	/**
	 * 某用户创建公司
	 * @param userId 用户id
	 * @param name 公司名称
	 * @param industryId 行业id
	 * @param personNumRangeId 人员范围id
	 * @param provinceId 省份id
	 * @param cityId 城市id
	 * @param areaId 城区id
	 * @return status:error/success msg:错误或成功消息  enterpriseId:新创建公司的id
	 */
	@Override
	public Map<String, Object> createEnterprise(String userId, String name, String industryId, String personNumRangeId, String provinceId, String cityId, String areaId) {
		boolean hasUser = userRepository.exists(userId);
		if(!hasUser){
			return MapAsReturn.setStatusError("查询不到该用户");
		}
		
		Industry industry = industryRepository.findOne(industryId);
		if(industry == null){
			return MapAsReturn.setStatusError("查询不到该行业");
		}
		
		PersonNumRange personNumRange = personNumRangeRepository.findOne(personNumRangeId);
		if(personNumRange == null){
			return MapAsReturn.setStatusError("查询不到该人数范围");
		}
		
		Province province = provinceRepository.findOne(provinceId);
		if(province == null){
			return MapAsReturn.setStatusError("查询不到该省份");
		}
		
		City city = cityRepository.findOne(cityId);
		if(city == null){
			return MapAsReturn.setStatusError("查询不到该城市");
		}
		
		Area area = areaRepository.findOne(areaId);
		if(area == null){
			return MapAsReturn.setStatusError("查询不到该城区");
		}
		
		String industryName = industry.getName();
		if(industryName.equals("其他未分类")){
			Industry parentIndustry = industryRepository.findOne(industry.getParentId());
			industryName = parentIndustry.getName() + " " + industryName;
		}
		 
		//保存公司信息
		Enterprise enterprise = new Enterprise();
		enterprise.setName(name); //公司名称
		enterprise.setNumberRange(personNumRange.getRange()); //公司人员范围
		enterprise.setCurrentNumber(1);//公司当前人数为1,仅有创建者一人
		enterprise.setCreator(userId);//公司创建人id
		enterprise.setIndustry(industryName); //公司所属行业
		String place = province.getName() + " " + province.getName() + " " + area.getName(); 
		enterprise.setLocation(place); //公司所在地区
		enterprise.setStatus("0");//公司状态"未认证"
		Date date = new Date();
		enterprise.setCreateTime(date);//公司创建时间
		enterprise.setSurplusAmount(0);//设置已购商务卡金额
		enterprise.setUsedAmount(0);//设置已使用商务卡金额
		enterprise.setUpdateNameTime(date);//设置更改名称时间
		Enterprise newEnterprise = enterpriseRepository.save(enterprise);
		
		//将创建者设置为该公司的主管理员
		UserEnterprisePermissionGroup uepg = new UserEnterprisePermissionGroup();
		uepg.setUserId(userId);
		uepg.setEnterpriseId(newEnterprise.getId());
		uepg.setPermissionGroupsDescription(PermissionGroup.parseToString(new ArrayList<PermissionGroup>(){
			{
				add(PermissionGroup.superManagerPermissionGroup());
			}
		}));
		uepg.setManagerTime(new Date());
		userEnterprisePermissionGroupRepository.save(uepg);
		
		//将创建者放入该公司下，不设置部门，不设置职位
		UserEnterpriseDepartmentPosition uedp = new UserEnterpriseDepartmentPosition();
		uedp.setEnterpriseId(newEnterprise.getId());//公司id
		uedp.setUserId(userId);//创建者id
		uedp.setEnterpriseRemarks(userRepository.findNicknameById(userId));//设置默认企业备注
		uedp.setUpdateTime(new Date());//设置修改部门、职位、企业备注的时间
		userEnterpriseDepartmentPositionRepository.save(uedp);
		
		//记录员工选择
		userActionInfoRedis.recordChosenEnterpriseId(userId, newEnterprise.getId());
		
		//设置默认职级
		jobRankService.setDefault(newEnterprise.getId());
		
		//设置默认职级排序
		jobRankSortService.setDefault(newEnterprise.getId());
		
		return MapAsReturn.setStatusSuccess("enterpriseId", newEnterprise.getId());
	}
	
	/**
	 * 查询公司的组织架构(若公司id为空,则返回上次用户选择的公司的组织架构)
	 * @param enterpriseId 公司id
	 * @param userId 操作者id
	 * @return status:error/success msg:错误或成功消息   
	 * 		   enterpriseName:公司名称    enterpriseNum:公司人数    
	 * 		   departmentItemList:一级部门ItemVO集合  noDepartmentNum:无部门人数   
	 * 		   isManager(是否是公司管理员):true/false enterpriseId:公司id
	 * 		   permissionList:操作者所具有的关于公司管理的权限
	 */
	@Override
	public Map<String, Object> findEnterpriseFramework(String enterpriseId, String userId) {
		if(StringUtils.isBlank(enterpriseId)){
			enterpriseId = userActionInfoRedis.getLastChosenEnterpriseId(userId);
			if(StringUtils.isBlank(enterpriseId)){
				return MapAsReturn.setStatusSuccess("noEnterprise", true);
			}
		}
		
		if(userEnterpriseDepartmentPositionRepository.countByUserIdAndEnterpriseId(userId, enterpriseId) == 0){
			return MapAsReturn.setStatusError("非法操作");
		}
		
		//根据公司id查询该公司,并验证
		Enterprise enterprise = enterpriseRepository.findById(enterpriseId);
		if(null == enterprise){
			return MapAsReturn.setStatusError("查无此公司");
		}

		//放置公司名称和人数
		Map<String, Object> map = new HashMap<>();
		
		map.put("enterpriseName", enterprise.getName() == null?"":enterprise.getName());
		map.put("enterpriseNum", enterprise.getCurrentNumber() == null?0:enterprise.getCurrentNumber());
		
		//查询该公司的所有一级部门ItemVO
		List<DepartmentItemVO> departmentItemVOList = departmentRepository.findTopDepartmentItemVOByEnterpriseId(enterpriseId);
		
		//对departmentItemVOList排序,按照部门名称
		Comparator<DepartmentItemVO> comparator = new Comparator<DepartmentItemVO>(){
			@Override
			public int compare(DepartmentItemVO d1, DepartmentItemVO d2) {
				String s1 = Pinyin.getStringPinYin(d1.getName());
				String s2 = Pinyin.getStringPinYin(d2.getName());
				return s1.compareTo(s2);
			}
		};
		Collections.sort(departmentItemVOList, comparator);
		map.put("departmentItemList", departmentItemVOList);
		
		//查询"无部门人员"数量
		Integer num = userEnterpriseDepartmentPositionRepository.findNoDepartmentNumByEnterpriseId(enterpriseId);
		map.put("noDepartmentNum", num==null?0:num);
		
		//判断操作者是否是公司管理员
		UserEnterprisePermissionGroup uepg = userEnterprisePermissionGroupRepository.findByUserIdAndEnterpriseId(userId, enterpriseId);
		if(null != uepg){
			map.put("isManager", true);
		}else{
			map.put("isManager", false);
		}

		//记录员工选择
		userActionInfoRedis.recordChosenEnterpriseId(userId, enterpriseId);
		
		//操作者所具有的关于公司管理的权限
		map.put("permissionList", userService.findManageRangeAboutEnterpriseManage(enterpriseId, userId).get("permissionList"));
		
		map.put("enterpriseId", enterpriseId);
		map.put("status", "success");
		map.put("msg", "查询成功");
		
		return map;
	}

	/**
	 * 删除员工
	 * @param enterpriseId 公司id
	 * @param userId 用户id
	 * @param operatorId 操作者id
	 * @return status:error/success msg:错误或成功消息
	 */
	@Override
	public Map<String, Object> deleteUserFromEnterprise(String enterpriseId, String userId, String operatorId) {
		if(userId.equals(operatorId)){
			return MapAsReturn.setStatusError("无法对自己进行删除操作");
		}
		
		Map<String, Object> map = new HashMap<>();
		
		boolean hasEnterprise = enterpriseRepository.exists(enterpriseId);
		if(!hasEnterprise){
			map.put("status", "error");
			map.put("msg", "查无此公司");
			return map;
		}
		
		List<UserEnterpriseDepartmentPosition> uedpList =  userEnterpriseDepartmentPositionRepository.findByUserIdAndEnterpriseId(userId, enterpriseId);
		
		if(uedpList.size() == 0){
			map.put("status", "error");
			map.put("msg", "此用户不在此公司");
			return map;
		}
		
		String enterpriseName = enterpriseRepository.findNameById(enterpriseId);
		//查询被删除员工对操作者的好友备注
		String friendRemarks = contactsRepository.findFriendRemarks(operatorId, userId);
		//查询操作者的企业备注
		String enterpriseRemarks = userEnterpriseDepartmentPositionRepository.findEnterpriseRemarks(operatorId, enterpriseId);
		
		String text = "";
		if(friendRemarks != null && friendRemarks.trim().length() != 0){
			text = "您已被管理员【" + friendRemarks + "】从" + enterpriseName + "中删除";
		}else{
			text = "您已被管理员【" + enterpriseRemarks + "】从" + enterpriseName + "中删除";
		}
		
		//向该公司的员工发送消息,告知公司已经将其踢出
		Message message = new Message();
		message.setUserid(userId);//设置接受者id
		message.setMessage(text);//设置消息内容
		message.setCreatetime(new Date());//设置创建时间
		message.setTitle("系统消息");//设置标题
		message.setMesType(5);//设置消息类型为"系统消息"
		message.setState(2);//设置消息状态为"未处理"
		message.setCreateId(operatorId);//设置创建者id
		message.setSendWay(2);//设置发送方式为"站内信"
		message.setIsRead(0);//设置为"未读"
		messageRepository.save(message);
		
		//获取到该员工所在部门id,及其父部门及父父部门的id,再将相应部门人数减一
		String departmentId = uedpList.get(0).getDepartmentId();
		if(departmentId != null && departmentId.trim().length() != 0){
			List<String> allParentDepartmentIdList = departmentService.findAllParentDepartmentId(departmentId);
			allParentDepartmentIdList.add(departmentId);
			departmentDao.minusDepartmentNumber(allParentDepartmentIdList);
		}
		
		//将公司人数减一
		enterpriseRepository.minusCurrentNumber(enterpriseId);
		
		//从公司删除该员工
		userEnterpriseDepartmentPositionRepository.delete(uedpList.get(0));
		
		//从公司的权限组中删除该员工
		userEnterprisePermissionGroupRepository.deleteByUserIdAndEnterpriseId(userId, enterpriseId);
		
		map.put("status", "success");
		map.put("msg", "操作成功");
		
		return map;
		
	}
	
	/**
	 * 修改公司名称
	 * @param enterpriseId 公司id
	 * @param enterpriseName 公司新名称
	 * @param userId 用户id
	 * @return status:error/success msg:错误或成功消息
	 */
	@Override
	public Map<String, Object> updateEnterpriseName(String enterpriseId, String enterpriseName, String userId) {
		enterpriseRepository.updateEnterpriseName(enterpriseId, enterpriseName, new Date());
		return MapAsReturn.setStatusSuccess(null, null);
	}

	/**
	 * 修改公司基本信息(所属行业、人员数量、所在地区)
	 * @param enterpriseId 公司id
	 * @param whichOne industry:行业   personNumRange:人员数量  province;city;area:省份;城市;城区
	 * @param whichOneId 行业id或人员数量id或省份id;城市id;城区id
	 * @return status:error/success msg:错误或成功消息
	 */
	@Override
	public Map<String, Object> updateEnterpriseBasicInfo(String enterpriseId, String whichOne, String whichOneId){
		switch(whichOne){
		case "industry":
			if(!industryRepository.exists(whichOneId)){
				return MapAsReturn.setStatusError("选择非法行业");
			}
			String industryName = getIndustryName(whichOneId);
			enterpriseRepository.updateIndustryByEnterpriseId(enterpriseId, industryName);
			return MapAsReturn.setStatusSuccess(null, null);
		case "personNumRange":
			if(!personNumRangeRepository.exists(whichOneId)){
				return MapAsReturn.setStatusError("选择非法人员范围");
			}
			String personNumRangeName = personNumRangeRepository.findOne(whichOneId).getRange();
			enterpriseRepository.updatePersonNumRangeByEnterpriseId(enterpriseId, personNumRangeName);
			return MapAsReturn.setStatusSuccess(null, null);
		case "province;city;area":
			String[] elementArray = whichOneId.split(";");
			if(elementArray.length != 3){
				return MapAsReturn.setStatusError("选择非法地区");
			}
			Province province = provinceRepository.findOne(elementArray[0]);
			City city = cityRepository.findOne(elementArray[1]);
			Area area = areaRepository.findOne(elementArray[2]);
			if(province == null || city == null || area == null){
				return MapAsReturn.setStatusError("选择空地区");
			}
			enterpriseRepository.updateLocationByEnterpriseId(enterpriseId, province.getName() + " " + city.getName() + " " + area.getName());
			return MapAsReturn.setStatusSuccess(null, null);
		default:
			return MapAsReturn.setStatusError("非法类型");
		}
	}
	
	/**
	 * 查看公司信息(公司名称,所属行业,人员数量,所在地区)
	 * @param enterpriseId 公司id
	 * @param userId 用户id
	 * @return status:error/success msg:错误或成功消息  name:公司名称  industry:所属行业  personRange:人员数量   location:所在地区  hasPermission:true/false(是否有更改权限)
	 */
	@Override
	public Map<String, Object> findEnterpriseInfo(String enterpriseId, String userId){
		Enterprise enterprise = enterpriseRepository.findById(enterpriseId);
		
		if(null == enterprise){
			return MapAsReturn.setStatusError("非法查询");
		}
		
		Map<String, Object> map = MapAsReturn.setStatusSuccess("name", enterprise.getName());
		map.put("industry", enterprise.getIndustry());
		map.put("personRange", enterprise.getNumberRange());
		map.put("location", enterprise.getLocation());
		
		UserEnterprisePermissionGroup uepg = userEnterprisePermissionGroupRepository.findByUserIdAndEnterpriseId(userId, enterpriseId);
		if(null == uepg){
			map.put("hasPermission", false);
		}else{
			if(uepg.getOpenDescription().contains("SUPER_PERMISSION") || uepg.getOpenDescription().contains("UPDATE_ENTERPRISE_BASIC_INFO")){
				map.put("hasPermission", true);
			}else{
				map.put("hasPermission", false);
			}
		}
		
		return map;
	}
	
	/**
	 * 查询某公司ItemVO
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息   enterpriseItem:公司ItemVO
	 */
	public Map<String, Object> findEnterpriseItemVOByEnterpriseId(String enterpriseId){
		Map<String, Object> map = new HashMap<>();
		
		boolean hasEnterprise = enterpriseRepository.exists(enterpriseId);
		if(!hasEnterprise){
			map.put("status", "error");
			map.put("msg", "查无此公司");
			return map;
		}

		map.put("enterpriseItem", enterpriseRepository.findEnterpriseItemVOByEnterpriseId(enterpriseId));
		map.put("status", "success");
		map.put("msg", "操作成功");
		
		return map;
	}
	
	/**
	 * 发送短信验证码,用来解散公司
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息
	 */
	@Override
	public Map<String, Object> sendCodeForDissolveEnterprise(String enterpriseId){
		Map<String, Object> map = managerService.findSuperManagerUsername(enterpriseId);
		if(map.get("status").equals("error")){
			return map;
		}
		
		String username = (String)map.get("username");
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<4; i++){
			sb.append(new Random().nextInt(10));
		}

		jedis.setex(username + "FordissolvedEnterprise", 5 * 60, sb.toString());
	
		try{
			SendCode.sendMessage(username, "验证码是" + sb.toString() + ",您正在申请解散公司,验证码5分钟内有效,如非本人操作,请忽略本信息,切勿转发告知他人。");
		}catch(Exception e){
			logger.error("发送解散公司验证码异常。enterpriseId=" + enterpriseId);
		}
		
		return MapAsReturn.setStatusSuccess(null, null);
	}
	
	/**
	 * 解散公司
	 * @param enterpriseId 公司id
	 * @param code 验证码
	 * @return status:error/success msg:错误或成功消息
	 */
	@Override
	public Map<String, Object> dissolveEnterprise(String operatorId, String enterpriseId, String code){
		//判断验证码是否正确
		Map<String, Object> map = managerService.findSuperManagerUsername(enterpriseId);
		if(map.get("status").equals("error")){
			return map;
		}
		String codeStored = jedis.get(map.get("username") + "FordissolvedEnterprise");
		if(StringUtils.isBlank(codeStored)){
			return MapAsReturn.setStatusError("验证码失效,请重新获取");
		}
		if(!code.equals(codeStored)){
			return MapAsReturn.setStatusError("验证码有误,请重新输入");
		}
		
		//查询公司下所有用户id
		List<String> userIdList = userEnterpriseDepartmentPositionRepository.findUserIdByEnterpriseId(enterpriseId);
		
		Date date = new Date();
		//将公司状态置为"已解散"
		enterpriseRepository.deleteByEnterpriseId(enterpriseId, date);
		
		//删除公司下的职级
		jobRankRepository.deleteByEnterpriseId(enterpriseId, date);
		
		//删除公司下的职位
		positionRepository.deleteByEnterpriseId(enterpriseId, date);
		
		//删除公司下的部门
		departmentRepository.deleteDepartmentByEnterpriseId(enterpriseId, date);
		
		//删除公司下的权限组中间表
		userEnterprisePermissionGroupRepository.deleteByEnterpriseId(enterpriseId);
		
		//删除公司下的职位中间表
		userEnterpriseDepartmentPositionRepository.deleteByEnterpriseId(enterpriseId);
		
		//告知用户解散公司(发送站内信、push、短信)
		String enterpriseName = enterpriseRepository.findNameById(enterpriseId);
		String text = "您所在的" + enterpriseName + "已被解散。";
		
		for(String element : userIdList){
			sendInMail(operatorId, element, text, date);//发送站内信
			sendPush(operatorId, userRepository.findUsernameById(element), text, date);//发送push
			sendShortMail(operatorId, element, text, date);//发送短信
		}
		
		return MapAsReturn.setStatusSuccess(null, null);
	}

	/**
	 * 查询当前公司id
	 * @param userId 用户id
	 * @return status:error/success msg:错误或成功消息  enterpriseId:公司id 
	 */
	@Override
	public Map<String, Object> findCurrentEnterpriseId(String userId){
		String enterpriseId = userActionInfoRedis.getLastChosenEnterpriseId(userId);
		return MapAsReturn.setStatusSuccess("enterpriseId", enterpriseId);
	}
	
	/**
	 * 发送与公司解散有关的站内信
	 * @param senderId 发送者id
	 * @param receiverId 接受者id
	 * @param text 消息内容
	 * @param date 创建时间
	 * @return
	 */
	private Message sendInMail(String senderId, String receiverId, String text, Date date){
		Message message = new Message();
		message.setCreateId(senderId);
		message.setUserid(receiverId);
		message.setMessage(text);
		message.setTitle("变更消息");
		message.setSendWay(2);
		message.setMesType(5);
		message.setState(null);
		message.setIsRead(0);
		message.setDetailId(null);
		message.setCreatetime(date);
		return messageRepository.save(message);
	}
	
	/**
	 * 发送与公司解散有关的短信,并保存发送短信记录
	 * @param senderId 发送者id
	 * @param receiverId 接受者id
	 * @param text 消息内容
	 * @param date 创建时间
	 * @return
	 */
	private Message sendShortMail(String senderId, String receiverId, String text, Date date){
		String receiverUsername = userRepository.findUsernameById(receiverId);
		//发送短信
		try{
			SendCode.sendMessage(receiverUsername, text);
		}catch(Exception e){
			logger.error("发送与管理员权限变更有关的短信时失败。");
		}
		
		//保存发送短信记录
		Message message = new Message();
		message.setCreateId(senderId);//设置创建者id为邀请者id
		message.setUserid(receiverUsername);//设置接受者id为被邀请者username
		message.setMessage(text);//设置消息内容
		message.setTitle("短信");//设置标题
		message.setSendWay(1);//设置发送方式为"短信"
		message.setMesType(5);//设置消息类型为"系统消息"
		message.setState(null);//设置消息状态为null
		message.setIsRead(null);//设置读状态为null
		message.setDetailId(null);//设置该消息所关联的邀请记录表中的记录
		message.setCreatetime(date);//设置创建时间
		return messageRepository.save(message);//保存消息记录
	}

	/**
	 * 发送与公司解散有关的push
	 * @param senderId 发送者id
	 * @param receiverUsername 接受者手机号
	 * @param text 短信内容
	 * @param date 创建时间
	 * @return
	 */
	private void sendPush(String senderId, String receiverUsername, String text, Date date){
		JPushRestAPI jPushRestAPI = new JPushRestAPI();
		List<PushSwitch> list = pushSwitchRepository.findAll();
		PushSwitch pushSwitch  = list.get(0);
		if (pushSwitch.getIsSwitch() == 0) {
			jPushRestAPI.setApns_production(false);
		}
		jPushRestAPI.SendPush(text, null, null, "Default", jPushRestAPI.jsonDataByOneAlias(receiverUsername));
	}

	/**
	 * 查找行业名称
	 * @param industryId 公司id
	 * @return
	 */
	private String getIndustryName(String industryId){
		Industry industry = industryRepository.findOne(industryId);
		String industryName = industry.getName();
		if(industryName.equals("其他未分类")){
			Industry parentIndustry = industryRepository.findOne(industry.getParentId());
			industryName = parentIndustry.getName() + " " + industryName;
		}
		
		return industryName;
	}
	
}

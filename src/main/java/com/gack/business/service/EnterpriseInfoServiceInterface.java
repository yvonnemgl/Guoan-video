package com.gack.business.service;

import com.gack.helper.common.abstractobj.Result;

/**
 * 
* @ClassName: EnterpriseInfoServiceInterface 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年5月31日 下午12:52:25 
* @version V1.0
 */
public interface EnterpriseInfoServiceInterface {
	public Result getEnterpriseInfo(String enterpriseId);
	public Result resetEnterpriseName(String userid,String enterpriseid,String newname);
	public Result dissolvedEnterprise(String userid,String enterpriseid);
	public Result commitEnterpriseAuthentiaction(String userid,String enterpriseid,String name,String contractName,String idNumber,
			String unifiedSocialCreditCode,String detailedAddress,String positiveIdCard,
			String negativeIdCard,String businessLicense,String administratorJobCertificate);
}

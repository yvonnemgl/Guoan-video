package com.gack.business.service;

import java.util.Map;

/**
 * 
 * @author ws
 * 2018-5-29
 */
public interface EnterpriseServiceInterface {

	/**
	 * 某用户创建公司
	 * @param userId 用户id
	 * @param name 公司名称
	 * @param industryId 行业id
	 * @param personNumRangeId 人员范围id
	 * @param provinceId 省份id
	 * @param cityId 城市id
	 * @param areaId 城区id
	 * @return status:error/success msg:错误或成功消息  enterpriseId:新创建公司的id
	 */
	Map<String, Object> createEnterprise(String userId, String name, String industryId, String personNumRangeId, String provinceId, String cityId, String areaId);
	
	/**
	 * 查询公司的组织架构(若公司id为空,则返回上次用户选择的公司的组织架构)
	 * @param enterpriseId 公司id
	 * @param userId 操作者id
	 * @return status:error/success msg:错误或成功消息   enterpriseName:公司名称    enterpriseNum:公司人数    departmentItemList:一级部门ItemVO集合  noDepartmentNum:无部门人数   isManager(是否是公司管理员):true/false enterpriseId:公司id
	 */
	Map<String, Object> findEnterpriseFramework(String enterpriseId, String userId);
	
	/**
	 * 删除员工
	 * @param enterpriseId 公司id
	 * @param userId 用户id
	 * @param operatorId 操作者id
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> deleteUserFromEnterprise(String enterpriseId, String userId, String operatorId);
	
	/**
	 * 修改公司名称
	 * @param enterpriseId 公司id
	 * @param enterpriseName 公司新名称
	 * @param userId 用户id
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> updateEnterpriseName(String enterpriseId, String enterpriseName, String userId);
	
	/**
	 * 修改公司基本信息(所属行业、人员数量、所在地区)
	 * @param enterpriseId 公司id
	 * @param whichOne industry:行业   personNumRange:人员数量  province;city;area:省份;城市;城区
	 * @param whichOneId 行业id或人员数量id或省份id;城市id;城区id
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> updateEnterpriseBasicInfo(String enterpriseId, String whichOne, String whichOneId);
	
	/**
	 * 查看公司信息(公司名称,所属行业,人员数量,所在地区)
	 * @param enterpriseId 公司id
	 * @param userId 用户id
	 * @return status:error/success msg:错误或成功消息  name:公司名称  industry:所属行业  personRange:人员数量   location:所在地区  hasPermission:true/false(是否有更改权限)
	 */
	Map<String, Object> findEnterpriseInfo(String enterpriseId, String userId);
	
	/**
	 * 查询某公司ItemVO
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息   enterpriseItem:公司ItemVO
	 */
	Map<String, Object> findEnterpriseItemVOByEnterpriseId(String enterpriseId);
	
	/**
	 * 发送短信验证码,用来解散公司
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> sendCodeForDissolveEnterprise(String enterpriseId);
	
	/**
	 * 解散公司
	 * @param operatorId 操作者id
	 * @param enterpriseId 公司id
	 * @param code 验证码
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> dissolveEnterprise(String operatorId, String enterpriseId, String code);
	
	/**
	 * 查询当前公司id
	 * @param userId 用户id
	 * @return status:error/success msg:错误或成功消息  enterpriseId:公司id
	 */
	Map<String, Object> findCurrentEnterpriseId(String userId);
}

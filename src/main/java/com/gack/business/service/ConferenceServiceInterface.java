package com.gack.business.service;

import java.util.List;
import java.util.Map;

import org.dom4j.DocumentException;

import com.gack.business.model.Conference;
import com.gack.business.model.Message;
import com.gack.business.vo.UpdateModel;
import com.gack.helper.common.abstractobj.Result;

public interface ConferenceServiceInterface {
	/**
	 * 
	
	  * @Title: createConference
	
	  * @Description: 创建会议室
	
	  * @param @param name 会议室名称
	  * @param @param topic 会议室主题
	  * @param @param uid 创建者id
	  * @param @param joinId 其他入会者的id数组
	  * @param @param type 会议室类型 1 普通会议，2 通讯录拉会
	  * @param @return
	  * @param @throws Exception    
	
	  * @return Result 
	
	  * @throws
	 */
	public Result createConference(String name, String topic, String uid, String[] joinId, Integer type) throws Exception;
	/**
	 * 
	
	  * @Title: checkType
	
	  * @Description: 根据创建者id，和被拉会人id,判断会议室类型
	
	  * @param @param uid 创建者id
	  * @param @param joinUid 被拉会人id
	  * @param @return    
	
	  * @return String 
	
	  * @throws
	 */
	public String checkType(String uid, String joinUid);
	/**
	 * 
	
	  * @Title: createConferenceCode
	
	  * @Description: 创建6～8位随机数字加字符串
	
	  * @param @return    
	
	  * @return String 
	
	  * @throws
	 */
	public String createConferenceCode();
	/**
	 * 
	
	  * @Title: createVidyo
	
	  * @Description: 传入6～8位conferenceCode，创建vidyo会议室
	
	  * @param @param conferenceCode
	  * @param @return
	  * @param @throws Exception    
	
	  * @return Conference 
	
	  * @throws
	 */
	public Conference createVidyo(String conferenceCode) throws Exception;
	/**
	 * 
	
	  * @Title: findConferences
	
	  * @Description: 根据用户id查找会议室列表
	
	  * @param @param uid
	  * @param @return    
	
	  * @return List<Conference> 
	
	  * @throws
	 */
	public List<Conference> findConferences(String uid);
	/**
	 * 
	
	  * @Title: searchConference
	
	  * @Description: 根据conferenceCode 搜索会议室
	
	  * @param @param code
	  * @param @return    
	
	  * @return Result 
	
	  * @throws
	 */
	public Result searchConference(String code);
	/**
	 * 
	
	  * @Title: conferenceDetail
	
	  * @Description: 获取会议室详情
	
	  * @param @param id
	  * @param @return    
	
	  * @return Conference 
	
	  * @throws
	 */
	public Conference conferenceDetail(String id);
	/**
	 * 
	
	  * @Title: exchangeManager
	
	  * @Description: 更换管理员
	
	  * @param @param uid
	  * @param @param id
	  * @param @param managerId
	  * @param @return    
	
	  * @return Result 
	
	  * @throws
	 */
	public Result exchangeManager(String uid, String id, String managerId);
	/**
	 * 
	    * @Title: removeConference
	    * @Description: 删除会议室
	    * @param @param id
	    * @param @return
	    * @param @throws Exception
	    * @return Result
	    * @throws
	 */
	public Result removeConference(String id) throws Exception;
	public Result removeVidyo(String roomId) throws Exception;
	/**
	 * 
	
	  * @Title: getParticipants
	
	  * @Description: 获取参与会议室的人
	
	  * @param @param id
	  * @param @param uid
	  * @param @return    
	
	  * @return Result 
	
	  * @throws
	 */
	public Result getParticipants(String id, String uid);
	/**
	 * 
	
	  * @Title: addParticipant
	
	  * @Description: 添加参会人员
	
	  * @param @param cid 会议室id
	  * @param @param uids 被邀请参会人员id数组(ios在接收数组时需要特殊处理uid.replaceAll("\"", ""))
	  * @param @param uid 创建者id
	  * @param @return    
	
	  * @return Result 
	
	  * @throws
	 */
	public Result addParticipant(String cid, String[] uids, String uid);
	/**
	 * 
	    * @Title: removeParticipant
	    * @Description: 踢人接口
	    * @param @param cid
	    * @param @param uids
	    * @param @param id
	    * @param @return
	    * @param @throws Exception
	    * @return Result
	    * @throws
	 */
	public Result removeParticipant(String cid, String[] uids, String id)  throws Exception;
	/**
	 * 
	
	  * @Title: joinConference
	
	  * @Description: 加入会议室后调用，获取vidyo分配的participant
	
	  * @param @param uid
	  * @param @param cid
	  * @param @return    
	
	  * @return Result 
	
	  * @throws
	 */
	public Result joinConference(String uid, String cid, Integer channel) throws DocumentException;
	/**
	 * 
	    * @Title: checkJoin
	    * @Description: 检查是否可以进入会议
	    * @param @param uid
	    * @param @param cid
	    * @param @return
	    * @return Result
	    * @throws
	 */
	public Result checkJoin(String uid, String cid);
	/**
	 * 
	    * @Title: updateJoinConference
	    * @Description: 更新入会出会的人员的状态
	    * @param 
	    * @return void
	    * @throws
	 */
	public void updateJoinConference() throws DocumentException;
	/**
	 * 
	    * @Title: beginRemind
	    * @Description: 设置参会提醒
	    * @param @param conferenceId 会议室id
	    * @param @param isSwitch 开关 0关 1开
	    * @param @param beginTime 开始时间
	    * @param @param firstTime 提醒时间
	    * @param @param secondTime 废弃字段
	    * @param @param id 参会提醒的业务id
	    * @param @return
	    * @param @throws Exception
	    * @return Result
	    * @throws
	 */
	public Result beginRemind(String conferenceId, Integer isSwitch,String beginTime,String firstTime,String secondTime, String id) throws Exception;
	/**
	 * 
	    * @Title: iosLeave
	    * @Description: ios切掉应用vidyo有bug，调用踢人，推出会议室
	    * @param @param cid
	    * @param @param uid
	    * @param @return
	    * @return Result
	    * @throws
	 */
	public Result iosLeave(String cid, String uid) throws DocumentException;
	/**
	 * 
	    * @Title: enterConference
	    * @Description: 加入会议室
	    * @param @param cid
	    * @param @param uid
	    * @param @return
	    * @return Result
	    * @throws
	 */
	public Result enterConference(String cid, String uid);
	/**
	 * 
	
	  * @Title: push
	
	  * @Description: 推送消息
	
	  * @param @param message
	  * @param @param cid    
	
	  * @return void 
	
	  * @throws
	 */
	public void push(Message message, String cid);
	/**
	 * 
	
	  * @Title: saveMessage
	
	  * @Description: 保存消息
	
	  * @param @param cid
	  * @param @param uids
	  * @param @param uid    
	
	  * @return void 
	
	  * @throws
	 */
	public void saveMessage(String cid, String[] uids, String uid);
	/**
	 * 
	    * @Title: autoRemind
	    * @Description: 参会提醒
	    * @param @throws Exception
	    * @return void
	    * @throws
	 */
	public void autoRemind() throws Exception;
	/**
	 * 
	    * @Title: leaveConference
	    * @Description: 离开会议室
	    * @param @param uid
	    * @param @param cid
	    * @param @return
	    * @param @throws Exception
	    * @return Result
	    * @throws
	 */
	public Result leaveConference(String uid, String cid) throws Exception;
	/**
	 * 
	    * @Title: updateConferenceCode
	    * @Description: 更新会议室编号
	    * @param @param cid
	    * @param @param uid
	    * @param @return
	    * @return Result
	    * @throws
	 */
	public Result updateConferenceCode(String cid, String uid);
	/**
	 * 
	    * @Title: updateName
	    * @Description: 更新会议室名称
	    * @param @param uid
	    * @param @param cid
	    * @param @param name
	    * @param @return
	    * @return Result
	    * @throws
	 */
	public Result updateName(String uid, String cid, String name);
	/**
	 * 
	    * @Title: updateTopic
	    * @Description: 更新主题
	    * @param @param uid
	    * @param @param cid
	    * @param @param topic
	    * @param @return
	    * @return Result
	    * @throws
	 */
	public Result updateTopic(String uid, String cid, String topic);
	/**
	 * 
	    * @Title: findRemindTime
	    * @Description: 查询参会提醒
	    * @param @param cid
	    * @param @return
	    * @return Result
	    * @throws
	 */
	public Result findRemindTime(String cid);
	/**
	 * 
	    * @Title: findConferene
	    * @Description: PC端查找会议室
	    * @param @param conferenceId
	    * @param @return
	    * @return Result
	    * @throws
	 */
	public Result findConferene(String conferenceId);
	/**
	 * 
	    * @Title: recentlyConference
	    * @Description: PC端会议室列表
	    * @param @param uid
	    * @param @param page
	    * @param @param size
	    * @param @return
	    * @return Map<String,Object>
	    * @throws
	 */
	public Map<String, Object> recentlyConference(String uid, Integer page, Integer size);
	/**
	 * 
	    * @Title: toEnterConference
	    * @Description: PC加入会议室
	    * @param @param uid
	    * @param @param cid
	    * @param @return
	    * @return Result
	    * @throws
	 */
	public Result toEnterConference(String uid, String cid);
	/**
	 * 
	    * @Title: updateUserVideoDate
	    * @Description: 每一小时更新用户使用会议室情况
	    * @param @return
	    * @return Result
	    * @throws
	 */
	public Result updateUserVideoDate();
	/**
	 * 
	    * @Title: PushAPI
	    * @Description: 封装了一个推送
	    * @param @param cid
	    * @param @param mes_type
	    * @param @param object
	    * @return void
	    * @throws
	 */
	public void PushAPI(String cid,String mes_type, Object object);
	public Result inviteToConference(String conferenceID, String invite) throws Exception;
	public Result JoininviteToConference(String conferenceID, String invite) throws Exception;
	public Result leaveinviteToConference(String conferenceID, String participantID) throws Exception;
	public Result joinConferenceCameras(String conferenceCode, String storeCamerasId) throws Exception;
	public Result leaveConferenceCameras(String storeCamerasId) throws Exception;
	public Result leaveConferenceVidyo(String conferenceID, String participantID) throws Exception;
	public Result createAndJoinConferenceCameras(String cid, String[] storeCamerasId) throws Exception;
	/**
	 * 
	    * @Title: detailLeaveConferenceCamera
	    * @Description: 详情页管理员移除设备
	    * @param @param cid
	    * @param @param storeCamerasId
	    * @param @return
	    * @param @throws Exception
	    * @return Result
	    * @throws
	 */
	public Result detailLeaveConferenceCamera(String cid, String[] storeCamerasId) throws Exception;
	/**
	 * 
	    * @Title: getConferenceCamera
	    * @Description: 或许详情页的摄像头
	    * @param @param cid
	    * @param @return
	    * @param @throws Exception
	    * @return Result
	    * @throws
	 */
	public Result getConferenceCamera(String cid) throws Exception;
	/**
	 * 
	
	  * @Title: updateLastModifyTime
	
	  * @Description: 更新对会议室进行操作的时间
	
	  * @param @param cid
	  * @param @param uid    
	
	  * @return void 
	
	  * @throws
	 */
	public void updateLastModifyTime(String cid, String uid);

}

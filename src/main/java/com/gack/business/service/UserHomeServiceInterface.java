package com.gack.business.service;

import java.util.Map;

import com.gack.helper.common.abstractobj.Result;

/**
 * 
* @ClassName: UserHomeServiceInterface 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月12日 下午2:47:58 
* @version V1.0
 */
public interface UserHomeServiceInterface {
	public Result sendIdentifyCode(String username);
	public Map<String, Object> getIdentifyPic();
	public Result homeLogin(String username,String password,String code,String validateid);
	public Result addHomeOpinion(String opiniontext,String username,String code,Integer type);
}

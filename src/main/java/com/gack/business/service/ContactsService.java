package com.gack.business.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.dao.ContactsDao;
import com.gack.business.model.Contacts;
import com.gack.business.model.User;
import com.gack.business.repository.ContactsRepository;
import com.gack.business.repository.UserRepository;
import com.gack.business.vo.ContactsVo;
import com.gack.helper.common.abstractobj.Result;






@Service
public class ContactsService implements ContactsServiceInterface {
	@PersistenceContext
	private EntityManager manager;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private ContactsRepository contactsRepository;
	@Autowired
	private ContactsDao contactsDao; 
	
	@Override
	public List<ContactsVo> selectContactsList(String username, String nickname) {
		// TODO Auto-generated method stub
		String sql = "SELECT u.nickname,c.user_username,c.id,u.username,c.friend_remarks,c.userid_friend_id,u.portrait,c.friend_addtime,c.friend_passtime FROM"     
				+ " contacts c left join user u on u.id=c.userid_friend_id ";
		sql+=" where 1=1";
		sql+=" and type=1";
		if(username!=null&&!username.equals("")){
			sql+=" and c.user_username=:username";
			
			}
		/*
		if(nickname!=null&&!nickname.equals("")){
			sql+=" and u.username like :nickname";
			
			}
		
		sql+=" union  SELECT u.nickname,c.user_username,c.id,u.username,c.friend_remarks,c.userid_friend_id,u.portrait,c.friend_addtime,c.friend_passtime FROM"
				+ " contacts c left join user u on u.id=c.userid_friend_id";
		sql+=" where 1=1";
		sql+=" and type=1";
		if(username!=null&&!username.equals("")){
			sql+=" and c.user_username like :username";
			
			}
		if(nickname!=null&&!nickname.equals("")){
			
			
			sql+=" and u.nickname like :nickname";
		
			
	}
		
		sql+=" union  SELECT u.nickname,c.user_username,c.id,u.username,c.friend_remarks,c.userid_friend_id,u.portrait,c.friend_addtime,c.friend_passtime FROM"
				+ " contacts c left join user u on u.id=c.userid_friend_id";
		sql+=" where 1=1";
		sql+=" and type=1";
		if(username!=null&&!username.equals("")){
			sql+=" and c.user_username like :username";
			
			}
		if(nickname!=null&&!nickname.equals("")){
			
			
			sql+=" and c.friend_remarks like :nickname";
			
			
			}*/
		
		Query query = manager.createNativeQuery(sql,ContactsVo.class);
		
		query.setParameter("username",username);
		/*if(nickname!=null&&!nickname.equals("")){
		query.setParameter("nickname", "%"+nickname+"%");
		}*/
		List<ContactsVo> enlist =(List<ContactsVo>)query.getResultList();
		
		return enlist;
	}
	@Override
	public List<ContactsVo> selectUserName(String username) {
		// TODO Auto-generated method stub
		
String sql="SELECT u.nickname,c.user_username,c.id,u.username,c.friend_remarks,c.userid_friend_id,u.portrait,c.friend_addtime,c.friend_passtime FROM"
				+ " contacts c left join user u on u.id=c.user_userid";
		sql+=" where 1=1";
		sql+=" and type=0";
		if(username!=null&&!username.equals("")){
			sql+=" and c.userid_friend_username like '%" + username + "%'";
			
			}
Query query = manager.createNativeQuery(sql,ContactsVo.class);
		
		List<ContactsVo> enlist =(List<ContactsVo>)query.getResultList();
		return enlist;
		
	}
	/**
	 * userUsername 发起者 useridFriendUsername接受者
	* @Title: delectAllBycitycode 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param userUsername
	* @param useridFriendUsername    设定文件 
	* @return void    返回类型 
	* @throws 
	 */
	@Transactional
	  public void delectAllBycitycode(String userUsername,String useridFriendUsername){
		  String jpql="delete  from Contacts  u where u.userUsername=:userUsername and u.useridFriendUsername=:useridFriendUsername";
		  Query query2=manager.createQuery(jpql);//
			query2.setParameter("userUsername", userUsername);
			query2.setParameter("useridFriendUsername", useridFriendUsername);
			query2.executeUpdate();
			 String jpql1="delete  from Contacts  u where u.userUsername=:userUsername and u.useridFriendUsername=:useridFriendUsername";
			  Query query3=manager.createQuery(jpql);//查询商品提供的服务区域
				query3.setParameter("userUsername", useridFriendUsername);
				query3.setParameter("useridFriendUsername", userUsername);
				query3.executeUpdate();
		  
	  }

	/**
	 * 修改某用户的好友备注
	 * @param userId 被修改者id
	 * @param operatorId 操作者id
	 * @return
	 */
	@Transactional
	@Override
	public Map<String, Object> updateFriendRemarks(String userId, String operatorId, String friendRemarks) {
		Map<String, Object> map = new HashMap<>();
		
		if(friendRemarks == null || friendRemarks.trim().length() == 0){
			contactsDao.setFriendRemarksNull(userId, operatorId);
		}else{
			contactsRepository.updateFriendRemarks(userId, operatorId, friendRemarks);
		}
		
		map.put("status", "success");
		map.put("msg", "操作成功");
		return map;
	}
	@Override
	@Transactional
	public Result updateContastsInformation(String username) {
		// TODO Auto-generated method stub
		Result result = new Result();
		List<Contacts> contactslist=contactsRepository.findContactsforuseriduseridFriendUsername(username);
		
		if(contactslist.size()>0){
			/*try{*/
			for(Contacts cts:contactslist){
				
				User user=userRepository.findUserforUsername(cts.getUserUsername());//发起者
				cts.setUserUserid(user.getId());
				cts.setType(0);
				User user1=userRepository.findUserforUsername(cts.getUseridFriendUsername());//受邀者
				cts.setUseridFriendId(user1.getId());
				contactsRepository.save(cts);
			}
			result.setKey("success");
			result.setValue("更新完毕");
			/*}catch(Exception e){
				e.printStackTrace();
				result.setKey("error");
				result.setValue("跟新失败");
		}*/
		}else{
			result.setKey("success");
			result.setValue("没有可以更新的内容");
		}
		
		return result;
	}
	
}
 
package com.gack.business.service;

import java.util.List;
import java.util.Map;

import com.gack.business.model.StoreSupportFacity;
import com.gack.business.model.StoreSupportService;
import com.gack.helper.common.abstractobj.Result;

/**
 * 
* @ClassName: StoreSupportFacityAndServiceServiceInterface 
* @Description: TODO(门店配套设施及服务操作接口) 
* @author (ZhangXinYu)  
* @date 2018年6月4日 上午11:28:55 
* @version V1.0
 */
public interface StoreSupportFacityAndServiceServiceInterface {
	public Result AddStoreSupportFacity(String facityName);
	public Result AddStoreSupportService(String serviceName);
	
	public List<StoreSupportFacity> getAllFacitys();
	public List<StoreSupportService> getAllServices();
	
	public Map<String, Object> getAllFacitysAndServices();
}

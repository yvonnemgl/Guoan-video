package com.gack.business.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.controller.AssignBusinessCardMoneyController;
import com.gack.business.dao.AssignBusinessCardMoneyDao;

/**
 * 
 * @author ws
 * 2018-6-20
 */
@Service
@Transactional
public class AssignBusinessCardMoneyService implements AssignBusinessCardMoneyServiceInterface{

	@Autowired
	private AssignBusinessCardMoneyDao assignBusinessCardMoneyDao;
	
	/**
	 * 根据公司id,获取某公司的一级部门的名称以及剩余金额
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息  topDepartmentList:一级部门的名称以及剩余金额实体集合
	 */ 
	public Map<String, Object> findTopDepartmentList(String enterpriseId){
		Map<String, Object> map = new HashMap<>();
		
		List<AssignBusinessCardMoneyController.TopDepartment> list = assignBusinessCardMoneyDao.findTopDepartmentList(enterpriseId);
		map.put("status", "success");
		map.put("msg", "操作成功");
		map.put("topDepartmentList", list);
		
		return map;
	}
	
	/**
	 * 根据公司id,获取某公司的一级部门的名称以及剩余金额(剩余可用额度大于0的一级部门)
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息  topDepartmentList:一级部门的名称以及剩余金额实体集合
	 */ 
	public Map<String, Object> findAssignedTopDepartmentList(String enterpriseId){
		Map<String, Object> map = new HashMap<>();
		
		List<AssignBusinessCardMoneyController.TopDepartment> list = assignBusinessCardMoneyDao.findTopDepartmentList(enterpriseId);
		List<AssignBusinessCardMoneyController.TopDepartment> resultList = new ArrayList<>();
		for(AssignBusinessCardMoneyController.TopDepartment t : list){
			if(t.getSurplus_amount() > 0){
				resultList.add(t);
			}
		}
		map.put("status", "success");
		map.put("msg", "操作成功");
		map.put("topDepartmentList", resultList);
		
		return map;
	}
	
	/**
	 * 根据公司id和部门id获取该部门可调配金额的最大值(即公司剩余可用额度+部门剩余可用额度)
	 * @param enterpriseId 公司id
	 * @param departmentId 一级部门id
	 * @return status:error/success msg:错误或成功消息  money:可调配金额的最大值
	 */
	public Map<String, Object> findMaxCouldAssignedMoney(String enterpriseId, String departmentId){
		Map<String, Object> map = new HashMap<>();
		
		//检查该公司下是否有此一级部门
		long num = assignBusinessCardMoneyDao.isTopDepartmentInEnterprise(enterpriseId, departmentId);
		if(num == 0){
			map.put("status", "error");
			map.put("msg", "公司或部门有误");
			return map;
		}
		
		List<Object[]> list = assignBusinessCardMoneyDao.findMaxCouldAssignedMoney(enterpriseId, departmentId);
		if(list.size() > 0){
			map.put("status", "success");
			map.put("msg", "操作成功");
			map.put("money", ((BigInteger)list.get(0)[0]).intValue() + ((BigInteger)list.get(0)[1]).intValue());
			return map;
		}
		
		map.put("status", "error");
		map.put("msg", "查询有误");
		return map;
	}

	/**
	 * 设置某公司的某一级部门的剩余可用额度(在更新数据库前,需判定公司可用金额以及部门可用金额之和是否大于等于设定金额)
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @param money 金额(分)
	 * @return status:error/success msg:错误或成功消息
	 */
	public Map<String, Object> setDepartmentSurplusAmount(String enterpriseId, String departmentId, String money) throws Exception{
		Map<String, Object> map = new HashMap<>();
		
		//检查该公司下是否有此一级部门
		long num = assignBusinessCardMoneyDao.isTopDepartmentInEnterprise(enterpriseId, departmentId);
		if(num == 0){
			map.put("status", "error");
			map.put("msg", "公司或部门有误");
			return map;
		}
		
		List<Object[]> list = assignBusinessCardMoneyDao.findMaxCouldAssignedMoney(enterpriseId, departmentId);
		if(list.size() == 0){
			map.put("status", "error");
			map.put("msg", "查询有误");
			return map;
		}
		
		int maxMoney = ((BigInteger)list.get(0)[0]).intValue() + ((BigInteger)list.get(0)[1]).intValue();
		if(maxMoney < Integer.parseInt(money)){
			map.put("status", "error");
			map.put("msg", "输入金额有误,请重试");
			return map;
		}
		
		int result1 = 0;
		int result2 = 0;
		//设置部门剩余可用额度
		result1 = assignBusinessCardMoneyDao.setDepartmentSurplusAmount(enterpriseId, departmentId, Integer.parseInt(money));
		//减少公司剩余可用额度
		if(result1 == 1){
			result2 = assignBusinessCardMoneyDao.setEnterpriseSurplusAmount(enterpriseId, maxMoney-Integer.parseInt(money));
			if(result2 != 1){//部门和公司剩余可用额度需同时设定成功,否则回滚
				throw new RuntimeException("---msg:操作失败");
			}
			map.put("status", "success");
			map.put("msg", "操作成功");
		}
		
		map.put("status", "error");
		map.put("msg", "操作失败");
		return map;
	}
	
}

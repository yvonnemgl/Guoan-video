package com.gack.business.service;

import java.util.List;
import java.util.Map;

import com.gack.business.model.PermissionGroup;

/**
 * 
 * @author ws
 * 2018-6-2
 */
public interface ManagerServiceInterface {

	/**
	 * 查找公司的所有管理员(包含主管理员)
	 * @param enterpriseId 公司id
	 * @param userId 操作者id
	 * @return status:error/success  msg:错误或成功消息  managerList:管理员集合(不包含主管理员) superManager:主管理员
	 */
	Map<String, Object> findAllManager(String enterpriseId, String userId);
	
	/**
	 * 删除管理员
	 * @param userId 用户id
	 * @param enterpriseId 公司id
	 * @param operatorId 操作者id
	 * @return  status:error/success msg:错误或成功消息
	 */
	Map<String, Object> deleteManager(String userId, String enterpriseId, String operatorId);
	
	/**
	 * 根据用户id、公司id查找权限组(若无权限组,则返回默认权限组)
	 * @param userId 用户id
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息  permissionGroupList:PermissionGroup集合
	 */
	Map<String, Object> findPermissionGroupList(String userId, String enterpriseId);
	
	/**
	 * 保存权限组
	 * @param userId 用户id
	 * @param enterpriseId 公司id
	 * @param permissionGroupList 权限组集合
	 * @param operatorId 操作者id
	 * @return status:error/success msg:错误或成功消息  managerList:管理员集合(包含主管理员)
	 */
	Map<String, Object> savePermissionGroup(String userId, String enterpriseId, List<PermissionGroup> permissionGroupList, String operatorId);
	
	/**
	 * 查找某人所具有的权限
	 * @param enterpriseId 公司id
	 * @param userId 用户id
	 * @return status:error/success msg:错误或成功消息   permissionList:Permission集合
	 */
	Map<String, Object> findPermissionByUserId(String enterpriseId, String userId);
	
	
	/**
	 * 查找公司超级管理员的手机号
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息  username:超级管理员手机号
	 */
	Map<String, Object> findSuperManagerUsername(String enterpriseId);
	
	/**
	 * 发送短信验证码,用来更换超级管理员
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> sendCodeForSetSuperManager(String enterpriseId);
	
	/**
	 * 更换超级管理员
	 * @param operatorId 操作者id
	 * @param enterpriseId 公司id
	 * @param userId 用户id
	 * @param text 验证码
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> setSuperManager(String operatorId, String enterpriseId, String userId, String text);
}

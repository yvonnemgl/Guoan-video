package com.gack.business.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.model.VideoMeeting;
import com.gack.business.model.VideoParticipants;
import com.gack.business.repository.VideoMeetingRepository;
import com.gack.helper.common.util.PackerUtil;
import com.gack.helper.common.util.WebServiceUtils;





@Service
public class VideoMeetingService {
	@Autowired
	private VideoMeetingRepository videoMeetingRepository;

	    public List<VideoMeeting> getAllVideoMeeting() {
	        return videoMeetingRepository.findAll();
	    }
	    public void addAllVideoMeeting(Map remap) {
	    	
	    	Map canshu=new HashMap<String,String>();
			PackerUtil packerUtil=new PackerUtil();
			packerUtil.packBody("v1:AddRoomRequest", canshu);
			String data = packerUtil.xDoc.getDocument().asXML();
			System.out.println(data);
			//调用webservice创建room接口
			Map<String,String> fanhuimap=new HashMap<String, String>();
			String xmlStr=WebServiceUtils.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService",data,"addRoom");
	    	
	       
	    }
	    
	/**
	 * 查询会议室存在人数
	 * @param roomId
	 * @return
	 */
	public Map<String, Object> getParticipants(String roomId) {
		String xmlStr = WebServiceUtils.getParticipants(roomId);
		//从total标签中获得参加会议人数总数量
		Pattern pattern1 = Pattern.compile("<ns1:total>(.*)</ns1:total>");//匹配总人数正则
		Matcher matcher1 = pattern1.matcher(xmlStr);
		Integer total = 0;
		if (matcher1.find()) {
			total = Integer.parseInt(matcher1.group(1));//查询总人数
		}
		List<VideoParticipants> list= new ArrayList<>();
		Pattern pattern2 = Pattern.compile("<ns1:displayName>([^<>]*)</ns1:displayName>");//匹配name正则
		Matcher matcher2 = pattern2.matcher(xmlStr);
		while (matcher2.find()) {
			VideoParticipants port = new VideoParticipants();
			port.setDisplayName(matcher2.group(1));//查询displayName
			list.add(port);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("total", total);
		map.put("participants", list);
		return map;
	}
	


	  public VideoMeeting updateVideoMeeting(VideoMeeting videoMeeting) {
	        return videoMeetingRepository.save(videoMeeting);
	    }


	/**
	 * 传入会议32位ID 去查询roomID 根据roomID查询详情
	 * @param id 32位业务id
	 * @return
	 */
	public Map<String, Object> getParticipant(String id) {
		Map<String, Object> map = new HashMap<>();
		if (id != null && !id.equals("")) {
			VideoMeeting meeting = videoMeetingRepository.findOne(id);
			if (meeting != null) {
				map = getParticipants(meeting.getRoomid());
			}
			
		}
		return map;
	}
	
	public VideoMeeting updateVideoMeeting(String roomId) {
		String xmlStr = WebServiceUtils.getParticipants(roomId);
		//从total标签中获得参加会议人数总数量
		Pattern pattern1 = Pattern.compile("<ns1:total>(.*)</ns1:total>");//匹配总人数正则
		Matcher matcher1 = pattern1.matcher(xmlStr);
		Integer total = 0;
		if (matcher1.find()) {
			total = Integer.parseInt(matcher1.group(1));//查询总人数
		}
		VideoMeeting videoMeeting=videoMeetingRepository.selectByOneVideoMeeting(roomId);
		videoMeeting.setPeopleNum(total);
        return videoMeetingRepository.save(videoMeeting);
    }

}

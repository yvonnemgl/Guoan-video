package com.gack.business.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.gack.business.model.VideoStores;
import com.gack.business.repository.VideoStoresRepository;
import com.gack.helper.common.abstractobj.Result;

/**
 * 
* @ClassName: StoresOptService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月4日 下午3:09:54 
* @version V1.0
 */
@Service
public class StoresOptService implements StoresOptServiceInterface{

	@Autowired
	private VideoStoresRepository storesRepository;
	
	/**
	 * 查询门店信息
	 * searchType  查询类型  1 门店名称查询  2门店面积查询  3门店使用价格查询
	 */
	@Override
	public Map<String, Object> getStores(Integer searchType, String searchKey, Integer currentPage, Integer pageSize) {
		Map<String , Object> map = new HashMap<>();
		Sort sort = new Sort(Sort.Direction.DESC, "createtime");
		Pageable pageable = new PageRequest(currentPage, pageSize, sort);
		Page<VideoStores> stores = null;
		if(searchType == 1){
			stores = storesRepository.findByStoreNameLike("%"+searchKey+"%", pageable);
		}else if(searchType == 2){
			stores = storesRepository.findByStoreArea(Integer.valueOf(searchKey), pageable);
		}else if(searchType == 3){
			stores = storesRepository.findByStorePrice(Integer.valueOf(searchKey), pageable);
		}
		Integer deposit = storesRepository.findMaxDeposit();
		map.put("key", "success");
		map.put("stores", stores);
		map.put("deposit", deposit);
		return map;
	}

	/**
	 * 非条件查询门店信息
	 */
	@Override
	public Map<String, Object> getAllStores(Integer currentPage, Integer pageSize) {
		Map<String , Object> map = new HashMap<>();
		Sort sort = new Sort(Sort.Direction.DESC,"createtime");
		Pageable pageable = new PageRequest(currentPage, pageSize,sort);
		Page<VideoStores> stores = null;
		stores = storesRepository.findAll(pageable);
		Integer deposit = storesRepository.findMaxDeposit();
		map.put("key", "success");
		map.put("stores", stores);
		map.put("deposit", deposit);
		return map;
	}
	
	@Override
	public List<VideoStores> getAllStores(){
		return storesRepository.findAll();
	}

	/**
	 * 门店细信息导出
	 */
	@Override
	public Workbook exportStores() {
		Workbook wb = new HSSFWorkbook();
		Sheet sheet = wb.createSheet("门店信息");
		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("编号");
		header.createCell(1).setCellValue("门店名称");
		header.createCell(2).setCellValue("门店地址");
		header.createCell(3).setCellValue("门店开始营业时间");
		header.createCell(4).setCellValue("门店结束营业时间");
		header.createCell(5).setCellValue("门店配套设置");
		header.createCell(6).setCellValue("门店配套服务");
		header.createCell(7).setCellValue("门店管理者");
		header.createCell(8).setCellValue("门店管理者联系方式");
		header.createCell(9).setCellValue("门店座机号码");
		header.createCell(10).setCellValue("注意事项");
		header.createCell(11).setCellValue("门店经度");
		header.createCell(12).setCellValue("门店维度");
		header.createCell(13).setCellValue("门店首页图片");
		header.createCell(14).setCellValue("门店第一张图片");
		header.createCell(15).setCellValue("门店第二张图片");
		header.createCell(16).setCellValue("门店第三张图片");
		header.createCell(17).setCellValue("门店第四张图片");
		header.createCell(18).setCellValue("门店第五张图片");
		header.createCell(19).setCellValue("门店第六张图片");
		header.createCell(20).setCellValue("门店面积");
		header.createCell(21).setCellValue("门店押金");
		header.createCell(22).setCellValue("门店价格");
		header.createCell(23).setCellValue("门店状态");
		List<VideoStores> stores = storesRepository.findAll();
		for(int i=0;i<stores.size();i++){
			VideoStores store = stores.get(i);
			Row dataRow = sheet.createRow(sheet.getLastRowNum() + 1);
			dataRow.createCell(0).setCellValue(store.getId()!=null?store.getId():"");
			dataRow.createCell(1).setCellValue(store.getStoreName()!=null?store.getStoreName():"");
			dataRow.createCell(2).setCellValue(store.getStoreAddress()!=null?store.getStoreAddress():"");
			Date startDate = store.getStarttime();
			Date endDate = store.getEndtime();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
			dataRow.createCell(3).setCellValue(startDate!=null?simpleDateFormat.format(startDate):"");
			dataRow.createCell(4).setCellValue(endDate!=null?simpleDateFormat.format(endDate):"");
			dataRow.createCell(5).setCellValue(store.getSupportfacity()!=null?store.getSupportfacity():"");
			dataRow.createCell(6).setCellValue(store.getStoreService()!=null?store.getStoreService():"");
			dataRow.createCell(7).setCellValue(store.getStoreUser()!=null?store.getStoreUser():"");
			dataRow.createCell(8).setCellValue(store.getStorePhone()!=null?store.getStorePhone():"");
			dataRow.createCell(9).setCellValue(store.getStoreLandline()!=null?store.getStoreLandline():"");
			dataRow.createCell(10).setCellValue(store.getAttention()!=null?store.getAttention():"");
			dataRow.createCell(11).setCellValue(store.getLongitude()!=null?store.getLongitude():"");
			dataRow.createCell(12).setCellValue(store.getLatitude()!=null?store.getLatitude():"");
			dataRow.createCell(13).setCellValue(store.getStorep_hotourl()!=null?store.getStorep_hotourl():"");
			dataRow.createCell(14).setCellValue(store.getOnephotourl()!=null?store.getOnephotourl():"");
			dataRow.createCell(15).setCellValue(store.getTwophoneurl()!=null?store.getTwophoneurl():"");
			dataRow.createCell(16).setCellValue(store.getThreephotourl()!=null?store.getThreephotourl():"");
			dataRow.createCell(17).setCellValue(store.getFourphotourl()!=null?store.getFourphotourl():"");
			dataRow.createCell(18).setCellValue(store.getFivephotourl()!=null?store.getFivephotourl():"");
			dataRow.createCell(19).setCellValue(store.getSixphotourl()!=null?store.getSixphotourl():"");
			dataRow.createCell(20).setCellValue(store.getStoreArea());
			dataRow.createCell(21).setCellValue(store.getStoreDeposit());
			dataRow.createCell(22).setCellValue(store.getStorePrice());
			String status = "";
			if(store.getStatus() == 0){
				status = "可使用";
			}else if(store.getStatus() == 1){
				status = "预约中";
			}else if(store.getStatus() == 2){
				status = "使用中";
			}
			dataRow.createCell(23).setCellValue(status);
		}
		return wb;
	}

	/**
	 *  获取门店详情
	 */
	@Override
	public Result getStoresInfo(String storeid) {
		Result result = new Result();
		VideoStores stores = storesRepository.findOne(storeid);
		if(stores == null){
			result.setKey("error_noneStores");
			result.setMsg("门店不存在，门店详情查询失败");
			return result;
		}
		result.setKey("success");
		result.setValue(stores);
		return result;
	}
	
	
}

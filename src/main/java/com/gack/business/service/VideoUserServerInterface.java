package com.gack.business.service;

import java.util.Map;

import com.gack.helper.common.abstractobj.Result;

/**
 * 
 * 
 * 
* @ClassName: VideoUserServerInterface.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月20日
*  
 */
public interface VideoUserServerInterface {
	public Result sendIdentifyCode(String username);
	public Result sendIdentifyCodeByRegistUser(String username);
	public Result registUser(String username, String password, String identifyCode,String source);
	public Result loginByPassword(String username,String password,String type,String equipmentid);
	public Result loginByIdentifyCode(String username,String identifyCode,String type,String equipmentid);
	public Result signOut(String id,String type);
	public Result resetPassword(String  username,String identifyCode,String password);
	public Result feedbackOpinion(String id,String opiniontext,String source);
	public Result resetNickname(String id,String nickname);
	public Result forgetOldPassword(String id,String password,String identifyCode);
	public Result getUserInfo(String id);
	public Result resetUserPortait(String id,String portait);
	public Map<String, Object> heartbeat(String id,String token,String passwordToken);
	public Result getUserNickname(String id);
	
	public Result testLoginByPassword(String username,String password,String type,String equipmentid,String token);
	public Result testLoginByIdentifyCode(String username,String identifyCode,String type,String equipmentid);
	
	public Result getLoginStatus(String userid,String type,String equimentid, String passwordToken);
	
	public Map<String, Object> validateIdentifyCode(String mobile,String code);
	public Map<String, Object> validateIdentifyCodeForForget(String mobile,String code);
	public Map<String, Object> registByPc(String username,String nickname,String password,String source);
	public Map<String, Object> sendIdentifyForForget(String username);
	public Map<String, Object> sendIdentifyForRegist(String username);
	
}

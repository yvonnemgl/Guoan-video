package com.gack.business.service;

import java.util.List;

import com.gack.business.fkbModel.output.DepartmentOutputData;
import com.gack.business.fkbModel.output.EnterpriseOutputData;
import com.gack.business.fkbModel.output.PeopleOutputData;
import com.gack.business.fkbModel.output.PositionOutputData;

import net.sf.json.JSONObject;

/**
 * 费控宝接口(被费控宝调用)的service层
 * @author ws
 * 2018-7-18
 */
public interface FKBServiceInterface {

	/**
	 * 公司查询(若orgcode为空,则查询所有公司;否则,查询指定id的公司)
	 * @param orgcode 公司id 可为空
	 * @return
	 */
	List<EnterpriseOutputData> findEnterprise(String orgcode);
	
	/**
	 * 部门查询(若deptcode为空,则查询公司下所有部门;否则,查询指定部门信息及其子部门信息)
	 * @param orgcode 公司id 不可为空
	 * @param deptcode 部门id 可为空
	 * @return
	 */
	List<DepartmentOutputData> findDepartment(String orgcode, String deptcode);
	
	/**
	 * 职位职级查询
	 * @param orgcode 公司id
	 * @return
	 */
	List<PositionOutputData> findPosition(String orgcode);
	
	/**
	 * 人员信息查询
	 * 情况①：若部门编码为空，人员编码为空，则返回指定公司下所有人员信息
	 * 情况②：若部门编码不为空，人员编码为空，则返回指定部门下所有人员信息(包括子部门人员信息)
	 * 情况③：若部门编码为空，人员编码不为空，则返回指定公司下指定人员信息
	 * 情况④：若部门编码不为空，人员编码不为空，则返回指定部门下指定人员信息
	 * @param orgcode 公司id 不可为空
	 * @param deptcode 部门id 可为空
	 * @param peoplecode 人员id 可为空
	 * @return
	 */
	List<PeopleOutputData> findPeople(String orgcode, String deptcode, String peoplecode);
	
	/**
	 * 移动消息推送
	 * @param operatorNo 通知对象(人员登录名，多个用逗号隔开)
	 * @param remarks 通知内容
	 * @param H5url 手机端入口链接
	 * @param PCurl PC端入口链接
	 * @param type 1.审批提醒 2.消息提醒 3.其他
	 */
	JSONObject pushMessage(String operatorNo, String remarks, String H5url, String PCurl, String type);
	
}

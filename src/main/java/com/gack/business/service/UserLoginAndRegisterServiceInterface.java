package com.gack.business.service;

import com.gack.helper.common.abstractobj.Result;

/**
 * 
 * @author ws
 * 2018-5-30
 */
public interface UserLoginAndRegisterServiceInterface {

	/**
	 * 用户注册
	 * @param username 用户名
	 * @param password 密码
	 * @param identifyCode 短信验证码
	 * @param register_source 注册来源 android或ios
	 * @return error:用户名存在    error:验证码超时   error:验证码不正确   success:注册成功
	 */
	Result registUser(String username, String password, String identifyCode, String register_source);
	
	/**
	 * 用户名密码登录
	 * @param username 用户名
	 * @param password 密码
	 * @param login_resource 登录来源   pc或android或ios
	 * @param equipment 设备号
	 * @return error:用户名或密码错误   success:User对象
	 */
	Result loginByPassword(String username, String password, String login_resource, String equipment);
	
	/**
	 * 用户名短信验证码登录
	 * @param username 用户名
	 * @param identifyCode 短信验证码
	 * @param login_resource 登录来源   pc或android或ios
	 * @param equipment 设备号
	 * @return error:用户名或密码错误   success:User对象
	 */
	Result loginByIdentifyCode(String username, String identifyCode, String login_resource, String equipment);
	
	/**
	 * 验证短信验证码
	 * @param username 用户名
	 * @param identifyCode 短信验证码
	 * @return
	 */
	Result validateIdentify(String username,String identifyCode);
	
	/**
	 * 发送短信验证码
	 * @param username 用户名，即手机号
	 * @return
	 */
	Result sendIdentifyCode(String username);
	
}

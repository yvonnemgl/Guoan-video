package com.gack.business.service;

import java.util.Map;

import com.gack.helper.common.abstractobj.Result;

/**
 * 
* @ClassName: CertifiedServiceInterface 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月14日 上午10:41:24 
* @version V1.0
 */
public interface CertifiedServiceInterface {

	/**
	 * 
	* @Title: personalAuthentication 
	* @Description: TODO(个人认证) 
	* @param @param name    姓名
	* @param @param identity	身份证号码
	* @param @return    入参
	* @return String    返回类型
	* @author （ZhangXinYu） 
	* @throws
	* @date 2018年6月14日 上午10:47:10 
	* @version V1.0
	 */
	public Map<String, Object> personalAuthentication(String userid,String name,String identity,String positiveIdCard,String negativeIdCard);
	/**
	 * 
	* @Title: enterpriseCertification 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param @param name		公司名称		
	* @param @param identity	企业统一社会信用码
	* @param @param legalPerson	法人
	* @param @param detailedAddress	详细地址
	* @param @param businessLicense	公司营业执照url
	* @param @param administratorJobCertificate	公司管理员在职证明url
	* @param @return    入参
	* @return String    返回类型
	* @author （ZhangXinYu） 
	* @throws
	* @date 2018年6月25日 上午8:28:09 
	* @version V1.0
	 */
	public Map<String, Object> enterpriseCertification(String userid,String enterpriseid,String name,String identity,String legalPerson,
			String detailedAddress,String businessLicense,String administratorJobCertificate);
	/**
	 * 
	* @Title: getpersonalAuthentication 
	* @Description: (检索是否已经实名认证) 
	* @param @param uid		用户id
	* @return Result    返回类型
	* @author lsy 
	 */
	public Result getpersonalAuthentication(String uid);
	/**
	 * 
	* @Title: getpersonalAuthentication 
	* @Description: (检索是否已经缴纳押金) 
	* @param @param uid		用户id
	* @return Result    返回类型
	* @author lsy 
	 */
	public Result getpersonaIsPledge(String uid);
}

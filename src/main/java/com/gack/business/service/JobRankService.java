package com.gack.business.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.controller.PositionController;
import com.gack.business.model.JobRank;
import com.gack.business.repository.JobRankHelperRepository;
import com.gack.business.repository.JobRankRepository;
import com.gack.business.repository.PositionRepository;
import com.gack.business.vo.JobRankVO;
import com.gack.business.vo.PositionVO;
import com.gack.helper.common.util.MapAsReturn;

/**
 * 
 * @author ws
 * 2018-9-10
 */
@Transactional
@Service
public class JobRankService implements JobRankServiceInterface{

	@Autowired
	private JobRankRepository jobRankRepository;
	@Autowired
	private PositionController positionController;
	@Autowired
	private PositionRepository positionRepository;
	@Autowired
	private JobRankHelperRepository jobRankHelperRepository;
	
	/**
	 * 设置默认职级
	 * @param enterpriseId 公司id
	 */
	@Override
	public void setDefault(String enterpriseId) {
	
		Date date = new Date();
		JobRank jobRank1 = new JobRank("行政一级", 1, enterpriseId, 1, date, date, null);
		JobRank jobRank2 = new JobRank("行政二级", 2, enterpriseId, 1, date, date, null);
		JobRank jobRank3 = new JobRank("行政三级", 3, enterpriseId, 1, date, date, null);
		JobRank jobRank4 = new JobRank("行政四级", 4, enterpriseId, 1, date, date, null);
		JobRank jobRank5 = new JobRank("行政五级", 5, enterpriseId, 1, date, date, null);
		jobRankRepository.save(jobRank1);
		jobRankRepository.save(jobRank2);
		jobRankRepository.save(jobRank3);
		jobRankRepository.save(jobRank4);
		jobRankRepository.save(jobRank5);
		
	}
	
	/**
	 * 保存或修改职级
	 * @param enterpriseId 公司id
	 * @param rankId 职级id(若有职级id,则为修改;若无职级id,则为新增)
	 * @param name 职级新名称
	 * @param levle 职级新等级
	 * @return status:error/success msg:错误或成功消息
	 */
	@Override
	public Map<String, Object> saveOrUpdate(String enterpriseId, String rankId, String name, Integer level){
		//验证是否重名
		if(!validateName(enterpriseId, rankId, name)){
			return MapAsReturn.setStatusError("hasName");
		}
		
		JobRank jobRank = new JobRank();
		Date date = new Date();
		if(StringUtils.isBlank(rankId)){//新增
			jobRank.setEnterpriseId(enterpriseId);
			jobRank.setStatus(1);
			jobRank.setCreateTime(date);
		}else{//修改
			jobRank = jobRankRepository.findById(rankId);
			if(null == jobRank){
				return MapAsReturn.setStatusError("查无此职级");
			}
		}

		jobRank.setName(name);
		jobRank.setLevel(level);
		jobRank.setUpdateTime(date);
		
		jobRankRepository.save(jobRank);
		
		return MapAsReturn.setStatusSuccess(null, null);
	}
	
	/**
	 * 删除职级
	 * @param enterpriseId 公司id
	 * @param userId 操作者id
	 * @param rankId 职级id
	 * @return status:error/success msg:错误或成功消息
	 */
	@Override
	public Map<String, Object> delete(String enterpriseId, String userId, String rankId){
		JobRank jobRank = jobRankRepository.findByEnterpriseIdAndId(enterpriseId, rankId);
		if(null == jobRank){
			return MapAsReturn.setStatusError("非法操作");
		}
		
		jobRankRepository.deleteById(rankId, new Date());
		//获取与该职级关联的职位
		List<String> positionIdList = positionRepository.findIdByJobRankId(rankId);
		positionController.deletePositionByBatch(enterpriseId, positionIdList.toArray(new String[0]), userId);
		
		return MapAsReturn.setStatusSuccess(null, null);
	}
	
	/**
	 * 查找公司下所有职级
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息  jobRankList:职级集合
	 */
	@Override
	public Map<String, Object> findAll(String enterpriseId){
		List<JobRankVO> jobRankList = jobRankRepository.findVOByEnterpriseId(enterpriseId);
		
		return MapAsReturn.setStatusSuccess("jobRankList", jobRankList);
	}
	
	/**
	 * 验证职级是否重名
	 * @param enterpriseId 公司id
	 * @param rankId 职级id
	 * @param name 职级新名称
	 * @return true---验证通过,未重名;false---验证失败,有重名
	 */
	private boolean validateName(String enterpriseId, String rankId, String name){
		
		JobRank jobRank = jobRankRepository.findByEnterpriseIdAndName(enterpriseId, name);
		if(jobRank == null){
			return true;
		}
		
		if(StringUtils.isBlank(rankId)){//新增
			return false;
		}
		
		if(!jobRank.getId().equals(rankId)){
			return false;
		}
		
		return true;
	}
	
	/**
	 * 查找职级下所有职位(未被删除)
	 * @param jobrankId 职级id
	 * @return status:error/success  msg:错误或成功消息  PositionList:positionVO集合
	 */
	@Override
	public Map<String, Object> findPosition(String jobrankId){
		List<PositionVO> positionList = positionRepository.findVOByJobRankId(jobrankId);
		return MapAsReturn.setStatusSuccess("positionList", positionList);
	}

	/**
	 * 查找"职级使用帮助"
	 * @return status:error/success  msg:错误或成功消息  messageList:JobRankHelper集合
	 */
	@Override
	public Map<String, Object> findHelper(){
		return MapAsReturn.setStatusSuccess("messageList", jobRankHelperRepository.findAll());
	}
	
}

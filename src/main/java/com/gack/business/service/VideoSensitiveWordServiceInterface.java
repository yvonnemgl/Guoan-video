package com.gack.business.service;


import java.util.Map;
import java.util.Set;

import com.gack.helper.common.abstractobj.Result;

/**
 * 
 * @author ws
 * @date 2017-11-27
 */
public interface VideoSensitiveWordServiceInterface {

	Set<String> sensitiveWordVerification(String text);
	
	/**
	 * @description 向表t_operator_sensitiveword中保存或修改敏感字记录,不能出现重复敏感字记录
	 * @param word 敏感字
	 * @param id 敏感字id
	 * @return ReturnResult
	 */
	Result saveOrUpdateSensitiveWord(String word, String id);
	
	/**
	 * @description 根据id删除敏感字记录
	 * @param id 敏感字id
	 * @return ReturnResult
	 */
	Result deleteSensitiveWord(String id);
	
	/**
	 * @description 根据敏感字模糊查询
	 * @param word 敏感字
	 * @param page 当前页数,从0开始
	 * @param size 页面中可显示记录数
	 * @return Map<String, Object>
	 */
	Map<String, Object> findByWord(String word, String page, String size);
	
}

package com.gack.business.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.model.PushSwitch;
import com.gack.business.model.User;
import com.gack.business.repository.PushSwitchRepository;
import com.gack.business.repository.UserRepository;
import com.gack.helper.common.util.JPushRestAPI;

/**
 * 
* @ClassName: PushService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年7月13日 上午11:12:23 
* @version V1.0
 */
@Service
public class PushService {
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PushSwitchRepository pushSwitchRepository;
	
	public void pushStoreReflsh(String mes_type,Object object){
		JPushRestAPI jPushRestAPI = new JPushRestAPI();
		List<PushSwitch> list = pushSwitchRepository.findAll();
		PushSwitch pushSwitch  = list.get(0);
		if (pushSwitch.getIsSwitch() == 0) {
			jPushRestAPI.setApns_production(false);
		}
		List<User> users = userRepository.findAll();
		List<String> usernames = users.stream().map(User::getUsername).collect(Collectors.toList());
		jPushRestAPI.SendPush("", mes_type,object, "", jPushRestAPI.jsonDataByManyAlias(usernames));
	}
}

package com.gack.business.service;

import java.util.List;

import com.gack.business.vo.IndustryVO;

/**
 * 
 * @author ws
 * 2018-5-29
 */
public interface IndustryServiceInterface{

	/**
	 * 查询一级行业,并带有子行业的嵌套关系
	 * @return 
	 */
	List<IndustryVO> findTopIndustry();

	/**
	 * 按照输入,查询公司名称
	 * @param inputName 输入
	 * @return
	 */
	List<IndustryVO> findIndustryByInputName(String inputName);
}

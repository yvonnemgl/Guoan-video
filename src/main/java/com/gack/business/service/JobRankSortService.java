package com.gack.business.service;

import java.util.Date;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.model.JobRankSort;
import com.gack.business.repository.JobRankSortRepository;
import com.gack.helper.common.util.MapAsReturn;

/**
 * 
 * @author ws
 * 2018-9-10
 */
@Transactional
@Service
public class JobRankSortService implements JobRankSortServiceInterface{

	@Autowired
	private JobRankSortRepository jobRankSortRepository;
	
	/**
	 * 设置默认职级排序
	 * @param enterpriseId
	 */
	@Override
	public void setDefault(String enterpriseId) {
		
		JobRankSort jrs = new JobRankSort();
		jrs.setEnterpriseId(enterpriseId);
		jrs.setSort("ASC");
		Date date = new Date();
		jrs.setCreateTime(date);
		jrs.setUpdateTime(date);
		jobRankSortRepository.save(jrs);
		
	}
	
	/**
	 * 设置职级排序
	 * @param enterpriseId 公司id
	 * @param sort 排序(DESC---等级越小，权限越高、ASC---等级越大,权限越高)
	 * @return status:error/success msg:错误或成功消息
	 */
	@Override
	public Map<String, Object> setSort(String enterpriseId, String sort){
		jobRankSortRepository.updateSort(enterpriseId, sort, new Date());
		return MapAsReturn.setStatusSuccess(null, null);
	}
	
	/**
	 * 查询职级排序(DESC---等级越小，权限越高、ASC---等级越大,权限越高)
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息  sort:DESC/ASC
	 */
	@Override
	public Map<String, Object> findSort(String enterpriseId){
		JobRankSort jobRankSort = jobRankSortRepository.findByEnterpriseId(enterpriseId);
		
		if(null == jobRankSort){
			return MapAsReturn.setStatusError("职级为空");
		}
		
		return MapAsReturn.setStatusSuccess("sort", jobRankSort.getSort());
	}
	
}

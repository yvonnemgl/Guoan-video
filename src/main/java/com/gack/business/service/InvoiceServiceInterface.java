package com.gack.business.service;

import java.util.List;

import com.gack.business.model.Invoice;

public interface InvoiceServiceInterface {

	String save(String userId,String[] ids,int amount,String type,String content,String head,String number,String username,String phone,String address,String detail);

	List<Invoice> get(String userId,String kind);
	
	Object getList(String userId);
} 

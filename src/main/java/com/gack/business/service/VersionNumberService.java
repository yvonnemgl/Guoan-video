package com.gack.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.gack.business.model.VersionNumber;
import com.gack.business.repository.VersionNumberRepository;

@Service
public class VersionNumberService implements VersionNumberServiceInteface {

	@Autowired
	private VersionNumberRepository versionNumberRepository;
	
	public VersionNumber getVersionNumber(){
		List<VersionNumber> list = versionNumberRepository.getVersionNumberByCreatetime(new PageRequest(0, 1, new Sort(Sort.Direction.DESC, "createtime")));
		if(list == null || (list != null && list.size() == 0)){
			return null;
		}
		return list.get(0);
	}
}

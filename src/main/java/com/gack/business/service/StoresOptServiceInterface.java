package com.gack.business.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.gack.business.model.VideoStores;
import com.gack.helper.common.abstractobj.Result;

/**
 * 
* @ClassName: StoresOptServiceInterface 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月4日 下午3:09:12 
* @version V1.0
 */
public interface StoresOptServiceInterface {
	public Map<String, Object> getStores(Integer searchType,String searchKey,Integer currentPage,Integer pageSize);
	public Map<String, Object> getAllStores(Integer currentPage,Integer pageSize);
	public Workbook exportStores();
	public List<VideoStores> getAllStores();
	public Result getStoresInfo(String storeid);
}

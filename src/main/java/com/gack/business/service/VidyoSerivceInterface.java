package com.gack.business.service;

import java.util.Map;

public interface VidyoSerivceInterface {
	Map<String, Object> addRoom(String id);
	Map<String, Object> getParticipants(String roomId);
	Map<String, Object> leaveConference(String conferenceID, String participantID);
	Map<String, Object> ininviteToConference(String conferenceID, String invite);
}

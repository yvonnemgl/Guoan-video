package com.gack.business.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.model.StoreSupportFacity;
import com.gack.business.model.StoreSupportService;
import com.gack.business.repository.StoreSupportFacityRepository;
import com.gack.business.repository.StoreSupportServiceRepository;
import com.gack.helper.common.abstractobj.Result;

/**
 * 
* @ClassName: StoreSupportFacityAndServiceService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月4日 上午11:30:27 
* @version V1.0
 */
@Service
public class StoreSupportFacityAndServiceService implements StoreSupportFacityAndServiceServiceInterface{

	@Autowired
	private StoreSupportFacityRepository facityRepository;
	@Autowired
	private StoreSupportServiceRepository serviceRepository;
	
	@SuppressWarnings("null")
	@Override
	public Result AddStoreSupportFacity(String facityName) {
		Result result = new Result();
		List<StoreSupportFacity> facitys = facityRepository.findByName(facityName);
		if(facitys != null || facitys.size() > 0){
			result.setKey("error_duplicateName");
			result.setMsg("配套设施已存在，请勿重复添加");
			return result;
		}
		StoreSupportFacity facity = new StoreSupportFacity();
		facity.setName(facityName);
		facity.setCreatetime(new Date());
		facity = facityRepository.save(facity);
		result.setKey("success");
		result.setValue(facity);
		return result;
	}

	@SuppressWarnings("null")
	@Override
	public Result AddStoreSupportService(String serviceName) {
		Result result = new Result();
		List<StoreSupportService> services = serviceRepository.findByName(serviceName);
		if(services != null || services.size() > 0){
			result.setKey("error_duplicateName");
			result.setMsg("配套服务已存在，请勿重复添加");
			return result;
		}
		StoreSupportService service = new StoreSupportService();
		service.setName(serviceName);
		service.setCreatetime(new Date());
		service = serviceRepository.save(service);
		result.setKey("success");
		result.setValue(service);
		return result;
	}


	@Override
	public List<StoreSupportFacity> getAllFacitys() {
		return facityRepository.findAll();
	}

	@Override
	public List<StoreSupportService> getAllServices() {
		return serviceRepository.findAll();
	}

	@Override
	public Map<String, Object> getAllFacitysAndServices() {
		List<StoreSupportFacity> facities = facityRepository.findAll();
		List<StoreSupportService> services = serviceRepository.findAll();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("facities", facities);
		map.put("services", services);
		return map;
	}
}

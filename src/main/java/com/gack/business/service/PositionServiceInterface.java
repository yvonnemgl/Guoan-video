package com.gack.business.service;

import java.util.Map;

/**
 * 
 * @author ws
 * 2018-6-1
 */
public interface PositionServiceInterface {

	/**
	 * 查询某公司下所有职位
	 * @param enterpriseId 公司id
	 * @return status:error/success  msg:错误或成功信息  positionList:职位集合
	 */
	Map<String, Object> findAllPosition(String enterpriseId);
	
	/**
	 * 新增某公司的职位   不可重名
	 * @param enterpriseId 公司id
	 * @param positionName 职位名称
	 * @param rankId 职级id
	 * @return status:error/success  msg:错误或成功消息(hasName)
	 */
	Map<String, Object> insertPosition(String enterpriseId, String positionName, String rankId);
	
	/**
	 * 修改职位   不可重名
	 * @param enterpriseId 公司id 
	 * @param positionId 职位id
	 * @param positionName 职位名称
	 * @param operatorId 操作者id
	 * @param rankId 职级id
	 * @return  status:error/success  msg:错误或成功消息(hasName)
	 */
	Map<String, Object> updatePosition(String enterpriseId, String positionId, String positionName, String operatorId, String rankId);

	/**
	 * 删除某职位,并将相应用户的职位置为null
	 * @param enterpriseId 公司id
	 * @param positionId 职位id
	 * @param operatorId 操作者id
	 * @return status:error/success  msg:错误或成功消息
	 */
	Map<String, Object> deletePosition(String enterpriseId, String positionId, String operatorId);
	
	/**
	 * 批量删除某些职位,并将相应用户的职位置为null
	 * @param enterpriseId 公司id
	 * @param positionIds 职位id集合
	 * @return status:error/success  msg:错误或成功消息
	 */
	Map<String, Object> deletePositionByBatch(String enterpriseId, String[] positionIds, String operatorId) throws Exception;

}

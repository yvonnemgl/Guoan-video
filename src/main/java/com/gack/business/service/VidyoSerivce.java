package com.gack.business.service;

import java.util.HashMap;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.stereotype.Service;

import com.gack.helper.common.util.PackerUtil;
import com.gack.helper.common.util.WebServiceUtils;

@Service
public class VidyoSerivce implements VidyoSerivceInterface {

	@Override
	public Map<String, Object> addRoom(String id) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("i", id);
		PackerUtil packerUtil = new PackerUtil();
		packerUtil.packBody("v1:AddRoomRequest", map);
		String data = packerUtil.xDoc.getDocument().asXML();
		String xmlStr = WebServiceUtils
				.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService", data, "addRoom");
		
		/*Map<String, Object> map = new HashMap<String, Object>();
		map.put("i", conference.getConferenceCode());
		PackerUtil packerUtil = new PackerUtil();
		packerUtil.packBody("v1:AddRoomRequest", map);
		String data = packerUtil.xDoc.getDocument().asXML();
		String xmlStr = WebServiceUtils
				.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService", data, "addRoom");
		Document dom = DocumentHelper.parseText(xmlStr);
		Element root = dom.getRootElement();
		Element Body = root.element("Body");
		Element AddRoomResponse = Body.element("AddRoomResponse");*/
		return null;
	}

	@Override
	public Map<String, Object> getParticipants(String roomId) {
		Map<String, Object> map = new HashMap<>();
		String xmlStr = WebServiceUtils.getParticipants(roomId);
		map.put("xmlStr", xmlStr);
		return map;
	}

	@Override
	public Map<String, Object> leaveConference(String conferenceID, String participantID) {
		Map<String, Object> map = new HashMap<>();
		String xmlStr = WebServiceUtils.leaveConference(conferenceID, participantID);
		map.put("xmlStr", xmlStr);
		return map;
	}

	@Override
	public Map<String, Object> ininviteToConference(String conferenceID, String invite) {
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> resMap = new HashMap<>();
		resMap.put("conferenceID", conferenceID);
		resMap.put("invite", invite);
		
		PackerUtil packerUtil = new PackerUtil();
		packerUtil.packBody6("v1:InviteToConferenceRequest", resMap);
		String RequestXml = packerUtil.xDoc.getDocument().asXML();
		String ResponseXml = WebServiceUtils
				.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService", RequestXml, "inviteToConference");
		
		map.put("xmlStr", ResponseXml);
		return map;
	}

}

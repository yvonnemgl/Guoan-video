package com.gack.business.service;

import java.util.List;

import com.gack.business.model.PersonNumRange;

/**
 * 
 * @author ws
 * 2018-5-29
 */
public interface PersonNumRangeServiceInterface {
	
	List<PersonNumRange> findAll();
	
}

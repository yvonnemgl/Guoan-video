package com.gack.business.service;

import java.util.Map;

/**
 * 
 * @author ws
 * 2018-9-10
 */
public interface JobRankSortServiceInterface {

	/**
	 * 设置默认职级排序
	 * @param enterpriseId
	 */
	void setDefault(String enterpriseId);
	
	/**
	 * 设置职级排序
	 * @param enterpriseId 公司id
	 * @param sort 排序(DESC---等级越小，权限越高、ASC---等级越大,权限越高)
	 * @return status:error/success msg:错误或成功消息
	 */
	Map<String, Object> setSort(String enterpriseId, String sort);
	
	/**
	 * 查询职级排序(DESC---等级越小，权限越高、ASC---等级越大,权限越高)
	 * @param enterpriseId 公司id
	 * @return status:error/success msg:错误或成功消息  sort:DESC/ASC
	 */
	Map<String, Object> findSort(String enterpriseId);
}

package com.gack.business.service;
/**
 * 
* @ClassName: VideoOrderformServiceInterface.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年3月21日
*  
 */

import com.gack.helper.common.abstractobj.Result;

public interface VideoOrderformServiceInterface {
	public Result reservateStore(String userid,String storeid,String operation);
	public Result cancelReservation(String id,String reason);
	public Result startToUseOrderform(String id);
	public Result endToUseOrderform(String id);
	public Result startToUseOrderformByUserid(String id);
	public Result endToUseOrderformByUserid(String id);
	public Result findAllOrderform(String id,Integer currentPage,Integer pageSize);
	public Result findOrderformInfo(String id);
	public Result findOrderformInfoByUserId(String id);
	
}

package com.gack.business.service;

import java.util.List;

import com.gack.business.model.Area;
import com.gack.business.model.City;
import com.gack.business.model.Province;
import com.gack.business.vo.ProvinceItemVO;

/**
 * 
 * @author ws
 * 2018-5-29
 */
public interface PlaceServiceInterface {

	/**
	 * 查询所有省份
	 * @return
	 */
	List<Province> findAllProvince();
	
	/**
	 * 查询某省份下所有城市
	 * @param provinceId 省份id
	 * @return
	 */
	List<City> findCitiesByProvince(String provinceId);
	
	/**
	 * 查询某城市下所有城区
	 * @param cityId 城市id
	 * @return
	 */
	List<Area> findAreasByCity(String cityId);
	
	/**
	 * 查询所有地址 省 省下的城市  城市下的城区
	 * @return
	 */
	List<ProvinceItemVO> findProvinceItem();
}

package com.gack.business.service;

import com.gack.business.model.VersionNumber;

public interface VersionNumberServiceInteface {

	VersionNumber getVersionNumber();
}


package com.gack.business.service;

import java.util.Map;
import com.gack.business.model.User;


/**
 * 
 * @author ws
 * 2018-5-30
 */
public interface UserServiceInterface {

	/**
	 * 根据查询名称,查找当前公司的用户(输入的查询名称应匹配用户应显示的名称(好友备注名或企业备注名))
	 * 根据查询手机号,查找当前公司用户(模糊查询)
	 * @param userId 操作者id
	 * @param enterpriseId 公司id
	 * @param input 用户输入的查询名称或查询手机号
	 * @return  status:error/success  msg:错误或成功消息   userItemList:匹配到的用户VO集合
	 */
	Map<String, Object> findUserItemVOByInput(String userId, String enterpriseId, String input);
	
	/**
	 * 根据查询名称,查找当前公司的用户
	 * 			规则为:输入的查询名称应匹配用户应显示的名称(好友备注名或企业备注名)
	 * @param userId 操作者id
	 * @param enterpriseId 公司id
	 * @param inputName 用户输入的查询名称
	 * @return  status:error/success  msg:错误或成功消息   userItemList:匹配到的用户VO集合
	 */
	Map<String, Object> findUserItemVOByInputName(String userId, String enterpriseId, String inputName);
	
	/**
	 * 根据查询手机号,查找当前公司用户(模糊查询)
	 * @param userId 操作者id
	 * @param enterpriseId 公司id
	 * @param inputTel 用户输入的手机号
	 * @return  status:error/success  msg:错误或成功消息   userItemList:匹配到的用户VO集合
	 */
	Map<String, Object> findUserItemVOByInputTel(String userId, String enterpriseId, String inputTel);
	
	/**
	 * 用户查询无部门的人员信息
	 * @param enterpriseId 企业id
	 * @param userId 用户id
	 * @return  status:error/success msg:错误后成功信息  userItemVOList:无部门用户VO集合
	 */
	Map<String, Object> findNoDepartmentUserItemVO(String enterpriseId, String userId);
	/**

	 * 查询用户信息
	* @Title: findUser 
	* @Description: TODO(这里用一句话描述这个方法的作用) 
	* @param id
	* @return    设定文件 
	* @return User    返回类型 
	* @throws 
	 */
	User findUser(String id);
/*
	 * 
	 * @Title: payDeposit
	 * @Description: 用户支付押金接口
	 * @param userId
	 * @param channel
	 * @param amount
	 * @return Map<String,String> 
	 * @throws
	 */
	Map<String, String> payDeposit(String userId,String channel,String amount);

	/**
	 * 
	 * @Title: payDeposit
	 * @Description: 用户退还押金接口
	 * @param userId 
	 * @return Map<String,String> 
	 * @throws
	 */
	Map<String, String> refundDeposit(String userId);

	
	/**
	 * 查询某用户的角色等级
	 * @param userId 用户id
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @return status:error/success  msg:错误或成功消息   level:角色等级
	 */
	Map<String, Object> findLevelByUserIdAndEnterpriseIdAndDepartmentId(String userId, String enterpriseId, String departmentId);
	
	/**
	 * 查询某用户所在的公司,并将当前公司放在第一位。其他公司按创建时间,降序排序
	 * @param userId 用户id
	 * @param enterpriseId 企业id
	 * @return  status:error/success  msg:错误或成功消息      enterpriseItemVOList:公司ItemVO集合
	 */
	Map<String, Object> findEnterpriseItemVOListByUserId(String userId, String enterpriseId);
	
	/**
	 * 查询某用户管理的公司(在该公司是管理员权限),并将当前公司放在第一位。其他公司按创建时间,降序排序
	 * @param userId 用户id
	 * @param enterpriseId 公司id 可为空
	 * @param inviteeId 被邀请者id 可为空
	 * @return
	 */
	Map<String, Object> findManageEnterpriseListByUserId(String userId, String enterpriseId, String inviteeId);
	
	/**
	 * 为用户设置某个公司下的企业备注
	 * @param userId 用户id
	 * @param enterpriseId 公司id
	 * @param enterpriseRemarks 企业备注
	 * @return status:error/success  msg:错误或成功消息
	 */
	Map<String, Object> setEnterpriseRemarks(String userId, String enterpriseId, String enterpriseRemarks);
	
	/**
	 * 为某个用户设置职位    更改或新增
	 * @param userId 用户id
	 * @param enterpriseId 公司id
	 * @param positionId 职位id
	 * @param operatorId 操作者id
	 * @return  status:error/success  msg:错误或成功消息
	 */
	Map<String, Object> setPosition(String userId, String enterpriseId, String positionId, String operatorId);
	
	/**
	 * 为某个用户设置部门     更新或新增    若为更新,则减少原部门及其父部门及父父部门的人数,再增加新部门及其父部门及父父部门的人数     若为新增,则增加新部门及其父部门及父父部门的人数
	 * @param userId 用户id
	 * @param enterpriseId 公司id
	 * @param departmentId 部门id
	 * @param operatorId 操作者id
	 * @return  status:error/success  msg:错误或成功消息
	 */
	Map<String, Object> setDepartment(String userId, String enterpriseId, String departmentId, String operatorId);
	
	/**
	 * 查询某个联系人的详情,从搜索进入
	 * @param username 用户手机号
	 * @param showToUserId 向谁展示的用户id
	 * @return status:error/success msg:错误或成功消息  contactsDetail:联系人详情  isFriend:true/false(是否为好友)
	 */
	Map<String, Object> findContactsDetailFromSearch(String username, String showToUserId);
	
	/**
	 * 查询某个联系人的详情,从好友列表进入
	 * @param username 用户手机号
	 * @param showToUserId 向谁展示的用户id
	 * @return status:error/success msg:错误或成功消息  contactsDetail:联系人详情  isFriend:true/false(是否为好友)
	 */
	Map<String, Object> findContactsDetailFromContactsList(String username, String showToUserId);
	
	/**
	 * 查询某个用户在某个公司下的详情
	 * @param enterpriseId 公司id
	 * @param username 用户手机号
	 * @param showToUserId 向谁展示的用户id
	 * @return status:error/success  msg:错误或成功消息
	 *         userDetail:userDetailVO对象
	 *		   permissionList:操作者权限集合
	 *		   isFriend:是否为好友关系
	 */
	Map<String, Object> findUserDetailVO(String enterpriseId, String username, String showToUserId);
	
	/**
	 * 查询某人在某公司下的关于公司管理的管理范围
	 * @param enterpriseId 公司id
	 * @param userId 用户id
	 * @return status:error/success msg:错误或成功消息   permissionList:基本权限集合
	 */
	Map<String, Object> findManageRangeAboutEnterpriseManage(String enterpriseId, String userId);
}


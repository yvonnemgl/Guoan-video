package com.gack.business.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gack.business.dao.EnterpriseInfoDao;
import com.gack.business.model.Enterprise;
import com.gack.business.model.Message;
import com.gack.business.repository.DepartmentRepository;
import com.gack.business.repository.EnterpriseRepository;
import com.gack.business.repository.MessageRepository;
import com.gack.business.repository.UserEnterpriseDepartmentPositionRepository;
import com.gack.business.repository.UserEnterprisePermissionGroupRepository;
import com.gack.business.repository.UserRepository;
import com.gack.helper.common.abstractobj.Result;

/**
 * 
* @ClassName: EnterpriseInfoService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年5月31日 下午12:52:18 
* @version V1.0
 */
@Service
public class EnterpriseInfoService implements EnterpriseInfoServiceInterface{

	@Autowired
	private EnterpriseRepository enterpriseRepository;
	@Autowired
	private EnterpriseInfoDao enterpriseInfoDao;
	@Autowired
	private DepartmentRepository departmentRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserEnterpriseDepartmentPositionRepository userEnterpriseDepartmentPositionRepository;
	@Autowired
	private MessageRepository messageRepository;
	@Autowired
	private UserEnterprisePermissionGroupRepository userEnterprisePermissionGroupRepository;
	
	@Override
	public Result getEnterpriseInfo(String enterpriseId) {
		Result result = new Result();
		Enterprise enterprise = enterpriseRepository.findOne(enterpriseId);
		if(enterprise != null){
			result.setKey("success");
			result.setValue(enterprise);
		}else{
			result.setKey("error_none");
			result.setMsg("公司状态查询失败，公司不存在");
		}
		return result;
	}

	@Override
	public Result resetEnterpriseName(String userid, String enterpriseid, String newname) {
		Result result = new Result();
		if(userRepository.findOne(userid) == null){
			result.setKey("error_noneUser");
			result.setMsg("用户不存在，操作失败");
			return result;
		}
		Enterprise enterprise = enterpriseRepository.findOne(enterpriseid);
		if(enterprise == null){
			result.setKey("error_noneEnterprise");
			result.setMsg("公司不存在，操作失败");
			return result;
		}

		enterprise.setName(newname);
		enterprise.setUpdateNameTime(new Date());
		enterprise = enterpriseRepository.save(enterprise);
		result.setKey("success");
		result.setValue(enterprise);
		return result;
	}

	@Transactional
	@Override
	public Result dissolvedEnterprise(String userid, String enterpriseid) {
		Result result = new Result();
		Enterprise enterprise = enterpriseRepository.findOne(enterpriseid);
		if(enterprise == null){
			result.setKey("error_noneEnterprise");
			result.setMsg("公司不存在，操作失败");
			return result;
		}
		if(enterprise.getStatus().equals("4")){
			result.setKey("error_hasdissolved");
			result.setMsg("公司已解散，解散操作失败");
			return result;
		}
//		boolean isCan = enterpriseInfoDao.validateIsCanEnterpriseSuperAdmin(userid, enterpriseid);
//		if(!isCan){
//			result.setKey("error_cantdissolved");
//			result.setMsg("权限不足，公司解散操作失败");
//			return result;
//		}
		
		String enterpriseName = enterpriseRepository.findNameById(enterpriseid);
		//查询该公司下所有员工id
		List<String> userIds = userEnterpriseDepartmentPositionRepository.findUserIdByEnterpriseId(enterpriseid);
		for(String t : userIds){
			Message message = new Message();
			message.setUserid(t);//设置接受者id
			message.setMessage("您所在的" + enterpriseName + "已被解散");//设置消息内容
			message.setCreatetime(new Date());//设置创建时间
			message.setTitle("系统消息");//设置标题
			message.setMesType(5);//设置消息类型为"系统消息"
			message.setState(2);//设置消息为"未处理"
			message.setCreateId(userid);//设置创建者id
			message.setSendWay(2);//设置发送方式为"站内信"
			message.setIsRead(0);//设置为"未读"
			messageRepository.save(message);
		}
		
		userEnterpriseDepartmentPositionRepository.deleteByEnterpriseId(enterpriseid);
		userEnterprisePermissionGroupRepository.deleteByEnterpriseId(enterpriseid);
		departmentRepository.deleteDepartmentByEnterpriseId(enterpriseid, new Date());
		enterprise.setStatus("4");
		enterprise.setDeleteTime(new Date());
		enterprise = enterpriseRepository.save(enterprise);
		result.setKey("success");
		result.setValue("公司解散成功");
		return result;
	}

	@Override
	public Result commitEnterpriseAuthentiaction(String userid,String enterpriseid,String name,String contractName,String idNumber,
			String unifiedSocialCreditCode,String detailedAddress,String positiveIdCard,
			String negativeIdCard,String businessLicense,String administratorJobCertificate) {
		Result result = new Result();
		Enterprise enterprise = enterpriseRepository.findOne(enterpriseid);
		if(enterprise == null){
			result.setKey("error_noneEnterprise");
			result.setMsg("公司不存在，操作失败");
			return result;
		}else if(enterprise.getStatus().equals("1")){
			result.setKey("error_hasCertified");
			result.setMsg("企业已成功认证，请勿重复认证");
			return result;
		}else if(enterprise.getStatus().equals("2")){
			result.setKey("error_inCertification");
			result.setMsg("企业认证申请中，请耐心等待认证结果");
			return result;
		}
		if(enterpriseInfoDao.getSameCertifiedEnterpriseNameCount(name).intValue() > 0){
			result.setKey("error_duplicateName");
			result.setMsg("该名称已认证或认证审核在，请重新提交认证");
			return result;
		}
		//公司认证只有公司创建者 既超级管理员有权限认证   
		//确认认证权限
//		boolean isCan = enterpriseInfoDao.validateIsCanEnterpriseSuperAdmin(userid, enterpriseid);
//		if(!isCan){
//			result.setKey("error_cantAuthentiaction");
//			result.setMsg("权限不足，公司认证操作失败");
//			return result;
//		}
		enterprise.setCertifiedName(name);
		enterprise.setContractName(contractName);
		enterprise.setIdNumber(idNumber);
		enterprise.setUnifiedSocialCreditCode(unifiedSocialCreditCode);
		enterprise.setDetailedAddress(detailedAddress);
		enterprise.setPositiveIdCard(positiveIdCard);
		enterprise.setNegativeIdCard(negativeIdCard);
		enterprise.setBusinessLicense(businessLicense);
		enterprise.setAdministratorJobCertificate(administratorJobCertificate);
		enterprise.setCertificationSubmissionTime(new Date());
		enterprise.setStatus("2");
		enterprise = enterpriseRepository.save(enterprise);
		result.setKey("success");
		result.setValue(enterprise);
		return result;
	}

}

package com.gack.business.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.model.StoreAttention;
import com.gack.business.repository.StoreAttentionRepository;


/**
 * 
* @ClassName: StoreAttentionService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年9月6日 下午5:20:38 
* @version V1.0
 */
@Service
public class StoreAttentionService implements StoreAttentionServiceInterface{

	@Autowired
	private StoreAttentionRepository attentionRepository;
	
	@Override
	public Map<String, Object> edit(String attentionStr) {
		Map<String, Object> map = new HashMap<>();
		StoreAttention attenion = null;
		if(attentionStr == null || attentionStr.equals("")){
			map.put("key", "error");
			map.put("msg", "注意事项不可为空");
			return map;
		}
		List<StoreAttention> attentions = attentionRepository.findAll();
		if(attentions != null && attentions.size() != 0){
			attenion = attentions.get(0);
		}else{
			attenion = new StoreAttention();
		}
		attenion.setAttention(attentionStr);
		attenion.setUpdateTime(new Date());
		attenion = attentionRepository.save(attenion);
		if(attenion == null){
			map.put("key", "error");
			map.put("msg", "注意事项编辑失败");
			return map;
		}
		map.put("key", "success");
		map.put("msg", "注意事项编辑成功");
		return map;
	}

	@Override
	public Map<String, Object> get() {
		Map<String, Object> map = new HashMap<>();
		List<StoreAttention> attentions = attentionRepository.findAll();
		if(attentions != null && attentions.size() > 0){
			map.put("attention", attentions.get(0).getAttention());
		}else{
			map.put("attention", null);
		}
		map.put("key", "success");
		map.put("msg", "注意事项查询成功");
		return map;
	}

	@Override
	public StoreAttention attention() {
		StoreAttention attention = null;
		List<StoreAttention> attentions = attentionRepository.findAll();
		if(attentions != null && attentions.size() > 0){
			attention = attentions.get(0);
		}else{
			attention = null;
		}
		return attention;
	}

}

package com.gack.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.model.PushSwitch;
import com.gack.business.repository.PushSwitchRepository;
import com.gack.helper.common.util.JPushRestAPI;

/**
 * 
* @ClassName: JPushServer 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年9月11日 上午10:54:35 
* @version V1.0
 */
@Service
public class JPushServer {

	@Autowired
	private PushSwitchRepository pushSwitchRepository;
	
	/**
	 * 执行推送 
	 * alias 类型
	 * 	1.字符串>> 单别名推送
	 *  2.List集合>>  多别名推送
	 *  3.空对象>> 全设备/平台推送
	 */
	@SuppressWarnings("unchecked")
	public void sendPush(String alert,String mesType, String content, Object alias){
		if(alias == null) {
			sendAllPushAll(alert, mesType, content);
		} else if(alias instanceof String) {
			sendPushOneAlias(alert, mesType, content, (String)alias);
		} else if(alias instanceof List){
			sendPushManyAlias(alert, mesType, content, (List<String>)alias);
		}
	}
	
	// 针对所用设备/平台的推送
	private void sendAllPushAll(String alert,String mesType, String content){
		JPushRestAPI jPushRestAPI = new JPushRestAPI();
		List<PushSwitch> list = pushSwitchRepository.findAll();
		PushSwitch pushSwitch  = list.get(0);
		if (pushSwitch.getIsSwitch() == 0) {
			jPushRestAPI.setApns_production(false);
		}
		jPushRestAPI.SendPush(alert, mesType, content, "", "all");
	}
	
	// 多个别名推送
	private void sendPushManyAlias(String alert,String mesType, String content,List<String> usernames){
		JPushRestAPI jPushRestAPI = new JPushRestAPI();
		List<PushSwitch> list = pushSwitchRepository.findAll();
		PushSwitch pushSwitch  = list.get(0);
		if (pushSwitch.getIsSwitch() == 0) {
			jPushRestAPI.setApns_production(false);
		}
		jPushRestAPI.SendPush(alert, mesType, content, "", jPushRestAPI.jsonDataByManyAlias(usernames));
	}
	
	// 发送指定别名推送
	private void sendPushOneAlias(String alert,String mesType, String content,String username){
		JPushRestAPI jPushRestAPI = new JPushRestAPI();
		List<PushSwitch> list = pushSwitchRepository.findAll();
		PushSwitch pushSwitch  = list.get(0);
		if (pushSwitch.getIsSwitch() == 0) {
			jPushRestAPI.setApns_production(false);
		}
		jPushRestAPI.SendPush(alert, mesType, content, "", jPushRestAPI.jsonDataByOneAlias(username));
	}
}

package com.gack.business.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gack.business.dao.StoreCameraDao;
import com.gack.business.model.StoreCameras;
import com.gack.business.model.User;
import com.gack.business.model.VideoStores;
import com.gack.business.repository.UserRepository;
import com.gack.business.repository.VideoStoresRepository;

/**
 * 
* @ClassName: StoreCameraService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年7月11日 下午2:54:22 
* @version V1.0
 */
@Service
public class StoreCameraService implements StoreCameraServiceInterface{

	@Autowired
	private StoreCameraDao cameraDao;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private VideoStoresRepository storeRepository;
	
	@Override
	public Map<String, Object> getCamerasByUser(String userid) {
		Map<String, Object> map = new HashMap<>();
		if(userid == null || userid.equals("")){
			map.put("key", "error");
			map.put("msg", "用户id不可为空");
			map.put("cameras", null);
			return map;
		}
		User user = userRepository.findOne(userid);
		if(user == null ){
			map.put("key", "error");
			map.put("msg", "用户不存在");
			map.put("cameras", null);
			return map;
		}
//		List<StoreCameras> cameras = cameraDao.getUserCanUseCameras(user.getId());
		StoreCameras cameras = cameraDao.getUserCanUseCameras(user.getId());
		if(cameras == null || cameras.equals("")){
			map.put("key", "error");
			map.put("msg", "设备不存在");
			map.put("cameras", null);
			return map;
		}
		map.put("key", "success");
		map.put("msg", "摄像头查询成功");
		map.put("cameras", cameras);
		return map;
	}

	@Override
	public Map<String, Object> getStoreCameras(String storeid) {
		Map<String, Object> map = new HashMap<>();
		if(storeid == null || storeid.equals("")){
			map.put("key", "error");
			map.put("msg", "门店id不可为空");
			map.put("cameras", null);
			return map;
		}
		VideoStores stores = storeRepository.findOne(storeid);
		if(stores == null){
			map.put("key", "error");
			map.put("msg", "门店不存在");
			map.put("cameras", null);
			return map;
		}
		if(stores.getIsUse() == 1){
			map.put("key", "error");
			map.put("msg", "门店 已被删除");
			map.put("cameras", null);
			return map;
		}
//		List<StoreCameras> cameras = cameraDao.getStoreCameras(storeid);
		StoreCameras cameras = cameraDao.getStoreCameras(storeid);
		if(cameras == null || cameras.equals("")){
			map.put("key", "error");
			map.put("msg", "设备不存在");
			map.put("cameras", null);
			return map;
		}
		map.put("key", "success");
		map.put("msg", "摄像头查询成功");
		map.put("cameras", cameras);
		return map;
	}

	@Override
	public Map<String, Object> getCameraStatus(String userid) {
		Map<String, Object> map = new HashMap<>();
		if(userid == null || userid.equals("")){
			map.put("key", "error");
			map.put("msg", "用户id不可为空");
			map.put("status", null);
			return map;
		}
		User user = userRepository.findOne(userid);
		if(user == null ){
			map.put("key", "error");
			map.put("msg", "用户不存在");
			map.put("status", null);
			return map;
		}
		StoreCameras cameras = cameraDao.getUserCanUseCameras(user.getId());
		if(cameras == null || cameras.equals("")){
			map.put("key", "error");
			map.put("msg", "设备不存在");
			map.put("status", null);
			return map;
		}
		map.put("key", "success");
		map.put("msg", "摄像头查询成功");
		map.put("status", cameras.getStatus());
		return map;
	}
	
}

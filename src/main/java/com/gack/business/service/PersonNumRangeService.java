package com.gack.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gack.business.model.PersonNumRange;
import com.gack.business.repository.PersonNumRangeRepository;

/**
 * 
 * @author ws
 * 2018-5-29
 */
@Service
@Transactional
public class PersonNumRangeService implements PersonNumRangeServiceInterface{

	@Autowired
	private PersonNumRangeRepository personNumRangeRepository;
	
	@Override
	public List<PersonNumRange> findAll() {
		return personNumRangeRepository.findAll();
	}
	
}

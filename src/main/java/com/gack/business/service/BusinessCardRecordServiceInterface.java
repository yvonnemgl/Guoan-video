package com.gack.business.service;

import java.util.Map;

public interface BusinessCardRecordServiceInterface {

	Map<String, String> save(String uid,String cid,String eid,int number,String channel,String back_url);
	
	boolean updateByOid(String oid);

	Map<String, String> findEnterprisesIdsByUserId(String userId);
}

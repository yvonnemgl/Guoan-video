package com.gack.business.service;

import java.util.List;
import java.util.Map;

import com.gack.business.model.Contacts;
import com.gack.business.vo.ContactsVo;
import com.gack.helper.common.abstractobj.Result;

public interface ContactsServiceInterface {
  List<ContactsVo> selectContactsList(String username,String nickname);

  List<ContactsVo> selectUserName(String username);

  

  /**
   * 修改某用户的好友备注
   * @param userId 被修改者id
   * @param operatorId 操作者id
   * @param friendRemarks 好友备注
   * @return
   */
  Map<String, Object> updateFriendRemarks(String userId, String operatorId, String friendRemarks);

  void delectAllBycitycode(String userUsername,String useridFriendUsername);
  
  Result updateContastsInformation(String username);

	

}

package com.gack.business.service;

import com.gack.helper.common.abstractobj.Result;
import java.util.Map;

/**
 * 
* @ClassName: MessageServiceInterface 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年6月1日 下午3:09:12 
* @version V1.0
 */
public interface MessageServiceInterface {
	Result getUserMessages(String userid,Integer currentPage,Integer pageSize);
	Result setMessageRead(String messageId,Integer is_read);

	/**
	 * 对"邀请加入公司"类型的消息的处理
	 * @param messageId 消息id
	 * @param operation "agree" "refuse"
	 * @param remarks 备注
	 * @return status:error/success  msg:错误或成功消息
	 */
	Map<String, Object> dealMessage(String messageId, String operation, String remarks);
	
	/**
	 * 获取"邀请加入公司"类型的消息的备注
	 * @param messageId 消息id
	 * @return status:error/success  msg:错误或成功消息   remarks:备注
	 */
	Map<String, Object> getRemarksAboutInvitationToEnterprise(String messageId);
	
	/**
	 * 删除消息
	 * @param messageId 消息id
	 * @return status:error/success  msg:错误或成功消息
	 */
	Map<String, Object> deleteMessage(String messageId);
	
	Map<String, Object> getUserStatus(String userid);
	Map<String, Object> updateAllMessageIsRead(String userid,Integer is_read);
	
}

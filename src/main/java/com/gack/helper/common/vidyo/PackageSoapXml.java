
package com.gack.helper.common.vidyo;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import lombok.Getter;
import lombok.Setter;
/**
 * 

  * @ClassName: PackageSoapXml

  * @Description: 封装调用webservice接口的xml。（文档描述信息不全，部分参数不知道如何使用，直接默认就行）

  * @author lsy

  * @date 2018年8月29日 下午1:53:44

  *
 */

@Setter
@Getter
public class PackageSoapXml {

	public static final String SOAP_ENCODING = "UTF-8";
	public static final String ROOT_ELEMENT_NAME = "soapenv:Envelope";
	public static final String ROOT_ELEMENT = "http://schemas.xmlsoap.org/soap/envelope/";
	public static final String ELEMENT_NAME = "v1";
	public static final String NAMESPACE = "http://portal.vidyo.com/admin/v1_1";
	public static final String HEAD_ELEMENT = "soapenv:Header";
	public static final String BODY_ELEMENT = "soapenv:Body";
	public Document document;
	private Element headElement;
	private Element bodyElement;
	
	
	private PackageSoapXml() {
		this.document = DocumentHelper.createDocument();
		//设置xml字符编码
		this.document.setXMLEncoding(SOAP_ENCODING);
		// 包装最外层元素
		Element rootElement = this.document.addElement(ROOT_ELEMENT_NAME, ROOT_ELEMENT); 
		// 添加命名空间
		rootElement.addNamespace(ELEMENT_NAME, NAMESPACE); 
		// 添加head元素
		this.headElement = rootElement.addElement(HEAD_ELEMENT);
		// 添加body元素
		this.bodyElement = rootElement.addElement(BODY_ELEMENT);
	
	}
	
	/**
	 * 
	
	  * @Title: builder
	
	  * @Description: 创建PackageSoapXml对象
	
	  * @param @return    
	
	  * @return PackageSoapXml 
	
	  * @throws
	 */
	public static PackageSoapXml builder() {
		return new PackageSoapXml();
	}
	
	 //body中加入元素
	 private  Element addSimpleElement(String name) {
		 return (name != null && !"".equals(name)) ? bodyElement.addElement(name) : null;

	 }
	 
	 /**
		 * 
		
		  * @Title: addRoom
		
		  * @Description: 调用vidyo创建会议室接口
		
		  * @param @param preElement 创建会议室的元素名称
		  * @param @param elementBody 方法体内容
		
		  * @return String 返回包装后的xml  
		
		  * @throws
		 */
	 public String packageAddRoom(String preElement, Map<String, String> elementBody) {
			Element element1 = this.addSimpleElement(preElement);
			if (element1 == null) {
				return null;
			}
			// 非必须元素
			element1.addElement(VidyoConstant.ADD_ROOM_ELEMENT_1).addText("");
			// room为必须元素
			Element  element2 = element1.addElement(VidyoConstant.ADD_ROOM_ELEMENT_2);
			// roomID不用传参，等待vidyo返回
			element2.addElement(VidyoConstant.ROOM_ID);
			// name 自定义会议名称，这里取值conferenceCode
			element2.addElement(VidyoConstant.NAME).addText(elementBody.get("conferenceCode") == null ? "" : VidyoConstant.DEFAULT_NAME + elementBody.get("conferenceCode"));
			// 默认元素
			element2.addElement(VidyoConstant.ROOM_TYPE).addText(VidyoConstant.PUBLIC);
			// 会议室管理员 admin
			element2.addElement(VidyoConstant.OWNER_NAME).addText(VidyoConstant.ADMIN);
			Random random = new Random();
			Integer number = random.nextInt(1000000000);
			// extension vidyo为国安分配默认前缀 479 加一个随机数字
			element2.addElement(VidyoConstant.EXTENSION).addText(VidyoConstant.EXTENSION_PREFIX + number);
			// 默认元素
			element2.addElement(VidyoConstant.GROUP_NAME).addText(VidyoConstant.DEFAULT);
			// 会议其他参数
			Element  element3 = element2.addElement(VidyoConstant.ADD_ROOM_ELEMENT_3);
			// roomURL 不用传参，等待vidyo返回
			element3.addElement(VidyoConstant.ROOM_URL).addText("");
			// 会议室是否上锁，如果上锁会议室则进不去。传false
			element3.addElement(VidyoConstant.IS_LOCKED).addText(VidyoConstant.FALSE);
			// hasPIN, 设置PIN码
			element3.addElement(VidyoConstant.HAS_PIN).addText(VidyoConstant.TRUE);
			// 会议室PIN码，入会需要使用。自定义123456,可以更改。
			element3.addElement(VidyoConstant.ROOM_PIN).addText(VidyoConstant.PIN);
			// 以下都为默认元素
			element3.addElement(VidyoConstant.HAS_MODERATOR_PIN).addText(VidyoConstant.FALSE);
			element3.addElement(VidyoConstant.MODERATOR_PIN).addText("");
			element2.addElement(VidyoConstant.DESCRIPTION).addText("");
			return this.getDocument().asXML();
			
	 }
	
	 /**
	  * 
	 
	   * @Title: deleteRoom
	 
	   * @Description: 删除vidyo会议室
	 
	   * @param @param preElement 删除会议室的元素名称
	   * @param @param elementBody 方法体内容
	   * @return String 包装后的xml
	 
	   * @throws
	  */
	 public String packageDeleteRoom(String preElement, Map<String, String> elementBody) {
		 Element element1 = this.addSimpleElement(preElement);
		 if (element1 == null) {
			 return null;
		 }
		 
		 // 根据roomID进行删除
		 element1.addElement(VidyoConstant.ROOM_ID).addText((String)elementBody.get("roomID") == null ? "" : elementBody.get("roomID"));
		 return this.getDocument().asXML();
	 }
	 
	 /**
	  * 
	 
	   * @Title: getRoom
	 
	   * @Description: 查询会议室接口
	 
	   * @param @param preElement 查询会议室的元素名称
	   * @param @param elementBody 方法体内容
	   * @param @return    
	 
	   * @return String 包装后的xml
	 
	   * @throws
	  */
	 public String packageGetRoom(String preElement, Map<String, String> elementBody) {
		 Element element1 = this.addSimpleElement(preElement);
		 if (element1 == null) {
			 return null;
		 }
		 
		 // 根据roomID进行查询
		 element1.addElement(VidyoConstant.ROOM_ID).addText(elementBody.get("roomID") == null ? "" : elementBody.get("roomID"));
		 return this.getDocument().asXML();
	 }
	
	 /**
	  * 
	 
	   * @Title: getRooms
	 
	   * @Description: 根据相关条件查询会议室集合
	 
	   * @param @param preElement 查询会议室集合的元素名称
	   * @param @param elementBody 方法体内容
	   * @param @return    
	 
	   * @return String 包装后的xml
	 
	   * @throws
	  */
	 public String packageGetRooms(String preElement, Map<String, String> elementBody) {
		 Element element1 = this.addSimpleElement(preElement);
		 if (element1 == null) {
			 return null;
		 }
		 
		 // 以下为查询条件
		 Element element2 = element1.addElement(VidyoConstant.FILTER);
		 element2.addElement(VidyoConstant.START).addText(elementBody.get("start") == null ? "" : elementBody.get("start"));
		 element2.addElement(VidyoConstant.LIMIT).addText(elementBody.get("limit") == null ? "" : elementBody.get("limit"));
		 element2.addElement(VidyoConstant.SORTBY).addText(elementBody.get("sortBy") == null ? "" : elementBody.get("sortBy"));
		 element2.addElement(VidyoConstant.DIR).addText(elementBody.get("dir") == null ? "" : elementBody.get("dir"));
		 element2.addElement(VidyoConstant.QUERY).addText(elementBody.get("query") == null ? "" : elementBody.get("query"));
		 return this.getDocument().asXML();

	 }
	 
	 /**
	  * 
	 
	   * @Title: getParticipants
	 
	   * @Description: 根据会议号查询会议室参与人
	 
	   * @param @param preElement 查询会议室参会人的元素名称
	   * @param @param elementBody 方法体内容
	   * @param @return    
	 
	   * @return String 包装后的xml
	 
	   * @throws
	  */
	 public String packageGetParticipants(String preElement, Map<String, String> elementBody) {
		 Element element1 = this.addSimpleElement(preElement);
		 if (element1 == null) {
			 return null;
		 }
		 
		 // 根据会议室id进行查询
		 element1.addElement(VidyoConstant.CONFERENCE_ID).addText(elementBody.get("roomID") == null ? "" : elementBody.get("roomID"));
		 Element element2 = element1.addElement(VidyoConstant.FILTER);
		 element2.addElement(VidyoConstant.START).addText(elementBody.get("start") == null ? "" : elementBody.get("start"));
		 element2.addElement(VidyoConstant.LIMIT).addText(elementBody.get("limit") == null ? "" : elementBody.get("limit"));
		 element2.addElement(VidyoConstant.SORTBY).addText(elementBody.get("sortBy") == null ? "" : elementBody.get("sortBy"));
		 element2.addElement(VidyoConstant.DIR).addText(elementBody.get("dir") == null ? "" : elementBody.get("dir"));
		 element2.addElement(VidyoConstant.QUERY).addText(elementBody.get("query") == null ? "" : elementBody.get("query"));
		 return this.getDocument().asXML();
		 
	 }
	 
	 /**
	  * 
	 
	   * @Title: leaveConference
	 
	   * @Description: 离开会议室接口
	 
	   * @param @param preElement 离开会议室的元素名称
	   * @param @param elementBody 方法体内容
	   * @param @return    
	 
	   * @return String 包装后的xml
	 
	   * @throws
	  */
	 public String pakageLeaveConference(String preElement, Map<String, String> elementBody) {
		 Element element1 = this.addSimpleElement(preElement);
		 if (element1 == null) {
			 return null;
		 }
		 
		 // 会议室id
		 element1.addElement(VidyoConstant.CONFERENCE_ID).addText(elementBody.get("conferenceID") == null ? "" : elementBody.get("conferenceID"));
		 // 参会人id
		 element1.addElement(VidyoConstant.PARTICIPANT_ID).addText(elementBody.get("participantID") == null ? "" : elementBody.get("participantID"));
		 return this.getDocument().asXML();
	 }
	 
	 /**
	  * 
	 
	   * @Title: inviteToConference
	 
	   * @Description: 邀请外部设备入会
	 
	   * @param @param preElement 邀请外部设备入会的元素名
	   * @param @param elementBody 方法体内容
	   * @param @return    
	 
	   * @return String 包装后的xml
	 
	   * @throws
	  */
	 public String packageInviteToConference(String preElement, Map<String, String> elementBody) {
		 Element element1 = this.addSimpleElement(preElement);
		 if (element1 == null) {
			 return null;
		 }
		 // 会议室id
		 element1.addElement(VidyoConstant.CONFERENCE_ID).addText(elementBody.get("conferenceID") == null ? "" : elementBody.get("conferenceID"));
		 // 呼叫外部终端设备选用这个参数，sip呼叫方式<Vidyo extension> @ <gw IP address 192.168.1.110 or URL mydomain.com >
		 element1.addElement(VidyoConstant.INVITE).addText(elementBody.get("invite") == null ? "" : elementBody.get("invite"));
		 return this.getDocument().asXML();

	 }
	 
	 public static void main(String[] args) {
		PackageSoapXml packageSoapXml = new PackageSoapXml();
		Map<String, String> elementBody = new HashMap<>();
				
		System.out.println(packageSoapXml.packageAddRoom(VidyoConstant.ADD_ROOM, elementBody));
		System.out.println("--------------------");
		System.out.println(packageSoapXml.packageAddRoom(VidyoConstant.ADD_ROOM, elementBody));
	}
	
	
}

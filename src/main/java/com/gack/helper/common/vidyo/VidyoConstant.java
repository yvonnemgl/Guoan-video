
package com.gack.helper.common.vidyo;



/**
 * 

  * @ClassName: VidyoConstant

  * @Description: 此类为调用第三方vidyo的webservice接口所需用到的参数常量。

  * @author lsy

  * @date 2018年8月29日 下午1:52:38

  *
 */
public class VidyoConstant {
	/**
	 *  会议
	 */
	public static final String DEFAULT_NAME = "会议";
	/**
	 *  Public
	 */
	public static final String PUBLIC = "Public";
	/**
	 *  admin
	 */
	public static final String ADMIN = "admin";
	/**
	 *  default
	 */
	public static final String DEFAULT = "default";
	/**
	 *  true
	 */
	public static final String TRUE = "true";
	/**
	 *  false
	 */
	public static final String FALSE = "false";
	/**
	 *  PIN 默认PIN码
	 */
	public static final String PIN = "123456";
	/**
	 *  OK
	 */
	public static final String OK = "OK";
	/**
	 *  NO
	 */
	public static final String NO = "NO";
	/**
	 *  total
	 */
	public static final String TOTAL = "total";
	/**
	 *  room
	 */
	public static final String ROOM = "room";
	/**
	 *  Request
	 */
	public static final String REQUEST = "Request";
	/**
	 *  Response
	 */
	public static final String RESPONSE = "Response";
	/**
	 *  extensionPrefix 前缀，vidyo分配
	 */
	public static final String EXTENSION_PREFIX = "479";
	
	/**
	 *  v1:returnObjectInResponse <br />
	 *  增加会议室第一个元素（不知道干嘛用的，非必须）
	 */
	public static final String ADD_ROOM_ELEMENT_1 = "v1:returnObjectInResponse";
	/**
	 *  v1:room <br />
	 *  增加会议第二个元素，该元素下面的节点为创建会议室参数
	 */
	public static final String ADD_ROOM_ELEMENT_2 = "v1:room";
	/**
	 *  v1:RoomMode <br />
	 *  增加会议第三个元素，该元素下面的节点为创建会议室参数
	 */
	public static final String ADD_ROOM_ELEMENT_3= "v1:RoomMode";
	/**
	 *  v1:roomID 
	 */
	public static final String ROOM_ID = "v1:roomID";
	/**
	 *  v1:name 
	 */
	public static final String NAME = "v1:name";
	/**
	 *  v1:RoomType 
	 */
	public static final String ROOM_TYPE = "v1:RoomType";
	/**
	 *  v1:ownerName 
	 */
	public static final String OWNER_NAME = "v1:ownerName";
	/**
	 *  v1:extension
	 */
	public static final String EXTENSION= "v1:extension";
	/**
	 *  v1:groupName 
	 */
	public static final String GROUP_NAME = "v1:groupName";
	/**
	 *  v1:roomURL
	 */
	public static final String ROOM_URL = "v1:roomURL";
	/**
	 *  v1:isLocked
	 */
	public static final String IS_LOCKED = "v1:isLocked";
	/**
	 *  v1:hasPIN
	 */
	public static final String HAS_PIN = "v1:hasPIN";
	/**
	 *  v1:roomPIN
	 */
	public static final String ROOM_PIN = "v1:roomPIN";
	/**
	 *  v1:hasModeratorPIN
	 */
	public static final String HAS_MODERATOR_PIN = "v1:hasModeratorPIN";
	/**
	 *  v1:moderatorPIN
	 */
	public static final String MODERATOR_PIN = "v1:moderatorPIN";
	/**
	 * v1:description
	 */
	public static final String DESCRIPTION = "v1:description";
	/**
	 *  v1:Filter"
	 */
	public static final String FILTER = "v1:Filter";
	/**
	 *  v1:start
	 */
	public static final String START = "v1:start";
	/**
	 *  v1:limit
	 */
	public static final String LIMIT = "v1:limit";
	/**
	 *  v1:sortBy
	 */
	public static final String SORTBY = "v1:sortBy";
	/**
	 *  v1:dir
	 */
	public static final String DIR = "v1:dir";
	/**
	 *  v1:query
	 */
	public static final String QUERY = "v1:query";
	/**
	 *  v1:conferenceID
	 */
	public static final String CONFERENCE_ID = "v1:conferenceID";
	/**
	 *  v1:participantID
	 */
	public static final String PARTICIPANT_ID = "v1:participantID";
	/**
	 *  v1:invite
	 */
	public static final String INVITE = "v1:invite";
	/**
	 *  v1:AddRoomRequest
	 */
	public static final String ADD_ROOM_REQUEST = "v1:AddRoomRequest";
	/**
	 *  v1:AddRoomResponse
	 */
	public static final String ADD_ROOM_RESPONSE = "AddRoomResponse";
	/**
	 *  addRoom
	 */
	public static final String ADD_ROOM = "addRoom";
	/**
	 *  v1:DeleteRoomRequest
	 */
	public static final String DELETE_ROOM_REQUEST = "v1:DeleteRoomRequest";
	/**
	 *  DeleteRoomRequest
	 */
	public static final String DELETE_ROOM_RESPONSE = "DeleteRoomResponse";
	/**
	 *  deleteRoom
	 */
	public static final String DELETE_ROOM = "deleteRoom";
	/**
	 *  v1:GetRoomsRequest
	 */
	public static final String GET_ROOMS_REQUEST = "v1:GetRoomsRequest";
	/**
	 *  GetRoomsResponse
	 */
	public static final String GET_ROOMS_RESPONSE = "GetRoomsResponse";
	/**
	 *  getRooms
	 */
	public static final String GET_ROOMS = "getRooms";
	/**
	 *  v1:GetParticipantsRequest
	 */
	public static final String GET_PARTICIPANTS_REQUEST = "v1:GetParticipantsRequest";
	/**
	 *  GetParticipantsResponse
	 */
	public static final String  GET_PARTICIPANTS_RESPONSE = "GetParticipantsResponse";
	/**
	 *  getParticipants
	 */
	public static final String GET_PARTICIPANTS = "getParticipants";
	/** 
	 *  leaveConference
	 */
	public static final String LEAVE_CONFERENCE = "leaveConference";
	/**
	 *  v1:LeaveConferenceRequest
	 */
	public static final String LEAVE_CONFERENCE_REQUEST = "v1:LeaveConferenceRequest";
	/**
	 *   LeaveConferenceResponse
	 */
	public static final String LEAVE_CONFERENCE_RESPONSE = "LeaveConferenceResponse";
	/**
	 * v1:InviteToConferenceRequest
	 */
	public static final String INVITE_TO_CONFERENCE_REQUEST = "v1:InviteToConferenceRequest";
	/**
	 * InviteToConferenceResponse 
	 */
	public static final String INVITE_TO_CONFERENCE_RESPONSE = "InviteToConferenceResponse";
	/**
	 * inviteToConference 
	 */
	public static final String INVITE_TO_CONFERENCE = "inviteToConference";

}

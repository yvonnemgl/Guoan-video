package com.gack.helper.common;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import lombok.extern.slf4j.Slf4j;


@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(value = Exception.class)
	public Object defaultErrorHandler(HttpServletRequest request, Exception e) throws Exception {
		
		log.error("-------------------------ERROR-------------------------");
		log.error("URL : " + request.getRequestURL().toString());
		log.error("HTTP_METHOD : " + request.getMethod());
		Enumeration<String> paraneters = request.getParameterNames();
		while(paraneters.hasMoreElements()){
			String name = paraneters.nextElement();
			log.error("{} = {}",name,request.getParameter(name));
		}
		log.error("IP : " + request.getRemoteAddr());
		log.error("EXCEPTION : " + e);
		e.printStackTrace();
		return new AjaxJson().setSuccess(false).setStatus(500).setMsg("操作失败").setData(null);
	}
	
}

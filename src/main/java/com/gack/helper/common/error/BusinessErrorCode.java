/*
 * Copyright  (c) 2017. By AsherLi0103
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.gack.helper.common.error;

/**
 * @author AsherLi0103
 * @version 1.0.00
 */
public enum BusinessErrorCode {


    MISS_REQUIRED_PARAMETER(100000, "缺少必要参数",false),

    FAILED_SAVE_DATA(110000, "保存数据失败",false),
    FAILED_LOAD_DATA(110001, "数据加载失败",false),
    UNAUTHORIZED_ACCESS(100001, "非法访问",false),

    //业务逻辑异常
    BUSINESS_MSG_PHONE_EMPTY(120001, "手机号码不能为空",false),
    BUSINESS_MSG_PHONE_ERROR(120002, "请输入正确的手机�?",false),
    BUSINESS_MSG_SEND_SUCCESS(120003, "短信已发�?,请注意查�?",true),
    BUSINESS_VALIDATE_ERROR(120004, "验证码错�?",false),
    BUSINESS_VALIDATE_TIME_OUT(120005, "验证码超�?",false),
    BUSINESS_VALIDATE_EMPTY(120006, "验证码不能为�?",false),
    BUSINESS_MSG_SEND_ERROR(120007, "验证短信发�?�失�?,请稍后重�?",false);

    private int status;
    private String message;
    private boolean success;

    BusinessErrorCode(int status, String message, boolean success) {
        this.status = status;
        this.message = message;
        this.success = success;
    }

    public int getStatus() {
        return status;
    }

    public BusinessErrorCode setCode(int status) {
        this.status = status;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public BusinessErrorCode setMessage(String message) {
        this.message = message;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public BusinessErrorCode setSuccess(boolean success) {
        this.success = success;
        return this;
    }
}

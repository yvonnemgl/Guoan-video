package com.gack.helper.common.config;

public class PayConfig {

	public static String BASE_URL = "https://pays.gack.citic/";
	
	public static String PAY_URL = BASE_URL + "iShangWuPay";
	
	public static String REFUND_URL = BASE_URL + "askrefund";
	
	public static String REFUND_QUERY = BASE_URL + "select/payorder";
	
	public static String CREATE_INVOICE = BASE_URL + "selIswInvoice";
}

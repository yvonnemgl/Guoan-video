package com.gack.helper.common.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.gack.business.model.UrlAndPermission;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * @author ws
 * 2018-8-2
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Component
@PropertySource("urlAndPermission.properties")
@ConfigurationProperties()
public class UrlAndPermissionConfig {

	private List<UrlAndPermission> urlAndPermission;
	
}

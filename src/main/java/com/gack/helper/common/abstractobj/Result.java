package com.gack.helper.common.abstractobj;

import lombok.Getter;
import lombok.Setter;

/**
 * 
* @ClassName: Result.java 
* @Description: 同一返回值类型
* @author Cancerl
* @date 2018年3月21日
*  
 */
@Setter
@Getter
public class Result {
	String key;
	Object value;
	String msg;
}

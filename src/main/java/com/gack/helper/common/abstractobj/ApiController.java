package com.gack.helper.common.abstractobj;

public abstract class ApiController {
    private static final String API_PATH = "/com/gack";

    public static final String USER_URL = API_PATH + "/user";
    public static final String USER_LOGIN_AND_REGISTER_URL = API_PATH + "/userLoginAndRegister";
    public static final String ENTERPRISE_URL = API_PATH + "/enterprise";
    public static final String DEPARTMENT_URL = API_PATH + "/department";
    public static final String MESSAGE_URL = API_PATH + "/message";
    public static final String MESSAGES_URL = API_PATH + "/messages";
    public static final String ORDERFORM_URL = API_PATH + "/orderform";
    public static final String VIDEO_SENSITIVE_WORD = API_PATH + "/sensitiveWord";
    public static final String USER_OPERATOR_URL = API_PATH + "/operator" + "/user";
    public static final String STORES_OPERATOR_URL = API_PATH + "/operator" + "/stores";
    public static final String POSITION_URL = API_PATH + "/position";
    public static final String MANAGER_URL = API_PATH + "/manage";
    public static final String ORDER_URL = API_PATH + "/order";
    public static final String CERTIFIED_URL = API_PATH + "/certified";
    public static final String USER_HOME_URL = API_PATH + "/home/user";
    public static final String ASSIGN_BUSINESS_CARD_MONEY_URL = API_PATH + "/assignBusinessCardMoney";
    public static final String JOB_RANK_URL = API_PATH + "/jobRank";
    public static final String JOB_RANK_SORT_URL = API_PATH + "/jobRankSort";
    
    public static final String VIDEO_CONFERENCE = API_PATH + "/conference";
    public static final String REMIND_TIME = API_PATH + "/remindtime";
    public static final String CARD = API_PATH + "/card";
    public static final String INVOICE = API_PATH + "/invoice";
    
    public static final String PERSON_NUM_RANGE_URL = API_PATH + "/personNumRange";
    public static final String INDUSTRY_URL = API_PATH + "/industry";
    public static final String PLACE_URL = API_PATH + "/place";

    public static final String CAMERA_URL = API_PATH + "/camera";
    
    public static final String FKB_URL = API_PATH + "/fkb";
    public static final String FKB_URL1 = API_PATH + "/fkb1";
    public static final String PERMISSION_URL = API_PATH + "/permission";
    public static final String INVITE_TO_ENTERPRISE_URL = API_PATH + "/inviteToEnterprise";
    public static final String VERSIONNUMBER_URL = API_PATH + "/versionNumber";//版本号
    
    public static final String UNITE_URL = API_PATH + "/unite";
    
    // netty
    public static final String PUSH_PATH = API_PATH + "/push";
    
    //OSS上传微信头像保存文件夹目�?
    public static final String USERHEADW_URL =  "guoanmaker/personal/userhead/wechat/";
    //OSS上传qq头像保存文件夹目�?
    public static final String USERHEADQ_URL = "guoanmaker/personal/userhead/qq/";
    //OSS上传微博头像保存文件夹目�?
    public static final String USERHEADWB_URL = "guoanmaker/personal/userhead/weibo/";
    public static final String ROLE_URL = API_PATH + "/role";
    public static final String APP_URL = API_PATH + "/app";
    public static final String INVOICE_URL = API_PATH + "/invoice";
    public static final String ORDER_FORM_URL = API_PATH + "/orderform";
    public static final String APPRAISE_URL = API_PATH + "/appraise";
    public static final String CAMPAIGN_URL = API_PATH + "/campaign";
    public static final String USERSTATISTICS_URL = API_PATH + "/userstatistics";
    public static final String CONTACTS = API_PATH + "/contacts";
    public static final String VIDYO = API_PATH + "vidyo";
    //加密密钥
    public static final String PASSWORD_SALT = "qwer,1234";
    
    //OSS上传头像保存文件夹目�?
    public static final String PORTRAINT_URL = "guoanmaker/personal/portrait/";
    
    public static final String INVOICE_PIC_URL="guoanmaker/personal/invoice/";
    
    //短信接口相关配置
    public static final String SMS_USERNAME="gacksw";
    public static final String SMS_PASSWORD="Gack123456";
    public static final String SMS_URL="http://114.255.71.158:8061/";
    public static final String SMS_EPID="121772";
    public static final String PAY_URL="http://123.56.48.63/pay/original/";
    public static final String VIDEO_MEETING = API_PATH + "/VideoMeeting";
    
}
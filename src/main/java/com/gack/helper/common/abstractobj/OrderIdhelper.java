package com.gack.helper.common.abstractobj;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gack.helper.redis.RedisClient;

@Component
public class OrderIdhelper {

	@Autowired
	private RedisClient redisClient;

	// 获取当前时间 格式yyyyMMddHHmmss 14位
	public String getCurrentTime() {
		Date now = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyMMdd");
		String currentTime = format.format(now);
		return currentTime;
	}

	// 获取前几天时间
	public Date getAfterDay(int i) {
		Date date = getDayZero(new Date());
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, i);
		date = calendar.getTime();
		return date;
	}

	// 计算两个时间差值（秒数）
	public long differFromSecond(Date date1, Date date2) {
		long minutes = (date2.getTime() - date1.getTime()) / 1000;
		return minutes;
	}

	// 获取当前时间到明天0点秒数
	public long getToZeroSecond() {
		Date date = new Date();
		Date afterDate = getAfterDay(1);
		return differFromSecond(date, afterDate);
	}

	// 获取时间的零点
	public Date getDayZero(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		Date start = calendar.getTime();
		return start;
	}

	// 产生count位随机字符串
	public String getNumber() {

		String result = redisClient.get("orderId");
		if (result == null) {
			redisClient.setex("orderId", (int) getToZeroSecond(), "1");
			result = "1";
		} else {
			int rs = Integer.parseInt(result) + 1;
			result = String.valueOf(rs);
			redisClient.setex("orderId", (int) getToZeroSecond(), result);
		}
		if (result.length() == 1) {
			result = "00" + result;
		} else if (result.length() == 2) {
			result = "0" + result;
		}
		return result;
	}

	// 88+业务+年（6位）+太阳日（3位）+自然数（3位）

	public String getRentId() {
		return "8802" + getCurrentTime() + "365" + getNumber();
	}

	public String getBusinessId() {
		return "8803" + getCurrentTime() + "365" + getNumber();
	}
	// 订单号
	public String getOrderId() {
		return "8801" + getCurrentTime() + "365" + getNumber();
	}

}

package com.gack.helper.common.util;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

public class XmlDocument {

	public static final String SOAP_ENCODING = "UTF-8";
	
	private Document document;
	private Element headElement;
	private Element bodyElement;
	
	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
		headElement = document.getRootElement().element("Header");
		Element _bodyElement = document.getRootElement().element("Body");
		bodyElement = _bodyElement == null ? document.addElement("ERROR")
				: _bodyElement;
	}

	public Element getHeadElement() {
		return headElement;
	}

	public void setHeadElement(Element headElement) {
		this.headElement = headElement;
	}

	public Element getBodyElement() {
		return bodyElement;
	}

	public void setBodyElement(Element bodyElement) {
		this.bodyElement = bodyElement;
	}

	public XmlDocument() {
	}

	public Element createRoot() {
		document = DocumentHelper.createDocument();
		document.setXMLEncoding(SOAP_ENCODING);

		Element rootElement = document.addElement("soapenv:Envelope",
				"http://schemas.xmlsoap.org/soap/envelope/");
		rootElement.addNamespace("v1", "http://portal.vidyo.com/admin/v1_1");

		headElement = rootElement.addElement("soapenv:Header");
		bodyElement = rootElement.addElement("soapenv:Body");
		return rootElement;
	}

	public void addSimpleHead(String name, String value) {
		if (name != null && !"".equals(name)) {
			if (value == null)
				value = "";
			addSimpleField(headElement, name, value);
		}
	}
	
	public void addSimpleField(String name, String value) {
		if (name != null && !"".equals(name)) {
			if (value == null)
				value = "";
			addSimpleField(bodyElement, name, value);
		}
	}
	
	public Element addSimpleElement(String name) {
		if (name != null && !"".equals(name)) {
			
			return bodyElement.addElement(name);
		}
		return null;
	}

	public void addSimpleField(Element listElement, String name, String value) {

		if (listElement == null) {
			addSimpleField(name, value);
		} else {
			if (name != null && !"".equals(name)) {
				if (value == null)
					value = "";
				listElement.addElement("gateway:field")
						.addAttribute("name", name).setText(value);
			}
		}
	}

	public String getSimpleBodyElementValue(String name) {

		return getSplBodyEleFieldValue(bodyElement, name);
	}

	public String getSplBodyEleFieldValue(Element element, String name) {

		List<Element> _lis = element.elements("field");

		Iterator<Element> it = _lis.iterator();
		Element field = null;
		while (it.hasNext()) {
			field = it.next();
			if (field.attribute("name").getText() != null
					&& name.equalsIgnoreCase(field.attribute("name").getText())) {

				return field.getTextTrim();
			}
		}
		return "";
	}

	public List<Element> getFieElementByName(String name) {

		List<Element> lis = bodyElement.elements("fie");
		List<Element> _lis = new ArrayList<Element>();

		Iterator<Element> it = lis.iterator();
		while (it.hasNext()) {
			Element fel = it.next();
			if (fel.attribute("name").getText() != null
					&& name.equalsIgnoreCase(fel.attribute("name").getText())) {

				_lis.add(fel);
			}
		}

		return _lis;
	}

	private static Element Parse(String xmlStream) {
		StringReader stringReader = new StringReader(xmlStream);
		SAXReader saxReader = new SAXReader();
		saxReader.setEncoding(SOAP_ENCODING);
		Element rootElement = null;
		try {
			Document document = saxReader.read(stringReader);
			rootElement = document.getRootElement();
		} catch (DocumentException ex) {
			ex.printStackTrace();
		}
		return rootElement;
	}

}


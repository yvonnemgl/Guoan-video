package com.gack.helper.common.util;


import java.net.URLEncoder;

import com.gack.helper.common.abstractobj.ApiController;

public class SendCode {
	/**
	 * 
	* @Description: 发送短信 
	* @param telephone 手机号
	* @param message 信息内容
	* @param @throws Exception
	* @author Yvonne_MGL 
	* @date 2017年8月28日 上午9:39:30     
	* @return void    
	* @throws 
	 */
	public static void sendMessage(String telephone,String message) throws Exception{
		String url = ApiController.SMS_URL;
		String username = ApiController.SMS_USERNAME;
		String password = ApiController.SMS_PASSWORD;
		String epid = ApiController.SMS_EPID;
		String param = "";
		param = "username=" + username + "&password=" + password + "&phone=" + telephone + "&epid=" + epid
				+ "&linkid=&subcode=" + "&message=" + URLEncoder.encode("【i商务】" + message, "gb2312");
		SMSHelper.SendGET(url, param);
	}

}

package com.gack.helper.common.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONArray;

import net.sf.json.JSONObject;
import sun.misc.BASE64Encoder;

/**
 * java后台极光推送方式一：使用Http API
 * 此种方式需要自定义http请求发送客户端:HttpClient
 */
public class Push {
	private static final Logger log = LoggerFactory.getLogger(Push.class);
    private String masterSecret = "62237ea0baf9db54d1be0b99";
    private String appKey = "dde685abb543dcc27de8f744";
    private String pushUrl = "https://api.jpush.cn/v3/push";    
    private boolean apns_production = true;    
    private int time_to_live = 86400;
    
    public void setApns_production(boolean apns_production) {
    	this.apns_production = apns_production;
    }
    public boolean getApns_production() {
    	return this.apns_production;
    }
    
    public JSONObject jiguangPush(String username, String mesType, String message, String title, String url){
        String alias =username;//声明别名
        JSONObject resData=null;
//        System.out.println(id+"id室");
        try{
            String result = push(mesType,url,pushUrl,alias,message,appKey,masterSecret,apns_production,time_to_live,title);
            resData = JSONObject.fromObject(result);
                if(resData.containsKey("error")){
                    log.info("针对别名为" + alias + "的信息推送失败！");
                    JSONObject error = JSONObject.fromObject(resData.get("error"));
                    log.info("错误信息为：" + error.get("message").toString());
                }
//            log.info("针对别名为" + alias + "的信息推送成功！");
            System.out.println(resData);
        }catch(Exception e){
            log.error("针对别名为" + alias + "的信息推送失败！",e);
        }
        
        return resData;
    }
    
    /**
     * 组装极光推送专用json串
     * @param alias
     * @param alert
     * @return json
     *///mesType,url,alias,alert,apns_production,time_to_live,title
    public static JSONObject generateJson(String mesType,String url, String alias,String alert,boolean apns_production,int time_to_live,String title){
        JSONObject json = new JSONObject();
        JSONArray platform = new JSONArray();//平台
        platform.add("android");
        platform.add("ios");
        
        JSONObject audience = new JSONObject();//推送目标
        JSONArray alias1 = new JSONArray();
        alias1.add(alias);
        audience.put("alias", alias1);
        
        JSONObject notification = new JSONObject();//通知内容
        JSONObject android = new JSONObject();//android通知内容
        android.put("alert", alert);
        android.put("builder_id", 1);
        JSONObject android_extras = new JSONObject();//android额外参数
       /*JSONObject android_news = new JSONObject();//android额外参数
        * 
*/     /*  if(mesType.equals("19")){
    	   android_extras.put("content", id);
           android_extras.put("mesType", "19");
       }else if(mesType.equals("20")){
    	   android_extras.put("content", url);
           android_extras.put("mesType", "20");
       }else if(mesType.equals("21")){
    	   android_extras.put("content", commodityid);
           android_extras.put("mesType", "21");
       }else if(mesType.equals("22")) {
    	   android_extras.put("content", url);
           android_extras.put("mesType", "22");
       }*/
     /* android_extras.put("content", alert);*/
        /*android_extras.put("type", title);*/
        android_extras.put("content", url);
        android_extras.put("mesType", mesType);
        android.put("extras", android_extras);
        
        /*android.put("json", android_extras);*/
        
        JSONObject ios = new JSONObject();//ios通知内容
        ios.put("alert", alert);
        ios.put("sound", "default");
        ios.put("badge", "+1");
        JSONObject ios_extras = new JSONObject();//ios额外参数
//        ios_extras.put("type", id);
        ios_extras.put("content", url);
    	ios_extras.put("mesType", mesType);
        
        ios.put("extras", ios_extras);
        notification.put("android", android);
        notification.put("ios", ios);
        
        JSONObject options = new JSONObject();//设置参数
        options.put("time_to_live", Integer.valueOf(time_to_live));
        options.put("apns_production", apns_production);
      
        json.put("platform", platform);
        json.put("audience", audience);
        json.put("notification", notification);
        json.put("options", options);
        
        
        return json;
        
    }

    
    /**
     * 推送方法-调用极光API
     * @param reqUrl
     * @param alias
     * @param alert
     * @return result
     *///mesType,url,pushUrl,alias,message,appKey,masterSecret,apns_production,time_to_live,title)
    public static String push(String mesType,String url,String reqUrl,String alias,String alert,String appKey,String masterSecret,boolean apns_production,int time_to_live,String title){
        String base64_auth_string = encryptBASE64(appKey + ":" + masterSecret);
        String authorization = "Basic " + base64_auth_string;
        return sendPostRequest(reqUrl,generateJson(mesType,url,alias,alert,apns_production,time_to_live,title).toString(),"UTF-8",authorization);
    }
    
    /**
     * 发送Post请求（json格式）
     * @param reqURL
     * @param data
     * @param encodeCharset
     * @param authorization
     * @return result
     */
    @SuppressWarnings({ "resource" })
    public static String sendPostRequest(String reqURL, String data, String encodeCharset,String authorization){
        HttpPost httpPost = new HttpPost(reqURL);
        HttpClient client = new DefaultHttpClient();
        HttpResponse response = null;
        String result = "";
        try {
             StringEntity entity = new StringEntity(data, encodeCharset);
             entity.setContentType("application/json");
             httpPost.setEntity(entity);
             httpPost.setHeader("Authorization",authorization.trim());
             response = client.execute(httpPost);
             result = EntityUtils.toString(response.getEntity(), encodeCharset);
        } catch (Exception e) {
            log.error("请求通信[" + reqURL + "]时偶遇异常,堆栈轨迹如下", e);  
        }finally{
            client.getConnectionManager().shutdown();
        }
        return result;
    }
    
    /** 
　　　　* BASE64加密工具
　　　　*/
     public static String encryptBASE64(String str) {
         byte[] key = str.getBytes();
       BASE64Encoder base64Encoder = new BASE64Encoder();
       String strs = base64Encoder.encodeBuffer(key);
         return strs;
     }
     public static void main(String[] args) {
		Push push = new Push();
		push.setApns_production(false);
		push.jiguangPush("18843013258", "12", "消息", "标题", "123");
	}

}

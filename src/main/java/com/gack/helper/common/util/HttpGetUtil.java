package com.gack.helper.common.util;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.URI;
import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.methods.PostMethod;


public class HttpGetUtil {
    // 发起http get类型请求获取返回结果
   public static String httpGet(String urlstr) {
	   String ret = "";
       URL url;
	try {
		url = new URL(urlstr);
	       HttpURLConnection conn = (HttpURLConnection) url.openConnection();

	       conn.connect();
	       InputStream is = conn.getInputStream();

	       byte[] buff = new byte[is.available()];
	       is.read(buff);
	       ret = new String(buff, "utf-8");

	       is.close();
	       conn.disconnect();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
       return ret;
   }
   
   
   // 发起http post类型请求获取返回结果
  public static String httpPost(String urlstr, String grant_type, String code, String client_id, String client_secret, String redirect_uri) {
		HttpClient client = new HttpClient();
		PostMethod method = new PostMethod();
		try {
			long timestamps = System.currentTimeMillis(); 
			//URI base = new URI(url, false);
			method.setURI(new URI(urlstr, /*"HttpSendSM",*/ false));
			method.setQueryString(new NameValuePair[] { 
					new NameValuePair("grant_type", grant_type),
					new NameValuePair("code", code), 
					new NameValuePair("client_id", client_id),
					new NameValuePair("client_secret", client_secret),
					new NameValuePair("redirect_uri", redirect_uri),
				});
			int result = client.executeMethod(method);
			if (result == HttpStatus.SC_OK) {
				InputStream in = method.getResponseBodyAsStream();
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				byte[] buffer = new byte[1024];
				int len = 0;
				while ((len = in.read(buffer)) != -1) {
					baos.write(buffer, 0, len);
				}
				return URLDecoder.decode(baos.toString(), "UTF-8");
			} else {
				throw new Exception("HTTP ERROR Status: " + method.getStatusCode() + ":" + method.getStatusText());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		finally {
			method.releaseConnection();
		}
  }

/**
    * 下载头像图片
    * @author pl
    * @param portrait
    * @return
    */
   public static byte[] downloadPortrait(String portraitUrl) {

       ByteArrayOutputStream baos = new ByteArrayOutputStream();

       try {
           URL url = new URL(portraitUrl);
           HttpURLConnection conn = (HttpURLConnection) url.openConnection();
           conn.connect();
           InputStream is = (InputStream) conn.getInputStream();

           byte[] buff = new byte[1024];
           int cnt;
           while ((cnt = is.read(buff)) > 0) {
               baos.write(buff, 0, cnt);
           }

           is.close();
           conn.disconnect();

           baos.close();
           return baos.toByteArray();
       } catch (IOException e) {
           e.printStackTrace();
           return null;
       }
   }
   
   
   // 写头像文�?
   public static void savePortrait(String fileName, byte[] portraitData) {
       FileOutputStream fos = null;
       try {
           fos = new FileOutputStream(fileName);
           fos.write(portraitData);
       } catch (IOException e) {
           e.printStackTrace();
       } finally {
           if (fos != null) {
               try {
                   fos.close();
               } catch (IOException e) {
               }
           }
       }
   }

}

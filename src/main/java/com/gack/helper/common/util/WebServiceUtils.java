package com.gack.helper.common.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.springframework.beans.factory.annotation.Autowired;

import com.gack.business.sendscheduled.SendScheduledUpdateMetting;


public class WebServiceUtils{

	public static void main(String[] args) {
		deleteRoom();
		/*createRoom();*/
	
		/*sendScheduledUpdateMetting.updateVideoMeeting();*/
		/*Map canshu=new HashMap<String,Object>();
		canshu.put("i",1);
		canshu.put("extension","689735569875");
		PackerUtil packerUtil1=new PackerUtil();
		packerUtil1.packBody3("v1:GetRoomsRequest", canshu);
		String data1 = packerUtil1.xDoc.getDocument().asXML();
		System.out.println(data1);
		//调用webservice获取rooms接口
		Map<String,String> fanhuimap=new HashMap<String, String>();
		String xmlStr1=WebServiceUtils.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService",data1,"getRooms");
		System.out.println(xmlStr1);*/
		
	}
	
	//创建虚拟房间
	public static void createRoom() throws Exception{
		Map canshu=new HashMap<String,String>();
		PackerUtil packerUtil=new PackerUtil();
		packerUtil.packBody("v1:AddRoomRequest", canshu);
		String data = packerUtil.xDoc.getDocument().asXML();
		System.out.println(data);
		//调用webservice创建room接口
		Map<String,String> fanhuimap=new HashMap<String, String>();
		String xmlStr=WebServiceUtils.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService",data,"addRoom");
		System.out.println(xmlStr);
		Document dom = DocumentHelper.parseText(xmlStr);
		
        Element root=dom.getRootElement();  
        Element Body=root.element("Body");
        Element AddRoomResponse=Body.element("AddRoomResponse");
        String OK=AddRoomResponse.element("OK").getText();
        System.out.println(OK+"55");
	}
	
	//删除虚拟房间
	public static void deleteRoom(){
		Map canshu=new HashMap<String,String>();
		PackerUtil packerUtil=new PackerUtil();
		canshu.put("roomID", "58515");
		packerUtil.packBody1("v1:DeleteRoomRequest", canshu);
		String data = packerUtil.xDoc.getDocument().asXML();
		System.out.println(data);
		//调用webservice/删除room接口
		Map<String,String> fanhuimap=new HashMap<String, String>();
		String xmlStr=WebServiceUtils.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService",data,"deleteRoom");
	}
	
	//通过房间id获取房间
	public static void getRoom(){
		Map canshu=new HashMap<String,String>();
		PackerUtil packerUtil=new PackerUtil();
		packerUtil.packBody2("v1:GetRoomRequest", canshu);
		String data = packerUtil.xDoc.getDocument().asXML();
		System.out.println(data);
		//调用webservice获取room接口
		String xmlStr=WebServiceUtils.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService",data,"getRoom");
	}
	
	//查询虚拟房间列表
	public static void getRooms(){
		Map canshu=new HashMap<String,String>();
		PackerUtil packerUtil=new PackerUtil();
		packerUtil.packBody3("v1:GetRoomsRequest", canshu);
		String data = packerUtil.xDoc.getDocument().asXML();
		System.out.println(data);
		//调用webservice获取rooms接口
		Map<String,String> fanhuimap=new HashMap<String, String>();
		String xmlStr=WebServiceUtils.HttpClientForEsb("http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService",data,"getRooms");
	}
	
	public static String getParticipants(String roomId) {
		Map<String, Object> room = new HashMap<>();// room为参数的map集合
		room.put("roomId", roomId);// 传入roomID
		PackerUtil packerUtil = new PackerUtil();
		packerUtil.packBody4("v1:GetParticipantsRequest", room);
		String data = packerUtil.xDoc.getDocument().asXML();// 传入的xml形式
		System.out.println(data);
		// 调用webservice获取rooms接口
		String xmlStr = WebServiceUtils.HttpClientForEsb(
				"http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService", data, "getParticipants");
		return xmlStr;
	}
	/**
	 * 踢人接口
	 * @param conferenceID 会议roomid
	 * @param participantID vidyo客户id
	 * @return
	 */
	public static String leaveConference(String conferenceID, String participantID) {
		Map<String, Object> map = new HashMap<>();// 参数集合
		map.put("conferenceID", conferenceID);// 传入会议id(roomId)
		map.put("participantID", participantID);// 传入客户id(vidyo的客户id)
		PackerUtil packerUtil = new PackerUtil();
		packerUtil.packBody5("v1:LeaveConferenceRequest", map);
		String data = packerUtil.xDoc.getDocument().asXML();// 传入的xml形式
		System.out.println(data);
		// 调用webservice获取rooms接口
		String xmlStr = WebServiceUtils.HttpClientForEsb(
				"http://guoan.byshang.cn/services/v1_1/VidyoPortalAdminService", data, "leaveConference");
		return xmlStr;
		
		
	}
	
	//通过http协议远程访问，返回xml格式的字符串
	public  static String HttpClientForEsb(String url,String data,String functionName) {
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost post = new HttpPost(url);
		String xmlStr=null;
	    post.addHeader("Content-Type", "text/xml; charset=UTF-8"); 
	    post.addHeader("Authorization", "Basic YWRtaW46YWRtaW4wMQ==");
	    post.setHeader("SOAPAction",functionName);
	    try {
	    	StringEntity myEntity = new StringEntity(data,"UTF-8");
	        post.setEntity(myEntity);
	        HttpResponse response = client.execute(post);
	        String xmlString=EntityUtils.toString(response.getEntity());
	       // System.out.println(xmlString);
	        xmlStr = xmlString;
	        Header[] hs = response.getAllHeaders();
	       /* for(Header hs1 : hs){
	        	System.out.println(hs1.getName());
	        	System.out.println(hs1.getValue());
	        }*/
	        
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return xmlStr;
	}
	
	public static Map readXmlMapOut(String xml){
		Map map=new HashMap<Object, Object>();
		Document document=null;
		try{
			//将字符串转为xml
			document=DocumentHelper.parseText(xml);
			//获取根节点
			Element rootElement=document.getRootElement();
			//拿到根节点的名称
			System.out.println("根节点"+rootElement.getName());
			
			//获取根节点下的子节点Body
			Iterator iteratorbody=rootElement.elementIterator("Body");
			//遍历Body标签
			while(iteratorbody.hasNext()){
				Element element=(Element)iteratorbody.next();
				//获取Body子节点下的queryDueFeeResponse
				Iterator iterator2=element.elementIterator("queryDueFeeResponse");
				while(iterator2.hasNext()){
					Element element2=(Element)iterator2.next();
					//获取queryDueFeeResponse子节点下的return
					Iterator iterator3=element2.elementIterator("return");
					while(iterator3.hasNext()){
						Element element3=(Element)iterator3.next();
						//获取dueFee的值
						String dueFee=element3.elementTextTrim("dueFee");
						//获取item的值
						String item=element3.elementTextTrim("item");
						//获取term的值
						String term=element3.elementTextTrim("term");
						//测试数据
						System.out.println("-----------");
						System.out.println("dueFee"+dueFee);
						System.out.println("item"+item);
						System.out.println("term"+term);
						System.out.println("-------------");
						//将得到的数据分别放入map集合中
						map.put("dueFee", dueFee);
						map.put("item", item);
						map.put("term", term);
					}

				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return map;
	}
}

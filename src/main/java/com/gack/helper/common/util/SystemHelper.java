package com.gack.helper.common.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.LifecycleState;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import redis.clients.jedis.Jedis;

/**
 * 
 * @ClassName: SystemHelper 
 * @Description: 常用工具�?,例如字符转换�?
 * @author Yvonne_MGL
 * @date 2017�?7�?11�? 下午7:08:32    
 */
public class SystemHelper {

	/**
	 * 
	 * @Description: 获取6位数字验证码
	 * @param @return
	 * @author Yvonne_MGL
	 * @date 2017�?7�?11�? 下午7:09:24    
	 * @return String    @throws 
	 */
	public static String getSixCode() {
		StringBuilder code = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 6; i++) {
			code.append(String.valueOf(random.nextInt(10)));
		}

		String smsText = "" + code;
		return smsText;
	}

	/**
	 * 
	 * @Description: 获取5分钟之后的时间�?
	 * @param @return
	 * @author Yvonne_MGL
	 * @date 2017�?7�?11�? 下午7:09:49    
	 * @return Date    @throws 
	 */
	public static Date getMoreFiveMinTime() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 5);
		Date date = c.getTime();
		return date;
	}

	/**
	 * 
	 * @Description: 随机生成用户昵称,规则是gack_as123
	 * @param @return
	 * @author Yvonne_MGL
	 * @date 2017�?7�?11�? 下午7:10:04    
	 * @return String    @throws 
	 */
	public static String getNickName() {
		String chars = "abcdefghijklmnopqrstuvwxyz";
		String num = "1234567890";
		String nikename = "gack_";
		for (int i = 0; i < 2; i++) {
			nikename += chars.charAt((int) (Math.random() * 26));
		}
		for (int i = 0; i < 3; i++) {
			nikename += num.charAt((int) (Math.random() * 10));
		}
		return nikename;
	}

	/**
	 * 
	 * @Description: 判断当前时间是否在两个时间之间�?
	 * @param timeBegin起始时间
	 * @param timeEnd结束时间
	 * @param @return
	 * @author Yvonne_MGL
	 * @date 2017�?7�?12�? 下午2:45:53    
	 * @return boolean    @throws 
	 */
	public static boolean isBetween(Date timeBegin, Date timeEnd) {
		Date now = new Date();
		if (now.getTime() >= timeBegin.getTime() && now.getTime() <= timeEnd.getTime()) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @Description: remove掉session里以指定用户名开头的信息 
	 * @param username
	 *            用户�?
	 * @author Yvonne_MGL
	 * @date 2017�?7�?29�? 下午5:14:43    
	 * @return void    @throws 
	 */
	public static void removeSessionByUsername(HttpServletRequest request, String username) {
		Enumeration enumeration = request.getSession().getAttributeNames();
		while (enumeration.hasMoreElements()) {
			String AddFileName = enumeration.nextElement().toString();
			if (AddFileName.startsWith(username)) {
				request.getSession().removeAttribute(AddFileName);
			}
		}
	}

	/**
	 * 
	 * @Description: remove掉session里以指定sessionid结尾的信息�?
	 * @param sessionid
	 * @author Yvonne_MGL
	 * @date 2017�?7�?29�? 下午5:16:05    
	 * @return void    @throws 
	 */
	public static void removeSessionBySessionid(HttpServletRequest request, String sessionid) {
		Enumeration enumeration = request.getSession().getAttributeNames();
		while (enumeration.hasMoreElements()) {
			String AddFileName = enumeration.nextElement().toString();
			if (AddFileName.endsWith(sessionid)) {
				request.getSession().removeAttribute(AddFileName);
			}
		}
	}

	/**
	 * 
	 * @Description: 删除redis上username登陆下的sessionid为key的缓存信�?
	 * @param username
	 *            用户�?
	 * @author Yvonne_MGL
	 * @date 2017�?9�?14�? 下午3:42:30    
	 * @return void    @throws 
	 */
	public static void removeRedis(String username) {
		Jedis jedis = new Jedis("123.56.48.63", 6379);
		jedis.auth("218007");
		String value = jedis.get(username);
		if (value != null) {
			String keys[] = value.split(",");
			if (keys.length > 0) {
				for (int i = 0; i < keys.length; i++) {
					String key = keys[i];
					if (key != null && !key.equals("")) {
						jedis.del(key);
					}
				}
			}
		}
		jedis.del(username);
		jedis.disconnect();
	}

	/**
	 * 
	 * @Description: 调用财务支付平台
	 * @param url接口地址
	 * @param param参数拼接
	 * @author Yvonne_MGL
	 * @date 2017�?7�?30�? 下午5:16:05    
	 * @return void    @throws 
	 */
	public static String httpGetHelper(String url, String param) {
		String result = "";// 访问返回结果
		BufferedReader read = null;// 读取访问结果

		try {
			// 创建url
			URL realurl = new URL(url + "?" + param);
			// 打开连接
			URLConnection connection = realurl.openConnection();
			// 设置通用的请求属�?
			connection.setRequestProperty("accept", "*/*");
			connection.setRequestProperty("connection", "Keep-Alive");
			connection.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
			// 建立连接
			connection.connect();
			// 获取�?有响应头字段
			Map<String, List<String>> map = connection.getHeaderFields();
			// 遍历�?有的响应头字段，获取到cookies�?
			for (String key : map.keySet()) {
				System.out.println(key + "--->" + map.get(key));
			}
			// 定义 BufferedReader输入流来读取URL的响�?
			read = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
			String line;// 循环读取
			while ((line = read.readLine()) != null) {
				result += line;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (read != null) {// 关闭�?
				try {
					read.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}



	/**
	 * 
	 * @Description: 计算两个字符串时间之间的天数差�?
	 * @param smdate
	 *            起始时间
	 * @param bdate
	 *            结束时间
	 * @param @return
	 * @param @throws
	 *            ParseException
	 * @author Yvonne_MGL
	 * @date 2017�?8�?23�? 下午5:22:44    
	 * @return int    @throws 
	 */
	public static int daysBetween(String smdate, String bdate) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(sdf.parse(smdate));
		long time1 = cal.getTimeInMillis();
		cal.setTime(sdf.parse(bdate));
		long time2 = cal.getTimeInMillis();
		long between_days = (time2 - time1) / (1000 * 3600 * 24);

		return Integer.parseInt(String.valueOf(between_days)) + 1;
	}

	/**
	 * 
	 * @Description: 或许�?周前的时间�?
	 * @param @return
	 * @author Yvonne_MGL
	 * @date 2017�?8�?24�? 下午3:39:54    
	 * @return Date    @throws 
	 */
	public static Date getLess7Days() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) - 24 * 7);
		return calendar.getTime();
	}

	/**
	 * 
	 * @Description: 获取2小时前的时间 
	 * @param @return
	 * @author Yvonne_MGL
	 * @date 2017�?8�?25�? 下午2:36:50    
	 * @return Date    @throws 
	 */
	public static Date getLessTwoHourTime() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) - 120);
		Date date = c.getTime();
		return date;
	}

	/**
	 * 
	 * @Description: 获取days天以后的日期 
	 * @param @param
	 *            days
	 * @param @return
	 * @author Yvonne_MGL
	 * @date 2017�?9�?11�? 下午7:28:23    
	 * @return Date    @throws 
	 */
	public static Date getAfterTime(int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + days);
		return calendar.getTime();
	}

	/**
	 * 
	* @Description: 获取用户真实IP 
	* @param @param request
	* @param @return
	* @author Yvonne_MGL 
	* @date 2017�?10�?13�? 下午2:17:06     
	* @return String    
	* @throws 
	 */
	public static String getIp2(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)) {
			// 多次反向代理后会有多个ip值，第一个ip才是真实ip
			int index = ip.indexOf(",");
			if (index != -1) {
				return ip.substring(0, index);
			} else {
				return ip;
			}
		}
		ip = request.getHeader("X-Real-IP");
		if (StringUtils.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)) {
			return ip;
		}
		return request.getRemoteAddr();
	}
	
	/**
	 * 
	* @Description: 查询某个时间10分钟前的时间 
	* @param @return
	* @param @throws Exception
	* @author Yvonne_MGL 
	* @date 2017�?8�?8�? 下午6:39:54     
	* @return Date    
	* @throws 
	 */
	public static String getLess15Min(){
		Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.MINUTE, calendar.get(Calendar.MINUTE) - 15);
	    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    return sdf.format(calendar.getTime());
	}
}

package com.gack.helper.common.util;

/**
 * 进制转换
 * @author ws
 * 2018-6-12
 */
public class SysConvert {

	/**
	 * 传入十进制正整数,转换为36进制(0-z)的字符串
	 * @param num 
	 * @return
	 */
	public static String tenToThirtySix(int num){
		String[] member = new String[]{
			"0", "1", "2", "3", "4", "5",
			"6", "7", "8", "9", "a", "b",
			"c", "d", "e", "f", "g", "h",
			"i", "j", "k", "l", "m", "n",
			"o", "p", "q", "r", "s", "t",
			"u", "v", "w", "x", "y", "z"
		};
		StringBuilder sb = new StringBuilder();
		
		do{
			int mod = num % 36;
			sb.insert(0 ,member[mod]);
			num /= 36;
		}while(num > 0);
		
		return sb.toString();
	}

}

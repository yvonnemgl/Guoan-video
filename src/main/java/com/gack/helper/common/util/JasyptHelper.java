package com.gack.helper.common.util;
import java.util.Random;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
/**
 * Jasypt加密解密
 * @author yvonne
 *
 */
public class JasyptHelper {   
	
    /** 
     * 加密 
     * @param text 明文 
     * @param key 密钥
     * @return     密文 
     */  
    public static String encrypt(String text,String salt) {  
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();  
        encryptor.setPassword(salt);  
        return encryptor.encrypt(text);  
    }  
    
    /** 
     * 解密 
     * @param ciphertext 密文 
     * @param key 密钥
     * @return           明文 
     */  
    public static String decrypt(String ciphertext,String salt) {  
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();  
        encryptor.setPassword(salt);  
        return encryptor.decrypt(ciphertext);  
    }  
    
    public static void main(String[] args) {  
        String ciphertext1 = encrypt("123456","qwer,1234"); 
        System.out.println(ciphertext1);  
        
        
        String ciphertext2 = encrypt("123456","qwer,1234"); 
        System.out.println(ciphertext2);  
        
        String text1 = decrypt(ciphertext1,"qwer,1234");  
        System.out.println(text1);    
        
        String text2 = decrypt(ciphertext2,"qwer,1234");  
        System.out.println(text2);      
        
    } 
    
}

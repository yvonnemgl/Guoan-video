package com.gack.helper.common.util;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class SensitivewordEngine
{
    /**
     * 敏感词库
     */
    public static Map sensitiveWordMap = null;

    /**
     * 只过滤最小敏感词
     */
    public static int minMatchTYpe = 1;

    /**
     * 过滤�?有敏感词
     */
    public static int maxMatchType = 2;

    public static String specialChar="!@#$%^&*()_+=-~`,./<>?|{}[]，�?�？；�?�：“�?��?��?�）�?*&…�??%�?#@！~·";
    /**
     * 敏感词库敏感词数�?
     * 
     * @return
     */
    public static int getWordSize()
    {
        if (SensitivewordEngine.sensitiveWordMap == null)
        {
            return 0;
        }
        return SensitivewordEngine.sensitiveWordMap.size();
    }

    /**
     * 是否包含敏感�?
     * 
     * @param txt
     * @param matchType
     * @return
     */
    public static boolean isContaintSensitiveWord(String txt, int matchType)
    {
        boolean flag = false;
        for (int i = 0; i < txt.length(); i++)
        {
            int matchFlag = checkSensitiveWord(txt, i, matchType);
            if (matchFlag > 0)
            {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 获取敏感词内�?
     * 
     * @param txt
     * @param matchType
     * @return 敏感词内�?
     */
    public static Set<String> getSensitiveWord(String txt, int matchType)
    {
        Set<String> sensitiveWordList = new HashSet<String>();

        for (int i = 0; i < txt.length(); i++)
        {
        	char word = txt.charAt(i);
			if(specialChar.indexOf(word+"")>0){
				System.out.println("读输入的不是中文");
				continue;
			}
            int length = checkSensitiveWord(txt, i, matchType);
            if (length > 0)
            {
                // 将检测出的敏感词保存到集合中
                sensitiveWordList.add(txt.substring(i, i + length));
                i = i + length - 1;
            }
        }

        return sensitiveWordList;
    }

    /**
     * 替换敏感�?
     * 
     * @param txt
     * @param matchType
     * @param replaceChar
     * @return
     */
    public static String replaceSensitiveWord(String txt, int matchType, String replaceChar)
    {
        String resultTxt = txt;
        Set<String> set = getSensitiveWord(txt, matchType);
        Iterator<String> iterator = set.iterator();
        String word = null;
        String replaceString = null;
        while (iterator.hasNext())
        {
            word = iterator.next();
            replaceString = getReplaceChars(replaceChar, word.length());
            resultTxt = resultTxt.replaceAll(word, replaceString);
        }

        return resultTxt;
    }

    /**
     * 替换敏感词内�?
     * 
     * @param replaceChar
     * @param length
     * @return
     */
    private static String getReplaceChars(String replaceChar, int length)
    {
        String resultReplace = replaceChar;
        for (int i = 1; i < length; i++)
        {
            resultReplace += replaceChar;
        }

        return resultReplace;
    }

    /**
     * �?查敏感词数量
     * 
     * @param txt
     * @param beginIndex
     * @param matchType
     * @return
     */
    public static int checkSensitiveWord(String txt, int beginIndex, int matchType)
    {
        boolean flag = false;
        // 记录敏感词数�?
        int matchFlag = 0;
        char word = 0;
        Map nowMap = SensitivewordEngine.sensitiveWordMap;
        for (int i = beginIndex; i < txt.length(); i++)
        {
            word = txt.charAt(i);
			if(specialChar.indexOf(word+"")>0){
				System.out.println("输入的不是中�?");
				matchFlag++;
				continue;
			}
            // 判断该字是否存在于敏感词库中
            nowMap = (Map) nowMap.get(word);
            if (nowMap != null)
            {
                matchFlag++;
                // 判断是否是敏感词的结尾字，如果是结尾字则判断是否继续�?�?
                if ("1".equals(nowMap.get("isEnd")))
                {
                    flag = true;
                    // 判断过滤类型，如果是小过滤则跳出循环，否则继续循�?
                    if (SensitivewordEngine.minMatchTYpe == matchType)
                    {
                        break;
                    }
                }
            }
            else
            {
                break;
            }
        }
        if (!flag)
        {
            matchFlag = 0;
        }
        return matchFlag;
    }
    public Set<String> sensitiveWordFiltering(String text)
    {
        // 初始化敏感词库对�?
        /*SensitiveWordInit sensitiveWordInit = new SensitiveWordInit();
        // 从数据库中获取敏感词对象集合（调用的方法来自Dao层，此方法是service层的实现类）
        List<OperatorSensitiveWord> sensitiveWords = sensitiveWordDao.getSensitiveWordListAll();
        // 构建敏感词库
        Map sensitiveWordMap = sensitiveWordInit.initKeyWord(sensitiveWords);
        // 传入SensitivewordEngine类中的敏感词�?
        SensitivewordEngine.sensitiveWordMap = sensitiveWordMap;
        // 得到敏感词有哪些，传�?2表示获取�?有敏感词
        Set<String> set = SensitivewordEngine.getSensitiveWord(text, 2);*/
        return null;
    }
}
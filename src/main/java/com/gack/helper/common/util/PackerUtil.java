package com.gack.helper.common.util;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.dom4j.Element;



public class PackerUtil {
	
	 public static ConcurrentHashMap<String,String> errorMap = new ConcurrentHashMap<String,String>();
	  
	  static{
	
	  }
	  
	public XmlDocument xDoc;

	private String qryIDNo;
	private String brrwrCstTp;

	public PackerUtil() {
		this.xDoc = new XmlDocument();
		this.xDoc.createRoot();
	}
	
	public PackerUtil(String qryIDNo, String brrwrCstTp) {
		this.xDoc = new XmlDocument();
		this.xDoc.createRoot();
		this.qryIDNo = qryIDNo;
		this.brrwrCstTp = brrwrCstTp;
	}

	public String packSoapMsg(Map<String, Object> respMap) {
		
		packHeader();
		packBody(respMap);

		return xDoc.getDocument().asXML();
	}

	public void packHeader() {
	}

	public void packBody(Map<String, Object> respMap) {
		
	}
	
	//创建空间接口报文
   public void packBody(String parElement,Map<String, Object> respMap) {
   	Element el = this.xDoc.addSimpleElement(parElement);
   	el.addElement("v1:returnObjectInResponse").addText("");
   	Element  el2=el.addElement("v1:room");
   	el2.addElement("v1:roomID");
   	el2.addElement("v1:name").addText("会议"+respMap.get("i"));
   	el2.addElement("v1:RoomType").addText("Public");
   	el2.addElement("v1:ownerName").addText("admin");
   	Random rand = new Random();
	String s = rand.nextInt(1000000000)+"";
	respMap.put("extension", "479"+s);
   	el2.addElement("v1:extension").addText("479"+s);
   	el2.addElement("v1:groupName").addText("Default");
   	
   	Element  el3=el2.addElement("v1:RoomMode");
   	el3.addElement("v1:roomURL").addText("");
   	el3.addElement("v1:isLocked").addText("false");
   	el3.addElement("v1:hasPIN").addText("true");
   	el3.addElement("v1:roomPIN").addText("123456");
   	el3.addElement("v1:hasModeratorPIN").addText("false");
   	el3.addElement("v1:moderatorPIN").addText("");
   	el2.addElement("v1:description").addText("");
	}
   

   //删除空间接口报文
   public void packBody1(String parElement,Map<String, Object> respMap) {
   	Element el = this.xDoc.addSimpleElement(parElement);
   	el.addElement("v1:roomID").addText((String) respMap.get("roomID"));
	}
   
   //获取空间接口报文
   public void packBody2(String parElement,Map<String, Object> respMap) {
   	Element el = this.xDoc.addSimpleElement(parElement);
   	el.addElement("v1:roomID").addText("56809");
	}
   
   //获取空间列表接口报文
   public void packBody3(String parElement,Map<String, Object> respMap) {
   	Element el = this.xDoc.addSimpleElement(parElement);
   	Element  el2=el.addElement("v1:Filter");
   	el2.addElement("v1:start").addText("0");
   	el2.addElement("v1:limit").addText("1");
   	el2.addElement("v1:sortBy").addText("");
   	el2.addElement("v1:dir").addText("");
   	el2.addElement("v1:query").addText("会议"+respMap.get("i")+","+respMap.get("extension"));
	}
   //查询全部参会人接口报文
   public void packBody4(String parElement, Map<String, Object> resMap) {
	   String roomId = (String)resMap.get("roomId");
	   Element el = this.xDoc.addSimpleElement(parElement);
	   el.addElement("v1:conferenceID").addText(roomId);
	   Element el2 = el.addElement("v1:Filter");
		el2.addElement("v1:start").addText("");
	   	el2.addElement("v1:limit").addText("");
	   	el2.addElement("v1:sortBy").addText("");
	   	el2.addElement("v1:dir").addText("");
	   	el2.addElement("v1:query").addText("");
   }
   //移除参会人员接口报文
   public void packBody5(String parElement, Map<String, Object> resMap) {
	   String conferenceID = (String)resMap.get("conferenceID");
	   String participantID = (String)resMap.get("participantID");
	   Element el = this.xDoc.addSimpleElement(parElement);
	   el.addElement("v1:conferenceID").addText(conferenceID);
	   el.addElement("v1:participantID").addText(participantID);
   }
   //呼叫外部终端设备接口报文
   public void packBody6(String parElement, Map<String, Object> resMap) {
	   String conferenceID = (String)resMap.get("conferenceID");//会议室Roomid
//	   String entityID = (String)resMap.get("entityID"); //与下面的参数二选一 呼叫外部只用下面的一个
	   String invite = (String)resMap.get("invite"); //呼叫外部终端设备选用这个参数，sip呼叫方式<Vidyo extension> @ <gw IP address 192.168.1.110 or URL mydomain.com >
	   Element el = this.xDoc.addSimpleElement(parElement);
	   el.addElement("v1:conferenceID").addText(conferenceID);
//	   el.addElement("v1:entityID").addText(entityID);
	   el.addElement("v1:invite").addText(invite);
   }
   
	public static String getTrCodeReqMsg(String qryIDNo, String brrwrCstTp) {
		return null;
	}
	
}

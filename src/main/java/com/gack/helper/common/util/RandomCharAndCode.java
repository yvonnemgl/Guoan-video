package com.gack.helper.common.util;

import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gack.business.model.Conference;

public class RandomCharAndCode {
	/**
	 * 随机生成字母和数字混合
	 * @param length 长度
	 * @return
	 */
	public static String getCharAndNum(int length) {
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			// 输出字母还是数字
			String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
			// 字符串
			if ("char".equalsIgnoreCase(charOrNum)) {
				// 取得大写字母还是小写字母
				int choice = 65;
				sb.append((char) (choice + random.nextInt(26)));
			} else if ("num".equalsIgnoreCase(charOrNum)) { // 数字
				sb.append(String.valueOf(random.nextInt(10)));
			}
		}
		return sb.toString();
	}
	/**
	 * 随机生成6～8为的 数字字母混合验证码
	 * @return
	 */
	public synchronized static String createConferenceCode() {
		Random random = new Random();
		int r = random.nextInt(3);
		
		switch (r) {
		case 0:
			return getCharAndNum(6);
		case 1:
			return getCharAndNum(7);
		default :
			return getCharAndNum(8);

		}
		/*double ran = Math.random();
		if (ran <= 0.33) { // 6 位
			System.out.println(6);
			return getCharAndNum(6);
			
		} else if ((ran > 0.33) && (ran < 0.67)) { // 7 位
			System.out.println(7);
			return getCharAndNum(7);
		} else { // 8 位
			System.out.println(8);
			return getCharAndNum(8);
		}*/
	}
	public static String create() {
		String code = RandomCharAndCode.createConferenceCode();
		System.out.println(code);
		Pattern pattern1 = Pattern.compile("[0-9]*");
        Matcher isNum = pattern1.matcher(code);
        Pattern pattern2 = Pattern.compile("[a-zA-Z]*");
        Matcher isLetter = pattern2.matcher(code);
        
        if (isNum.matches()) {
        	System.out.println("isNum");
			return create();
		}else if (isLetter.matches()) {
			System.out.println("isLetter");
			return create();
		} else {
			return code;
		}
	}
	public static void main(String[] args) {
		System.out.println(create());
		
	}

}

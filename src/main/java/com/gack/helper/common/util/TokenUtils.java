package com.gack.helper.common.util;

import java.util.Date;
import java.util.UUID;

import org.springframework.stereotype.Component;

/**
 * 
* @ClassName: TokenUtils.java 
* @Description: 评价controller
* @author Cancerl
* @date 2018年5月10日
*  
 */
@Component
public class TokenUtils {

	public static String getUserLoginToken(String userid){
		String token = "";
		long now = (new Date()).getTime();
		String uuid = UUID.randomUUID().toString().replaceAll("-","");
		token = uuid + userid + now;
		token = MD5.md5(token);
		return token;
	}
	
	public static String getUUID(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
}

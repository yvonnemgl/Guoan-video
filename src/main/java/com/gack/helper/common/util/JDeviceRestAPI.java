package com.gack.helper.common.util;

import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.json.JSONObject;
import sun.misc.BASE64Encoder;

public class JDeviceRestAPI {
	private static final Logger log = LoggerFactory.getLogger(JPushRestAPI.class);
    private String masterSecret = "62237ea0baf9db54d1be0b99";
    private String appKey = "dde685abb543dcc27de8f744";
//    private String pushUrl = "https://api.jpush.cn/v3/push";
    private String deleteUrl = "https://device.jpush.cn/v3/aliases/%s";
    private boolean apns_production = true;    
    private int time_to_live = 86400;
    
    public void setApns_production(boolean apns_production) {
    	this.apns_production = apns_production;
    }
    public boolean getApns_production() {
    	return this.apns_production;
    }
	
	 /** 
　　　　* BASE64加密工具
　　　　*/
     @SuppressWarnings("restriction")
	public  String encryptBASE64(String str) {
         byte[] key = str.getBytes();
       BASE64Encoder base64Encoder = new BASE64Encoder();
       String base64_auth_string = base64Encoder.encodeBuffer(key);
         return base64_auth_string;
     }
     
     public  String sendDeleteRequest(String reqURL, String encodeCharset,String authorization){
    	 HttpDelete deletePost = new HttpDelete(reqURL);
         HttpClient client = new DefaultHttpClient();
         HttpResponse response = null;
         String result = "";
         try {
              deletePost.setHeader("Authorization",authorization.trim());
              response = client.execute(deletePost);
              result = EntityUtils.toString(response.getEntity(), encodeCharset);
         } catch (Exception e) {
             log.error("请求通信[" + reqURL + "]时偶遇异常,堆栈轨迹如下", e);  
         }finally{
             client.getConnectionManager().shutdown();
         }
         return result;
     }
     
     public String deletePush(String reqUrl, String appKey, String masterSecret){
    	 String base64_auth_string = encryptBASE64(appKey + ":" + masterSecret);
         String authorization = "Basic " + base64_auth_string;
         return sendDeleteRequest(reqUrl,"UTF-8",authorization);
     }
     
	 public void removeAlias(String alias) {
//	 	String resultData = deletePush(String.format(this.deleteUrl, alert), this.appKey, this.masterSecret);
//	 	JSONObject result = JSONObject.fromObject(resultData);
//	 	 if(result.containsKey("error")){
//	          log.info("针对别名为" + alert + "的信息推送失败！");
//	          JSONObject error = JSONObject.fromObject(result.get("error"));
//	          log.info("错误信息为：" + error.get("message").toString());
//	      }
		 System.out.println("移除别名地址：>>"+String.format(this.deleteUrl, alias));
			deletePush(String.format(this.deleteUrl, alias), this.appKey, this.masterSecret);
		 	System.out.println("------推送了一条移除别名推送>>alert为"+alias+"时间"+new Date());
	 	return ;
	 }
     
     public static void main(String[] args) {
    	 JDeviceRestAPI jDeviceRestAPI = new JDeviceRestAPI();
    	 jDeviceRestAPI.setApns_production(false);
    	 jDeviceRestAPI.removeAlias("15145984869");
	}

}

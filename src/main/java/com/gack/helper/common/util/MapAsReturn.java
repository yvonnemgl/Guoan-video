package com.gack.helper.common.util;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author ws
 * 2018-8-14
 */
public class MapAsReturn {

	public static Map<String, Object> setStatusError(String msg){
		Map<String, Object> map = new HashMap<>();
		map.put("status", "error");
		map.put("msg", msg);
		return map;
	}
	
	public static Map<String, Object> setStatusSuccess(String key, Object value){
		Map<String, Object> map = new HashMap<>();
		map.put("status", "success");
		map.put("msg", "操作成功");
		if(null != key){
			map.put(key, value);
		}
		return map;
	}
	
}

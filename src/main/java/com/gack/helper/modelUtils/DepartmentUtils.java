package com.gack.helper.modelUtils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gack.business.repository.DepartmentRepository;

/**
 * 关于部门实体的工具类
 * @author ws
 * 2018-8-23
 */
@Component
public class DepartmentUtils {

	@Autowired
	private DepartmentRepository departmentRepository;
	
	/**
	 * 查询某部门名称(一级部门名称_二级部门名称_三级部门名称)
	 * @param departmentId 部门id
	 * @param enterpriseId 公司id
	 * @return 一级部门名称_二级部门名称_三级部门名称
	 */
	public String getDepartmentName(String departmentId){
		if(StringUtils.isBlank(departmentId)){
			return "无";
		}
		
		List<String> allParentDepartmentIdList = findAllParentDepartmentId(departmentId);
		allParentDepartmentIdList.add(allParentDepartmentIdList.size(), departmentId);
		
		StringBuilder sb = new StringBuilder();
		for(String t : allParentDepartmentIdList){
			sb.append("_" + departmentRepository.findNameById(t));
		}
		return sb.substring("_".length()).toString();
	}
	
	/**
	 * 获取某部门的父部门,及其父父部门的id
	 * @param departmentId 部门id
	 * @return 父部门id集合
	 */
	public List<String> findAllParentDepartmentId(String departmentId){
		List<String> allParentDepartmentIdList = new ArrayList<>();
		
		if(StringUtils.isBlank(departmentId)){
			return allParentDepartmentIdList;
		}
		
		String parentId = departmentRepository.findParentDepartmentId(departmentId);
		while(!"0".equals(parentId)){
			allParentDepartmentIdList.add(0, parentId);
			parentId = departmentRepository.findParentDepartmentId(parentId);
		}
		
		return allParentDepartmentIdList;
	}
	
}

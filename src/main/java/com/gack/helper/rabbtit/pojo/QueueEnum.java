package com.gack.helper.rabbtit.pojo;


/**
 * * 消息队列枚举配置 * * @author：于起宇 <br/>
 * * =============================== * Created with IDEA. * Date：2018/3/3 *
 * Time：下午4:33 * 简书：http://www.jianshu.com/u/092df3f77bca *
 * ================================
 */

public enum QueueEnum {
    /** * 消息通知队列 */
    MESSAGE_QUEUE("message.center.direct", "message.center.create", "message.center.create"),
    /** * 消息通知死队列 */
    MESSAGE_TTL_QUEUE("dead.center.topic.ttl", "dead.center.create.ttl", "dead.center.create.ttl");
    /** * 交换名称 */
    private String exchange;
    /** * 队列名称 */
    private String name;
    /** * 路由键 */
    private String routeKey;

    QueueEnum(String exchange, String name, String routeKey) {
        this.exchange = exchange;
        this.name = name;
        this.routeKey = routeKey;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRouteKey() {
        return routeKey;
    }

    public void setRouteKey(String routeKey) {
        this.routeKey = routeKey;
    }
    
}

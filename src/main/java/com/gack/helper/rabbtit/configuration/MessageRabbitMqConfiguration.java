package com.gack.helper.rabbtit.configuration;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.gack.helper.rabbtit.pojo.QueueEnum;


@Configuration
public class MessageRabbitMqConfiguration {
    /** * 消息中心实际消费队列交换配置 * * @return */
    @Bean
    DirectExchange messageDirect() {
        return (DirectExchange) ExchangeBuilder.directExchange(QueueEnum.MESSAGE_QUEUE.getExchange()).durable().build();
    }

    /** * 消息中心延迟消费交换配置 * * @return */
    @Bean
    DirectExchange messageTtlDirect() {
        return (DirectExchange) ExchangeBuilder.directExchange(QueueEnum.MESSAGE_TTL_QUEUE.getExchange()).durable().build();
    }

    /** * 消息中心实际消费队列配置 * * @return */
    @Bean
    public Queue messageQueue() {
        return new Queue(QueueEnum.MESSAGE_QUEUE.getName());
    }

    /** * 消息中心TTL队列 * * @return */
    @Bean
    public  Queue messageTtlQueue() {
        return QueueBuilder.durable(QueueEnum.MESSAGE_TTL_QUEUE.getName())
                .withArgument("x-dead-letter-exchange", QueueEnum.MESSAGE_QUEUE.getExchange())
                .withArgument("x-dead-letter-routing-key", QueueEnum.MESSAGE_QUEUE.getRouteKey()).build();
    }

    /**
     * * 消息中心实际消息交换与队列绑定 
     * @param messageDirect 消息中心交换配置 
     *  @param messageQueue
     * 消息中心队列 * @return
     */
    @Bean
    public Binding messageBinding(DirectExchange messageDirect, Queue messageQueue) {
        return BindingBuilder.bind(messageQueue).to(messageDirect).with(QueueEnum.MESSAGE_QUEUE.getRouteKey());
    }

    /**
     * * 消息中心TTL绑定实际消息中心实际消费交换机 * * @param messageTtlQueue * @param messageTtlDirect
     * * @return
     */
    @Bean
    public Binding messageTtlBinding(Queue messageTtlQueue, DirectExchange messageTtlDirect) {
        return BindingBuilder.bind(messageTtlQueue).to(messageTtlDirect)
                .with(QueueEnum.MESSAGE_TTL_QUEUE.getRouteKey());
    }
}

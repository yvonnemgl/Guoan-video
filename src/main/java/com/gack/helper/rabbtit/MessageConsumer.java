package com.gack.helper.rabbtit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gack.business.service.OrderService;

@Component
// @RabbitListener(queues = "message.center.create")
public class MessageConsumer {
	@Autowired
	private OrderService orderService;

	/**
	 * 延时监听
	 * 
	 **/
	static Logger logger = LoggerFactory.getLogger(MessageConsumer.class);

	@RabbitHandler
	@RabbitListener(queues = "message.center.create")
	public void handler(String content) {
		logger.info("消费内容：{}", content);
		try {
			orderService.cancelReservation(content, "订单超时自动关闭","2");
		} catch (Exception e) {
			e.printStackTrace();  
		}
	}

	@RabbitHandler
	@RabbitListener(queues = "ibussiness_invoice_queue")
	public void process2(String value) {
		System.out.println("财务后台最低开票金额  : " + value);

	}

}

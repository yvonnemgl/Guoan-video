//package com.gack.helper.interceptor;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.stereotype.Component;
//import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
//
//import com.alibaba.fastjson.JSONObject;
//import com.aliyun.oss.common.utils.IOUtils;
//import com.gack.helper.common.config.FKBConfig;
//import com.gack.helper.common.util.MD5;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//@Component
//@PropertySource("classpath:personal.properties")  
//public class FKBAccessIntercepter extends HandlerInterceptorAdapter{
//
//	private final static Logger LOGGER = LoggerFactory.getLogger(FKBAccessIntercepter.class);
//	
//	@Override
//	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//
//		String body = IOUtils.readStreamAsString(IOUtils.newRepeatableInputStream(request.getInputStream()), "UTF-8");
//		JSONObject jsonObject = JSONObject.parseObject(body);
//		
//		String appid = jsonObject.getString("appid");
//		String time = jsonObject.getString("time");
//		String token = jsonObject.getString("token");//token = (appid+appkey+time组成字符串，再用MD5加密出32位长度的字符串，字符串为小写)
//		
//		if(appid == null || appid.trim().length() == 0 || time == null || time.trim().length() == 0 || token == null || token.trim().length() == 0){
//			LOGGER.error("费控宝token验证失败,验证参数为空。URL=" + request.getRequestURL() + "。appid=" + appid + "&time=" + time + "&token=" + token);
//			return false;
//		}
//
//		String appkey = FKBConfig.FKBAppKey.replace("-", "");
//		String myToken = MD5.md5(appid+appkey+time);
//		
//		if(token.equals(myToken)){
//			return true;
//		}
//		
//		LOGGER.error("费控宝token验证失败。URL=" + request.getRequestURL() + "。appid=" + appid + "&time=" + time + "&token=" + token + "&appkey=" + appkey + "&myToken=" + myToken);
//		return false;
//	}
//	
//}

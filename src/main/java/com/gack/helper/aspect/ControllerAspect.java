package com.gack.helper.aspect;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class ControllerAspect {

	@Pointcut("execution(public * com.gack.business.service.*.*(..))")
	public void log() {
	}

	@Pointcut("execution(public * com.gack.business.sendscheduled.*.*(..)) ")
	public void log_scheduled() {
	}

	@Pointcut("execution(public * com.gack.business.controller.*.*(..))")
	public void log_controller() {
	}


	@Around("log()")
	public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {



		Object result = null;
		try {
			result = proceedingJoinPoint.proceed();
		}catch (DuplicateSubmitException e) {
			log.error("-------------------------ERROR-------------------------");
			log.error("form repeat submission : " + e.toString());
			throw e;
		} catch (Exception e) {
			log.error("-------------------------ERROR-------------------------");
			ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
					.getRequestAttributes();
			HttpServletRequest request = attributes.getRequest();
			log.error("URL :" + request.getRequestURL().toString());
			log.error("HTTP_METHOD : " + request.getMethod());
			Enumeration<String> paraneters = request.getParameterNames();
			while(paraneters.hasMoreElements()){
				String name = paraneters.nextElement();
				log.error("{} = {}",name,request.getParameter(name));
			}
			log.error("ERROR_INFO : " + proceedingJoinPoint.getSignature().getDeclaringTypeName());
			log.error("EXCEPTION : ", e);
			throw e;
		}
		return result;
	}

	@Around("log_scheduled()")
	public void doAround_sc(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		try {
			proceedingJoinPoint.proceed();
		} catch (Exception e) {
			log.error("-------------------------ERROR-------------------------");
			log.error("ERROR_SCHEDULED : " + proceedingJoinPoint.getSignature().getDeclaringTypeName());
			log.error("EXCEPTION : ", e);
		}
	}

	@Before(value = "log_controller()")
	public void doBeforeController() {

		ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = attributes.getRequest();

		log.info("-------------------------INFO-------------------------");
		log.info("URL : " + request.getRequestURL().toString());
		log.info("HTTP_METHOD : " + request.getMethod());
		log.info("IP : " + request.getRemoteAddr());

		Enumeration<String> paraneters = request.getParameterNames();

		while(paraneters.hasMoreElements()){
			String name = paraneters.nextElement();
			log.info("{} = {}",name,request.getParameter(name));
		}
		log.info("-------------------------INFO-------------------------");
	}

}

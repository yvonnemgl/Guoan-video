package com.gack.helper.aspect;

/**
 * 
* @ClassName: DuplicateSubmitException 
* @Description: TODO(重复提交异常) 
* @author (ZhangXinYu)  
* @date 2018年6月25日 下午2:51:59 
* @version V1.0
 */
public class DuplicateSubmitException extends RuntimeException {
    public DuplicateSubmitException(String msg) {
        super(msg);
    }

    public DuplicateSubmitException(String msg, Throwable cause){
        super(msg,cause);
    }
}

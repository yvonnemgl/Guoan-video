package com.gack.netty.dispatcher;

import com.gack.netty.constant.ClientRequestMessageType;
import com.gack.netty.proto.MessageModule.Message;
import com.gack.netty.serverMessageService.basic.BusinessLogicExecutorBase;
import com.gack.netty.util.common.ServerMessageTypeToClassUtil;

import io.netty.channel.Channel;

/**
 * 
* @ClassName: ServerDispatcher 
* @Description: TODO(消息分发器) 
* @author (ZhangXinYu)  
* @date 2018年8月31日 下午5:14:22 
* @version V1.0
 */
public class ServerDispatcher {
	private static final int MAX_THREAD_NUM = 50;
//	private static ExecutorService executorService = Executors.newFixedThreadPool(MAX_THREAD_NUM);
	private static VeryTraceThreadPoolExecutor executorService = VeryTraceThreadPoolExecutor.newFixedThreadPool(MAX_THREAD_NUM);
	
	public static void submit(Channel channel, Object msg) throws InstantiationException, IllegalAccessException {
		Message message = (Message)msg;
		// get executorClass by msgType
		Class<?> executorClass = ServerMessageTypeToClassUtil.getMsgClassByType(message.getMsgType());
		if(executorClass == null){
			executorClass = ServerMessageTypeToClassUtil.getMsgClassByType(ClientRequestMessageType.REQUEST_NOT_FOUND);
		}
		BusinessLogicExecutorBase executor = (BusinessLogicExecutorBase)executorClass.newInstance();
		executor.setChannel(channel);
		executor.setMessage(message);
		executorService.submit(executor);
	}
}
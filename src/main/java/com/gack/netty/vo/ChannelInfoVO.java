package com.gack.netty.vo;

import java.util.Date;

import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;

/**
 * 
* @ClassName: ChannelInfoVO 
* @Description: TODO(管道对应信息实体) 
* @author (ZhangXinYu)  
* @date 2018年8月31日 上午11:26:34 
* @version V1.0
 */
@Getter
@Setter
public class ChannelInfoVO {
	private String assets;	// 别名
	private String address; // 地址
	private Channel channel; // 管道
	private Date createTime;	// 管道创建时间
	private String equipment;	//管道客户端设备号
	
	// 频繁接收消息的开始时间和结束时间  频繁接收消息的次数
	private boolean busy;	//接受消息是否繁忙
	private Date receiveMessageBeginTime;	// 接收消息开始时间
	private Date receiveMessageEndTime;		// 接收消息结束时间
	private int receiveMessageCount;		// 接收消息繁忙时接收消息次数
}

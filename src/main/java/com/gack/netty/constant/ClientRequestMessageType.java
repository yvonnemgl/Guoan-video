package com.gack.netty.constant;


/**
 * 
* @ClassName: MessageCode 
* @Description: TODO(保存自定义的客户端请求Message的消息类型的常量) 
* @author (ZhangXinYu)  
* @date 2018年8月30日 下午8:52:39 
* @version V1.0
 */
public class ClientRequestMessageType {
	public static final int CLIENT_HEARTBEAT_REQUEST = 2;	// 客户端心跳请求
	public static final int CLIENT_BIND_ASSETS_REQUEST = 3;	// 客户端绑定别名请求
	public static final int REQUEST_NOT_FOUND = 404;	//未找到请求消息类型对应的处理服务类
}

package com.gack.netty.constant;

/**
 * 
* @ClassName: MessageState 
* @Description: TODO(保存自定义的Message消息状态) 
* @author (ZhangXinYu)  
* @date 2018年8月30日 下午8:55:25 
* @version V1.0
 */
public class MessageState {
	public static final int NULL = 0;	// 请求状态默认值  请求不存在成功失败状态值  状态码为0  当Message类型为响应信息时  采用以下响应状态
	public static final int SUCCESS = 200; // 响应成功
}

package com.gack.netty.constant;

/**
 * 
* @ClassName: MessageRequestCode 
* @Description: TODO(保存自定义的代表服务器请求Message的消息类型的常量) 
* @author (ZhangXinYu)  
* @date 2018年8月30日 下午9:05:27 
* @version V1.0
 */
public class ServerRequestMessageType {
	public static final int SERVER_PUSH_MESSAGE = 1;	// 服务器推送信息
	public static final int REQUEST_NOT_FOUND = 404;	// 客户端请求消息类型未找到对应业务处理类
	public static final int HEARTBEAT_TIME_OUT_CODE = 408;	// 客户端心跳超时提醒（处理>>提醒客户端心跳超时 断开客户端连接）
}

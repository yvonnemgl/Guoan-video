package com.gack.netty.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 
* @ClassName: NettyIpBlacklistDao 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年9月3日 上午9:13:18 
* @version V1.0
 */
@Repository
public class NettyIpBlacklistDao {
	@Autowired
	private EntityManager entityManager;
	
	/**
	 * 获取所有禁用ip
	 */
	@SuppressWarnings("unchecked")
	public Set<String> allDisableIp(){
		Set<String> ipSet = new HashSet<String>();
		String jpql = "select ip from netty_ip_blacklist";
		Query query = entityManager.createNativeQuery(jpql);
		List<String> ipList = query.getResultList();
		if(ipList != null){
			ipSet.addAll(ipList);
		}
		return ipSet;
	}
}

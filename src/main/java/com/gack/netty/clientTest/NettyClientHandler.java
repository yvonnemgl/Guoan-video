package com.gack.netty.clientTest;

import com.gack.netty.constant.ClientRequestMessageType;
import com.gack.netty.proto.MessageModule.Message;
import com.gack.netty.util.protoBuildUtil.MessageBuildUtil;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * 
* @ClassName: NettyClientHandler 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年8月30日 下午4:28:56 
* @version V1.0
 */
public class NettyClientHandler extends ChannelInboundHandlerAdapter{

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		Message message = MessageBuildUtil.buildMessage(ClientRequestMessageType.CLIENT_HEARTBEAT_REQUEST, "看来是没救了,又让我给连上了","testEquipment");
		ctx.channel().writeAndFlush(message);
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		Message message = (Message)msg;
		System.out.println("接收到服务端发送信息"
				+ "{msgType:"+message.getMsgType()+", content:"+message.getContent()+", timestamp:"+message.getTimestamp()+"}");
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
		ctx.close();
	}
	
}

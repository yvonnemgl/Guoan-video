package com.gack.netty.clientTest;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * 
* @ClassName: ReConnnectionListener 
* @Description: TODO(重连handler  当连接断开时进行重连) 
* @author (ZhangXinYu)  
* @date 2018年8月31日 上午10:26:46 
* @version V1.0
 */
public class ReConnnectionListener extends ChannelInboundHandlerAdapter{
	private NettyClient client;
	public ReConnnectionListener(NettyClient client) {
		this.client = client;
	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
//		final EventLoop eventLoop = ctx.channel().eventLoop();  
//	     eventLoop.schedule(new Runnable() {  
//	       @Override 
//	       public void run() {  
//	         try {
//				client.run();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//	       }  
//	    }, 1L, TimeUnit.SECONDS);
//		super.channelInactive(ctx);
		client.run();
	}
	
	
}

package com.gack.netty.clientTest;

import com.gack.netty.encoderAndDecoder.decoder.MyDecoder;
import com.gack.netty.encoderAndDecoder.encoder.MyEncoder;

import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * 
* @ClassName: NettyClientInitializer 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年8月30日 下午4:34:20 
* @version V1.0
 */
@Sharable
public class NettyClientInitializer extends ChannelInitializer<SocketChannel>{
	
	private NettyClient client;
	
	public NettyClientInitializer(NettyClient client) {
		this.client = client;
	}
	
	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();
//		pipeline.addLast(new ProtobufVarint32FrameDecoder());
//		pipeline.addLast(new ProtobufDecoder(MessageModule.Message.getDefaultInstance()));
//		pipeline.addLast(new ProtobufVarint32LengthFieldPrepender());
//		pipeline.addLast(new ProtobufEncoder());
		pipeline.addLast(new MyDecoder());
		pipeline.addLast(new MyEncoder());
		pipeline.addLast(new IdleStateHandler(0, 14, 0));
		pipeline.addLast(new NettyClientHeartbeatHandler());
		pipeline.addLast(new NettyClientHandler());
		pipeline.addLast(new ReConnnectionListener(client));
	}

}

package com.gack.netty.Repostory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.gack.netty.po.NettyIpBlacklist;

/**
 * 
* @ClassName: NettyIpBlacklistRepository 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年9月3日 上午10:58:26 
* @version V1.0
 */
public interface NettyIpBlacklistRepository extends JpaRepository<NettyIpBlacklist, String>,JpaSpecificationExecutor<NettyIpBlacklist>{

}

package com.gack.netty.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
* @ClassName: RequestMessageAnnotation 
* @Description: TODO(修饰消息类和业务执行类) 
* @author (ZhangXinYu)  
* @date 2018年8月31日 下午4:10:06 
* @version V1.0
 */
@Target(ElementType.TYPE)  
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestMessageAnnotation {
	int msgType();
}

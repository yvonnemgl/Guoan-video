package com.gack.netty.messageModle;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** 
* @ClassName: testModle 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年9月2日 下午11:22:14 
* @version V1.0 
*/
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class testModle {
	private String id;
	private int age;
	private List<String> info;
	private Date date;
}
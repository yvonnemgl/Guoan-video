package com.gack.netty.po;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.gack.helper.common.abstractobj.IdEntity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
* @ClassName: NettyIpBlacklist 
* @Description: TODO(netty连接 ip黑名单) 
* @author (ZhangXinYu)  
* @date 2018年9月3日 上午9:08:28 
* @version V1.0
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "netty_ip_blacklist")
public class NettyIpBlacklist extends IdEntity{
	private String ip;	// 禁止连接ip
	@Temporal(TemporalType.TIMESTAMP)
	private Date create_time;	//添加时间
}

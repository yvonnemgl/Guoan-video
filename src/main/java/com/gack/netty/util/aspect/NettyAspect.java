package com.gack.netty.util.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.gack.netty.util.common.ExceptionUtil;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class NettyAspect {
		
	@Pointcut("execution(public * com.gack.netty.serverMessageService.*.*(..))")
	public void netty_server_service() {
	}
	
	@Pointcut("execution(public * com.gack.netty.dispatcher.*.*(..))")
	public void netty_server_dispatcher() {
	}

	@Around("netty_server_service(), netty_server_dispatcher()")
	public void doAround_service(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		try {
			proceedingJoinPoint.proceed();
		} catch (Exception e) {
			log.error("-------------------------NETTY ERROR-------------------------");
			log.error("ERROR_SERVICE : " + proceedingJoinPoint.getSignature().getDeclaringTypeName());
			log.error("ERROR_INFO : " + e.toString());
			log.error("ERROR_STACKTRACE >> ");
			String[] stackTraceArray = ExceptionUtil.getThrowableStrRep(e);
    		for(String stackTrace : stackTraceArray){
    			log.error(stackTrace);
    		}
		}
	}
}

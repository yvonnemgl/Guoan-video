package com.gack.netty.util.protoConvertUtil;

import com.gack.netty.proto.MessageModule.Message;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * 
* @ClassName: MessageModuleConvertUtil 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年8月30日 下午12:05:23 
* @version V1.0
 */
public class MessageModuleConvertUtil {
	
	/**
	 * 对Message象转为字节数组
	 */
//	public static byte[] messageToBytes(Message message){
//		byte[] byteArray = null;
//		byteArray = message.toByteArray();
//		return byteArray;
//	}
	
	/**
	 * 根据参数build出Message对象并转换为字节数组
	 */
//	public static byte[] messageToBytes(String id,String content){
//		byte[] byteArray = null;
//		Message.Builder builder = Message.newBuilder();
//		builder.setId(id);
//		builder.setContent(content);
//		Message message = builder.build();
//		byteArray = message.toByteArray();
//		return byteArray;
//	}
	
	/**
	 * 将字节数组转换为Message对象
	 */
	public static Message bytesToMessage(byte[] byteArray) throws InvalidProtocolBufferException{
		Message message = Message.parseFrom(byteArray);
		return message;
	}
}

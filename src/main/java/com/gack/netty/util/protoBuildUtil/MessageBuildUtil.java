package com.gack.netty.util.protoBuildUtil;

import com.gack.netty.proto.MessageModule.Message;
import com.gack.netty.proto.MessageModule.Message.Builder;

/**
 * 
* @ClassName: MessageBuildUtil 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年8月30日 下午4:13:36 
* @version V1.0
 */
public class MessageBuildUtil {

	/**
	 * 构架传递消息message
	 */
	public static Message buildMessage(int msgType, String content){
		Builder builder = Message.newBuilder();
		Message message = builder.setMsgType(msgType).setContent(content).setTimestamp(System.currentTimeMillis()).setEquipment("").build();
		return message;
	}
	
	public static Message buildMessage(int msgType, String content, String equipment){
		Builder builder = Message.newBuilder();
		Message message = builder.setMsgType(msgType).setContent(content).setTimestamp(System.currentTimeMillis()).setEquipment(equipment).build();
		return message;
	}
	
	public static Message buildMessage(int msgType, String content, long timestamp){
		Builder builder = Message.newBuilder();
		Message message = builder.setMsgType(msgType).setContent(content).setTimestamp(timestamp).setEquipment("").build();
		return message;
	}
}

package com.gack.netty.util.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
* @ClassName: IpUtil 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年9月3日 上午9:34:48 
* @version V1.0
 */
public class IpUtil {

	public static String getIp(String address){
		String resultIp = null;
		String regEx="((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)"; 
		Pattern p = Pattern.compile(regEx); 
		Matcher m = p.matcher(address); 
		
		// get the first ip
		while (m.find()) {
			resultIp = m.group();
			break;
		}
		return resultIp;
	}
}

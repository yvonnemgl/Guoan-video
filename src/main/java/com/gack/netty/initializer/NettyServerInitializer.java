package com.gack.netty.initializer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gack.netty.encoderAndDecoder.decoder.MyDecoder;
import com.gack.netty.encoderAndDecoder.encoder.MyEncoder;
import com.gack.netty.handler.HeartbeatHandler;
import com.gack.netty.handler.NettyServerHandler;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

/**
 * 
* @ClassName: NettyServerInitializer 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年8月30日 下午12:47:44 
* @version V1.0
 */
@Component
public class NettyServerInitializer extends ChannelInitializer<SocketChannel>{

	@Autowired
	private NettyServerHandler nettyServerHandler;
	@Autowired
	private HeartbeatHandler heartbeatHandler;
	
	@Value("${netty.idle.reader_time_out}")
	private int READER_TIME_OUT;
	@Value("${netty.idle.writer_time_out}")
	private int WRITE_TIME_OUT;
	@Value("${netty.idle.all_time_out}")
	private int ALL_TIME_OUT;
	
	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();
//		pipeline.addLast("frameDecoder", new ProtobufVarint32FrameDecoder());
//		pipeline.addLast("protobufDecoder", new ProtobufDecoder(MessageModule.Message.getDefaultInstance()));
//		pipeline.addLast("protobufPrepender", new ProtobufVarint32LengthFieldPrepender());
//		pipeline.addLast("protobufEncoder", new ProtobufEncoder());
		pipeline.addLast(new MyDecoder());
		pipeline.addLast(new MyEncoder());
		// set idle state time out
		pipeline.addLast("idleStateHandler", new IdleStateHandler(READER_TIME_OUT,WRITE_TIME_OUT,ALL_TIME_OUT));
		pipeline.addLast("hearbearHandler", heartbeatHandler);
		pipeline.addLast("nettyServerHandler", nettyServerHandler);
	}
	
}

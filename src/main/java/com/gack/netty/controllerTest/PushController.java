package com.gack.netty.controllerTest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gack.helper.common.AjaxJson;
import com.gack.helper.common.abstractobj.ApiController;
import com.gack.netty.constant.ConstantChannels;
import com.gack.netty.constant.ServerRequestMessageType;
import com.gack.netty.proto.MessageModule.Message;
import com.gack.netty.util.protoBuildUtil.MessageBuildUtil;

import io.netty.channel.group.ChannelGroup;
import io.swagger.annotations.ApiOperation;

/**
 * 
* @ClassName: PushController 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年8月30日 下午4:47:43 
* @version V1.0
 */
@RestController
@RequestMapping(ApiController.PUSH_PATH)
public class PushController {

	@ApiOperation(value="全体推送",notes="全体推送")
	@PostMapping("pushAll")
	public AjaxJson pushAll(String content){
		AjaxJson ajaxJson = new AjaxJson();
		try{
			ChannelGroup channelGroup = ConstantChannels.buildEmptyChannelGroup();
			channelGroup.addAll(ConstantChannels.allChannels());
			Message message = MessageBuildUtil.buildMessage(ServerRequestMessageType.SERVER_PUSH_MESSAGE, content);
			if(channelGroup.size() == 0){
				return ajaxJson.setStatus(200).setSuccess(true).setMsg("没有channel客户端连接,推送失败");
			}
			channelGroup.writeAndFlush(message);
			ajaxJson.setStatus(200).setSuccess(true).setData("推送成功");
		}catch (Exception e) {
			e.printStackTrace();
			ajaxJson.setStatus(500).setSuccess(false).setData("推送失败");
		}
		return ajaxJson;
	}
}

package com.gack.netty.serverMessageService.basic;

import com.gack.netty.proto.MessageModule.Message;

import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;

/**
 * 
* @ClassName: BusinessLogicExecutorBase 
* @Description: TODO(所有业务类的基类   业务类继承并实现run方法) 
* @author (ZhangXinYu)  
* @date 2018年8月31日 下午5:24:08 
* @version V1.0
 */
@Getter
@Setter
public class BusinessLogicExecutorBase implements Runnable {

	private Channel channel;
	private Message message;

	@Override
	public void run() {
		// TODO Auto-generated method stub
	}
}

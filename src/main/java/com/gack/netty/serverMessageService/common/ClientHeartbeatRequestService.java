package com.gack.netty.serverMessageService.common;

import com.gack.netty.annotation.RequestMessageAnnotation;
import com.gack.netty.constant.ClientRequestMessageType;
import com.gack.netty.serverMessageService.basic.BusinessLogicExecutorBase;

/**
 * 
* @ClassName: TestRequestService 
* @Description: TODO(这里用一句话描述这个类的作用) 
* @author (ZhangXinYu)  
* @date 2018年8月31日 下午5:37:32 
* @version V1.0
 */
@RequestMessageAnnotation(msgType = ClientRequestMessageType.CLIENT_HEARTBEAT_REQUEST)
public class ClientHeartbeatRequestService extends BusinessLogicExecutorBase {

	@Override
	public void run(){
//		System.out.println("[测试消息分发功能] 打印来自客户端的请求数据>>" + this.getMessage().getContent());
		System.out.println("接收到客户端 ["+this.getChannel().remoteAddress().toString()+"] 的心跳请求");
	}
}

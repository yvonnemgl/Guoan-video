package com.gack.netty.serverMessageService.common;

import com.gack.netty.annotation.RequestMessageAnnotation;
import com.gack.netty.constant.ClientRequestMessageType;
import com.gack.netty.constant.ServerRequestMessageType;
import com.gack.netty.serverMessageService.basic.BusinessLogicExecutorBase;
import com.gack.netty.util.protoBuildUtil.MessageBuildUtil;

/** 
* @ClassName: RequestNotFoundService 
* @Description: TODO(请求服务未找到对应业务处理类) 
* @author (ZhangXinYu)  
* @date 2018年8月31日 下午8:05:09 
* @version V1.0 
*/
@RequestMessageAnnotation(msgType = ClientRequestMessageType.REQUEST_NOT_FOUND)
public class RequestNotFoundService extends BusinessLogicExecutorBase{

	@Override
	public void run() {
		System.out.println("客户端["+this.getChannel().remoteAddress().toString()+"] 请求业务未找到");
		this.getChannel().writeAndFlush(MessageBuildUtil.buildMessage(ServerRequestMessageType.REQUEST_NOT_FOUND,"未找到该消息类型["+this.getMessage().getMsgType()+"]对应的业务"));
	}
	
}

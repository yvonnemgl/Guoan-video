package com.gack.netty.serverMessageService.common;

import com.gack.netty.annotation.RequestMessageAnnotation;
import com.gack.netty.constant.ClientRequestMessageType;
import com.gack.netty.constant.ConstantChannels;
import com.gack.netty.serverMessageService.basic.BusinessLogicExecutorBase;
import com.gack.netty.vo.ChannelInfoVO;

/**
 * 
* @ClassName: ClientBindAssetsService 
* @Description: TODO(客户端绑定别名业务) 
* @author (ZhangXinYu)  
* @date 2018年8月31日 下午9:37:58 
* @version V1.0
 */
@RequestMessageAnnotation(msgType = ClientRequestMessageType.CLIENT_BIND_ASSETS_REQUEST)
public class ClientBindAssetsService extends BusinessLogicExecutorBase {

	@Override
	public void run() {
		ChannelInfoVO channelInfo = ConstantChannels.get(this.getChannel());
		if(channelInfo == null){
			return;
		}
		channelInfo.setAssets(this.getMessage().getContent());
		channelInfo.setEquipment(this.getMessage().getEquipment());
		ConstantChannels.push(this.getChannel(), channelInfo);
	}

}

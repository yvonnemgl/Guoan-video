package guoanmaker.video;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.gack.App;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
public class AppTest {
    @Autowired
    private TestRestTemplate testRestTemplate;
    /**
     * 
     * @Title: get
     * @Description: http get 请求模拟 请注释https配置
     * @throws Exception void 
     * @throws
     */
    //@Test
    public void get() throws Exception {
        Map<String,String> multiValueMap = new HashMap<>();
        multiValueMap.put("userId","xiaoqiang");//传值，但要在url上配置相应的参数
        String result = testRestTemplate.getForObject("/test/test?userId={userId}",String.class,multiValueMap);
        System.out.println(result);
    }
    /**
     * 
     * @Title: post
     * @Description: http post 请求模拟 请注释https配置
     * @throws Exception void 
     * @throws
     */
    //@Test
    public void post() throws Exception {
        MultiValueMap<String,String> multiValueMap = new LinkedMultiValueMap<>();
        multiValueMap.add("userId","laowang");
        String result = testRestTemplate.postForObject("/test/test",multiValueMap,String.class);
        System.out.println(result);
    }
    
    
    
    
}